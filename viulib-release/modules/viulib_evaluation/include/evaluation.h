/**
 * \defgroup viulib_evaluation viulib_evaluation
 * viulib_evaluation (Evaluation) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_evaluation is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_evaluation depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_evaluation.
 *
 */

#ifndef _EVALUATION_H_
#define _EVALUATION_H_

#include "core.h"
#include "videoContentDescription.h"

#include "evaluation_exp.h"

#ifdef __APPLE__
#   define max(a,b)({ __typeof__ (a) _a = (a);__typeof__ (b) _b = (b); _a > _b ? _a : _b; })
#   define min(a,b)({ __typeof__ (a) _a = (a);__typeof__ (b) _b = (b); _a < _b ? _a : _b; })
#endif

namespace viulib
{
/** \brief This namespace corresponds to the viulib_evaluation module.
\ingroup viulib_evaluation
*/
namespace evaluation
{
	/** \brief This struct is used to register the factory	
	* \ingroup viulib_evaluation
	*/
	struct EVALUATION_LIB factory_registration
	{
		factory_registration();
	};
	static factory_registration evaluation_module_init;	

	/** \brief This function compares two VideoContentDescription objects to verify they can be compared
	* \ingroup viulib_dtc
    */
	bool EVALUATION_LIB checkTypes( const VideoContentDescription& _vc1, const VideoContentDescription& _vc2 );
	
	/** \brief This function define Metrics for evaluation
	* \ingroup viulib_dtc
    */
	class EVALUATION_LIB Metrics
	{
	public:
		Metrics():m_TP(0), m_FP(0), m_FN(0), m_recall(0.0), m_precision(0.0), m_fmeasure(0.0), m_name(""),m_moda(0) {}
		void print() const
		{		
			if( !m_name.empty() )
				std::cout << "Metrics: " << m_name << std::endl;			
			std::cout << "Num. items: " << m_TP + m_FN << std::endl;
			std::cout.precision(3);			
			std::cout << std::fixed << "(R, P, F, M)= " << "(" << m_recall << ", " << m_precision << ", " << m_fmeasure << ", " << m_moda << ") TP: " << m_TP << " FP: " << m_FP << " FN: " << m_FN <<"\n";						
		}		
		void saveAsTxt(const std::string & filename) const
		{
			std::ofstream descriptor;

      descriptor.open(filename.c_str(),std::ios::out | std::ios::app);
			assert(descriptor.is_open());
			descriptor <<"- - - - - - - - - - - - - - - - - - -\n";
			descriptor << "Metrics: " << m_name<<"\n";
			descriptor << "Num. items: " << m_TP + m_FN <<" \n";		
			descriptor << std::fixed << "(R, P, F, M)= " << "(" << m_recall << ", " << m_precision << ", " << m_fmeasure << ", " << m_moda << ") TP: " << m_TP << " FP: " << m_FP << " FN: " << m_FN << "\n";						
			descriptor << "-------------------------------------\n";
			descriptor.close();
		}
		void computeStats( int _tp, int _fn, int _fp )
		{
			m_TP = _tp; m_FN = _fn; m_FP = _fp;

			if( m_TP > 0 )
			{
				m_recall = static_cast<double>(m_TP) / static_cast<double>(m_TP + m_FN); 
				m_precision = static_cast<double>(m_TP) / static_cast<double>(m_TP + m_FP); 
				m_fmeasure = 2*(m_precision*m_recall)/(m_precision + m_recall);				
				m_moda = 1.0 - (static_cast<double>(m_FN)+static_cast<double>(m_FP))/(static_cast<double>(m_TP)+static_cast<double>(m_FN));
			}
		}	
		void setName( std::string _name ) { m_name = _name; }
		double getValue( std::string name ) const;

	private:
		// Name or string
		std::string m_name;

		// Qualitative analysis
		int m_TP;		/// True Positives
		int m_FP;		/// False Positives
		int m_FN;		/// False Negatives

		double m_recall;
		double m_precision;
		double m_fmeasure;
		double m_moda;
	};

	/** \brief Container of time interval error information
	* \ingroup viulib_dtc
    */
	struct ErrorTimeInterval 
	{
		double diff_start;
		double diff_end;
		double diff_length;	
		double diff_mean;

		double overlap; // intersection/union
		double overlap_ab; // intersection/area_a
		double overlap_ba; // intersection/area_b	

		double intersection;		

		ErrorTimeInterval():diff_start(0.0),diff_end(0.0),diff_length(0.0), diff_mean(0.0),overlap(0.0), overlap_ab(0.0), overlap_ba(0.0), intersection(0.0) {}

		void print() const
		{
			std::cout << "Diff. start = " << diff_start << "\n"
					  << "Diff. end = " << diff_end << "\n"
					  << "Diff. length = " << diff_length << "\n"
					  << "Diff. mean = " << diff_mean << "\n"					  
					  << "Overlap = " << overlap << "\n"
					  << "Overlap_ab = " << overlap_ab << "\n"
					  << "Overlap_ba = " << overlap_ba << "\n"					  
					  << "Intersection = " << intersection << std::endl;					  
		}
		void saveAsTxt(const std::string & filename) const
		{
			std::ofstream f;
			f.open(filename.c_str(),std::ios::out | std::ios::app);

			assert(f.is_open());
			f << "Diff. start = " << diff_start << "\n"
					  << "Diff. end = " << diff_end << "\n"
					  << "Diff. length = " << diff_length << "\n"
					  << "Diff. mean = " << diff_mean << "\n"					  
					  << "Overlap = " << overlap << "\n"
					  << "Overlap_ab = " << overlap_ab << "\n"
					  << "Overlap_ba = " << overlap_ba << "\n"					  
					  << "Intersection = " << intersection << "\n";	
			f.close();
		}

	};	

	/** \brief Container of error at object data level
	* \ingroup viulib_dtc
    */
	struct ErrorObjectData
	{
		double mean_accuracy;			// when there is temporal coincidence
		double availability;		// percentage of time with coincidence (it is the RECALL from Metrics)
		Metrics frameLevelMetrics;	// Recall, precision, etc. at frame level

		ErrorObjectData():mean_accuracy(0.0),availability(0.0),frameLevelMetrics() {}

		void print() const
		{
			std::cout << "Mean. acc = " << mean_accuracy << "\n"
					  << "Availability = " << availability << std::endl;					  
			frameLevelMetrics.print();
		}
	};


	


	/** \brief This class is the contained for the evaluation exercise
	* \ingroup viulib_dtc
    */
	class EVALUATION_LIB Evaluation
	{		
	public:
		class DetectedObjectsErrorInfo
		{
		public:			
			/*void add( const ErrorTimeInterval& _eti, const ErrorObjectData& _eod ) 
			{ 
				errorTimeIntervalBestVector.push_back( _eti );
				avgETI = ErrorTimeInterval();
				for( size_t i=0; i<errorTimeIntervalBestVector.size(); ++i )
				{
					avgETI.diff_start += errorTimeIntervalBestVector[i].diff_start;
					avgETI.diff_end += errorTimeIntervalBestVector[i].diff_end;
					avgETI.overlap += errorTimeIntervalBestVector[i].overlap;
					avgETI.diff_length += errorTimeIntervalBestVector[i].diff_length;				
				}
				avgETI.diff_start /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_end /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.overlap /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_length /= static_cast<double>( errorTimeIntervalBestVector.size() );

				errorObjectDataBestVector.push_back( _eod );
				avgEOD = ErrorObjectData();
				int tp = 0, fn = 0, fp = 0;
				for( size_t i=0; i<errorObjectDataBestVector.size(); ++i )
				{
					avgEOD.mean_accuracy += errorObjectDataBestVector[i].mean_accuracy;
					avgEOD.availability += errorObjectDataBestVector[i].availability;
					tp += static_cast<int>( errorObjectDataBestVector[i].frameLevelMetrics.getValue("TP") );
					fn += static_cast<int>( errorObjectDataBestVector[i].frameLevelMetrics.getValue("FN") );
					fp += static_cast<int>( errorObjectDataBestVector[i].frameLevelMetrics.getValue("FP") );
				}			
				avgEOD.mean_accuracy /= static_cast<double>( errorObjectDataBestVector.size() );
				avgEOD.availability /= static_cast<double>( errorObjectDataBestVector.size() );				
				avgEOD.frameLevelMetrics.computeStats( tp, fn, fp );
			}*/
			void add( const ErrorTimeInterval& _eti ) 
			{ 
				errorTimeIntervalBestVector.push_back( _eti );
				avgETI = ErrorTimeInterval();
				for( size_t i=0; i<errorTimeIntervalBestVector.size(); ++i )
				{
					avgETI.diff_start += errorTimeIntervalBestVector[i].diff_start;
					avgETI.diff_end += errorTimeIntervalBestVector[i].diff_end;					
					avgETI.diff_length += errorTimeIntervalBestVector[i].diff_length;				
					avgETI.diff_mean += errorTimeIntervalBestVector[i].diff_mean;
					avgETI.overlap += errorTimeIntervalBestVector[i].overlap;
					avgETI.overlap_ab += errorTimeIntervalBestVector[i].overlap_ab;
					avgETI.overlap_ba += errorTimeIntervalBestVector[i].overlap_ba;
					avgETI.intersection += errorTimeIntervalBestVector[i].intersection;					
				}
				avgETI.diff_start /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_end /= static_cast<double>( errorTimeIntervalBestVector.size() );				
				avgETI.diff_length /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_mean /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.overlap /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.overlap_ab /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.overlap_ba /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.intersection /= static_cast<double>( errorTimeIntervalBestVector.size() );				
			}

			// Vectors containing information for each object in videocontent
			std::vector<ErrorTimeInterval> errorTimeIntervalBestVector;
			//std::vector<ErrorObjectData> errorObjectDataBestVector;
						
			ErrorTimeInterval avgETI;			
			//ErrorObjectData avgEOD;
		};
		class DetectedEventsErrorInfo
		{
		public:
			void add( const ErrorTimeInterval& _eti ) 
			{ 
				errorTimeIntervalBestVector.push_back( _eti );
				avgETI = ErrorTimeInterval();
				for( size_t i=0; i<errorTimeIntervalBestVector.size(); ++i )
				{
					avgETI.diff_start += errorTimeIntervalBestVector[i].diff_start;
					avgETI.diff_end += errorTimeIntervalBestVector[i].diff_end;
					avgETI.overlap += errorTimeIntervalBestVector[i].overlap;
					avgETI.diff_length += errorTimeIntervalBestVector[i].diff_length;				
				}
				avgETI.diff_start /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_end /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.overlap /= static_cast<double>( errorTimeIntervalBestVector.size() );
				avgETI.diff_length /= static_cast<double>( errorTimeIntervalBestVector.size() );
			}
			
			// Vector containing information for each event in videocontent
			std::vector<ErrorTimeInterval> errorTimeIntervalBestVector;		

			ErrorTimeInterval avgETI;
		};

		Evaluation() {}
		Evaluation( const VideoContentDescription& _gt, const VideoContentDescription& _vc ): m_gt(&_gt), m_videoContent(&_vc) {}
		
		void print() const;	
		void saveAsTxt(const std::string fileOutput) const;

		// Accumulated statistics (all and grouped per Object and Event classes)
		Metrics allEventsMetrics;					// Accumulated Metrics for all Events
		Metrics allObjectsMetrics;					// Accumulated Metrics for all Objects
		std::vector<Metrics> eventMetricsVector;	// Metrics for each type of Object: "person walking", "person running", ...
		std::vector<Metrics> objectMetricsVector;	// Metrics for each type of Object: "person", "face", ...

		// Object and Event-level
		DetectedEventsErrorInfo detectedEventsErrorInfo;		
		DetectedObjectsErrorInfo detectedObjectsErrorInfo;	
		
		// Frame-wise analysis
		ErrorObjectData objectFrameWiseError;				// Frame-wise for Objects: Looping over all the frames, ignoring pertenence to Objects or Events, this is
													// intra-frame information
		Metrics eventFrameWiseError;					// Frame-wise for Events: Recall, precision, etc. at frame level
	
		std::vector<ErrorObjectData> objectFrameWiseErrorByType;				// Frame-wise for Objects by type
	

		const VideoContentDescription* m_gt;
		const VideoContentDescription* m_videoContent;	
	};	



	class EVALUATION_LIB EvaluatorBase
	{		
	public:
		/** \brief Virtual Evaluation class
		*/
		EvaluatorBase() {}
		EvaluatorBase( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent ):m_groundTruth(_groundTruth), m_videoContent(_videoContent) {}
		/** \brief Evaluate Function generates correlation matrix between objects and events in groundtruth and detected objects and events
		*/
		virtual Evaluation evaluate() { return Evaluation();}
		/** \brief Evaluate Function generates correlation matrix between objects  in groundtruth and detected objects 
		*/
		virtual bool evaluateObjects( const VideoContentDescription& _videoContent, Evaluation& _eval ){return false;};
		/** \brief Evaluate Function generates correlation matrix between events in groundtruth and detected events
		*/
		virtual bool evaluateEvents( const VideoContentDescription& _videoContent, Evaluation& _eval ){return false;};

		/** \brief Sets a loaded groundtruth info
		*/
		virtual void setGroundTruth( const VideoContentDescription& _groundTruth ) { m_groundTruth = _groundTruth; }		
		/** \brief Sets detected info from  video
		*/
		virtual void setVideoContent( const VideoContentDescription& _videoContent ) { m_videoContent = _videoContent; }
		
		/** \brief Metrics to measure error between intervals
		*/
		virtual ErrorTimeInterval computeTimeError( const videoContentDescription::Object& _t1, const videoContentDescription::Object& _t2 ) const{return ErrorTimeInterval();};
		virtual ErrorTimeInterval computeTimeError( const videoContentDescription::Event& _t1, const videoContentDescription::Event& _t2 ) const{return ErrorTimeInterval();};
		/** \brief Metrics to measure spacial error, overlapping between object's bounding boxes
		*/
		virtual ErrorObjectData computeSpatialError( const viulib::videoContentDescription::Object& _gt, const Object& _obj ) const{return ErrorObjectData();};

		
	protected:
		// Ground truth
		VideoContentDescription m_groundTruth;			
		VideoContentDescription m_videoContent;
	};
	

	/** \brief This class provides tools to evaluate performance of algorithms
	* \ingroup viulib_dtc
    */
	class EVALUATION_LIB Evaluator : public EvaluatorBase
	{			
	public:			
		//Evaluator() : m_eot(0.5), m_oot(0.5), m_frame_wise( true )								{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( double _ot ) : m_eot(_ot), m_oot(_ot), m_frame_wise( true )					{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( double _eot, double _oot ) : m_eot(_eot),m_oot(_oot), m_frame_wise( true )	{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( bool _fw, double _eot = 0.5, double _oot = 0.5 ): m_eot(_eot),m_oot(_oot), m_frame_wise(_fw) {}
		Evaluator(): m_eot(15), m_oot(0.5), m_frame_wise( true ), m_sparseMode( false ) {}
		Evaluator( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent )
			: EvaluatorBase(_groundTruth,_videoContent), m_eot(15),m_oot(0.5),m_frame_wise(true), m_sparseMode( false ) {}		
		Evaluator( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent, double _eot, double _oot, bool _frameWise )
			: EvaluatorBase(_groundTruth,_videoContent),m_eot(_eot),m_oot(_oot),m_frame_wise(_frameWise), m_sparseMode( false ) {}		
		Evaluator( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent, double _eot, double _oot, bool _frameWise, bool _sparseMode )
			: EvaluatorBase(_groundTruth,_videoContent),m_eot(_eot),m_oot(_oot),m_frame_wise(_frameWise), m_sparseMode( _sparseMode ) {}		
		
		void config( double _eot, double _oot, bool _frameWise, bool _sparseMode ) 
		{
			m_eot = _eot;
			m_oot = _oot;
			m_frame_wise = _frameWise;
			m_sparseMode = _sparseMode;
		}
		void config( double _ot, bool _frameWise, bool _sparseMode )
		{
			m_eot = _ot;
			m_oot = _ot;
			m_frame_wise = _frameWise;
			m_sparseMode = _sparseMode;
		}		

		~Evaluator() { }//delete m_errorFunction; }		

		void setGroundTruth( const VideoContentDescription& _groundTruth ) { m_groundTruth = _groundTruth; }		
		void setVideoContent( const VideoContentDescription& _videoContent ) { m_videoContent = _videoContent; }		
		
		//Evaluation evaluate( const VideoContentDescription& _videoContent );		
		Evaluation evaluate();
		
	private:
		bool evaluateObjects( const VideoContentDescription& _videoContent, Evaluation& _eval );
		bool evaluateEvents( const VideoContentDescription& _videoContent, Evaluation& _eval );

		ErrorTimeInterval computeTimeError( const videoContentDescription::Event& _t1, const videoContentDescription::Event& _t2 ) const
		{
			return computeTimeError(_t1.getFrameStart(),_t1.getFrameEnd(),_t2.getFrameStart(),_t2.getFrameEnd());
		}

		ErrorTimeInterval computeTimeError( const videoContentDescription::Object& _t1, const videoContentDescription::Object& _t2 ) const
		{
			return computeTimeError(_t1.getFrameStart(),_t1.getFrameEnd(),_t2.getFrameStart(),_t2.getFrameEnd());
		}

		ErrorTimeInterval computeTimeError( const int& _t1Start, const int& _t1End, const int& _t2Start, const int& _t2End ) const
		{

			// Valid only for Object and Event objects
			ErrorTimeInterval data;
			data.diff_start		= static_cast<double>( abs( _t2Start - _t1Start ) );
			data.diff_end		= static_cast<double>( abs(_t2End - _t1End ) );
			data.diff_length	= static_cast<double>( abs( _t2End - _t2Start ) - abs( _t1End - _t1Start ) );
			data.diff_mean 		= static_cast<int>( ( abs( data.diff_start ) + abs( data.diff_end ) + abs( data.diff_length ) )/3.0 );

            #ifdef __APPLE__
                int minEndVal	= min( _t2End, _t1End );
                int maxEndVal	= max( _t2End, _t1End );
                int maxStartVal = max( _t2Start, _t1Start );
                int minStartVal = min( _t2Start, _t1Start );
            #else
                int minEndVal	= std::min( _t2End, _t1End );
                int maxEndVal	= std::max( _t2End, _t1End );
                int maxStartVal = std::max( _t2Start, _t1Start );
                int minStartVal = std::min( _t2Start, _t1Start );
            #endif

			data.intersection = minEndVal - maxStartVal;

			if( maxStartVal >= minEndVal )
			{
				data.overlap = 0.0;
				data.overlap_ab = 0.0;
				data.overlap_ba = 0.0;
				data.intersection = 0.0;				
			}
			else	
			{
				data.overlap = static_cast<double>(data.intersection)/static_cast<double>(maxEndVal - minStartVal);	
				data.overlap_ab = static_cast<double>(data.intersection)/static_cast<double>( _t1End - _t1Start );
				data.overlap_ba = static_cast<double>(data.intersection)/static_cast<double>( _t2End - _t2Start );
			}

			return data;
		}

		ErrorObjectData computeSpatialError( const videoContentDescription::Object& _gt, const videoContentDescription::Object& _obj ) const;

		std::pair<double,bool> compareData( const videoContentDescription::ObjectData* _obj1, const videoContentDescription::ObjectData* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::bbox* _obj1, const videoContentDescription::bbox* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::circle* _obj1, const videoContentDescription::circle* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::point* _obj1, const videoContentDescription::point* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::status* _obj1, const videoContentDescription::status* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::intervalInteger* _obj1, const videoContentDescription::intervalInteger* _obj2 ) const;

		// Mode
		bool m_frame_wise;
		bool m_sparseMode;
		
		

		// Thresholds
		double m_eot; // Event Overlap Threshold
		double m_oot; // Object Overlap Threshold

		// Required?		
		//ErrorFunctor* m_errorFunction;
	};



	/** \brief This class evaluates  tools to evaluate performance of algorithms
	
    */
	class EVALUATION_LIB EvaluatorOneMany : public EvaluatorBase
	{			
	public:			
		//Evaluator() : m_eot(0.5), m_oot(0.5), m_frame_wise( true )								{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( double _ot ) : m_eot(_ot), m_oot(_ot), m_frame_wise( true )					{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( double _eot, double _oot ) : m_eot(_eot),m_oot(_oot), m_frame_wise( true )	{ }//m_errorFunction = new ErrorTimeInterval(); }
		//Evaluator( bool _fw, double _eot = 0.5, double _oot = 0.5 ): m_eot(_eot),m_oot(_oot), m_frame_wise(_fw) {}
		EvaluatorOneMany(): m_eot(15), m_oot(0.5), m_frame_wise( true ), m_sparseMode( false ) {}
		EvaluatorOneMany( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent )
			: EvaluatorBase(_groundTruth,_videoContent), m_eot(15),m_oot(0.5),m_frame_wise(true), m_sparseMode( false ) {}		
		EvaluatorOneMany( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent, double _eot, double _oot, bool _frameWise )
			: EvaluatorBase(_groundTruth,_videoContent),m_eot(_eot),m_oot(_oot),m_frame_wise(_frameWise), m_sparseMode( false ) {}		
		EvaluatorOneMany( const VideoContentDescription& _groundTruth, const VideoContentDescription& _videoContent, double _eot, double _oot, bool _frameWise, bool _sparseMode )
			: EvaluatorBase(_groundTruth,_videoContent),m_eot(_eot),m_oot(_oot),m_frame_wise(_frameWise), m_sparseMode( _sparseMode ) {}		
		
		void config( double _eot, double _oot, bool _frameWise, bool _sparseMode ) 
		{
			m_eot = _eot;
			m_oot = _oot;
			m_frame_wise = _frameWise;
			m_sparseMode = _sparseMode;
		}
		void config( double _ot, bool _frameWise, bool _sparseMode )
		{
			m_eot = _ot;
			m_oot = _ot;
			m_frame_wise = _frameWise;
			m_sparseMode = _sparseMode;
		}		

		~EvaluatorOneMany() { }//delete m_errorFunction; }		
		
		//Evaluation evaluate( const VideoContentDescription& _videoContent );		
		Evaluation evaluate();
		
	private:
		bool evaluateObjects( const VideoContentDescription& _videoContent, Evaluation& _eval );
		bool evaluateEvents( const VideoContentDescription& _videoContent, Evaluation& _eval );

		ErrorTimeInterval computeTimeError( const videoContentDescription::Event& _t1, const videoContentDescription::Event& _t2 ) const
		{
			return computeTimeError(_t1.getFrameStart(),_t1.getFrameEnd(),_t2.getFrameStart(),_t2.getFrameEnd());
		}

		ErrorTimeInterval computeTimeError( const videoContentDescription::Object& _t1, const videoContentDescription::Object& _t2 ) const
		{
			return computeTimeError(_t1.getFrameStart(),_t1.getFrameEnd(),_t2.getFrameStart(),_t2.getFrameEnd());
		}

		ErrorTimeInterval computeTimeError( const int& _t1Start, const int& _t1End, const int& _t2Start, const int& _t2End ) const
		{
			// Valid only for Object and Event objects
			ErrorTimeInterval data;
			data.diff_start		= static_cast<double>( abs( _t2Start - _t1Start ) );
			data.diff_end		= static_cast<double>( abs( _t2End - _t1End ) );
			data.diff_length	= static_cast<double>( abs( _t2End - _t2Start ) - abs( _t1End - _t1Start ) );
			data.diff_mean 		= static_cast<int>( ( abs( data.diff_start ) + abs( data.diff_end ) + abs( data.diff_length ) )/3.0 );

            #ifdef __APPLE__
                int minEndVal	= min( _t2End, _t1End );
                int maxEndVal	= max( _t2End, _t1End );
                int maxStartVal = max( _t2Start, _t1Start );
                int minStartVal = min( _t2Start, _t1Start );
            #else
                int minEndVal	= std::min( _t2End, _t1End );
                int maxEndVal	= std::max( _t2End, _t1End );
                int maxStartVal = std::max( _t2Start, _t1Start );
                int minStartVal = std::min( _t2Start, _t1Start );
            #endif

			data.intersection = minEndVal - maxStartVal;

			if( maxStartVal >= minEndVal )
			{
				data.overlap = 0.0;
				data.overlap_ab = 0.0;
				data.overlap_ba = 0.0;
				data.intersection = 0.0;				
			}
			else	
			{
				//@@-- General overlap = existing overlap divided by total area --@@//
				data.overlap = static_cast<double>(data.intersection)/static_cast<double>(maxEndVal - minStartVal);	
				//@@--  overlap a b = existing overlap divided by length of t1 --@@//
				data.overlap_ab = static_cast<double>(data.intersection)/static_cast<double>( _t1End - _t1Start );
				//@@--  overlap b a = existing overlap divided by length of t2 --@@//
				data.overlap_ba = static_cast<double>(data.intersection)/static_cast<double>( _t2End - _t2Start );
			}

			return data;
		}

		ErrorObjectData computeSpatialError( const videoContentDescription::Object& _gt, const videoContentDescription::Object& _obj ) const;

		std::pair<double,bool> compareData( const videoContentDescription::ObjectData* _obj1, const videoContentDescription::ObjectData* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::bbox* _obj1, const videoContentDescription::bbox* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::circle* _obj1, const videoContentDescription::circle* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::point* _obj1, const videoContentDescription::point* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::status* _obj1, const videoContentDescription::status* _obj2 ) const;
		std::pair<double,bool> compareData( const videoContentDescription::intervalInteger* _obj1, const videoContentDescription::intervalInteger* _obj2 ) const;

		// Mode
		bool m_frame_wise;
		bool m_sparseMode;
		
		

		// Thresholds
		double m_eot; // Event Overlap Threshold
		double m_oot; // Object Overlap Threshold

		// Required?		
		//ErrorFunctor* m_errorFunction;
	};

	
}
}
#endif //_EVALUATION_H_
