/** 
 * viulib_system (System) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_system is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_system depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_system.
 *
 */

#pragma once


// ==============================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// ==============================================================================================
// When the symbol BOT_EXPORTS is defined in a project OMF exports functions and classes. 
// In other cases BOT_LIB imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_system_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define SL_LIB __declspec( dllexport )
	#else
		#define SL_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define SL_LIB __declspec( dllimport )
	#else
		#define SL_LIB
	#endif
#endif
#else
#define SL_LIB
#endif

// ==============================================================================================
// AUTOMATIC DLL LINKAGE
// ==============================================================================================
// If the code is compiled under MS VISUAL STUDIO, and BOT_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_system_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_system_d.lib" )
	#else
		#pragma comment( lib, "viulib_system.lib" )
	#endif
#endif

#ifndef SYSTEM_EXP_H
#define SYSTEM_EXP_H


#include <string>
#include <iostream>
#include <fstream>
#include "opencv2/core/core.hpp"

#define		VIU_F1			 0xCE					
#define		VIU_F2			 0xCF				
#define		VIU_F3			 0xD0				
#define		VIU_F4			 0xD1				
#define		VIU_F5			 0xD2				
#define		VIU_F6			 0xD3				
#define		VIU_F7			 0xD4				
#define		VIU_F8			 0xD5				
#define		VIU_F9			 0xD6				
#define		VIU_F10			 0xD7				
#define		VIU_F11			 0xD8				
#define		VIU_F12			 0xD9
#define		VIU_LCONTROL	 0xDA		
#define		VIU_LALT		 0xDB		
#define		VIU_RCONTROL	 0xDC		
#define		VIU_RALT		 0xDD		
#define		VIU_LSHIFT		 0xDE		
#define		VIU_RSHIFT		 0xDF		
#define		VIU_SPACE		 0xE0		
#define		VIU_ENTER		 0xE1		
#define		VIU_ESCAPE		 0xE2		
#define		VIU_INSERT		 0xE3
#define		VIU_BKSP		 0xE4
#define		VIU_TAB			 0xE5
#define		VIU_HOME		 0xE6
#define		VIU_PGUP		 0xE7
#define		VIU_DELETE		 0xE8
#define		VIU_END			 0xE9
#define		VIU_PDDOWN		 0xEA
#define		VIU_UPARROW		 0xEB
#define		VIU_LEFTARROW	 0xEC
#define		VIU_DOWNARROW	 0xED
#define		VIU_RIGHTARROW	 0xEE
#define		VIU_NUM			 0xEF
#define		VIU_KPDIV		 0xF0
#define		VIU_KPMUL		 0xF1
#define		VIU_KPSUB		 0xF2
#define		VIU_KPADD		 0xF3
#define		VIU_KPEN		 0xF4
#define		VIU_KPDOT		 0xF5
#define		VIU_KP0			 0xF6
#define		VIU_KP1			 0xF7
#define		VIU_KP2			 0xF8
#define		VIU_KP3			 0xF9
#define		VIU_KP4			 0xFA
#define		VIU_KP5			 0xFB
#define		VIU_KP6			 0xFC
#define		VIU_KP7			 0xFD
#define		VIU_KP8			 0xFE
#define		VIU_KP9			 0xFF

namespace viulib {

	namespace system {

	enum  VIU_M_EVENT {VIU_LEFT_CLICK = 0, VIU_RIGHT_CLICK = 1, VIU_LEFT_DOUBLE_CLICK = 2, VIU_RIGHT_DOUBLE_CLICK = 3,VIU_MOUSE_MOVE = 4, VIU_LEFT_DOWN = 5,VIU_LEFT_UP=6, VIU_RIGHT_DOWN=7, VIU_RIGHT_UP=8,VIU_SCROLL_UP=9, VIU_SCROLL_DOWN = 10};
	enum  VIU_K_EVENT {VIU_KEY = 0, VIU_KEY_DOWN = 1, VIU_KEY_UP = 2, VIU_PAGEDOWN = 3, VIU_PAGEUP = 4, VIU_PROGRAM_LEFT = 5,VIU_PROGRAM_RIGHT = 6, VIU_KEY_SCROLL_UP=7, VIU_KEY_SCROLL_DOWN = 8} ;
	
	struct viu_ProcessInfo
	{
		unsigned long    cntUsage;
		unsigned long    th32ProcessID;          // this process
    	unsigned long    th32ModuleID;           // associated exe
		unsigned long    cntThreads;
		unsigned long    th32ParentProcessID;    // this process's parent process
		long    pcPriClassBase;         // Base priority of process's threads
		std::string    exe_file;    // Path

	}; 
//	enum SL_LIB vkEVENT {KEY_DOWN = 0, KEY_UP = 1};
//	enum SL_LIB vmEVENT {MOUSE_LEFT_DOWN = 0, MOUSE_LEFT_UP = 1, MOUSE_RIGHT_DOWN=2, MOUSE_RIGHT_UP=3,MOUSE_MOVE=4};

//	enum KeyBoard{CLOSEAPP = 0, ENABLEEYE = 1, KEY = 2, CLICKSIM = 3, MOVESIM = 4, MOVEUP = 5, MOVEDOWN = 6, MOVELEFT = 7, MOVERIGHT = 8, ESC = 9, NO_KEY = -1};
}
}
#endif
