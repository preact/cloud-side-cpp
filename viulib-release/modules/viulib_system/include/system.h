/**  
 * \defgroup viulib_system viulib_system
 * viulib_system (System) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_system is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_system depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_system.
 *
 */



#ifndef SYSTEM_H
#define SYSTEM_H
#include "system_exp.h"

#define TEXT_COLOR_RED 0x0C
#define TEXT_COLOR_GREEN 0x0A
#define TEXT_COLOR_CYAN 0x03
#define TEXT_COLOR_WHITE FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY
#define TEXT_COLOR_BLACK 0x00

#define TEXT_COLOR_BLUE 0x01 

namespace viulib
{
	/** \brief System support functions
		\ingroup viulib_system
	*/
	namespace system
	{
		
		SL_LIB int kbhit (void);
		#ifndef WIN32
			void changemode(int dir);
			/* Initialize new terminal i/o settings */
			SL_LIB void initTermios(int echo); 
			/* Restore old terminal i/o settings */
			SL_LIB void resetTermios(void);
			/* Read 1 character - echo defines echo mode */
			char getch_(int echo); 
			/* Read 1 character without echo */
			SL_LIB char getch(void); 
			/* Read 1 character with echo */
			char getche(void);
		#else
			SL_LIB char getch(void); 
		#endif

		/** Devuelve la posicion del raton desde el TOP_LEFT de la ventana.
			- <code> getMousePos() </code> .
			\pre

			\remark

			\par Ejemplo:
				\code
					// lectura de la posicion del raton
					cv::Point p = viulib::system::getMousePos ();
				\endcode
		*/
		SL_LIB cv::Point getMousePos();

		/** Envia un evento de raton en el punto p al sistema.
			- <code> sendMouseEvent(evento,punto) </code> .
			\pre
				<code> (e debe pertenecer al enum M_EVENT) </code>
				<code> (p debe estar dentro de las dimensiones de la pantalla) </code>

			\remark
				En windows se utiliza SendInput.

			\par Ejemplo:
				\code
					// envio del evento de click con el boton izquierdo en el punto (10,20)
					viulib::system::sendMouseEvent (LEFT_CLICK,POINT(10,20));
				\endcode
		*/
		SL_LIB void sendMouseEvent(VIU_M_EVENT e, cv::Point p);
		
		/** Envia un evento de teclado junto con el codigo de tecla.
			- <code> sendKeyboardEvent(evento,tecla) </code> .
			\pre
				<code> (e debe pertenecer al enum K_EVENT) </code>
			\remark
				En windows se utiliza SendInput.

			\par Ejemplo:
				\code
					// envio del evento de tecla A pulsada 
					viulib::system::sendKeyboardEvent (KEY,'A');
				\endcode
		*/
		SL_LIB void sendKeyboardEvent(VIU_K_EVENT e,unsigned char  c=0xFF);
		
		/** Funcion que devuelve el identificador al proceso con nombre lpcszFileName.
			- <code> FindProcess(nombreProceso) </code> .
			\pre
				<code> (lpcszFileName debe existir) </code>
			\remark
				
			\par Ejemplo:
				\code
					// buscar proceso notepad 
					DWORD processID = viulib::system::sendKeyboardEvent ("notepad.exe");
				\endcode
		*/
		SL_LIB int FindProcess( std::string lpcszFileName);
		
		/** Devuelve el numero de Ticks del procesador.
			- <code> getTicks() </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// buscar proceso notepad 
					unsigned long ticks = viulib::system::getTicks();
				\endcode
		*/
		SL_LIB unsigned long getTicks();

		/** Devuelve el numero de microsegundos n Alta Resolucion (High Resolution) del procesador.
			- <code> getTicksHR() </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// devolver los milisegundos entre dos lecturas del contador
					double t1 = viulib::system::getTicksHR();
					double t2 = viulib::system::getTicksHR();
					double time_ms = (t2-t1) * 0.001;
				\endcode
		*/
		SL_LIB double getTicksHR();

		SL_LIB double getTicksFreq(long begin);
	
		/** Redefine el area del escritorio
			- <code> resizeDesktopArea(Area) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// redefinir tama�o escritorio 
					unsigned long ticks = viulib::system::resizeDesktopArea(RECT(0,0,100,100));
				\endcode
		*/
		SL_LIB void resizeDesktopArea(cv::Rect rect);
		
		/** Obtiene una lista de las voces para el TTS disponibles en el sistema
			- <code> getVoiceList() </code> .
			\pre
				<code> requiere que se haya inicializado el TTS con initTTS </code>
				<code> requiere la definicion de preproceso TTS_SUPPORT </code>
			\remark
				
			\par Ejemplo:
				\code
					// obtener la lista de voces
					viulib::system::initTTS();
					....
					std::vector<std::string> lista_voces= viulib::system::getVoiceList();
					if (lista_voces.isEmpty()) std::cout << "No hay voces instaladas."<<std::endl;
				\endcode
		*/
		SL_LIB std::vector<std::string> getVoiceList();
		
		/** Cambia a una nueva voz el TTS
			- <code> changeVoice(identificador_voz) </code> .
			\pre
				<code> requiere que se haya inicializado el TTS con initTTS </code>
				<code> requiere la definicion de preproceso TTS_SUPPORT </code>
			\remark
				
			\par Ejemplo:
				\code
					// cambiar a la voz llamada "Carmen"
					viulib::system::initTTS();
					....

					std::vector<std::string> lista_voces= viulib::system::getVoiceList();
					if (lista_voces.isEmpty()) std::cout << "No hay voces instaladas."<<std::endl;
					else
					{
						int id_voz = -1;
						for (int i=0;i<lista_voces.size();i++)
						{
							if (lista_voces.compare("Carmen")==0)
							{ 
								id_voz = i;
						 		break;
							}
						}
						if (id_voz!=-1) viulib::system::changeVoice(id_voz);
						else std::cout << "La voz Carmen no existe." << std::endl;
					}
				\endcode
		*/
		SL_LIB int changeVoice(int i);
		
		/** Envia un texto para ser reproducido por la voz sintetica
			- <code> speak(texto) </code> .
			\pre
				<code> requiere que se haya inicializado el TTS con initTTS </code>
				<code> requiere la definicion de preproceso TTS_SUPPORT </code>
			\remark
				Soporta lenguaje etiquetado para cambios en el tono y en la velocidad del discurso
				SAPI TTS XML soporta cinco etiquetas que controlan el estado de la voz actual: Volume, Rate, Pitch, Emph, Spell, Pron, Silence, Voice, Lang, p, Partofsp, Context, Bookmark.
			\par Ejemplo:
				\code
					// Reproducir la frase "Dicenme que decis, exreina mia, que os dicen que yo he dicho aquel secreto. Y yo digo que os digo en un soneto que es decir por decir tal tonteria."
					viulib::system::initTTS();
					....
					std::stringstream oss;
					std::string texto;
					oss << "<volume level=\"50\">";
					oss << "Dicenme que decis," << "<silence msec=\"500\"/>" << " exreina mia,";
					oss << "<volume level="100">";
					oss << "que os dicen que yo he dicho aquel" << "<emph> secreto </emph>!";
					oss << "</volume>"
					oss << "</volume>"
					oss << "<volume level=\"80\"/>";
					oss << "Y yo digo que os digo en un soneto que es decir por decir tal tonteria."
					texto = oss.str();
					viulib::system::speak(texto);					
				\endcode
		*/
		SL_LIB void speak(std::string texto);
		
		/** Inicializa el TTS del sistema
			- <code> initTTS() </code> .
			\pre
				<code> requiere la definicion de preproceso TTS_SUPPORT </code>
			\remark
				
			\par Ejemplo:
				\code
					// inicializar tts
					viulib::system::initTTS();
				\endcode
		*/
		SL_LIB void initTTS();
		
		/** Funcion que devuelve el tama�o de la pantalla
			- <code> getScreenSize(filas, columnas) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// obtener el tama�o de la pantalla
					int filas, columnas;
					viulib::system::getScreenSize(filas, columnas);
				\endcode
		*/
		SL_LIB void getScreenSize(int &rows, int &cols);


		/** Funcion que devuelve una marca de tiempo con el formato especificado
			- <code> time_stamp(formato) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// requerir una marca de tiempo con formato A�o-Mes-Dia Hora_Minutos_Segundos
					std::string marca = viulib::system::time_stamp("%Y-%m-%d %H_%M_%S");
				\endcode
		*/
		SL_LIB std::string time_stamp(std::string format);

		/** Devuelve una lista de los ficheros ubicados en el directorio indicado
			- <code> getdir(directorio,vector_de_ficheros) </code> .
			\pre
				
			\remark
				realiza la lista de manera recursiva accediendo tambien a los subdirectorios
				Si queremos solo los directorios habria que pasar el filtro *. (los ficheros sin extension apareceran sin barra / al final)
				Si queremos las imagenes podemos pasarle como filtro "images"
				Si queremos los videos pasar como filtro "videos"

				with filter == "*." only directories and files without extension are storaged
				with filetr == "directories" only directories are storaged

				Si no se pasa clear a true no se elimina lo que tenga el vector_de_ficheros que se la pasa
			\par Ejemplo:
				\code
					// lista de ficheros *.bmp y *.jpg del directorio "C:/temp"
					std::vector <string> lista_ficheros;
					viulib::system::getdir("C:/temp",lista_ficheros,"*.bmp",false,true);
					viulib::system::getdir("C:/temp",lista_ficheros,"*.jpg",false,true);
				\endcode
		*/
		SL_LIB int getdir (std::string dir, std::vector<std::string> &files,std::string filter=std::string(),bool recursive=false,bool clear = false);

		/** Devuelve una lista,  por cada carpeta presente en un directorio, de listas de  ficheros obtenidos de manera recursiva. El nombre de cada subcarpeta de primer nivel en el directorio que se procesa representar� una etiqueta.
			- <code> getdir(directorio,etiquetas, vector_de_ficheros,filtro) </code> .
			\pre
				
			\remark
			\par Ejemplo:
				\code
					// lista de ficheros *.bmp y *.jpg del directorio "C:/temp"
					std::vector <std::vector < string > > lista_ficheros;
					std::vectro <std::string> etiquetas;
					viulib::system::getdir("C:/temp",etiquetas,lista_ficheros,"*.bmp");
					viulib::system::getdir("C:/temp",etiquetas, lista_ficheros,"*.jpg");
				\endcode
		*/
		SL_LIB int getdir (std::string _dir, std::vector < std::string > &_labels, std::vector<std::vector<std::string> >& _files,std::string &filter);

		/** Crea un directorio si no existe, si existe devuelve 0 
			- <code> createdir(directorio) </code> .
			\pre
				recursive: flag que indica si se devolveran tambien los ficheros de los subdirectorios de manera recursiva
			\remark
				
				
			\par Ejemplo:
				\code
					// crea el directorio temp 
					viulib::system::createdir("C:/temp");
				\endcode
		*/
		SL_LIB int createdir (std::string &dir,bool remove_content=true);
		
		/** Borra un directorio si  existe
			- <code> createdir(directorio) </code> .
			\pre
				recursive: flag que indica si se borraran tambien los subdirectorios dentro de este directorio de manera recursiva
			\remark
				
				
			\par Ejemplo:
				\code
					// crea el directorio temp 
					viulib::system::removedir("C:/temp");
				\endcode
		*/
		SL_LIB int removedir (std::string &dir, bool remove_subdirs=false);

		/** Borra un fichero 
			- <code> deletefile(fichero) </code> .
			\pre
				
			\remark
				
				
			\par Ejemplo:
				\code
					// borrar el fichero temp.txt 
					viulib::system::deletefile("c:/temp/temp.txt");
				\endcode
		*/
		SL_LIB  bool deletefile (std::string file);

		/** Devuelve true si existe el fichero que se le pasa como parametro
			- <code> fexists(fichero) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// comprobar si existe el fichero config.xdl
					bool exists = viulib::system::fexists("config.xdl");
				\endcode
		*/
		SL_LIB bool fexists(const char *filename);
		SL_LIB bool dexists(const char *directory);

		/** Devuelve la extension del fichero que se le indica
			- <code> extension(fichero) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// extension del fichero "imagen.bmp"
					std::string ext = viulib::system::extension("imagen.bmp");
				\endcode
		*/
		SL_LIB std::string extension(std::string file);

		/** Devuelve el nombre del fichero que se le indica
			- <code> fileName(fichero) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// extension del fichero "C:\temp\imagen.bmp"
					std::string fileName = viulib::system::fileName("C:\temp\imagen.bmp");
				\endcode
		*/
		SL_LIB std::string fileName(std::string file,bool _ext=false);

		/** Devuelve el nombre del folder que se le indica
          Ignora el ultimo caracter '\' o '/', si esta en el string.
          - <code> dirName(fichero) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// output folder "imagenes"
					std::string dirName = viulib::system::dirName("C:\temp\imagenes\");
               std::string dirName = viulib::system::dirName("C:\temp\imagenes");
				\endcode
		*/
		SL_LIB std::string dirName(std::string file);

      /** Devuelve el path del fichero que se le indica
			- <code> path(fichero) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// extension del fichero "C:\temp\imagen.bmp"
					std::string path = viulib::system::path("C:\temp\imagen.bmp");
					//path = c:/temp
				\endcode
		*/
		SL_LIB std::string path(std::string file);


		/** Activa o desactiva un hook para el raton
			- <code> listenMouse(enabled,callback) </code> .
			\pre
				
			\remark
				El valor de retorno le indicara al hook si seguir propagando el mensaje (0) o no (1)
				
			\par Ejemplo:
				\code
					// activar el hook para el raton
					int callback_raton (cv::Point p, viulib::M_EVENT evento)
					{
					}
					.....

					viulib::system::listenMouse(true,&callback_raton);
				\endcode
		*/
		//SL_LIB void listenMouse(bool enabled,int (*callback)(cv::Point,viulib::VIU_M_EVENT));

		/** Activa o desactiva un hook para el teclado
			- <code> listenMouse(enabled,callback) </code> .
			\pre
				
			\remark
				El valor de retorno le indicara al hook si seguir propagando el mensaje (0) o no (1)
				El codigo que se le envia al callback es un byte 
				para caracteres imprimibles es su codigo ascii y para el resto un codigo definido en viulib
				estos codigos para otras teclas estan definidas como VIU_TECLA en viulib
				
			\par Ejemplo:
				\code
					// activar el hook para el raton
					int callback_teclado (int code, viulib::K_EVENT evento)
					{
						if (code == VIU_LCONTROL) ...
						else if (code == (int)'A') ...
					}
					.....

					viulib::system::listenKeyboard(true,&callback_teclado);
				\endcode
		*/
		SL_LIB void listenKeyboard(bool enabled,int (*callback)(uchar,viulib::system::VIU_K_EVENT));

		/** Funcion que nos indica si esta o no pulsado el ALT cuando el hook esta activo
			- <code> isAltPressed() </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// consultar la tecla ALT
					int callback_teclado (int code, viulib::K_EVENT evento)
					{
						if (code == VIU_LCONTROL) ...
						else if (code == (int)'A') ...
					}
					.....

					viulib::system::listenKeyboard(true,&callback_teclado);

					....
					viulib::isAltPressed();
				\endcode
		*/
		SL_LIB bool isAltPressed();
		/** Funcion que nos indica si esta o no pulsado el CTRL cuando el hook esta activo
			- <code> isCtrlPressed() </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// consultar la tecla CTRL
					int callback_teclado (int code, viulib::K_EVENT evento)
					{
						if (code == VIU_LCONTROL) ...
						else if (code == (int)'A') ...
					}
					.....

					viulib::system::listenKeyboard(true,&callback_teclado);

					....
					viulib::isCtrlPressed();
				\endcode
		*/
		SL_LIB bool isCtrlPressed();

		/** Funcion que muestra u oculta el cursor del raton
			- <code> showMouseCursor(visible) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// ocultar el raton
						viulib::system::showMouseCursor(false);
				\endcode
		*/
		SL_LIB bool showMouseCursor(bool s);

		/** Funcion que devuelve el directorio en el que se encuentra el ejecutable
			- <code> getAppDirectory() </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// obtener el path al ejecutable
						std::string dir = viulib::system::getAppDirectory();
				\endcode
		*/
		SL_LIB std::string getAppDirectory();

		/** Funcion que devuelve el directorio en el que se ejecuta la aplicaci�n
			- <code> getCurrentDirectory() </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// obtener el path al ejecutable
						std::string dir = viulib::system::getCurrentDirectory();
				\endcode
		*/
		SL_LIB std::string getCurrentDirectory();

		/** lanza la aplicacion que se le pasa por el exe_file con los parametros indicados en parameters
			- <code> runApp(path_al_ejecutable, parametros) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// lanzar el plaphoon.exe con parametros --load PLAPHOON
						viulib::system::runApp("C:/Program Files/plaphoon/plaphoon.exe", std::string("--load PLAPHOON"));
				\endcode
		*/
		SL_LIB viu_ProcessInfo runApp(std::string exe_file,std::string parameters=std::string(),bool wait=false);

		/** terminar aplicacion por nombre de ejecutable
			- <code> terminateProcess(path_al_ejecutable) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// terminar la aplicacion plaphoon.exe 
						viulib::system::terminateProcess("plaphoon.exe");
				\endcode
		*/
		SL_LIB bool terminateProcess(std::string process_name);
		
		/** terminar aplicacion por id de proceso
			- <code> terminateProcess(id_proceso) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// terminar la aplicacion plaphoon.exe 
						viulib::ProcessInfo  p = viulib::system::runApp("C:/Program Files/plaphoon/plaphoon.exe", std::string("--load PLAPHOON"));
						....
						viulib::system::terminateProcess(p.th32ModuleID);
				\endcode
		*/
		SL_LIB bool terminateProcess(int process_id);

		/** asigna o crea la variable variable y le asigna el valor value
			- <code> setEnvironmentVariable(variable,valor) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// a�adir un nuevo directorio a la variable path del sistema
						std::string path = getEnvironmentVariable ("PATH");
						viulib::system::setEnvironmentVariable(path, path+":c:/directorio");
				\endcode
		*/
		SL_LIB bool setEnvironmentVariable(std::string variable, std::string value);

		
		/** lee la variable variable 
			- <code> getEnvironmentVariable(variable,valor) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// a�adir un nuevo directorio a la variable path del sistema
						std::string path = getEnvironmentVariable ("PATH");
						viulib::system::setEnvironmentVariable(path, path+":c:/directorio");
				\endcode
		*/
		SL_LIB std::string getEnvironmentVariable(std::string variable);
	
		SL_LIB  void sleep(int msecs);

		SL_LIB void trace(int write, const char *text,...);
		SL_LIB void trace(const char *text, ...);
		SL_LIB void warning(const char *loc, const char *text, ...);
		SL_LIB void error(const char *loc, const char *text, ...);
		SL_LIB void errorENH(const char *loc, const char *text, ...);
		SL_LIB void message(unsigned char color, const char *text, ...);
		SL_LIB void info(unsigned char color, const char *text, ...);


		/** comprueba que la condicion se de, si no escribe un mensaje por la salida estandar
			- <code> check(condicion,mensaje, argumentos) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
						//-- Comprobar que una imagen no esta vacia, si escribir mensaje --//
						cv::Mat img;
						std::string filename;
						....				
						viulib::system::check(!img.empty(), "Error: la imagen %s esta vacia", filename.c_str());

				\endcode
		*/
		SL_LIB bool check(bool cond, const char *text,...);

		SL_LIB std::string normalizePath(std::string path,bool file=false);

		SL_LIB bool isImage(std::string f);

		SL_LIB bool isVideo(std::string f);

	}
	
}

#endif
