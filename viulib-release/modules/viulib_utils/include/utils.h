/** 
 * \defgroup viulib_utils viulib_utils
 * viulib_utils (Utilities) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_utils is a module of 
 * Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 */

#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <string>
#include <set>

#include <ctime>
#include <iostream>
#include <fstream>
#include <iterator>

// If it is linux system, define the functions we must use
#ifdef __unix__
	#include <sys/time.h>
	#include <stdint.h>
	#include <stdbool.h>
	#include <stddef.h>
	#include <assert.h>
#elif defined _WIN32
	#define NOMINMAX
	#include <windows.h>
#endif

// ==============================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// ==============================================================================================
// When the symbol viulib_utils_EXPORTS is defined in a project UTILS exports functions and 
// classes. 
// In other cases UTILS imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_utils_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define UTILS_LIB __declspec( dllexport )
	#else
		#define UTILS_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define UTILS_LIB __declspec( dllimport )
	#else
		#define UTILS_LIB
	#endif
#endif
#else
#define UTILS_LIB
#endif

// ==============================================================================================
// AUTOMATIC DLL LINKAGE
// ==============================================================================================
// If the code is compiled under MS VISUAL STUDIO, and UTILS_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_utils_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_utils_d.lib" )
	#else
		#pragma comment( lib, "viulib_utils.lib" )
	#endif
#endif

namespace viulib
{
	/** \brief This namespace corresponds to the viulib_utils module
	\ingroup viulib_utils
	*/
namespace utils
{
	

	//*******************************************************************************************
	// STRING FUNCTIONALITIES
	//*******************************************************************************************
    UTILS_LIB std::set<char> buildCharSet( const std::string &chars );
    UTILS_LIB bool isOneOf( char c, const std::set<char> &chars );
    UTILS_LIB size_t findFirstOf( const std::string &str, char c );
	UTILS_LIB size_t findFirstOf( const std::string &str, const std::string &chars );
    UTILS_LIB size_t findLastOf( const std::string &str, char c );
	UTILS_LIB size_t findLastOf( const std::string &str, const std::string &chars );
    UTILS_LIB std::vector<std::string> split( const std::string &phrase, const std::string &separator );
    UTILS_LIB bool endsWith( const std::string &str1, const std::string &end );
	UTILS_LIB std::string trimCopy( const std::string & str1 );
	UTILS_LIB void splitPath( const std::string &path, std::string &folder, std::string &fileName, 
		std::string &ext );
	UTILS_LIB std::string makeLower( const std::string &str );
	UTILS_LIB void makeLower( std::string &str );
	UTILS_LIB std::string makeUpper( const std::string &str );
	UTILS_LIB void makeUpper( std::string &str );
	UTILS_LIB void removeFrom( std::string &str, const std::string &toExtract );

	//*******************************************************************************************
	// TIMER CLASS
	//*******************************************************************************************
	class UTILS_LIB Timer
	{
	protected:
		long long __init, __global;

	public:
		Timer();
		virtual ~Timer(){}

		/* Helpful conversion constants. */
		static const unsigned usec_per_sec = 1000000;
		static const unsigned usec_per_msec = 1000;

		static void QueryCounter( long long *performance_count );
		void init();

		/**
		 * Devuelve el tiempo transcurrido (en milisegundos) desde el ultimo init/split.
		 */
		double get( std::string msg );

		/**
		 * Devuelve el tiempo transcurrido (en milisegundos) desde el ultimo init/split y define esta llamada como un init
		 */
		double split( std::string msg );

		/**
		 * Devuelve el tiempo transcurrido (en milisegundos) desde el ultimo init
		 */
		double getFull( std::string msg );
	};

	//*******************************************************************************************
	// TOOLS TO CONVERT FROM YML1.0 TO YML1.1
	//*******************************************************************************************
	UTILS_LIB bool convertYML( const std::string& _inputFileYML10, const std::string& _outputFileYML11 );
	void replaceAll( std::string &s, const std::string &search, const std::string &replace );
	struct modified_line
	{
		std::string value;
		operator std::string() const
		{ 		
			std::size_t index = 0;
			std::string modified = value;
			replaceAll( modified, std::string("%YAML:1.0"), std::string("") );
			replaceAll( modified, std::string(":"), std::string(": ") );
			return modified;
		}
	};
	std::istream& operator>>(std::istream& a_in, modified_line& a_line);


	// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
	UTILS_LIB const std::string currentDateTime(); 
	
}
}
#endif