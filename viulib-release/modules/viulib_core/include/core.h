/**  
 * \mainpage
 * 
 * Viulib (R) (Vision and Image Utility Library) is a set of precompiled libraries developed by Vicomtech that simplifies the building 
 * of complex computer vision, machine learning and artificial intelligence solutions. It reduces time in prototyping stages, facilitates 
 * technology validation, enables transparent licensing schemes and allow for multi-platform integration.
 *
 * Viulib (R) is composed of a set of SW modules each of them deployed for particular tasks. 
 * Below is a list of them with details about its nature, the current development status and the platforms in which they have been tested.
 *
 * <b>Application modules</b>
 *
 * viulib_adas			- Advanced Driver Assistance Systems<br>
 * viulib_evaluation	- Evaluation<br>
 * viulib_hbp			- Human <br>
 * viulib_vas			- Video Analytics for Surveillance<br>
 *
 * <b>Development modules</b>
 *
 * viulib_bgfg			- Background & Foreground Detection<br>
 * viulib_calibration	- Calibration<br>
 * viulib_dtc			- Detection, Tracking and Classification<br>
 * viulib_imgTools		- Image Tools<br>
 * viulib_mtools		- Mathematical Tools<br>
 * viulib_omf			- Object Model Fitting<br>
 * viulib_opflow		- Optical Flow<br>
 * viulib_ter			- Temporal Event Recognition<br>
 *
 * <b>Support modules</b>
 *
 * viulib_core			- Core<br>
 * viulib_security		- Security<br>
 * viulib_system		- System <br>
 * viulib_utils			- Utils<br>
 * 
 * \subsection Additional material
 *
 * Website: http://www.vicomtech.es/ <br>
 * Contact: viulib@vicomtech.org
 *
 * \subsection Copyright
 *
 * Copyright (C) 2011-2014, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 */

/**  
 *  \defgroup viulib_core viulib_core
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */
#ifndef VIULIB_CORE_H
#define VIULIB_CORE_H

//-- includes from STD --//
#include <string>
#include <list>
#include <typeinfo>
#include <cstdio>
#include <iostream>
#include <exception>

#include <opencv2/core/core.hpp>


#include "core_exp.h"
#include "param.h"

namespace viulib
{	
	/** \ingroup viulib_core
	 * This function provides information about the current version of Viulib.
	*/
	void CORE_LIB getVersion(int& _major, int& _minor, std::string& _patch);
	std::string CORE_LIB getVersion();

	static std::string executablePath;
	void CORE_LIB setExecutablePath( const std::string& _path );
	
	/** List of results to be returned by checkViulibLicense
	*/
	enum {LICENSE_VALID, LICENSE_NOT_VALID, LICENSE_REQUEST_CREATED, LICENSE_ERROR_CREATING_REQUEST, LICENSE_NOT_REQUIRED};
	
	/** This function returns a code that determines the status of the license.
	*/
	int CORE_LIB checkViulibLicense();

   //__func__  c++11 ;  __FUNCTION__  pre-standard; __PRETTY_FUNCTION__ gcc   ~  __FUNCSIG__ VC++; __FUNCDNAME__ - VC++ decorated
 /*  #define STR_EXCEPTION(mess) \ 
      viulib::StrException( " Viulib Exception: " + mess + std::endl + \
                            "File: " + __FILE__ + ", line: " + __LINE__ + ", function: " + __PRETTY_FUNCTION__); 

   #define THROW_STR_EXCEPTION(mess) { \ 
      std::stringstream ss; \
      ss << " Viulib Exception: " << mess << std::endl << "File: " << __FILE__ << ", line: " << __LINE__ << ", function: " << __PRETTY_FUNCTION__ ; \
      throw( StrException(ss.str()) ); \
   }*/
	/**
	*	\ingroup viulib_core
	*/
	class StrException : public std::exception
	{
	  public:
	    StrException(std::string s) : message(s) {}
	    ~StrException() throw() {} 
	    const char* str() const throw() { return message.c_str(); }
	  private:
	    std::string message;
	};


/** \brief Factory
	*	This class provides methods for creating and destroying base any exported class objects of the Viulib (e.g. Detector, Tracker, Classification ...). 
	*	It is a singleton, and this single instance can be accessed by using the instance() method.
	*
   *  !Important!
   *  All classes supporting the creation Factory inherit from VObject class. Only pointer to Vobject is returned by the Factory.
   *  This has to be upcasted. Mooreover, all new classes must be registered in order to be used in the Factory. HELPER MACROS exist for new class registration. 
   *  The macro REGISTER_BASE_CLASS is used for all base classes defining the common interface. The user only works with these classes.
   *  This macro also defines a static function ::create(std::string), which can be directly used for new object creation, returning the type we want.
   *  <code>
   *     Classifier* clusteringAlgo = viulib::dtc::Classifier::create("ClustererKMeans");
   *     if (clusteringAlgo) ... // ok clusterer exists
   *
   *     Tracker* m_Tracker = viulib::dtc::Tracker::create("TrackerDAAF");
   *     if (mTracker) ... // ok tracker exists
   *     ... 
   *     if (clusteringAlgo) delete clusteringAlgo;
   *     if (m_Tracker) delete m_Tracker;
   *  </code>
   *  In addition, a static CLASS_NAME* create(const cv::FileNode &node) function is defined which allows to create and initialize the specific class form given XML/YML file node.
   *  <code>
   *   cv::FileStorage fs("nativeHisto_histo.yaml",  cv::FileStorage::READ);
   *   std::string n = (*fs.root().begin()).name();
   *   std::cout << "loading node: " << n << std::endl;
   *   FeatureExtractor* fe = FeatureExtractor::create( *fs.root().begin() );
   *   if (fe)
   *     std::cout << "loaded extractor of type: " << fe->getClassName() << std::endl;
   *   else   
   *     std::cout << "ERROR loading node '" << n <<"'. Creation of this type of object is not implemented." << std::endl;
   *  </code>
	*
   *  Other possible use of the creation Factory is as follows:
	*	<code>
	*	viulib::dtc::Detector *d = dynamic_cast<viulib::dtc::Detector*> viulib::Factory::instance()->createObject("DetectorXY");
	*	Factory::instance()->destroyObject( &d );
    *	</code>
	*   ... or a simplified version, for the exported base class Detector
	*   Note the template variable not to be of pointer type, although pointer is returned!
	*	<code>
	*	viulib::dtc::Detector *d = viulib::VObjectFactory<viulib::dtc::Detector>::create("DetectorXY");
	*	if (d) delete d;
	*   </code>
	*   ... or, if acces to headers of specialized class are available
	*   <code>
	*	viulib::dtc::DetectorXY *d_XY = viulib::VObjectFactory<viulib::dtc::DetectorXY>::create();
	*	if (d_XY) delete d_XY;
    *	</code>
	*	\ingroup viulib_core

*/

 class Factory
 {
 public:
	/** \brief	Destructor. */
	virtual ~Factory(){};
		
	static CORE_LIB Factory *instance();

	virtual void setLicense()=0;
	
	virtual void setLicense(std::string _license_file_name)=0;
	
	virtual bool registerClass(std::string className, VObject*(*constructor)()) = 0;

	virtual VObject* createObject(const std::string& type) = 0;

	virtual void unlock(int _number) = 0;

    virtual void destroyObject( VObject** obj ) = 0;

    virtual std::list<std::string> getClassList() const { std::list<std::string> l; return l;};
    virtual void coutAvailableFactoryClasses() const {};

 };

 	/** \brief This class is the template class for all the objects the factory can create.
   *    It defines the basic interface to approach each of the objects with functionality for getClassName, get/set parameters, load/save inner data to xml/yml.
	*	\ingroup viulib_core
	*/
	class CORE_LIB VObject 
	{
	public:
		VObject(){} // keep public due to virutal inheritance
		virtual ~VObject(){}

		virtual std::string getClassName() const
		{ return std::string("VObject");}

		/** \brief	Sets inner parameters by parameter name. Automatic type conversion to most used types and classes is provided by the ParamBase class.
      *           This method is to be reimplemented by each descendant. The access to the parameters from outside is done via method set(T value, const std::string& name)
      *
		*	\param value Value of the parameter
		*	\param name Name of the parameter		
		*/		
   protected:
	   virtual void setParam(const ParamBase& value, const std::string& name) = 0;  // TODO exception if not found or wrong type
   public:
      /// wrapper for the former setParam() method
      template <typename T>
		void set( T& value, const std::string& name) 
		{	
			Param_<T>  var(&value);
         		this->setParam(var, name);
		}
      /// wrapper for the former setParam() method
      template <typename T>
		void set(const T& value, const std::string& name) 
		{	
			 ConstParam_<T> var(&value);
        		 this->setParam(var, name);
		}
      /// wrapper for the former setParam() method
      template <typename T>
		void set(T* value, const std::string& name) 
		{	
			 Param_<T> var(value);
        		 this->setParam(var, name);
		}
      /// wrapper for the former setParam() method
      template <typename T>
		void set(const T* value, const std::string& name) 
		{	
			 ConstParam_<T> var(value);
        		 this->setParam(var, name);
		}
      		
      /// template specialization of the set(const T&, name) wrapper method
      void set(const ParamBase& value, const std::string& name) 
      {
      	this->setParam(value, name);
      }		
      /// template specialization of the set(T&, name) wrapper method
      void set(ParamBase& value, const std::string& name) 
      {
      	this->setParam(value, name);
      }		

		/** \brief	Gets inner parameters by parameter name. Automatic type conversion to most used types and classes is provided by the ParamBase class.
      *           This method is to be reimplemented by each descendant. The access to the parameters from outside is done via method get(const std::string& name), providing
      *           automatic return type conversion (if available).
		*
		*	\param name Name of the parameter
		*/		
   protected:
      virtual ParamBase getParam(const std::string& name) const = 0;
   public:
      /// wrapper for the former getParam() method
      ParamBase get(const std::string& name) const { return getParam(name); } // inlined function forwarding to keep naming standard

   protected:
		/** \brief	Gets direct access to inner parameters. No type checking/conversion is performed - the type must be known by the developper.
      *  This method performs faster than the general get(), nevetheless, is not fool-proof due to missing typecheck.
      *
      *  Note that by development needs this function may not give access to all parameters!
      * 
		*	\param param Void pointer to the return variable
		*	\param name Name of the parameter
		*/		
		virtual void getParam(void** param, const std::string& name) {} 
   public:
      /// wrapper for the former getParam() method
      void get(void** param, const std::string &name) {
         getParam(param, name);
      }

   public:

		/** \brief	Load object from XML/YAML file (the root node is used)
		*
		*	\param	path Path to the object's XML/YAML file
		*/
     static VObject* createFromXML(const std::string& path) 
     {
       //cv::FileStorage fs(path, cv::FileStorage::READ);
       cv::FileStorage fs;
       VObject* ret = NULL;
       try{
         fs.open(path, cv::FileStorage::READ);
         if (!fs.isOpened()){
           std::cout << "ERROR opening configuration file for loading: " << path << std::endl;
           return ret;
         }
         //get root node
         cv::FileNode fn = fs.getFirstTopLevelNode();
         ret = viulib::Factory::instance()->createObject(fn.name());
         if (ret){
            bool ok = ret->load(fn);
            if (!ok) {
               fs.release();
               if (ret) delete ret;
               return NULL;
            }
         }
       }
       catch(...){
         std::cout << "ERROR problem loading configuration: " << path << std::endl;
         fs.release();
         return ret;
       }
       fs.release();
       return ret;
     }

		/** \brief	Load an XML/YAML file with object parameters
		*
		*	\param	path Path to the object's XML/YAML file
		*/
     bool load(const std::string& path) 
     {
       //cv::FileStorage fs(path, cv::FileStorage::READ);
       cv::FileStorage fs;
       bool ret = true;
       try{
         fs.open(path, cv::FileStorage::READ);
         if (!fs.isOpened()){
           std::cout << "ERROR opening configuration file for loading: " << path << std::endl;
           return false;
         }
         //try{
         ret = load(fs[this->getClassName()]);
       }
       catch(...){
         std::cout << "ERROR problem loading configuration: " << path << std::endl;
         fs.release();
         return false;
       }
       fs.release();
       return ret;
     }

	   /** \brief	Load object's parameters from given XML/YAML 
      *
      *	\param	_fs FileStorage's (XML/YAML) node holding the object's info to be loaded.
      *           This function calls loadInnerData for this node, if it is a valid node (i.e. node.node != NULL).
      *           The name of the node is not checked, since possibly a parent object can be initialized from the info of a derived one.
      *  
      *  \return true on success; false if error occurs or the method was not reimplemented for given object.
      */
		bool load(const cv::FileNode &node) {          
         if (node.node == NULL) //node.isNone() gives true for empty nodes (and some classes may be empty)
            return false;
         return this->loadInnerData(node); 
      } // will return false for objects which do not reimplement this method // TODO throw exception


		/** \brief	Save the object to a XML/YAML file
		*
		*	\param	path Path to the output XML/YAML file
		*/
		bool save(const std::string& path) const
		{
			cv::FileStorage fs;
         bool ret = true;
         try{
            fs.open(path, cv::FileStorage::WRITE);
			   if (!fs.isOpened()){
				   std::cout << "ERROR opening file for saving configuration: " << path << std::endl;
				   return false;
			   }
				ret = save(fs);
			}
			catch(...){
				std::cout << "ERROR problem saving configuration: " << path << std::endl;
				fs.release();
				return false;
			}
			fs.release();
			return ret;
		}

      /** \brief	Save the object to XML/YAML node
      *
      *	\param	fsPart The output file Storage, at the state at which the object's data should be saved (= object's data node).
      *           This function creats the node for this class (name given by getClassName()) and calls saveInnerData for this node.
      *
      *  \return true on success; false if error occurs or the method was not reimplemented for given object.
      */
      bool save(cv::FileStorage &fsPart) const // TODO throw exception
      { 
         /*
         //fsPart << "VObjectType" <<  this->getClassName();
         fsPart <<  this->getClassName() << "{";
         bool ret = this->saveInnerData(fsPart);
         fsPart <<"}";
         return ret; 
         */
         return saveWrapper(fsPart);
      } 
    protected:
      virtual bool saveWrapper(cv::FileStorage &fsPart) const // TODO throw exception
      { 
         //fsPart << "VObjectType" <<  this->getClassName();
         fsPart <<  this->getClassName() << "{";
         bool ret = this->saveInnerData(fsPart);
         fsPart <<"}";
         return ret; 
      } 

   protected:
	   /** \brief	Load object's parameters from given XML/YAML 
      *
      *	\param	_fs FileStorage's (XML/YAML) node holding the object's info to be loaded.
      *  
      *  \return true on success; false if error occurs or the method was not reimplemented for given object.
      */
		virtual bool loadInnerData(const cv::FileNode &node) { return false; } // will return false for objects which do not reimplement this method // TODO throw exception

      /** \brief	Save the object to XML/YAML node
      *
      *	\param	fsPart The output file Storage, at the state at which the object's data should be saved (~like object's data node).
      *
      *  \return true on success; false if error occurs or the method was not reimplemented for given object.
      */
      virtual bool saveInnerData(cv::FileStorage &fsPart) const { return false; } // not implemented
	};
// explicit specialisations 
template <> inline
void VObject::set <char> (const char* value, const std::string& name) 
{	
	 TempParam_<std::string> var(value);
       	 this->setParam(var, name);
}
 

  /** \brief RegisterFactory
	*	generic factory used to register new VObject classes
	*	- it has to be defined for each new VObject class;
	*   - the helper macros REGISTER_DTC_TYPE_DECLARATION and REGISTER_DTC_TYPE_DEFINITION
	*	provide the necessary definition and implementation code to be added
	*	</code>
	*	\ingroup viulib_core
*/
  template<typename T>
  class RegisterFactoryClass {
      // generic function definition, constructing new VObject 
      static VObject* createVObject() { return new T(); }

    public:
	  // constructor registers the new class T within the Factory::instance singleton under the name "fullClassName"
      // - the full calss name should be "namespace.T", according to internal viulib regulations
      RegisterFactoryClass(std::string const& className) {
          //printf("*** Registering: %s ***\n", className.c_str());
          //Factory::instance()->registerClass(typeid( T ).name(), alias, &createVObject);
        bool registered = Factory::instance()->registerClass(T::getModuleName()+"."+className, &createVObject);
      }
      
      bool exists(){ return true; }
  };
 
 
 //-- MACROS for object registration --//
  // to put in the declaration of each general  class (.h)
 // this macro is used to get and register each object with the corresponing namespace
 #define REGISTER_GENERAL_CLASS(CLASS_NAME,MODULENAME) \
  private: \
    static RegisterFactoryClass<CLASS_NAME> reg; \
	friend class viulib::RegisterFactoryClass<CLASS_NAME>; \
  public: \
    static std::string getModuleName() { return std::string(#MODULENAME);} \
    virtual std::string getClassName() const	{ return std::string(#CLASS_NAME);} \
    static CLASS_NAME* createFromXML(std::string str) { \
           VObject* obj = VObject::createFromXML( str ); \
           if (obj == NULL) { \
                  printf("Unknown class of VObject in XML: %s. \n", str.c_str() ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
                  return NULL; \
           } \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( obj ); \
           if (out == NULL) { \
                  printf("XML file of incompatible class: %s. [expected: %s] \n", obj->getClassName().c_str(), #CLASS_NAME ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
    static CLASS_NAME* create(std::string str) { \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( Factory::instance()->createObject( str ) ); \
           if (out == NULL) { \
                  printf("Unknown class of VObject: %s. \n", str.c_str() ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
	static CLASS_NAME* create() { \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( Factory::instance()->createObject( #CLASS_NAME ) ); \
           if (out == NULL) { \
                  printf("Unknown class: %s. \n", #CLASS_NAME ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
    static CLASS_NAME* create(const cv::FileNode &node) \
    { \
         if (node.node == NULL) \
            return NULL; \
         CLASS_NAME* out = create(node.name()); \
         if (out && out->load(node)) \
            return out; \
         return NULL; \
    }\
	static void static_register() { \
		  CLASS_NAME::reg =  viulib::RegisterFactoryClass<CLASS_NAME>(#CLASS_NAME);}; \
	ParamBase getParam(const std::string& name) const {return viulib::NullParam();}; \
	void setParam(const ParamBase& value, const std::string& name) {}; 


 // to put in the declaration of each exported BASE class (.h)
 // this macro is used to get and register each object with the corresponing namespace
 #define REGISTER_BASE_CLASS(CLASS_NAME,MODULENAME) \
  public: \
    static std::string getModuleName() { return std::string(#MODULENAME);} \
    virtual std::string getClassName() const	{ return std::string(#CLASS_NAME);} \
    static CLASS_NAME* createFromXML(std::string str) { \
           VObject* obj = VObject::createFromXML( str ); \
           if (obj == NULL) { \
                  printf("Unknown class of VObject in XML: %s. \n", str.c_str() ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
                  return NULL; \
           } \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( obj ); \
           if (out == NULL) { \
                  printf("XML file of incompatible class: %s. [expected: %s] \n", obj->getClassName().c_str(), #CLASS_NAME ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
    static CLASS_NAME* create(std::string str) { \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( Factory::instance()->createObject( str ) ); \
           if (out == NULL) { \
                  printf("Unknown class of VObject: %s. \n", str.c_str() ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
	static CLASS_NAME* create() { \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( Factory::instance()->createObject( #CLASS_NAME ) ); \
           if (out == NULL) { \
                  printf("Unknown class: %s. \n", #CLASS_NAME ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
	static CLASS_NAME* create(const cv::FileNode &node) \
    { \
         if (node.node == NULL) \
            return NULL; \
         CLASS_NAME* out = create(node.name()); \
         if (out && out->load(node)) \
            return out; \
         return NULL; \
    }
	


 // to put in .h for all new VObject classes (within each new class as it defines private class member)

  #define REGISTER_CLASS_DECLARATION(CLASS_NAME) \
  private: \
    static RegisterFactoryClass<CLASS_NAME> reg; \
  public:  \
    static CLASS_NAME* create_() { return new CLASS_NAME(); } \
    static CLASS_NAME* create() { \
           CLASS_NAME* out = dynamic_cast<CLASS_NAME*> ( Factory::instance()->createObject( #CLASS_NAME ) ); \
           if (out == NULL) { \
                  printf("Unknown class: %s. \n", #CLASS_NAME ); \
                  Factory::instance()->coutAvailableFactoryClasses(); \
           } \
           return out; \
    } \
    virtual std::string getClassName() const { return std::string(#CLASS_NAME);} \
	static void static_register() { \
		  CLASS_NAME::reg =  viulib::RegisterFactoryClass<CLASS_NAME>(#CLASS_NAME);};

	
    
  // to put in .cpp for all new VObject types
    #define REGISTER_CLASS_DEFINITION(CLASS_NAME) \
        viulib::RegisterFactoryClass<CLASS_NAME> CLASS_NAME::reg(#CLASS_NAME);

    #define REGISTER_CLASS_DEFINITION_AS_BASE_CLASS(CLASS_NAME,BASE_CLASS_NAME) \
        viulib::RegisterFactoryClass<CLASS_NAME> CLASS_NAME::reg(#BASE_CLASS_NAME);

  /* obsolete .. the typeid( T ).name() is not used for class registration

   /* \brief VObjectFactory
	*	Generic factory to obtain a concrete type of given DTC object, instead of the base class
	*	</code>
	*     use:
    *        VObjectFactory<ClassifierAdaBoost>::create();
*/ /* 
  template< class T >
  class VObjectFactory:public Factory
  {
  public:
    //static typename T* create()
    static T* create()
    { 
      T* ret = dynamic_cast< T * >( Factory::instance()->createObject( typeid( T ).name() ) ); // TODO demangle object name !!!
      if (ret == NULL){
        printf("Unknown class of VObject: %s. \n", typeid( T ).name());
        Factory::instance()->coutAvailableFactoryClasses();
      }      
      return ret;
    }
    //static typename T* create( std::string className )
    static  T* create( std::string className )
    { 
      T* ret = dynamic_cast< T * >( Factory::instance()->createObject( className ) );
      if (ret == NULL){
     		printf("Unknown class of VObject: %s. \n", className.c_str() );		
        Factory::instance()->coutAvailableFactoryClasses();
      }      
      return ret;
    }
  };
  */
 
 } 

#endif
