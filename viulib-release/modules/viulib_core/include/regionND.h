/** 
* viulib_core (Core) v13.10
* 
* Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
* (Spain) all rights reserved.
*
* viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
* Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
* (Spain) all rights reserved.
* 
* As viulib_core depends on other libraries, the user must adhere to and keep in place any 
* licencing terms of those libraries:
*
* * OpenCV v2.4.6 (http://opencv.org/)
* 
* License Agreement for OpenCV
* -------------------------------------------------------------------------------------
* Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
* Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
* Third party copyrights are property of their respective owners. 
* 
* BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
*
* The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
*
*/

// author: pleskovsky@vicomtech.org

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef REGION_ND_H_
#define REGION_ND_H_
#include <opencv2/core/core.hpp>

#include "extended_mat.h"

namespace viulib
{
      /* \brief Representation of N dimensional region.
      * Each information corresponding to specific dimension is stored in single matrix column.
      * At each matrix row are properties of the region. This could be e.g. Row1 = origin, Row2 = size, Row3 = velocity, Row4 = accelerations, ...
      * row(0): x, y ... row(1): width, height  ... row(2) etc.
      * default data type 'signed int' = CV_32SC1
      * In addition, characteristic scale, mostly used for point specifications, is stored in public variable 'scale'.
	*	\ingroup viulib_core
	*/      
  class RegionND : public ExtendedMat
  { 
  public:
    // \brief Default constructor with default data type 'signed int = CV_32SC1'
    RegionND( t_data_type dt = data_type(CV_32SC1)): ExtendedMat(dt), scale(1.0f),sampling_id(0) {}
    //! \brief Constructor specifying length (rows = depth = 2, cols = length = dimension) the data depth (rows)and the inner data type 
    RegionND(size_t dimension, int depth = 2, t_data_type dt = data_type(CV_32SC1)): ExtendedMat( cv::Mat::zeros(depth, int(dimension), dt.type) ), scale(1.0f),sampling_id(0) {}
    //! \brief Copy constructor; no data conversion is available;
    RegionND(const RegionND& rg): ExtendedMat(rg.clone()), scale(1.0f),sampling_id(0) {}
    //! \brief Decriptor copy constructor; no data conversion is available;
    RegionND(const ExtendedMat& em): ExtendedMat(em.clone()), scale(1.0f),sampling_id(0) {}

    //! \brief Matrix copy constructor; no data conversion is available;
    RegionND(const cv::Mat& m): ExtendedMat(m.clone()), scale(1.0f),sampling_id(0) {}
    //! \brief Rect copy constructor; no data conversion is available;
    template <typename Tp>
    RegionND(const cv::Rect_<Tp>& r): 
      ExtendedMat( (cv::Mat_<Tp>(2,2) << r.x, r.y, r.width, r.height) ), scale(1.0f),sampling_id(0) {}
    RegionND(const cv::Rect& r): 
      ExtendedMat( (cv::Mat_<int>(2,2) << r.x, r.y, r.width, r.height) ), scale(1.0f),sampling_id(0) {}

    //! \brief cv::Vec constructor
    template <typename Tp, int n>
    RegionND(const cv::Vec<Tp, n>& origin, const cv::Vec<Tp, n>& sz): 
      ExtendedMat( cv::Mat_<Tp>(2,n) ), scale(1.0f),sampling_id(0)
    { 
      this->row(0) = cv::Mat(origin).t(); 
      this->row(1) = cv::Mat(sz).t(); 
    }
    //! \brief cv::Vec size constructor; sets origin to 0
    template <typename Tp, int n>
    RegionND(const cv::Vec<Tp, n>& sz): 
      ExtendedMat( cv::Mat_<Tp>::zeros(2,n) ) , scale(1.0f),sampling_id(0)
    { 
      this->row(1) = cv::Mat(sz).t(); 
    }
    //! \brief Point, Size constructor
    template <typename Tp>
    RegionND(const cv::Point_<Tp>& origin, const cv::Size_<Tp>& sz): 
      ExtendedMat( (cv::Mat_<Tp>(2,2) << origin.x, origin.y, sz.width, sz.height) ), scale(1.0f),sampling_id(0) {}

    //! \brief Size constructor; sets origin to 0;
    template <typename Tp>
    RegionND( const cv::Size_<Tp>& sz): 
      ExtendedMat( (cv::Mat_<Tp>(2,2) << 0, 0, sz.width, sz.height) ), scale(1.0f),sampling_id(0) {}

    //! \brief Center point, characteristic scale constructor (region of one pixel size 1,1)
    template <typename Tp>
    RegionND(const cv::Point_<Tp>& center, float _scale): 
      ExtendedMat( (cv::Mat_<Tp>(2,2) << center.x, center.y, 1, 1) ), scale(_scale),sampling_id(0) {}

    //! \brief Center point, scale 1 size, characteristic scale constructor (region of size 0,0)
    template <typename Tp>
    RegionND(const cv::Point_<Tp>& center, const cv::Size_<Tp>& sz, float _scale): 
      ExtendedMat( (cv::Mat_<Tp>(2,2) << center.x, center.y, sz.width, sz.height) ), scale(_scale),sampling_id(0) {}

    //! \brief Constructor creating ExtendedMat object from array, given its size. 
    //!   Type conversion is available, by default original data type of the array is kept.
    template <typename Tp>
    RegionND(const Tp* arr, size_t arr_size, size_t dimension = 0, t_data_type dt = cv::DataType<Tp>::type): 
      ExtendedMat(arr, arr_size, true, dt), scale(1.0f),sampling_id(0) { 
        if (dimension == 0) dimension = arr_size/2;
        this->reshape(0, arr_size / dimension); 	      
    }

    //! \brief Constructor creating ExtendedMat object from std::vector. 
    //!   Type conversion is available, by default original data type of the vector is kept.
    template <typename Tp>
    RegionND(const typename std::vector<Tp>& vec, size_t dimension = 0, t_data_type dt = cv::DataType<Tp>::type): 
      ExtendedMat( vec, true, dt), scale(1.0f),sampling_id(0) { 
        if (dimension == 0) dimension = vec.size()/2;
        this->reshape(0, vec.size() / dimension); 
    }

    ~RegionND(){}

    //! Assignment operator with type conversion
    template <typename Tp>
    RegionND& operator = (const cv::Rect_<Tp>& r) 
    { 
      this->copy(RegionND(r)); 
      this->scale = 1.0;
      return *this; 
    }

    // TODO - good idea to convert ?
    //! Assignment operator with type conversion; Rounding is applied for 'real' to 'integer' types!
    RegionND& operator = (const RegionND& region) 
    { 
      this->copy(region); 
      this->scale = region.scale;
      return *this;        
    }

    //! return concatenated in one column vector
    ExtendedMat asVector()
    {
      return this->reshape(0, int(this->total()) );
    }

    //! conversion to 2D Rect.
    operator cv::Rect() const {
      if ((rows < 2) || (cols < 2)) 
        return cv::Rect(); 
      //return cv::Rect( this->get(0,0), this->get(0,1), this->get(1,0), this->get(1,1));
      return cv::Rect( this->get_<int>(0,0), this->get_<int>(0,1), this->get_<int>(1,0), this->get_<int>(1,1)); // faster
    }

    size_t dimension() const { return this->cols; }
    size_t depth() const { return this->rows; }

    viulib::ExtendedMat getCenter() const 
    {                
      if (this->rows < 2) 
        return this->row(0);

      return cv::Mat( this->row(0) + 0.5 * this->row(1) );
    }
    viulib::ExtendedMat getOrigin() const { return this->row(0); }
    viulib::ExtendedMat getSize() const { return this->row(1); }
    void setOrigin(const cv::Mat& mat)  // expected matrix of 1 row
    { 
      if (mat.cols < this->cols)  // zero the rest
        this->row(0) = cv::Mat::zeros(1, this->cols, this->type());
      if (mat.cols > this->cols) // take the first #dim values
        //this->row(0) = mat.row(0).colRange(0, this->cols);
          mat.row(0).colRange(0, this->cols).copyTo(this->row(0));
      else
        mat.row(0).copyTo(this->colRange(0,mat.cols).row(0));
    } 
    void setSize(const cv::Mat& mat) // expected matrix of 1 row
    {
      if (mat.cols < this->cols)  // zero the rest
        this->row(1) = cv::Mat::zeros(1, this->cols, this->type());
      if (mat.cols > this->cols) // take the first #dim values
        //this->row(1) = mat.row(0).colRange(0, this->cols);
          mat.row(0).colRange(0, this->cols).copyTo(this->row(1));
      else
        mat.row(0).copyTo(this->colRange(0,mat.cols).row(1));
    }

    // TODO translate, area, intersection, inside, contour -> see rect.h
  public:
    float scale;
	int sampling_id;
  };


}
#endif //REGION_ND_H_