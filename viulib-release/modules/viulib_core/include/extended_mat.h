/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

// author: pleskovsky@vicomtech.org

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef EXT_MAT_H_
#define EXT_MAT_H_
#include <opencv2/core/core.hpp>
#include <cassert>

namespace viulib
{
	// author: pleskovsky@vicomtech.org
	/** \brief Structure encapsulating the CV_ data types.
	*   This class is needed to allow for distinction between int and data_type variable types.
	*	\ingroup viulib_core
	*/
	typedef struct data_type
	{
		data_type(int _type = CV_32FC1): type(_type) {}
		int type;
	}t_data_type;

	//! \brief Class encapsulating the ExtendedMat vector(s) in cv::Mat structure (per row), with CV_32FC1 (float) format by default.
	//!
	//! This class is defined to ease the management of descriptors (i.e. mostly vectors of floats), by allowing for vertical 
   //! push_back() and horizontal concatenate() data insertion.
   //! It also allows for correct type casting when acessing separate matrix fields.
	//!
	//! - Constructors:
	//!   Functionality to construct a ExtendedMat from a single float or double, an array[], std::vector<...> or cv::Mat are supplied.
	//!   A construction of a ExtendedMat of a desired length (1 row only) is also given.
	//!
	//!   By default the ExtendedMat class is maintaining the data as floats (CV_32FC1) and thus, unless specified
	//!   explicitely by the constructor the data will be converted to this type, if necessary.
	//!
	//!   It is possible to hold only the pointer to the data, when constructing new ExtendedMat from array or std::vector.
	//!   Nevertheless, if data type is different from the specified one (float by default), new memory should be allocated 
	//!   and the data should be copied. Therefore if the flag copyData == false an assertion fault will be raised.
	//!
	//!   The copy constructors from ExtendedMat and cv::Mat classes do not copy the data and do not perform data conversion.
	//!   Use the function ExtendedMat::copy(..) to copy the data (forcing data conversion if necessary) and 
	//!   ExtendedMat::convert(..) if only data conversion inside is wanted (the inner data will be copied if new type differs from thr intern one). 
	//!   E.g. des.copy(des1); des.convert(cv::DataType<double>::type); 
	//!   Or use the cv::Mat functionality for conversion: des.convertTo(des,cv::DataType<double>::type);
	//!
	//! - Reserve / allocate data:
	//!   Use functions ExtendedMat::create(rows, cols) to anticipate the data allocation for the ExtendedMats.
	//!   If the length of the ExtendedMat is known, i.e. from the constructor or some data has been inserted already,
	//!   the function cv::Mat::resize( rows ) can be used. Note that the function cv::Mat::reserve( rows ), although similar,
	//!   it only allocates the memory, but does not update the matrix header to allow the access to more rows.
	//!  
	//! - Data access:
	//!   To get the pointer to the data, one can use the 'uchar* cv::Mat::data' variable ( e.g.: float* my_data = (float*)des.data; )
	//!   To access each separate ExtendedMat use des.row(int i) or des.row(int i).data for direct data access.
	//!  
	//!   When updating a specific row, the syntax ExtendedMat(...).copyTo(des.row(i)); should be used. 
	//!   A simplified function call is provided via ExtendedMat::set(i, ExtendedMat). The second parameter can be any of 
	//!   scalar, array, vector, mat, ExtendedMat; supposed they are of the corresponding length. Conversion to this->type will be
	//!   performed automatically.
	//! 
	//!   Const acces (read only) to single values is possible via ExtendedMat::get<type>(i,j) template.
	//!
	//!   For full access to single values, it should be done via the cv::Mat::at<type>(i,j) template function (e.g. des.at<float>(des_nr, col) = 5.0).
	//!   In this call, the programmer is responsible for specifying the correct type.
	//! 
	//! 
	//! - push_back(...):
	//!   Pushes data on new row in the matrix. This way more ExtendedMats of the same type can be added to form a set of ExtendedMats.
	//!   Note that the inserted data must be of the same length (= number of columns), oterwise the program will crash.
	//!
	//!   Overloads for scalar, std::vector and ExtendedMat are available.
	//!   The data will be copied and converted appropriately.
	//!   Inner data reallocation may occur if necessary.
	//!
	//! - concat(...):
	//!   Appends (concatenates) data horizontally, to augment the existing ExtendedMats.
	//!
	//!   Overloads for scalar, std::vector and ExtendedMat are available.
	//!   Concatenation of scalars and std::vectors is only possible for ExtendedMats with rows <= 0, assertion fault will be generated otherwise.
	//!   Concatenation of ExtendedMats is only possible for ExtendedMats with equal number of rows, assertion fault will be generated otherwise.
	//!   Concatenation to empty ExtendedMat invokes ExtendedMat::push_back(..) function.
	//!   Inner data reallocation may occur if necessary.
	//!
	// author: pleskovsky@vicomtech.org
	//	\ingroup viulib_core
	class ExtendedMat: public cv::Mat // TODO rename to ExtendedMatSet as it represents one real ExtendedMat per row
	{
	public:
		// ---------------  constructors
		//! \brief Constructor specifying inner data type 
		//! - CV_32FC1 (float, single channel) used by default
		//!
		//! examples: 
		//!    - ExtendedMat dacd(t_data_type(CV_64FC1)); - creates double
		//!    - ExtendedMat dacd(t_data_type(CV_MAKETYPE(CV_64F, 1))); - creates double
		//!    - ExtendedMat dacd(t_data_type(CV_MAKETYPE(6, 1))); - CV_64F = 6 ... double
		//!    - ExtendedMat dacd( (t_data_type(cv::DataType<double>::type)) );
		//!       ATTENTION remember to use this convention, otherwise ExtendedMat of size cv::DataType<double>::type = 6 will be created.
		//!       Note the extra parenthesis around t_data_type -> it is necessary to avoid MSVS C2751 compiler error.
		//! 
		ExtendedMat( t_data_type dt = data_type(CV_32FC1)): cv::Mat() 
		{
			flags = (CV_MAT_TYPE(dt.type) & CV_MAT_TYPE_MASK) | MAGIC_VAL;
		}
		//! \brief Constructor specifying length the data and the inner data type 
		//! - memory space of defined length is reserved for the data storage
		//! - CV_32FC1 (float, single channel) used by default
		//! 
		ExtendedMat(int ExtendedMat_length, t_data_type dt = data_type(CV_32FC1)): cv::Mat(1, ExtendedMat_length, dt.type) {} // rows = 1, cols = size

		//! \brief Copy constructor; no data conversion is available;
		//! 
		ExtendedMat(const ExtendedMat& des): cv::Mat(des) {}
		//! \brief Matrix 'Copy' constructor; no data conversion is available;
		//! 
		ExtendedMat(const cv::Mat& m): cv::Mat(m) {}

		//! \brief Constructors creating ExtendedMat object from float or double scalar 
		//! - the inner data type can be specified; CV_32FC1 (float, single channel) used by default
		//! 
		// ATTENTION - problem confusing with other functions if scalar type = int; only allow float and double
		// template <typename Tp> 
		// ExtendedMat(const Tp scalar, t_data_type dt = data_type(CV_32FC1)) 
		ExtendedMat(float scalar, t_data_type dt = data_type(CV_32FC1))// always copies data
		{
			flags = (CV_MAT_TYPE(dt.type) & CV_MAT_TYPE_MASK) | MAGIC_VAL;
			Mat(1,1, cv::DataType<float>::type, (uchar*)&scalar).convertTo(*this,dt.type);
		}

		//! \brief Constructors creating ExtendedMat object from float or double scalar 
		//! - the inner data type can be specified; CV_32FC1 (float, single channel) used by default
		//! - ATTENTION, the double is converted to float if the data type has not been set explicitely
		//! 
		ExtendedMat(double scalar, t_data_type dt = data_type(CV_32FC1))// always copies data 
		{
			flags = (CV_MAT_TYPE(dt.type) & CV_MAT_TYPE_MASK) | MAGIC_VAL;
			Mat(1,1, cv::DataType<double>::type, (uchar*)&scalar).convertTo(*this,dt.type);
		}

		//! \brief Constructor creating ExtendedMat object from array, given its size
		//! - the inner data type can be specified; CV_32FC1 (float, single channel) used by default
		//! - if inner data type != array's data type, the data MUST be copied; assertion fault is thrown otherwise
		//! - the data is organized in row 
		//! 
		template <typename Tp>
		ExtendedMat(const Tp* arr, size_t arr_size, bool copyData = true, t_data_type dt = data_type(CV_32FC1))
		{
			flags = (CV_MAT_TYPE(dt.type) & CV_MAT_TYPE_MASK) | MAGIC_VAL;

			if (cv::DataType<Tp>::type != dt.type)
			{
				//assert(copyData); // in this case the data must be copied (and converted)
            if (!copyData) // TODO EXCEPTION
               std::cout << " WARNING, the data must be copied due to noncompatible type " << std::endl;
				Mat(1, (int)arr_size, cv::DataType<Tp>::type, (uchar*)arr).convertTo(*this,dt.type);
			}
			else if( !copyData )
			{
				//Mat(1,(int)arr_size, cv::DataType<Tp>::type, (uchar*)arr).assignTo(*this); // .. not enough to change the type
				(*this) = Mat(1, (int)arr_size, cv::DataType<Tp>::type, (uchar*)arr);// .. not enough to change the type
			}
			else
				Mat(1, (int)arr_size, cv::DataType<Tp>::type, (uchar*)arr).copyTo(*this);
		}

		//! \brief Constructor creating ExtendedMat object from std::vector
		//! - the inner data type can be specified; CV_32FC1 (float, single channel) is used by default
		//! - if inner data type != array's data type, the data MUST be copied; assertion fault is thrown otherwise
		//! - the data is organized in row (ATT default by cv::Mat is to organize it in column)
		//! 
		template <typename Tp>
		ExtendedMat(const std::vector<Tp>& vec, bool copyData = true, t_data_type dt = data_type(CV_32FC1))
		{
			flags = (CV_MAT_TYPE(dt.type) & CV_MAT_TYPE_MASK) | MAGIC_VAL;

			if(vec.empty())
				return;
			if (cv::DataType<Tp>::type != dt.type)
			{
				//assert(copyData); // in this case the data must be copied (and converted)
            if (!copyData) // TODO EXCEPTION
               std::cout << " WARNING, the data must be copied due to noncompatible type " << std::endl;
				Mat(1,(int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]).convertTo(*this,dt.type);
			}
			else if( !copyData )
			{
				(*this) = Mat(1, (int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]);
			}
			else
				Mat(1, (int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]).copyTo(*this);
		}

      //!  \brief Construct one multiple lines ExtendedMat from std::vector <ExtendedMat> 
      ExtendedMat(const std::vector<ExtendedMat>& ExtendedMats)
      {
         if (ExtendedMats.empty()) return;            
         flags = (CV_MAT_TYPE(ExtendedMats[0].type()) & CV_MAT_TYPE_MASK) | MAGIC_VAL;

         size_t sz = 0;
         for (std::vector<ExtendedMat>::const_iterator it = ExtendedMats.begin(); it != ExtendedMats.end(); ++it)
            sz += it->rows;

         std::vector<ExtendedMat>::const_iterator it = ExtendedMats.begin();
         this->push_back( *it );

         this->reserve(sz);
         
         for (++it; it != ExtendedMats.end(); ++it)
            this->push_back( *it );
      }

      /*
      //! \brief Returns one multiple lines ExtendedMat from std::vector <ExtendedMat>; Releases the ExtendedMat's data in the input std::vector<>
      // pointless; have to heva enough memory available to reserve it - deprecated
      ExtendedMat& join(std::vector<ExtendedMat>& ExtendedMats)
      {
         if (ExtendedMats.empty()) return ExtendedMat();            
         flags = (CV_MAT_TYPE(ExtendedMats[0].type()) & CV_MAT_TYPE_MASK) | MAGIC_VAL;

         size_t sz = 0;
         for (std::vector<ExtendedMat>::const_iterator it = ExtendedMats.begin(); it != ExtendedMats.end(); ++it)
            sz += it->rows;

         std::vector<ExtendedMat>::iterator it = ExtendedMats.begin();
         this->push_back( *it );
         it->release();

         this->reserve(sz);
         
         for (++it; it != ExtendedMats.end(); ++it){
            this->push_back( *it );
            it->release();
         }
      }*/
      
      // --------------- create 
		//! \brief Creates ExtendedMat(cv::Mat) of scpecific size; allocates necessary memory.
		//! Note that function resize(rows=x) exists in cv::Mat, which allocates memory to fit 'x' elements of the current element length (=this->cols)
		//! 
		void create(int rows, int cols)
		{
			((cv::Mat*)this)->create(rows, cols, this->type());
		}
		//void create(int rows) { ((cv::Mat*)this)->resize(rows); }

		// --------------- copy for data type conversion and copy of cv::Mat and ExtendedMat
		//! \brief ExtendedMat copy with inner data copy and conversion
		//! ATTENTION does rounding! 1.5 float -> 2 int (uses cv::Mat::convertTo(...))
		ExtendedMat& copy(const ExtendedMat& des)// TODO channel conversion?
		{
			if (des.type() != this->type()){
				//  the call to convertTo does not take care of the number of channels
				//  correct the channel number
				des.reshape(this->channels(),0).convertTo(*this,this->type());
			}
			else 
				des.copyTo(*this);
			return *this;
		}
		//! \brief Matrix 'Copy' with inner data copy and conversion
		//! ATTENTION does rounding! 1.5 float -> 2 int (uses cv::Mat::convertTo(...))
		ExtendedMat& copy(const cv::Mat& m) //: cv::Mat(m) // TODO channel conversion?
		{
			if (m.type() != this->type()){
				//  the call to convertTo does not take care of the number of channels
				//  correct the channel number
				m.reshape(this->channels(),0).convertTo(*this,this->type());
			}
			else 
				m.copyTo(*this);
			return *this;
		}
		//! \brief MatExpr 'Copy' with inner data copy and conversion
		//! 
		ExtendedMat& copy(const cv::MatExpr& me) 
		{
			return this->copy(cv::Mat(me));
		}

		// --------------- hard data copy with no data type conversion 
		//! \brief ExtendedMat clone creation with hard inner data copy and NO conversion
      //! reimplementation of cv::Mat::clone()
		ExtendedMat clone() const 
      {
         return cv::Mat::clone();
      }
      //
		//! \brief ExtendedMat copy with hard inner data copy and NO conversion
		//! 
		ExtendedMat& clone(const ExtendedMat& des)// TODO channel conversion?
		{
			des.copyTo(*this);
			return *this;
		}
		//! \brief Matrix 'Copy' with hard inner data copy and NO conversion
		//! 
		ExtendedMat& clone(const cv::Mat& m) //: cv::Mat(m) // TODO channel conversion?
		{
			m.copyTo(*this);
			return *this;
		}

		//! \brief MatrixExpr 'Copy' with hard inner data copy and NO conversion
		//! 
		ExtendedMat& clone(const cv::MatExpr& me) //: cv::Mat(m) // TODO channel conversion?
		{
			return this->clone(cv::Mat(me));
		}

		//! \brief Converts the 2D matrix cv::Mat to one row and creates a corresponding ExtendedMat object (with data type conversion)
		//! By default the Matrix's type (i.e. channel number and depth) remains, only the dimensionality will be reduced to 1 row.
		//!
		//! Note: this function also changes the number of component to the destination one. Thus, the pixels of RGB images
		//! can be converted to separate B,G,R values (stored consecutively) when 1 channel use is specified (e.g. CV_32FC1, CV_32UC1);
		//!
		static ExtendedMat fromMat(const cv::Mat& m, t_data_type dt = t_data_type(-1))
		{
			if (dt.type < 0) return ExtendedMat( m.reshape(0,1) );

			ExtendedMat out(dt);// does type conversion first, if necessary
			out.copy(m);
			out = out.reshape(CV_MAT_CN(dt.type), 1);// reshape to one row and desired number of components .. should only change the header
			return out;
		}

		//! \brief Converts the data type with inner data copy if necessary (if dt.type differs from intern type)
		//! 
		ExtendedMat& convert(t_data_type dt)
		{
			if (CV_MAT_CN(dt.type) != this->channels()) 
				*this = this->reshape(CV_MAT_CN(dt.type),0);

			this->convertTo(*this,dt.type);
			return *this;
		}

		//! \brief Reshapes the mat to specific width; No error checking is applied (e.g. if the matrix size is not divisible by the specified width).
		//! Keeps number of channels intact.
		ExtendedMat& reshapeToWidth(int width)
		{
         (*this) = this->reshape(0, (this->rows * this->cols) / width);
         return *this;
		}
		//! \brief Reshapes the mat to specific height; No error checking is applied (e.g. if the matrix size is not divisible by the specified height).
		//! Keeps number of channels intact.
		ExtendedMat& reshapeToHeight(int height)
		{
         (*this) = this->reshape(0, height);
         return *this;
		}

      
  		//! \brief Set specific row to new ExtendedMat data
		//! 
		//! Should the input ExtendedMat des have more rows, all will be copied in consequent rows.
		//! 
		ExtendedMat& setRow(int row, const ExtendedMat& des) 
		{
         if (this->rows == 0)
            return this->copy(des);
          if (this->rows < row + des.rows) 
            this->resize( row + des.rows );
         des.copyTo(this->rowRange(row,row+des.rows)); 
         return *this;
		}

      public:
		//! \brief Gets the feature at row, col while performing correct type conversion.
		//! Note that the automatic type conversion can produce lost of precision or possible type overflow!
		//! The function "at" can reach out of memory as the type consistency is not checked automatically.
		//! 
      //! Template T is on return type and not the inner ExtendedMat type as by cv::Mat.at<> or cv::Mat.ptr<>
		template <typename T> 
		T get_(int row, int col) const // row - ExtendedMat order, col - feature order
		{
         // the ptr implementation moves correctly over the data (using uchar* and element size)
			switch (this->depth()){
		      case CV_8U://   0 unsigned char - bool
			      return static_cast<T>( *this->ptr<unsigned char>(row,col) );
		      case CV_8S://   1 char
			      return static_cast<T>( *this->ptr<signed char>(row,col) );
		      case CV_16U://  2 unsigned short
			      return static_cast<T>( *this->ptr<unsigned short>(row,col) );
		      case CV_16S://  3 short
			      return static_cast<T>( *this->ptr<signed short>(row,col) );
		      case CV_32S://  4 int
			      return static_cast<T>( *this->ptr<int>(row,col) );
		      case CV_32F://  5 float
			      return static_cast<T>( *this->ptr<float>(row,col) );
		      case CV_64F://  6 double
			      return static_cast<T>( *this->ptr<double>(row,col) );
			}
			return T(0); // error
      }
		template <typename T> 
		T get_(int col) const // row - ExtendedMat order, col - feature order
      {
         return get_<T>(col/cols,col%cols);
      }

      public:

      //! does not check the access boundaries
		template <typename T> 
		void set(int row, int col, const T& data) 
		{
         // the ptr implementation moves correctly over the data (using uchar* and element size)
			switch (this->depth()){
		      case CV_8U://   0 unsigned char - bool
			      *this->ptr<unsigned char>(row,col) = static_cast<unsigned char>(data);
               break;
		      case CV_8S://   1 char
			      *this->ptr<signed char>(row,col) = static_cast<signed char>(data);
               break;
		      case CV_16U://  2 unsigned short
			      *this->ptr<unsigned short>(row,col) = static_cast<unsigned short>(data);
               break;
		      case CV_16S://  3 short
			      *this->ptr<signed short>(row,col) = static_cast<signed short>(data);
               break;
		      case CV_32S://  4 int
			      *this->ptr<int>(row,col) = static_cast<int>(data);
               break;
		      case CV_32F://  5 float
			      *this->ptr<float>(row,col) = static_cast<float>(data);
               break;
		      case CV_64F://  6 double
			      *this->ptr<double>(row,col) = static_cast<double>(data);
               break;
			}
      }
      //! does not check the access boundaries
		template <typename T> 
		void set(int col, const T& data) {
         set(col/cols,col%cols,data);
      }

      /*
		template <typename T> 
		T get(int index) const 
		{
         int element = index*step.p[0]; // * element size
         
         TODO

         // the ptr implementation moves correctly over the data (using uchar* and element size)
			switch (this->depth()){
		      case CV_8U://   0 unsigned char - bool
			      return static_cast<T>( *this->ptr<unsigned char>(index) );
		      case CV_8S://   1 char
			      return static_cast<T>( *this->ptr<signed char>(index) );
		      case CV_16U://  2 unsigned short
			      return static_cast<T>( *this->ptr<unsigned short>(index) );
		      case CV_16S://  3 short
			      return static_cast<T>( *this->ptr<signed short>(index) );
		      case CV_32S://  4 int
			      return static_cast<T>( *this->ptr<int>(index) );
		      case CV_32F://  5 float
			      return static_cast<T>( *this->ptr<float>(index) );
		      case CV_64F://  6 double
			      return static_cast<T>( *this->ptr<double>(index) );
			}
			return T(0); // error
      }

		template <typename T> 
		void set(int index, const T& data) 
		{
         // the ptr implementation moves correctly over the data (using uchar* and element size)
			switch (this->depth()){
		      case CV_8U://   0 unsigned char - bool
			      *this->ptr<unsigned char>(index) = static_cast<unsigned char>(data);
               break;
		      case CV_8S://   1 char
			      *this->ptr<signed char>(index) = static_cast<signed char>(data);
               break;
		      case CV_16U://  2 unsigned short
			      *this->ptr<unsigned short>(index) = static_cast<unsigned short>(data);
               break;
		      case CV_16S://  3 short
			      *this->ptr<signed short>(index) = static_cast<signed short>(data);
               break;
		      case CV_32S://  4 int
			      *this->ptr<int>(index) = static_cast<int>(data);
               break;
		      case CV_32F://  5 float
			      *this->ptr<float>(index) = static_cast<float>(data);
               break;
		      case CV_64F://  6 double
			      *this->ptr<double>(index) = static_cast<double>(data);
               break;
			}
      }*/

      private:
         class ConstNumber {
         protected:
            ConstNumber (){ 
	            inner = NULL; 
	         }
         public:
            ConstNumber (const ConstNumber* c){ 
	            inner = c; 
	         }
	          virtual ~ConstNumber(){
                  if (inner) delete inner;
                  inner = NULL;
	          }

            virtual operator bool() const {      // outer type conversion       
               return inner->operator bool(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator char() const {      // outer type conversion       
               return inner->operator char(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator unsigned char() const {      // outer type conversion       
               return inner->operator unsigned char(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator signed char() const {      // outer type conversion       
               return inner->operator signed char(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator unsigned short() const {      // outer type conversion       
               return inner->operator unsigned short(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator signed short() const {      // outer type conversion       
               return inner->operator signed short(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator int() const {      // outer type conversion       
               return inner->operator int(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator float() const {      // outer type conversion       
               return inner->operator float(); // the internal ConstNumber holds the correct type ... forward the call
            }
            virtual operator double() const {      // outer type conversion       
               return inner->operator double(); // the internal ConstNumber holds the correct type ... forward the call
            }
         protected:
            const ConstNumber* inner;
         };

         template < typename T > // inner type - stored
         class ConstNumber_: public ConstNumber
         {
         public:
            ConstNumber_(const T* v): value(v) { 
            } 
            virtual ~ConstNumber_(){ 
            } 
            virtual operator bool() const {      // outer type conversion       
               return (bool) (*value > 0); 
            }
            virtual operator char() const {      // outer type conversion       
               return (char) *value; 
            }
            virtual operator unsigned char() const {      // outer type conversion       
               return (unsigned char) *value; 
            }
            virtual operator signed char() const {      // outer type conversion       
               return (signed char) *value; 
            }
            virtual operator unsigned short() const {      // outer type conversion       
               return (unsigned short) *value; 
            }
            virtual operator signed short() const {      // outer type conversion       
               return (signed short) *value; 
            }
            virtual operator int() const {      // outer type conversion    
               return (int) *value; 
            }
            virtual operator float() const {      // outer type conversion       
               return (float) *value; 
            }
            virtual operator double() const {      // outer type conversion       
               return (double) *value; 
            }
            const T* value;
         };

      public:		
      /// accedes to the Matrics as 1 component array
		ConstNumber get(int row, int col) const // row - ExtendedMat order, col - feature order
      {
         int ch=this->channels();
			switch (this->depth()){
		      case CV_8U://   0 unsigned char - bool
			      return new ConstNumber_<unsigned char>( this->ptr<unsigned char>(row,col/ch) + col%ch);
		      case CV_8S://   1 char
			      return new ConstNumber_<signed char>( this->ptr<signed char>(row,col/ch) + col%ch);
		      case CV_16U://  2 unsigned short
			      return new ConstNumber_<unsigned short>( this->ptr<unsigned short>(row,col/ch) + col%ch);
		      case CV_16S://  3 short
			      return new ConstNumber_<signed short>( this->ptr<signed short>(row,col/ch) + col%ch);
		      case CV_32S://  4 int
			      return new ConstNumber_<int>( this->ptr<int>(row,col/ch) + col%ch);
		      case CV_32F://  5 float
			      return new ConstNumber_<float>( this->ptr<float>(row,col/ch) + col%ch);
		      case CV_64F://  6 double
			      return new ConstNumber_<double>( this->ptr<double>(row,col/ch) + col%ch);
			}
         return ConstNumber_<unsigned char>( this->ptr<unsigned char>(row,col/ch) + col%ch);
      }
		const ConstNumber get(int col) const // row - ExtendedMat order, col - feature order
      {
         return get(col/cols,col%cols);
      }
		const ConstNumber operator[](int index) const 
		{
         return get(index);
      }

		// row - ExtendedMat order, col - feature order
      // TODO
		//bool           getBool(int row, int col) const { return get<unsigned char>(row,col) != 0; }

      // TODO 
      //       Iterator with inner type correction and set function
      // 
      //       + operator = (ExtendedMat)
      //
      //typedef cv::MatConstIterator iterator;
      //template <typename T> typedef cv::MatIterator_<T> iterator<T>;
      //template <typename T> typedef cv::MatConstIterator_<T> const_iterator<T>;
      // <template T> row0_iterator<T*> begin() .. end() .. mid() ++ -- +(int step) -(int step) row0_iterator<T*> [int index]

		// ---------------  push back functionality
		//! \brief Push data to new row. The length of the new data must match the existing data size (= number of columns)
		//!
		//! - converts and copies data if reserved size is not enough
		//! - adapts the number of channels to the existing one
		//! 
		//! !!! attention, this function does not work with C-style openCV types as the template instantiasion for DataType<...> class is not provided / correct
		//! e.g. (use cv::Scalar insted of CvScalar for CV_RGB colors)		
		template <typename Tp> void push_back(Tp scalar)
		{
			if (cv::DataType<Tp>::type != type())
			{
				cv::Mat m;
				Mat(1,1, cv::DataType<Tp>::type, (uchar*)&scalar).convertTo(m,type());

				//  the previous call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors will arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				((cv::Mat*)this)->push_back( m );
			}
			else
				((cv::Mat*)this)->push_back( Mat(1,1, cv::DataType<Tp>::type, (uchar*)&scalar) );
		}

		//! \brief Push data to new row. The length of the new data must match the existing data size (= number of columns)
		//!
		//! - converts and copies data if reserved size is not enough
		//! - adapts the number of channels to the existing one
		template <typename Tp> void push_back(const std::vector<Tp>& vec) 
		{
			if (cv::DataType<Tp>::type != type())
			{
				cv::Mat m;
				Mat(1,(int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]).convertTo(m,type());

				//  the previous call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors will arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				((cv::Mat*)this)->push_back( m );
			}
			else
				((cv::Mat*)this)->push_back( Mat(1,(int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]) );
		}

		//! \brief Push data to new row(s). The length of the new data must match the existing data size (= number of columns)
		//!
		//! - converts and copies data 
		//! - adapts the number of channels to the existing one
		void push_back(const ExtendedMat& des)
		{
			if (des.type() != type())
			{
				cv::Mat m;
				des.convertTo(m, type());

				//  the previous call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors will arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				((cv::Mat*)this)->push_back( m );
			}
			else
				((cv::Mat*)this)->push_back( (cv::Mat)des );
		}

		//! \brief Push data to new row(s). The length of the new data must match the existing data size (= number of columns)
		//!
		//! - converts and copies data 
		//! - adapts the number of channels to the existing one
		void push_back(const cv::Mat& mat)
		{
			if (mat.type() != type())
			{
				cv::Mat m;
				mat.convertTo(m, type());

				//  the previous call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors will arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				((cv::Mat*)this)->push_back( m );
			}
			else
				((cv::Mat*)this)->push_back( mat );
		}

		//! \brief Push data to new row(s). The length of the new data must match the existing data size (= number of columns)
		//!
		//! - converts and copies data 
		//! - adapts the number of channels to the existing one
		// overloading MatExpr for overcoming scalar templated function
		void push_back(const cv::MatExpr& me)
		{
			this->push_back(cv::Mat(me));
		}

		// ---------------  concat functionality
		//! \brief Push data to new column. The size of the new data must match the existing data size (= number of rows)
		//!
		template <typename Tp> void concat(const Tp scalar) //converts and copies data
		{
			assert(this->rows <= 1); // crashes otherwise .. TODO exception
			if (empty()) 
				push_back(scalar);
			else
			{
				cv::Mat m(1,1,cv::DataType<Tp>::type, (uchar*)&scalar);

				//  the later call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors in hconcat may arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				if (cv::DataType<Tp>::type != this->type())
					m.convertTo(m, type());

				cv::hconcat(Mat(*this),m,*this);
			}
		}

		//! \brief Push data to new column. The size of the new data must match the existing data size (= number of rows)
		//!
		template <typename Tp> void concat(const std::vector<Tp>& vec) //converts and copies data
		{
			assert(this->rows <= 1); // crashes otherwise .. TODO exception
			if (empty()) 
				push_back(vec);
			else
			{
				cv::Mat m(1,(int)vec.size(), cv::DataType<Tp>::type, (uchar*)&vec[0]);

				//  the later call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors in hconcat may arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				if (cv::DataType<Tp>::type != this->type())
					m.convertTo(m, type());

				cv::hconcat(Mat(*this), m, *this );
			}
		}

		//! \brief Push data to new column(s). The size of the new data must match the existing data size (= number of rows)
		//!
		void concat(const ExtendedMat& des) //converts and copies data
		{
			if (empty()) 
				push_back(des);
			else
			{
				assert(this->rows == des.rows);// crashes otherwise .. TODO exception
				cv::Mat m(des);

				//  the later call to convertTo does not take care of the number of channels
				//  correct the channel number, otherwise errors in hconcat may arise
				if (m.channels() != this->channels()) 
					m = m.reshape(this->channels(),0);

				if (des.type() != this->type())
					m.convertTo(m, type());

				cv::hconcat(Mat(*this), m, *this );
			}
		}

		//! \brief Erase specific row of the ExtendedMat.
		//! This function may be computationally heavy, as it moves all data located after the selected index onto its new position. It uses temporary matrix for that.
		//!
		void erase(int row)
		{
			if ( (row < 0) || (row >= this->rows))
				return;

			if (row < this->rows -1) {
				cv::Mat temp1 = (*this)(cv::Range(row+1, this->rows), cv::Range::all()); // TODO need clone ?
				cv::Mat temp2 = (*this)(cv::Range(row, this->rows-1), cv::Range::all()); 
				temp1.copyTo(temp2);      
			}
			this->pop_back();
		}

	};
	
}
#endif //EXT_MAT_H_