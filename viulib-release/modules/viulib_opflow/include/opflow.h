/** 
 * \defgroup viulib_opflow viulib_opflow
 * viulib_opflow (Optical Flow) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_opflow is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_opflow depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://joachimwuttke.de/lmfit/index.html)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_opflow.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * lmfit is released under the LMFIT-BEER-WARE licence:
 * 
 * In writing this software, I borrowed heavily from the public domain,
 * especially from work by Burton S. Garbow, Kenneth E. Hillstrom,
 * Jorge J. More, Steve Moshier, and the authors of lapack. To avoid
 * unneccessary complications, I put my additions and amendments also
 * into the public domain. Please retain this notice. Otherwise feel
 * free to do whatever you want with this stuff. If we meet some day,
 * and you think this work is worth it, you can buy me a beer in return.
 * 
 * - Joachim Wuttke <j.wuttke@fz-juelich.de>
 *
 */

#ifndef OPFLOW_H
#define OPFLOW_H

// C++ Standard Library
#include <vector>
#include <string>

#include <stdio.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
//#ifndef ANDROID
//	#include "opencv2/nonfree/features2d.hpp"
//#endif

// VIULIB
#include "core.h"
#include "calibration.h"
#include "mtools.h"

//#define MAX_NUMBER_MATCHES	100
#define MAX_DISTANCE 500 // mm
#define MAX_HAMMING_DISTANCE 20
//#define FAST_MARGIN	25
#define FAST_THRESHOLD 50
#define DEFAULT_WINDOW_SIZE 25

// ==============================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// ==============================================================================================
// When the symbol OMF_EXPORTS is defined in a project OMF exports functions and classes. 
// In other cases OMF imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_opflow_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define OPFLOW_LIB __declspec( dllexport )
	#else
		#define OPFLOW_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define OPFLOW_LIB __declspec( dllimport )
	#else
		#define OPFLOW_LIB
	#endif
#endif
#else
#define OPFLOW_LIB
#endif

// ==============================================================================================
// AUTOMATIC DLL LINKAGE
// ==============================================================================================
// If the code is compiled under MS VISUAL STUDIO, and RM_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_opflow_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_opflow_d.lib" )
	#else
		#pragma comment( lib, "viulib_opflow.lib" )
	#endif
#endif

/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 
 *	(http://www.vicomtech.es/).
 */
namespace viulib
{
/** \brief This class has tools for the detection, description and matching of keypoints
	\ingroup viulib_opflow
 */	
namespace opflow
{
	/** \cond HIDDEN_SYMBOLS
	*/
	struct OPFLOW_LIB factory_registration
	{
    factory_registration();
	};
	static factory_registration opflow_module_init;
	/** \endcond
	*/
	int OPFLOW_LIB detectDescribeMatch( const cv::Mat& _trainImg, const std::vector<cv::KeyPoint>& _train_kpts, const cv::Mat& _train_desc,
				 const cv::Mat& _queryImg, std::vector<cv::KeyPoint>& _query_kpts, cv::Mat& _query_desc,
				 const cv::Mat& _maskImg,
				 const cv::Ptr<cv::FeatureDetector> _detector, const cv::Ptr<cv::DescriptorExtractor> _descriptor, const cv::Ptr<cv::DescriptorMatcher> _matcher,
				 std::vector<cv::DMatch>& _matches, std::vector<bool>& _match_mask,
				 bool _crossCheckMatch = false, int _knnMatch = 1,
				 int _max_distance = MAX_DISTANCE, int _max_hamming_distance = MAX_HAMMING_DISTANCE,
				 int _windowSize = DEFAULT_WINDOW_SIZE, const cv::Mat& _homography = cv::Mat());

	void OPFLOW_LIB swap( cv::Mat& _trainImg, cv::Mat& _queryImg,
			   std::vector<cv::KeyPoint>& _train_kpts, std::vector<cv::KeyPoint>& _query_kpts,
			   cv::Mat& _train_desc, cv::Mat& _query_desc );
	
	void OPFLOW_LIB crossCheckMatching( const cv::Ptr<cv::DescriptorMatcher>& _descriptorMatcher,
				 const cv::Mat& _train_desc, const cv::Mat& _query_desc,
				 std::vector<cv::DMatch>& _matches, int _knn, const cv::Mat& _mask=cv::Mat());
	// -----------------------------------------------------
	

	std::vector<cv::Point2f> OPFLOW_LIB filterOutliers( const std::vector<cv::Point2f>& _pts, const std::vector<bool>& _mask );

	void OPFLOW_LIB maskUnmatchedPoints(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query, 
		std::vector<cv::DMatch>& matches, std::vector<int> &mask);
	        
	void OPFLOW_LIB maskLongDistMatches(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,
		std::vector<cv::DMatch>& matches, std::vector<bool> &mask, int max_distance);

	void OPFLOW_LIB maskZeroDistMatches(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,	const std::vector<cv::DMatch>& matches, 
		std::vector<bool> &mask);

	void OPFLOW_LIB maskDistMatches(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query, 
		std::vector<cv::DMatch>& matches, std::vector<bool> &mask, int max_hamming_distance);

	void OPFLOW_LIB drawMatchesRelative(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,
	        std::vector<cv::DMatch>& matches, cv::Mat& img, const std::vector<bool>& mask = std::vector<bool> ());
	
	void OPFLOW_LIB drawMatchesRelative(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,
        	std::vector<cv::DMatch>& matches, cv::Mat& img, cv::Size procSize, cv::Size rendSize, const std::vector<bool>& mask = std::vector<bool> ());

	void OPFLOW_LIB drawRansacInliers(const std::vector<cv::Point2f>& pts, cv::Mat& img, const std::vector<int>& inliers, cv::Scalar rgb);
	void OPFLOW_LIB drawRansacInliers(const std::vector<cv::Point2f>& pts, cv::Mat& img, const std::vector<int>& inliersMask, int clusterGroup, cv::Scalar rgb);

	//void drawMatchesDepth(const std::vector<cv::Point2f>& train, const std::vector<cv::Point2f>& query, const std::vector<cv::Point2f>& query_tr, const std::vector<cv::Point2f>& train_tr, cv::Mat& img, const int minY, const int maxY, const std::vector<bool>& mask = std::vector<bool> ());

	void OPFLOW_LIB drawMatchesDepth(const std::vector<cv::Point2f>& train, const std::vector<cv::Point2f>& query, cv::Mat& img, const int minY, const int maxY, const std::vector<bool>& mask = std::vector<bool> ());
	void drawMatchesColor(const std::vector<cv::Point2f>& train, const std::vector<cv::Point2f>& query, cv::Mat& img, cv::Scalar color=CV_RGB(255,255,255), const std::vector<bool>& mask = std::vector<bool> ());

	void OPFLOW_LIB keypoints2points(const std::vector<cv::KeyPoint>& in, std::vector<cv::Point2f>& out);

	void OPFLOW_LIB points2keypoints(const std::vector<cv::Point2f>& in, std::vector<cv::KeyPoint>& out);

	/** \brief Uses computed homography H to warp original input points to new planar position
	*/
	void OPFLOW_LIB warpKeypoints(const cv::Mat& H, const std::vector<cv::KeyPoint>& in, std::vector<cv::KeyPoint>& out);

	/** \brief Converts matching Keypoints to xy points
		\param train [in] List of train keypoints
		\param query [in] List of query keypoints
		\param matches [in] List of matches
		\param pts_train [out] List of 2D points
		\param pts_query [out] List of 2D points
	*/
	void OPFLOW_LIB matches2points(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,
	        const std::vector<cv::DMatch>& matches, std::vector<cv::Point2f>& pts_train,std::vector<cv::Point2f>& pts_query );
	
	/** \brief Converts acknowledged matching indices to xy points
		\param train [in] List of train keypoints
		\param query [in] List of query keypoints
		\param matches [in] List of matches
		\param pts_train [out] List of 2D points
		\param pts_query [out] List of 2D points
		\param mask [in] Matches mask
	*/
	void OPFLOW_LIB acknowledgedmatches2points(const std::vector<cv::KeyPoint>& train, const std::vector<cv::KeyPoint>& query,
	    	const std::vector<cv::DMatch>& matches, std::vector<cv::Point2f>& pts_train,
    		std::vector<cv::Point2f>& pts_query, std::vector<bool> mask);

	/** \brief Generates a mask for the LRref matches checking the complete loop
		\param mask [out] The generated mask (same size as LRref)
		\param LRref [in] Matches between left and right references images (Left is query)
		\param LLref [in] Matches between left reference and new images (ref is query)
		\param LRnew [in] Matches between left and right new images (left is query)
		\param RRref [in] Matches between right reference and new images (ref is query)
		\return the number of valid matches
	*/
	int OPFLOW_LIB generateStereoMask(std::vector<bool>& mask, 
		const std::vector<cv::DMatch> &LRref, const std::vector<cv::DMatch> &LLref, 
		const std::vector<cv::DMatch> &LRnew, const std::vector<cv::DMatch> &RRref );

	/** \brief Computes a mask for the matching process aplying maximum displacement restrictions

			This functions should be used with features detected in a pair of consecutive frame captured by a moving camera
			The mask will be a 8bit single channel matrix with query.size() rows and train.size() cols. 
			The element i,j indicates that the query feature i can be matched (if it is not zero) with the train featrure j

		\param query [in] List of query keypoints
		\param train [in] List of train keypoints
		\param queryMask [in] List of masks indicating if each keypoint should be considered
		\param mask [out] Generated mask with query.size() rows and train.size() cols		
		\param maxDistance [in] Max distance that a matched feature can be displaced between frames
	*/
	void OPFLOW_LIB computeDistanceMask(const std::vector<cv::KeyPoint>& query, const std::vector<cv::KeyPoint>& train, const std::vector<bool> &queryMask, const std::vector<bool> &trainMask, cv::Mat &mask, int maxDistance);

	/** \brief Computes a mask for the matching process aplying epipolar restrictions

			This functions should be used with features detected in a left-right stereo pair.
			The mask will be a 8bit single channel matrix with query.size() rows and train.size() cols. 
			The element i,j indicates that the query feature i can be matched (if it is not zero) with the train featrure j

		\param query [in] List of query keypoints
		\param train [in] List of train keypoints
		\param mask [out] Generated mask with query.size() rows and train.size() cols
		\param leftright_rightleft [in] Indicates the direction of the matches, If it is 0 then the query points correspond to the left image
		\param max_epipolar_distance [in] Max distance in Y coordinate between left and right features
		\param min_stereo_disparity [in] Max distance between left and right features
		\param max_stereo_disparity [in] Min distance between left and right features
	*/
	void OPFLOW_LIB computeEpipolarMask( const std::vector<cv::KeyPoint>& query,const std::vector<cv::KeyPoint>& train,cv::Mat &mask,
		int leftright_rightleft,int max_epipolar_distance,int min_stereo_disparity,int max_stereo_disparity);
	
	/** \brief Check the mutual consistency between a pair of matches lists

				If a pair of matches is consistent means that the train feature for match1 is the query feature for match2 and 
				the train feature for match2 is the query for match1

		\param matches1 [in] The first list of matches
		\param matches2 [in] The second list of matches
		\param match_mask1 [out] Mask for the first list of matches
		\param match_mask2 [out] Mask for the second list of matches		
	*/
	void OPFLOW_LIB mutualConsistency( const std::vector<cv::DMatch> &matches1, const std::vector<cv::DMatch> &matches2,  std::vector<bool> &match_mask1, std::vector<bool> &match_mask2);
		
	
	/** \brief Search a match with the specified query index

			The list of matches must be ordered by incrasing query index

		\param matches [in] The list of matches
		\param query [in] The query index that must be found
		\return the match index if found, -1 otherwise
	*/
	int OPFLOW_LIB searchMatchIndex( const std::vector<cv::DMatch> &matches, int query );


	/** \brief This class provides planar odometry functionality
	\ingroup viulib_opflow
	*/
	class PlanarOdometry :public VObject
	{	
		REGISTER_BASE_CLASS(PlanarOdometry,ofFlow);
	public:
		/** \brief	Destructor. */
		virtual ~PlanarOdometry(){}

		virtual void setPlane( viulib::calibration::Plane* _plane) = 0;
		virtual int run(cv::Mat& _outputImg, cv::Mat& _inputImg) = 0;
	};

	/** \brief This class provides stereo odometry functionality
	\ingroup viulib_opflow
	*/
	class StereoOdometry: public VObject
	{
		REGISTER_BASE_CLASS(StereoOdometry,opflow);
	public:
		/** \brief	Destructor. */
		virtual ~StereoOdometry(){}

		virtual void setStereoPairOp(viulib::calibration::StereoPair *m_spair)=0;
		virtual int run(cv::Mat& leftImg, cv::Mat&, const cv::Mat &rvec=cv::Mat(), const cv::Mat &tvec=cv::Mat() ) = 0;
		virtual void drawFlow(cv::Mat& img)=0;
	};

	/** \brief This class is a wrapper for using KLT feature tracking
	\ingroup viulib_opflow
	*/
	class KLTTracker: public VObject
	{
		REGISTER_BASE_CLASS(KLTTracker,ofFlow);

	public:
		/** \brief	Destructor. */
		virtual ~KLTTracker(){}

		virtual int run(const cv::Mat& _prevImg, const cv::Mat& _newImg, const std::vector<cv::KeyPoint>& _prevPts, std::vector<cv::KeyPoint>& _newPts, std::vector<cv::DMatch>& _matches) = 0;
		virtual int run( const cv::Mat& _trainImg, const std::vector<cv::KeyPoint>& _train_kpts, const cv::Mat& _train_desc,
		const cv::Mat& _queryImg, std::vector<cv::KeyPoint>& _query_kpts, cv::Mat& _query_desc, std::vector<cv::DMatch>& _matches) = 0;
		virtual void draw( cv::Mat& _img, const std::vector<cv::KeyPoint>& _prevPts, const std::vector<cv::KeyPoint>& _newPts, const std::vector<cv::DMatch>& _matches ) = 0;

	protected:
		/** \brief Set parameters with automatic type conversion. VObject inherrited method.
		*
		*	\param value Value of the parameter
		*	\param	name Name of the parameter
		*
		*	 KLTTracker_v1: "KLT version 1"<br>
		*		"verbose",					bool,						true to printf traces of the status of the tracker.<br>
		*		"max_number_matches",		int,						Desired maximum number of matches to obtain.<br>
		*		"grid_cols",				int,						Number of cols of the GridAdaptedFeatureDetector. Also applied to rows. <br>
		*		"minEigThreshold",			double,						Minimum Eigenvalue threshold. <br>
		*		"errThreshold",				double,						Error threshold. <br>		
		*/
		virtual void setParam(const ParamBase& value, const std::string& name) =0;
		//! \brief Get parameters with automatic type conversion. VObject inherrited method.
		virtual ParamBase getParam(const std::string& name) const =0;
		//! \brief direct access to inner parameters, via pointer. VObject inherrited method.
		virtual void getParam(void** param, const std::string& name) =0;
	};

	/** \brief This is a class that can be used to generate a dense/semi-dense optical flow
	*/
	class GridOpticalFlow : public VObject
	{
		REGISTER_BASE_CLASS(GridOpticalFlow,ofFlow);
	public:
		/** \brief	Destructor. */
		virtual ~GridOpticalFlow(){}

		virtual void run( const cv::Mat& _newImg, std::vector<cv::DMatch>& _matches ) = 0;		
		virtual void draw( cv::Mat& _img ) const = 0;
	};

};
};

#endif // OPFLOW_H
