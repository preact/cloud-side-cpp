#ifndef BLENDING_MODES_H
#define BLENDING_MODES_H
/** 
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/highgui/highgui.hpp"
#ifdef CV_VERSION_EPOCH
#include "opencv2/legacy/compat.hpp"
#endif

inline double round(double x) { return (x > 0.0) ? floor(x + 0.5) : ceil(x - 0.5); };


double linearDodge( double base_color,double blend_color);
double linearBurn( double base_color, double blend_color);
double linearLight( double base_color, double blend_color);
double softLight( double base_color, double blend_color);
double overlay ( double base_color, double blend_color);
double screen(double base_color, double blend_color);
double multiply(double base_color, double blend_color);
double overlay(double base_color, double blend_color);
double hardLight(double base_color, double blend_color);
double colorDodge(double base_color, double blend_color);


#endif
