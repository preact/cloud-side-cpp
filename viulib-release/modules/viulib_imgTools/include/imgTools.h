/** \defgroup viulib_imgTools viulib_imgTools
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'

#ifndef IMGTOOLS_MODULE_H
#define IMGTOOLS_MODULE_H

#include "imgTools_exp.h"
#define RANGE_FIXED_SCALE				0
#define RANGE_INDIVIDUALLY_SCALE		1
#define RANGE_ALL_SAME_SCALE			2

namespace viulib
{

/** \brief Module for image Tools 
	\ingroup viulib_imgTools

*/
namespace imgTools
{

	IMGTOOLS_LIB enum ALIGN {ALIGN_LEFT, ALIGN_RIGHT, ALIGN_CENTER};
	/** \brief Struct for RANGE
	\ingroup viulib_imgTools
	*/
	struct IMGTOOLS_LIB RANGE
	{
		double min;
		double max;
		RANGE(){min =DBL_MAX; max=0.0;};
		RANGE(int _min, int _max){min =_min; max=_max;};

	};
		
	struct IMGTOOLS_LIB factory_registration
	{
    factory_registration();
	};
	static factory_registration imgtools_module_init;

  /** \brief graphic
	 *  This class manages a 2D-multiple vector graphic
	 *	
	 - <code> // Generate a new 200x200 graphic going from 0 to 3 starting from vector's 10th position
			cv::Mat graphic_image;
			viulib::imgTools::graphic graphic_generator;
			std::vector<std::string> labels_vector;
			labels_vector.push_back("0");
			labels_vector.push_back("V");
			labels_vector.push_back("A");
			labels_vector.push_back("R");

			g.setSize(cv::Size(200,200));
			g.setRange(0,3);
			g.setData(100,1);
			g.setVerticalAxisLabels(vt);
			
			std::vector<double> *f;
			f = g.getData();
			(*f)[10] = 2;
			(*f)[11] = 2;
			(*f)[12] = 2;
			(*f)[13] = 2;

			g.generate(graphic_image,10);
			cv::imwrite("graph.bmp",graphic_image);
	 
	 </code> .

	 \remark
				La estructura de la grafica es la siguiente:
											margin0
											TITLE
											margin1
	 
									  |
				VERTICAL			  |
		margin2               margin3 |h								margin4
				LABELS				  |     
									  |_________ w _________________
						
											margin5 
										HORIZONTAL LABELS
											margin6
		\ingroup viulib_imgTools		
	 */
  class IMGTOOLS_LIB graphic : public VObject
  {
  private:
    graphic();

    REGISTER_GENERAL_CLASS(graphic,imgTools);

  public:
    ~graphic();

    void generate(cv::Mat &_img_out,int _current=0);
    void setData(int _size, double _val, cv::Scalar _lineColor = cv::Scalar(0,0,0) , int _index=-1);
    void setData(std::vector<double> _data, cv::Scalar _lineColor = cv::Scalar(0,0,0), int _index=-1);
    void setData(std::vector<float> _data, cv::Scalar _lineColor= cv::Scalar(0,0,0), int _index=-1);
    void setData(std::vector<int> _data, cv::Scalar _lineColor= cv::Scalar(0,0,0), int _index=-1);
    // each channel one vector
    void setData(cv::Mat _data, cv::Scalar _lineColor, int _index);

    std::vector<double> *getData(int _index=0);

    void setSize(int _w_pixels, int _h_pixels);
    void setSize(cv::Size _size);
    void setVerticalAxisLabels(std::vector<std::string> &_labels_v);
    /**
    \brief Set graphic's vertical range Labels

    \param _num_vaxis [in], define how many points should be on the vertical axis

    \return void
    */
    void setVerticalAxisNumLabels(int _num_vaxis=5);
    void setVerticalAxisLabels();
    void setMaxVertAxisRange(double &_min_value,double &_max_value);

    /** 
    \brief Set graphic's vertical range

    \param _fixed [in] to set range values. 
    RANGE_FIXED_SCALE				0		Range will not be or be fit to the min max of the data.
    RANGE_INDIVIDUALLY_SCALE		1		Range will be normalize to fit to the max of each data vector. 
    RANGE_ALL_SAME_SCALE			2		Range will be normalize to fit to the max of all the data vector.
    \param _min [in] min value of the y axis by default -1
    \param _max [in] max value of the y axis by default -1
    \return void
    */
    void setRange(int _fixed=RANGE_ALL_SAME_SCALE, float _min=-1.0, float _max=-1.0);
    void setDataRange(float _min, float _max,int _index);

    void setHRange(int _min, int _max);
    void setHRange(int _index);
    void setHNSteps(int _val) {if (_val>0) m_h_steps=_val;};
    void setShowHLabels(bool _show) {m_show_horizontal_range=_show;};

    void setTitle(std::string _title, ALIGN _align);
    void setBackgroundColor(cv::Scalar _color);
    void setBackgroundAlpha(double _alpha);
    /** 
    \brief Devuelve la posicion en pixels del punto x, y del grafico

    \param _x [in] x position of the image
    \param _y [in] y position of the image

    \return cv::Point position in graphic coordinates of (x,y) point in image coordinates
    */
    cv::Point getPos(float _x , float _y);

    void setDataColor( int _index, cv::Scalar _color, int _thickness=1);
    void setAxisColor( cv::Scalar _color);
    void setTitleColor( cv::Scalar _color);



  private:
    void getInPixels(int &_m0,int &_m1, int &_m2, int &_m3, int &_m4, int &_m5, int &_m6,
      cv::Size &_title_size ,
      cv::Size &_axis_labels_v_size,
      int &_graphic_w,
      int &_graphic_h);


    cv::Size m_size; // width and height pixels
    cv::Mat m_background;

    cv::Scalar m_background_color;
    double m_background_alpha;

    // axis
    cv::Scalar m_axis_color;
    double m_axis_width;
    std::vector<std::string> m_axis_labels_v,m_axis_labels_h;
    cv::Size2f m_axis_labels_v_size,m_axis_labels_h_size;
    RANGE m_axis_range_v_fixed;

    RANGE m_axis_range_h;

    RANGE m_axis_range_v;


    std::string m_title;
    cv::Size2f m_title_size;
    cv::Scalar m_title_color;
    int m_title_x_position;
    int m_title_y_position;
    viulib::imgTools::ALIGN m_title_align;
    double m_title_point_size;
    double m_margin0,m_margin1,m_margin2,m_margin3,m_margin4,m_margin5,m_margin6;
    int m_line_thickness;
    int m_num_vaxis_labels;


    std::vector<std::vector<double> > m_data;
    std::vector<cv::Scalar> m_data_color;
    std::vector<RANGE> m_data_range;
    std::vector<RANGE> m_data_max_min;


    int m_state_range;


    int m_current_pos;

    bool m_show_horizontal_range;
    int m_h_steps;

  };

	/** \brief This class wraps the MotionHistory functionality of OpenCV 1.X
	* Example usage:
	 <code>
	    viulib::imgTools::MotionEstimator me;
	    ...	
		IplImage imgIpl = _img;	
		IplImage outIpl = _img;
		double angle = me.update_mhi( &imgIpl, &outIpl, 15 );
		Mat out = cvarrToMat(&outIpl);
	</code>
	*/
	class IMGTOOLS_LIB MotionEstimator
	{
	public:	
#ifdef CV_VERSION_EPOCH
		MotionEstimator():last(0),buf(0),mhi(0),orient(0),mask(0),segmask(0),storage(0){}
		double update_mhi( IplImage* img, IplImage* dst, int diff_threshold );
#else
		MotionEstimator() :last(0){}
		double update_mhi(const cv::Mat& img, cv::Mat& dst, int diff_threshold);
#endif

	private:
		// various tracking parameters (in seconds)
		static const double MHI_DURATION;
		static const double MAX_TIME_DELTA;
		static const double MIN_TIME_DELTA;
		// number of cyclic frame buffer used for motion detection
		// (should, probably, depend on FPS)
		static const int N = 4;
		int last;

#ifdef CV_VERSION_EPOCH
		// ring image buffer
		IplImage **buf;

		// temporary images
		IplImage *mhi; // MHI
		IplImage *orient; // orientation
		IplImage *mask; // valid orientation mask
		IplImage *segmask; // motion segmentation map
		CvMemStorage* storage; // temporary storage
#else
		// ring image buffer
		std::vector<cv::Mat> buf;

		// temporary images
		cv::Mat mhi; // MHI
		cv::Mat orient; // orientation
		cv::Mat mask; // valid orientation mask
		cv::Mat segmask; // motion segmentation map
#endif
	};

	typedef struct zoning_t
	{
		cv::Rect_<float> r;
		double w;
		double val;
		zoning_t(){w=0.0;val=0.0;};
		zoning_t(cv::Rect_<float> _r, double _w=1.0, double _val=0.0){r=_r;w=_w;val=_val;};
		cv::Rect getRect(cv::Size s) {return cv::Rect((int)round(r.x*s.width), (int)round(r.y *s.height), (int)round(r.width*s.width),(int)round(r.height*s.height));};
	}zoning_t;

	/** \brief Interpolation8u
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB float interpolation8u( IplImage *img, const float &x, const float &y );

	/** \brief Interpolation32f
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB float interpolation32f( IplImage *img, const float &x, const float &y );

	/** \brief posterize
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void posterize(cv::Mat &img_in,cv::Mat &img_out,int number_levels=2,cv::Rect r=cv::Rect(0,0,0,0));

	/** \brief posterize_avg
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void posterize_avg(const cv::Mat &image,cv::Mat &img_out,int nbins = 4,int type=1);

	/** Devuelve la proyeccion del valor de intensidad de la matriz src en la direccion orientation.
		- <code> imageProjection( src, projection, orientation ,storage_orientation) </code> .
		\pre

		\remark
			the projection will be storaged in a colum or row vector depending of the storage_orientation parameter
			     STORAGE:
				            0 -> row vector
							1 -> colum vector
			the orientation of projection will be indicated by orientation parameter
			     PROJECTION:
				            0 -> vertical
							1 -> horizontal
			if img is (w,h) and storage = 0 projection proy will be (1,X)
			if img is (w,h) and storage = 1 projection proy will be (X,1)
			if img is (w,h) and PROJECTION = 0 X will be w
			if img is (w,h) and PROJECTION = 1 X will be h

		\par Ejemplo:
			\code
				// calculating vertical projection of an image and storage it as a colum vector
				cv::Mat proj;
				 viulib::imgTools::imageProjection (img, proj, 0, 1);
			\endcode
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void imageProjection( const cv::Mat &src, cv::Mat &proy, int orientation ,int storage_orientation);

	/** \brief Projection avgstddev
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void imageProjectionAvgStdDev( cv::Mat &src, cv::Mat &mean, cv::Mat &stdDev, int orientation );

	/** \brief Projection first derivative
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void imageProjectionFirstDerivative( cv::Mat &proy );

	/** \brief Draw projection Mat
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void drawProjectionMat( cv::Mat &proj, cv::Mat &img, int imgHeight );

	/** \brief Rotate Image
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void rotateImage( IplImage *img, IplImage *result, CvPoint2D64f centroRotacion, CvPoint2D64f nuevoCentro, double angle );
	
	/** \brief Rotate
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void rotate (cv::Mat &_img_in, cv::Mat &_img_out, double _angle);

	/** \brief Change Image Contrast
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void changeImageContrast( IplImage *img, const double &alpha );

	/** \brief Sum line pixels
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB float sumLinePixels( IplImage* image, CvPoint pt1, CvPoint pt2 );

	/** \brief Count non zero line
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB int countNonZeroLine( IplImage* image, CvPoint pt1, CvPoint pt2, int connectivity );

	/** \brief Generate SDV Image
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void generateSDVImage( IplImage *img, IplImage **sdvImg );

	/** \brief Generate Entropy image
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void generateEntropyImage( IplImage *img, IplImage **entropy );

	/** \brief Log polar
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void logPolar( const CvArr* srcarr, CvArr* dstarr, CvPoint2D32f center, double M, double angle, int flags );
	
	/** \brief Convert from 32f to 8U
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void convertFrom32FTo8U( const cv::Mat1f &img32F, cv::Mat1b &img8U, int mode = 0 );

	/** \brief Calculate Skeleton
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void calculateSkeleton( IplImage *binary, IplImage *tmp32, IplImage *edges, IplImage *skeletonImage );

	/** \brief Find maximum
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB float findMaximum( float *values, const int &valueCount );

	/** \brief Otsu thresholding
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB int getThreshVal_Otsu_8u( const cv::Mat& _src, const cv::Mat& _mask );

	/** \brief Find first and second derivatives of each row within a single channel matrix
		\param _src [in] Source matrix
		\param _first [out] first derivative
		\param _second [out] second derivative
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void derivatives( const cv::Mat& _src, cv::Mat& _first, cv::Mat& _second);
	
	/** \brief Find the zero crossings within each row of a matrix of first derivatives
		\param _src [in] Source matrix
		\param _zeros [out] Found zero crossings in each row
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void zeroCrossings( const cv::Mat& _src,std::vector< std::vector <int > > &_zeros);

	/** \brief Maximuns
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void maximuns( const cv::Mat& _src,std::vector<double> &_maximuns,std::vector <int >&_index);

	/** \brief Write
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void write( std::string _file_name, const cv::Mat& _img);

	/** \brief Write conditioned
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void if_write( std::string _file_name, const cv::Mat& _img,bool write =true);

	//*******************************************************************************************
	// GEOMETRIC FUNCTIONALITIES
	//*******************************************************************************************
	/** \brief Swap
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void swap(cv::Point &p0, cv::Point &p1);

	/** \brief Bresenham line
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB std::vector<cv::Point> getBressenhamLine(cv::Point p0, cv::Point p1);
	//IMGTOOLS_LIB POINT getNextPoint(POINT p0, POINT p1,POINT previous);

	/** \brief Dot
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB int dot(cv::Point U, cv::Point V);

	/** \brief Point in triangle
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB bool pointInTriangle(cv::Point p,cv::Point A,cv::Point B,cv::Point C);

	/** \brief Projection_PV
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB cv::Point2f projection_PV( int Px1,int Py1,int Px2,int Py2,int Vx1,int Vy1,int Vx2,int Vy2,double *alpha);

	/** \brief Generates ellipse points
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void generateEllipsePoints( const int &numPoints, const CvBox2D &ellipse, std::vector<CvPoint2D32f> &points );
	
	/** \brief Calculates distance to ellipse
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void calculateDistanceToEllipse( const CvBox2D &ellipse, const CvPoint2D32f *points, int numPoints, float *distances );

	/** \brief Fits ellipse
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB CvBox2D fitEllipse(const CvMoments& m);

	/** \brief Major axis of ellipse
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB CvPoint2D32f majorAxis(const CvBox2D& ellipse);
		
	/** \brief Estimate a global contrast value of the image
		\param image [in] Source image
		/return The contrast value
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB float estimateImageContrastValue( const cv::Mat1b &image );

	/** \brief Erase the pixels that produce diagonal connection between blobs
		\param image [in/out] Source and destination image
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void erase8ConnectivityPixels( cv::Mat1b &imgBin );



	/** Genera una grafica a partir del vector de doubles que se le pasa como parametro.
			- <code> generate_graphic(imagen_salida, indice_inicial, vector_valores, margin_0, margin_1, margin_2 margin_3, margin_4, margin_5, anchura_grafica, altura_grafica, titulo, texto_inferior, texto_superior, color_fondo, color_ejes, color_puntos) </code> .
			\pre
				
			\remark
				La estructura de la grafica es la siguiente:
											margin3
											title
											margin4
	 
				up_text				|
									|
		margin0               margin1|h								margin2
									|   
				bottom_text			|_________ w _________________
						
											margin5 	
	

			\par Ejemplo:
				\code
					// generacion de una grafica
					viulib::imgTools::generate_graphic (img_out,0,values_vector,0,0,10,10,10,10,200,200, "down", "top", backColor, axisColor, pointColor);
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void generate_graphic(cv::Mat &graphic, std::vector<double> &v, int w=200, int h=200,int current_index=0, int margin0=10, int margin1=10, int margin2=10, int margin3=10, int margin4=10, int margin5=10, std::string title=std::string(), std::string bottom_text=std::string(), std::string up_text=std::string(), cv::Scalar background_color=cv::Scalar(255,255,255), cv::Scalar axis_color=cv::Scalar(0,0,0), cv::Scalar points_color=cv::Scalar(0,0,255));
	
	
	/** Imprime a la imagen un tono de color con cierta trasparencia
			- <code> setTone(imagen, mascara, color_tono, trasparencia, rectangulo de aplicacion) </code> .
			\pre
				
			\remark
			  La mascara marcara a que pixeles se les aplica el tono dentro del rectangulo de interes
			  El color de un pixel pasara a ser:
					pixel =  trasparencia * color_tono + (1.0-trasparencia)*pixel;
	

			\par Ejemplo:
				\code
					// aplicar tono azulado a toda la imagen
					viulib::imgTools::setTone (img_out,cv::Mat(),cv::Scalar(255,0,0),0.5,cv::Rect());
				\endcode
				\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void setTone(cv::Mat &im_in_out,cv::Mat &im_mask,cv::Scalar &tone,double alpha,cv::Rect r);

	/** Imprime una imagen png en un rectangulo de la imagen destino
			- <code> writePNG(imagen, imagen_png, rectangulo_de_impresion) </code> .
			\pre
				la imagen png ha de tener 4 canales, el cuarto canal define  el alpha

			\remark
	

			\par Ejemplo:
				\code
					// imprimir una imagen png en el rectangulo (0,0,100,100) de la imagen
					viulib::imgTools::writePNG (img_out,img_png,cv::Rect(0,0,100,100));
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void writePNG(cv::Mat &im_in_out,cv::Mat &im,cv::Rect r);

	/** Imprime una imagen png en un rectangulo de la imagen destino
			- <code> writePNG(imagen, imagen_png, rectangulo_de_impresion) </code> .
			\pre
			\remark
	

			\par Ejemplo:
				\code
					// imprimir una imagen png en el rectangulo (0,0,100,100) de la imagen
					viulib::imgTools::writePNG (img_out,img_png,cv::Rect(0,0,100,100));
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void writePNG(cv::Mat &im_in_out,std::string png_file_name,cv::Rect r);

	
	/** dibuja un poligono en la imagen 
			- <code> drawPoly(imagen, puntos_poligono o rectangulo, trasparencia, color) </code> .
			\pre
			\remark
	

			\par Ejemplo:
				\code
					// dibujar un rectangulo negro con trasparencia 0.8  1->opaco 0->transparente
					viulib::imgTools::drawPoly (img_in_out,cv::Rect(5,30,110,25),0.8,cv::Scalar(0,0,0)));
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void drawPoly(cv::Mat &im_in_out,const std::vector<cv::Point> &poly,const float &alpha=1.0,const cv::Scalar &color=cv::Scalar(255,255,255));
	/** \brief Draws a polygon
	 \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void drawPoly(cv::Mat &im_in_out,const cv::Rect &rect,const float &alpha=1.0,const cv::Scalar &color=cv::Scalar(255,255,255));
	/** \brief Draws a polygon
	 \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void drawPoly(cv::Mat &im_in_out,const std::vector<cv::Point> &poly, const cv::Scalar &color=cv::Scalar(255,255,255));
	/** \brief This function draws a progress bar with OpenCV
	* \param [in] _outputImg Image in which draw things
	* \param [in] _rect Rectangle that defines the bar inside the image
	* \param [in] _ratio Ratio of image progressed
	* \param [in] _name <Optional> Write a text right to the bar
	* \param [in] _inColor <Optional> Color of inside bar
	* \param [in] _inColor <Optional> Color of outside bar
	* \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void drawProgressBar(cv::Mat& _outputImg, const cv::Rect& _rect, double _ratio, std::string _name=std::string(), cv::Scalar _inColor=cv::Scalar(0,0,255), cv::Scalar _outColor=cv::Scalar(255,0,0));


	/** dibuja una cruz en la imagen con centro en center
			- <code> drawCross(imagen, centro,radio, color,tipo_de_cruz, anchura_del_trazo) </code> .
			\pre
			\remark
	

			\par Ejemplo:
				\code
					// dibujar una cruz en la posicion (10,10) de la imagen
					viulib::imgTools::drawCross (img_in_out,cv::Point(10,10),5,cv::Scalar(10,10,20));
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void drawCross(cv::Mat &im_in_out,cv::Point center, int radio=5, cv::Scalar color=cv::Scalar(0,0,0),int crossType=1,int thickness=1);

	IMGTOOLS_LIB void dottedLine(cv::Mat &_img_in_out,cv::Point _p1,cv::Point _p2, double _segment_lenght, double _space_lenght,int _type=1,cv::Scalar _color=cv::Scalar(0,0,0));

	IMGTOOLS_LIB void dottedLine(cv::Mat &_img_in_out,cv::Point _p1,cv::Point _p2, int _segment_lenght, int _space_lenght,int _type=1,cv::Scalar _color=cv::Scalar(0,0,0));

	IMGTOOLS_LIB void dottedRect(cv::Mat &_img_in_out,cv::Rect _r,cv::Scalar color);




	/** Rotation matrix from euler angles
			- <code> euler2RotMat(rotation_over_x,rotation_over_y,rotation_over_z,rotation_matrix) </code> .
			\pre
			\remark
	

			\par Ejemplo:
				\code
					// obtener matriz de rotacion a partir de los angulos de euler Ox Oy y Oz
					cv::Mat rotMat(3,3, CV_32FC1);
					viulib::imgTools::euler2RotMat (Ox,Oy,Oz,rotMat);
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void euler2RotMat(double Ox,double Oy, double Oz,cv::Mat &mat);
	
	/** Euler Angles from Rotation 
			- <code> rotMat2euler(rotation_matrix,rotation_over_x,rotation_over_y,rotation_over_z) </code> .
			\pre
			\remark
	

			\par Ejemplo:
				\code
					// obtener angulos de euler a partir de la matriz de rotacion rotMat
					double Ox,Oy,Oz;
					viulib::imgTools::euler2RotMat (Ox,Oy,Oz,rotMat);
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void rotMat2euler(const cv::Mat &mat,double &Ox,double &Oy, double &Oz);

	/** Cambia unos puntos de un sistema de coordenadas a otro conociendo la rotacion y la traslacion que se debe aplicar previamente
			- <code> applyExtrinsics( puntos_3d_en_el_sistema_de_coordenadas_del_objeto, matriz_rotacion, vector_traslacion) </code> .
			\pre
			\remark
				Pasa un cjto de vectores de un sistema de coordenadas a otro conociendo la matriz de rotacion y el vector de traslacion
				Vcam = [R|T]Vobj

			\par Ejemplo:
				\code
					// aplicar rotacion y traslacion al vector vec de puntos 3d
					std::vector <cv::Point3f> vec;
					vec.push_back(...);
					....
					cv::Mat R(3,3,CV_32FC1);
					cv::Mat T(3,1,CV_32FC1);
					//Get R and T
					....
					viulib::imgTools::applyExtrinsics (vec,R,T);
				\endcode
			\ingroup viulib_imgTools
		*/
	
	IMGTOOLS_LIB void applyExtrinsics( std::vector<cv::Point3f> &vpoints, cv::Mat &R, cv::Mat &T);

	/** Devuelve la entropia de una imagen
			- <code> getImageEntropy(imagen,tipo_de_entropia); </code> .
			\pre
			\remark
				Estan implementados dos tipos de entropia:
				- los que usa Matlab (log y shannon)
				- entropia usando la matriz de co-ocurrencia

				Entropias obtenidas utilizando las ecuaciones propuestas en:
					http://www.cse.usf.edu/~r1k/MachineVisionBook/MachineVision.files/MachineVision_Chapter7.pdf

			\par Ejemplo:
				\code
					// entropia utilizada en matlab de una imagen
					cv::Mat image;
					....
					double entropy = viulib::imgTools::getImageEntropy(image,1,150,"shannon");
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB double getImageEntropy(cv::Mat &img,int type=1, double th=0.0, std::string subtype=std::string());

	/** Devuelve true si el rectangulo esta dentro de la imagen, si no modifica el rectangulo para que se ubique en el interior de la imagen y devuelve false
			- <code> isInside(rectangulo,imagen) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// comprobar si un rectangulo se encuentra en el interior a una imagen
					cv::Mat image;
					....
					bool is_interior = viulib::imgTools::isInside(cv::Rect(20,20,50,70),image);
				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB bool isInside(cv::Rect &r, const cv::Mat &img ,bool _modify=true );
	IMGTOOLS_LIB bool isInside( cv::Rect &r, const cv::Rect &rect_in, bool _modify=true);
	IMGTOOLS_LIB bool isOverlapped(cv::Rect &_r, const cv::Rect &_r_origin );
	IMGTOOLS_LIB float isOverlapped(cv::Rect &_r1, const cv::Rect &_r2,std::vector<cv::Point> &contour);


	
	/** devuelve el calculo de la integral del rectangulo que se le envia como parametro
			- <code> getImageEntropy(imagen,tipo_de_entropia); </code> .
			\pre
			\remark
				La imagen de entrada ha de ser una imagen integral
			\par Ejemplo:
				\code
					// Integral del rectangulo r de la imagen image
					cv::Mat image;
					cv::Rect r = cv::Rect(10,10,30,15);

					cv::Mat integral = cv::integral(image);
					....
					int integral = viulib::imgTools::integralVal(integral,r);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB double integralVal(const cv::Mat &integral, const cv::Rect &r, bool testIn = true);	
   IMGTOOLS_LIB double integralVal45(const cv::Mat &integral45, const cv::Rect &r, bool clockwise, bool testIn = true);


	//*******************************************************************************************
	// PHOTOSHOP FUNCTIONALITIES
	//*******************************************************************************************
	/** Funcion que separa una imagen en sus componentes de altas frecuencias y bajas frecuencias
		Esta funcion genera una imagen de bajas frecuencias a partir de un suavizado gaussiano y la de altas frecuencias
		sumandole la inversa de las bajas frecuencias a la imagen original 
			- <code> separateFrecuencies( imagen_original, imagen_bajas_frecuencias, imagen_altas_frecuencias, tamano_kernel_gaussiana, scale_para_apply_image) </code> .
			\pre
				scale es un valor entre 1.0 y 2.0 (se corresponde con el scale de la ventana ApplyImage de photoshop)
			\remark
				Para recuperar la imagen original basta con aplicar un blend mode soft light a la imagen de altas frecuencias sobre la de bajas

				Se sigue el algoritmo propuesto en http://vimeo.com/12317775
			\par Ejemplo:
				\code
					// separar en altas y bajas frecuencias la imagen image
					cv::Mat image;
					cv::Mat lowFreqs, highFreqs;
					...

					separateFrecuencies(image, lowFreqs, highFreqs, 5, 2);
				\endcode
			\ingroup viulib_imgTools
		*/
	IMGTOOLS_LIB void separateFrecuencies(cv::Mat &img, cv::Mat &lowFrecuencies, cv::Mat &highFrecuencies, int gauss=3, double scale=2.0);

	/** Aplica un tipo de fusion de imagenes entre dos imagenes
			- <code> applyBlend(imagen_original,imagen_blending, imagen_salida, funcion_de_fusion) </code> .
			\pre
			\remark
				Los tipos de modos de fusion que se pueden aplicar son:
					- normal
					- disolver					(no implementado)
					- oscurecer					(no implementado)
					- Multiplicar				(Multiply)
					- SubexponerColor			(no implementado)
					- SubexposicionLineal		(LinearBurn)
					- ColorMasOscuro			(no implementado)
					- Aclarar					(no implementado)
					- Trama						(Screen)
					- SobreexponerColor			(no implementado)
					- SobreexposicionLineal     (LinearDodge)
					- ColorMasClaro				(no implementado)
					- Superponer				(overlay)
					- LuzSuave					(SoftLight)
					- LuzFuerte					(no implementado)
					- LuzIntensa				(no implementado)
					- LuzLineal					(LinearLight)
					- LuzFocal					(no implementado)
					- MezclaDefinida			(no implementado)
					- Diferencia				(no implementado)
					- Exclusion					(no implementado)
					- Subtract					(no implementado)
					- Divide					(no implementado)
					- Tono						(no implementado)
					- Saturacion				(no implementado)
					- Color						(no implementado)
					- Luminosidad				(no implementado)

					Blendind mode's equations taken from:
						The Hidden Power Of Blend Modes In Adobe Photoshop
						Authors: Scott Valentine
						Publisher: Adobe Press
						Pages: 256
						Published: 2012-07-20
						Language: English
						ISBN-10: 0321823761     ISBN-13: 9780321823762

						and

						http://photoblogstop.com/photoshop/photoshop-blend-modes-explained

						and

						http://www.venture-ware.com/kevin/coding/lets-learn-math-photoshop-blend-modes/
			\par Ejemplo:
				\code
					// recuperar una imagen que se ha separado en altas y bajas
					cv::Mat image;
					cv::Mat lowFreqs, highFreqs;
					...

					separateFrecuencies(image, lowFreqs, highFreqs, 5, 2);
					applyBlend(lowFreqs, highFreqs,image, "LuzLineal");

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void applyBlend(cv::Mat &img_in,cv::Mat &img_in_blend, cv::Mat &img_out, std::string modo = std::string("normal"));

	/** Expande el histograma de una imagen (caso particular de normalizeMat
			- <code> expandHistogram(imagen_original,imagen_expandida) </code> .
			\pre
			\remark
				Los valores de la nueva imagen iran de 0 a 255, de tal modo que el minimo de la imagen original se correspondera con el 0 y el maximo con el 255
			\par Ejemplo:
				\code
					// Expandir histograma de la imagen image
					cv::Mat image;
					cv::Mat image_expanded;
					...

					viulib::imgTools::expandHistogram(image,image_expanded);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void expandHistogram(cv::Mat &img_in,cv::Mat &img_out);

	/** Normaliza los valores de una imagen para que pertenezcan al intervalo valmin..valmax
			- <code> normalizeMat(imagen_original,imagen_expandida) </code> .
			\pre
			\remark
				Los valores de la nueva imagen iran de valmin a valmax, de tal modo que el minimo de la imagen original se correspondera con el valmin y el maximo con el valmax
			\par Ejemplo:
				\code
					// convertir los valores de una imagen para que se encuentren en el intervalo de 0 a 1
					cv::Mat image;
					cv::Mat image_normalized;
					...

					viulib::imgTools::normalizeMat(image,image_normalized,0.0,1.0);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void normalizeMat(cv::Mat &img_in,cv::Mat &img_out,double valmin, double valmax);
	IMGTOOLS_LIB void normalizeMatRange(cv::Mat &img_in,cv::Mat &img_out,double _from_min, double _from_max, double _to_min, double _to_max);


	/** Obtiene los valores LBP para cada pixel de una imagen
			- <code> getLBP(imagen_original,imagen_expandida,Radio,Puntos) </code> .
			\pre
			\remark
			\par Ejemplo:
				\code
					// Obtener la imagen lbp (normalizada de 0 a 255) de una imagen
					cv::Mat image;
					cv::Mat image_lbp;
					...

					viulib::imgTools::getLBP(image,image_lbp);

				\endcode
		\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void getLBP(cv::Mat &img_in,cv::Mat &img_out, int R=1,int P=8);

	/** Obtiene los valores SILTP para cada pixel de una imagen
			- <code> getSILTP(imagen_original,imagen_expandida,Radio,threshold_ruido,Puntos) </code> .
			\pre
			\remark
			\par Ejemplo:
				\code
					// Obtener la imagen siltp (normalizada de 0 a 255) de una imagen
					cv::Mat image;
					cv::Mat image_siltp;
					...

					viulib::imgTools::getSILTP(image,image_siltp);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void getSILTP(cv::Mat &img_in,cv::Mat &img_out, int R,double tau=0.1,int P=8);

	/** Obtiene el mapa de isophotes de una imagen
			- <code> getIsoPhoteMap(imagen_grises,sigma_suavizado_imagen_grises, nivel_suavizado_mapa,center_threshold, closing_threshold, closing_ratio, center_map,closed) </code> .
			\pre
			\remark
					Eye centers through isophote curvature:
					Valenti, R. and Gevers, T., "Accurate Eye Center Location and Tracking Using 
					Isophote Curvature", IEEE Conference on Computer Vision and Pattern Recognition, 2008.
      
      \param _imgGray         Input Image
      \param _sigma           Sigma of the Gaussian Blur used to get the isophote map
      \param _smoothing       Size of the kernel used for the Gassian Blurring
      \param _centerTresh 
      \param _closingThresh 
      \param _closingRatio
      \param _centerMap       Output Center Map

			\par Ejemplo:
				\code
					// Obtener mapa de isophotes de una imagen
					cv::Mat image;
					cv::Mat map;
					...

					viulib::imgTools::getIsoPhoteMap(image, sigma, smoothing, centerThresh, closingThresh, closingRatio, map, closed);

				\endcode
				\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB cv::Point2f getIsoPhoteMap( const cv::Mat &_imgGray, const double &sigma, const double &smoothing, 
		const float &centerThresh, const float &closingThresh, const float &closingRatio, cv::Mat &centerMap, bool &closed ); 
   
   /// \brief Calculate IsoPhote image
   ///
   /// \param _img input image; 1 or 3 components; for 3 components (RGB) input, the input will be converted to grayscale using CV_BGR2GRAY
   /// \param blur the sigma of the preprocessing blur, removing noise; if negative value is given, than a gaussian kernel of size (-blur, -blur) will be used.
   /// \param out_smoothing sigma of the output grouping; if negative  value is given, than gaussian kernel of size (-out_smoothing, -out_smoothing) will be used.
   ///         if (out_smoothing == 0) no final filtering will be processed.
   /// \param normalize256 normalize the output image to CV_8UC1 of range [0,255]; if false, the output will be the centerness measure for each point in CV_32FC1 format.
   ///        Note that the values in the output image are also scaled accordingly in order to keep equal magnitudes across scale-space.
   ///        This allows to compare the absolute values across scales.
   /// \param use_filter_version if true, the computation of image derivatives is based on filter2d; It is slower (does 5x filtering instead of 1x) and has some bug.
   ///
   // if normalization is false, the output will be CV_32FC1, otherwise CV_8UC1
   IMGTOOLS_LIB cv::Mat generateIsoPhoteImageEx( const cv::Mat &_imgGray, double blur, bool only_curverness, bool black_on_white, bool use_filter_version );
    IMGTOOLS_LIB cv::Mat generateIsoPhoteImage( const cv::Mat &_img, double blur, double out_smoothing, bool normalize256 = true, bool black_on_white = true);
    IMGTOOLS_LIB cv::Mat generateCurvatureImage( const cv::Mat &_img, double blur, double out_smoothing, bool normalize256 = true);
    
	/** funcion que devuelve los gradientes tanto en x como en y de una imagen 
			- <code> getGradients(imagen_en_grises, imagen_magnitud_gradientes, gradientes_en_x, gradientes_en_y,valor_absoluto,aplicar_filtro_gausiano_previo,tipo_de_operador) </code> .
			\pre
			sobel: 
			     |-1 0 1|        | -1 -2 -1 |
			Gx = |-2 0 2|   Gy = |  0  0  0 |
			     |-1 0 1|        |  1  2  1 |
			 
			or scharr:
                 |-3  0 3 |        | -3 -10 -3 |
			Gx = |-10 0 10|   Gy = |  0   0  0 |
			     |-2  0 3 |        |  3  10  3 |
			
			\par Ejemplo:
				\code
					// Obtener imagen de gradientes de una imagen
					cv::Mat image;
					cv::Mat image_grises;
					cv::cvtColor(image,image_grises,CV_BGR2GRAY);
	
					cv::Mat image_gradientes,gx,gy;
					...

					viulib::imgTools::getGradients(image_grises, image_gradientes, gx, gy, true, true);

					cv::imshow("gradientes",image_gradientes);

				\endcode
				\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void getGradients(cv::Mat &_imgGray, cv::Mat &_dstGray, cv::Mat &_Gx, cv::Mat &_Gy,bool _abs,bool _gaussian,int _type=0);

	IMGTOOLS_LIB void getGradientsHistogram( cv::Mat &_imgGray, std::vector<double> &_histogram,bool _weighted=false,int _th=10, int histogram_size = 180);

	/** Obtiene el mapa de centros de una imagen
			- <code> getGradientCentersMap(imagen_grises,center_threshold, center_map) </code> .
			\pre
			\remark
				Eye centers by means of gradients:
				Timm, F. and Barth, E., "Accurate eye centre localisation by means of gradients", 
				Int. Conference on Computer Vision Theory and Applications (VISAPP), vol. 1, pp. 125-130, 
				Algarve, Portugal, 2011.
			\par Ejemplo:
				\code
					// convertir los valores de una imagen para que se encuentren en el intervalo de 0 a 1
					cv::Mat image;
					cv::Mat map;
					...

					viulib::imgTools::getGradientCentersMap(image, centerThresh, map);

				\endcode
				\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB cv::Point getGradientCentersMap( cv::Mat &_imgGray, const float &_gradientMagnitudeThresh, cv::Mat &_centerMap,cv::Mat &_acumulatorMap,float &radius, bool _smooth=true,int _both_dir=0,int lineLength=20,bool _weighted=false);
	IMGTOOLS_LIB cv::Point2f getGradientDirectionsCentersMap( cv::Mat &_imgGray, const float &_gradientMagnitudeThresh, cv::Mat &_centerMap,cv::Mat &_acumulatorMap,bool _smooth=true,int _dir=false,int lineLength=20,bool _weighted=false);


	/** Devuelve una estimacion de la rectangularidad del cuadrilatero formado por los cuatro puntos A B C D
			- <code> rectangularity(A,B,C,D) </code> .
			\pre

				A --------------- B
			    |                 |
				|                 |
				D --------------- C

				Se estima la rectangularidad calculando los angulos formados por los diferentes lados BA y DA , AB y CB , DC y BC y AD y CD

				devuelve un valor entre 0 (no rectangulo) y 1 (rectangulo)
			\remark
				
			\par Ejemplo:
				\code
					// estimar la rectangularidad de los puntos (10,10) (20,20) (15,15) y (10,20)
					double val = viulib::imgTools::rectangularity(cv::Point(10,10), cv::Point (20,20), cv::Point (15,15), cv::Point(10,20));

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB double rectangularity( cv::Point A, cv::Point B, cv::Point C,cv::Point D);
	IMGTOOLS_LIB double rectangularity( cv::Point2d A, cv::Point2d B, cv::Point2d C,cv::Point2d D);

		/** Ecualizacion de una imagen 
			- <code> ecualization(imagen_original, imagen_ecualizada,tipo,alpha) </code> .
			\pre

				
			\remark
				
			\par Ejemplo:
				\code
					// ecualizacion uniforme de la imagen image
					double val = viulib::imgTools::ecualization(image,image_dst,0);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void equalization(const cv::Mat &A, cv::Mat &B,const int &type=0,const int &gmin=0, const int &gmax=255, const double &alpha=0.03);
	IMGTOOLS_LIB void equalizationOpenCV(const cv::Mat &A, cv::Mat &B);

		/** Obtener bordes tipificados de una imagen 
			- <code> getEOI(imagen_original, imagen_salida,lista_de_imagenes_mascara_por_tipo,numero_de_tipos_de_borde,funcion_de_tipificacion,tipo_de_gradiente,threshold_gradiente) </code> .
			\pre
				int (*tipificar_borde)(double gx, double gy) : callback que en funcion del vector gradiente devuelve un entero que representa la clase de ese pixel
				se utilizara este valor para a\F1adir ese pixel a la imagen mascara guardada en eoi_images[valor]
				
			\remark
				
			\par Ejemplo:
				\code
					// obtener los edges of interest de una imagen img
					std::vector< cv::Mat > eoi_images;
					int tipificar_borde(double gx, double gy)
					{
						if (fabs(gx) < fabs(gy)) 
						{
							if (gy>0) return 0; // borde horizontal --> gradiente en vertical
							else return 1;
						}
						else 
						{
							if (gx>0) return 2; // oscuro a claro hacia la derecha
							else return 3; // oscuro a claro hacia la izda
						}
					}

					...
					viulib::imgTools::getEOI(img,im_out,eoi_images,4,&tipificar, 0, 50);
				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void getEOI(const cv::Mat &image,cv::Mat &img_out, std::vector< cv::Mat > &eoi_images,int number_of_edge_types, int (*tipificar_borde)(double gx, double gy), int gradient_type = 0, int gradient_threshold =20);


	/** Apply Custom 3x3 Kernel 
			- <code> applyCustomKernelOMP(imagen_original, imagen_salida,lista_de_imagenes_mascara_por_tipo,numero_de_tipos_de_borde,funcion_de_tipificacion,tipo_de_gradiente,threshold_gradiente) </code> .
			\pre
				int (*tipificar_borde)(double gx, double gy) : callback que en funcion del vector gradiente devuelve un entero que representa la clase de ese pixel
				se utilizara este valor para a\F1adir ese pixel a la imagen mascara guardada en eoi_images[valor]
				
			\remark
				
			\par Ejemplo:
				\code
					// obtener los edges of interest de una imagen img
					std::vector< cv::Mat > eoi_images;
					int tipificar_borde(double gx, double gy)
					{
						if (fabs(gx) < fabs(gy)) 
						{
							if (gy>0) return 0; // borde horizontal --> gradiente en vertical
							else return 1;
						}
						else 
						{
							if (gx>0) return 2; // oscuro a claro hacia la derecha
							else return 3; // oscuro a claro hacia la izda
						}
					}

					...
					viulib::imgTools::getEOI(img,im_out,eoi_images,4,&tipificar, 0, 50);
				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void applyCustomKernelOMP (const cv::Mat &img_in,cv::Mat &img_out, const CvRect &r,double (*eval_func)(int,int,int,int,int,int,int,int,int),cv::Mat &mask);
	IMGTOOLS_LIB void applyCustomKernelNxM (const cv::Mat &img_in,cv::Mat &img_out,cv::Size &kernel);


	/** Apply contrast limited adaptive histogram equalization (clahe)
			- <code> applyCLAHE(imagen_original, imagen_salida,xdivs,ydivs,bins,limit) </code> .
			\pre
				
			\remark
				// xdivs - number of cell divisions to use in vertical (x) direction (MIN=2 MAX = 16)
				// ydivs - number of cell divisions to use in vertical (y) direction (MIN=2 MAX = 16)
				// bins - number of histogram bins to use per division
				// range - either of CV_CLAHE_RANGE_INPUT or CV_CLAHE_RANGE_FULL to limit the output
				//         pixel range to the min/max range of the input image or the full range of the
				//         pixel depth (8-bit in this case)
				
			\par Ejemplo:
				\code
					
				\endcode
			\ingroup viulib_imgTools
	*/

	IMGTOOLS_LIB void applyCLAHE (const cv::Mat &img_in,cv::Mat &img_out,unsigned int xdivs=12,unsigned int ydivs=12, unsigned int bins=5, float limit=30);


	/** \brief: Local Orientation Estimation. This function calculates gradients/tangents for every pixel of the image
				
			- <code> 
			\pre
				
			\remark
								
			\par Ejemplo: Obtener el gradiente o la tangente para cada pixel de la imagen
						
				\code 
					viulib::imgTools::applyLOE(image, img_out,0,5,2,2); //for gradient
					viulib::imgTools::applyLOE(image, img_out,1,5,2,2); //for tangent
					
				\endcode
	*/
	IMGTOOLS_LIB void applyLOE(const cv::Mat &img_in, cv::Mat &img_out,int _type,int _kernel_radious=5,double _sigma1=2, double _sigma2=2);

	
	/** Apply a local normalization filter to equilibrate image luminance,  reduce the effect on a non-uniform illumination
			- <code> applyLocalNormalizationFilter(imagen_original, imagen_salida,param1, param2) </code> .
			\pre
				
			\remark
								
			\par Ejemplo:
				\code
					
				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void applyLNF(const cv::Mat &img_in, cv::Mat &img_out,cv::Size _window_size=cv::Size(0,0),double _sigma1=3, double _sigma2=20, bool _blur=false,float _norm_th_low=0.0, float _norm_th_high=0.0);

	/** \brief: This function calculates sobel gradients of the image after aplying a constrat normalization process to the image if required
				Then classifies different gradients related to the gradient orientation
				The angle of the gradient is converted to a scaled between 0 (0\BA) and 1 (360\BA -> 2*CV_PI)
				The scale of the atan is from 0  to CV_PI (0\BA..90\BA..180\BA) and from -CV_PI to 0 (180\BA..270\BA..360\BA)
				After converting to 0..1 scale a traslatation is applied to this angle and then a mapping to the 
				images of the output vector is applied in order to paint this gradient in the correspondient image.
			- <code> getColouredGradientOrientedImage(imagen_original, imagen_coloreada, vector de imagenes de gradientes separados,threshold para el gradiente, flag de aplicacion de normalizacion de la imagen, suavizado previo a la aplicacion de sobel, desplazamiento de los angulos) </code> .
			\pre
				
			\remark
								
			\par Ejemplo: Obtener los gradientes en cuatro secciones en el indice 0 y 2 los verticales 
						en el indice 1 y 3 los verticales 
				\code
					cv::Mat grayImg,image_out;
					std::vector <cv::Mat > oriented_gradients;
					oriented_gradients.resize(4); // queremos que nos divida el espacio de angulos en cuatro zonas 0-90 90-180 180-270 270-0 que en una escala de 0 a 1 seria 0-0.25 0.25-0.50 0.50-0.75 0.75-0
					// el desplazamiento lo ponemos para desplazar este mapeo 45\BA hacia de tal forma que ahora en la imagen cero entraran los angulos de 365\BA a 45\BA (horizontales) y asi sucesivamente
					// con este desplazamiento el mapeo de angulos lo hemos separado en estas secciones 0.875 - 0.125(45\BA) 0.125-0.375(135\BA) 0.375-0.625(225\BA) 0.625-0.875(315\BA)
					viulib::imgTools::getColouredGradientOrientedImage(grayImg,image_out,oriented_gradients,50,false,false,0,0.125);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void getColouredGradientOrientedImage( cv::Mat &_img_gray_in, cv::Mat &_img_out,std::vector<cv::Mat > &isolated_gradients,float _mag_th=30,bool _aply_normalization=false, bool _smooth=false,int base=-1,double bias=0.125);
	IMGTOOLS_LIB void getGradientImage( cv::Mat &_img_gray_in, cv::Mat &_img_out_bgr, cv::Mat &_img_out_gray,bool _smooth,float _noise_th=1000);

	/** \brief: This function generates zoning image, which is defined as the image matrix of cells in which the average of the intesity level in that area of the image has been computed
			- <code> applyDynamicZoning( image_original_gray, integral_image, zoning_image,number_of_cols_of_matriz, number_of_rows_of_matrix) </code> .
			\pre
				
			\remark
								
			\par Ejemplo: Get 20x10 zoning image
				\code
					cv::Mat grayImg,image_zoning,integral_image;
					...					
					cv::integral(grayImg,integral_image);
					viulib::imgTools::applyDynamicZoning(grayImg,integral_image,image_zoning,20,10);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void applyDynamicZoning( cv::Mat &_img_gray_in, cv::Mat &_img_integral, cv::Mat &_img_out,int _cols, int _rows);
	IMGTOOLS_LIB void applyDynamicZoning( cv::Mat &_img_gray_in, cv::Mat &_img_integral, cv::Mat &_img_out,std::vector<zoning_t > &_zoning_conf);

	/** \brief: This function calculate max and min points in a gray scale image
			- <code> findMinMax(input_image,output_image,minimuns_vector,maximuns_vector,threshold_to_eliminate_spurios_maxmins) </code> .
			\pre
				
			\remark
				NOT FINISHED YET
								
			\par Ejemplo: 
				\code
					cv::Mat grayImg,max_mins_img;
					...					
					std::vector<cv::Point> vmins,vmaxs;
					viulib::imgTools::findMinMax(grayImg,max_mins_img,vmins, vmaxs);

				\endcode
			\ingroup viulib_imgTools
	*/
	typedef struct _zerocross_t
	{
		int i;
		int val;
		int d;
		int type; //max-1 or min-0

	}_zerocross_t;
	IMGTOOLS_LIB void findMinMax(cv::Mat &img_in,cv::Mat &img_out,std::vector<cv::Point> &v_mins,std::vector<cv::Point> &v_max,int th=10);
	IMGTOOLS_LIB void localMaximuns(cv::Mat &img_in,cv::Mat &img_out,cv::Rect _r, cv::Rect _rUp, cv::Rect _rDown, cv::Rect _rLeft, cv::Rect _rRight);


	/** \brief: This function spreads black-pixels below a threshold and white-pixels above a threshold over their neighborhood max and min points in a gray scale image
			- <code> findMinMax(input_image,output_image,threshold_over_high_values, threshold_below_low_values, threshold_expand_high, threshold_expand_low,neighborhood_size) </code> .
			\pre
				
			\remark
				NOT FINISHED YET
								
			\par Ejemplo: 
				\code
					cv::Mat grayImg,img_out;
					...					
					viulib::imgTools::spreadBW(grayImg,img_out);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void spreadBW(cv::Mat &img_in,cv::Mat &img_out,int th_high=180, int th_low=80, int th_high_expand=130, int th_low_expand=125,cv::Size kernel_size=cv::Size(3,3));

	IMGTOOLS_LIB bool rCheck(cv::Rect &_r, cv::Size &_img_size,bool _modify=true);

	/** \brief: This function calculates edges using sobel or scharr and then applies a threshold obtained from image edges gaussian distribution
			- <code> edgesGaussianThreshold(input_image, output_image, peso_para_la_desviacion_tipica_en_el_calculo_del_umbral, mostrar_histograma_de_bordes) </code> .
			\pre
				
			\remark
				
								
			\par Ejemplo: 
				\code   Obtener imagen en el que se han eliminado los gradientes cuya magnitud es menor que th = (media + 0.5 *desviacion_tipica)
					cv::Mat grayImg,img_out;
					...					
					viulib::imgTools::edgesGaussianThreshold(grayImg,img_out,0.5,false);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB int edgesGaussianThreshold(cv::Mat &img_in, cv::Mat &img_out, double _w=1.0, bool show=false);

	IMGTOOLS_LIB cv::RotatedRect minimunBoundingBox(cv::Mat &img_in);
	
	IMGTOOLS_LIB void rotate(cv::Mat& image, cv::Mat &rotated_image, cv::RotatedRect rr);
	
	IMGTOOLS_LIB void crop(cv::Mat& image, cv::Mat &image_cropped, cv::RotatedRect &rr);

	IMGTOOLS_LIB void drawPoints(std::vector<cv::Point2f> &v, cv::Mat &img_out,cv::Scalar color);
	
	/** \brief Draws a cuboid using a projection matrix into a given image
	 *	The cuboid must be described as (x,y,z,w,h,l)^T, a 6x1 column Mat
	 *  Note: x-w, y-l, z-h
	 *	The projection matrix is P = K[R|t]
	 *  \param _cuboid [in] Cuboid in (x,y,z,w,h,l)^T format
	 *  \param _P [in] Projection matrix 3x4
	 *	\param _img [out] Image
	 * \ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void drawCuboid( const cv::Mat& _cuboid, const cv::Mat& _p, cv::Mat& _img, double scale_x=1.0, double scale_y=1.0,const cv::Scalar& _color = CV_RGB(255,0,0), int _thickness = 1 );

	IMGTOOLS_LIB void align(cv::Mat &_img_in,cv::Mat &_im_aligned, int _method=1);
	/** \brief: This function detects lane markings in typical forward looking views of the road from a car
			- <code> stepRowFilter(const cv::Mat& _srcGRAY, cv::Mat& _dstGRAY, const std::vector<int>& _tau, int _start_y, int _end_y) </code> .
			\pre
				Algorithm by Marcos Nieto.
				Reference: M. Nieto, J. Arrospide, and L. Salgado, \93Road environment modeling using robust perspective analysis and recursive 
				Bayesian segmentation,\94 Machine Vision and Applications, vol. 22, issue 6, pp. 927-945, 2011. (DOI.10.1007/s00138-010-0287-7)
			\remark
				_tau[j] corresponds to the expected width of lane markings at each row j. It should be large at the bottom (about 20), and
				zero above the vanishing point. If not provided, a default value of 20 is used
				_start_y defines the starting row from which the filter is applied. Values of rows lower than _start_y are set to 0
				_end_y defines the final row to which the filter is applied. Values of rows greater than _end_y are set to 0
								
			\par Ejemplo: 
				\code										
					viulib::imgTools::stepRowRilter(grayImg, img_out, grayImg.rows/2, grayImg.rows);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void stepRowFilter(const cv::Mat& _srcGRAY, cv::Mat& _dstGRAY, int tau, int _start_y, int _end_y);
	IMGTOOLS_LIB void stepRowFilter(const cv::Mat& _srcGRAY, cv::Mat& _dstGRAY, std::vector<int>& _tau, int _start_y, int _end_y);
		
	IMGTOOLS_LIB void computeVanishingPoints( std::vector<cv::Mat>& _vps, std::vector<int>& _lineLabels, const std::vector<cv::Mat>& _lines, int _num=1, const cv::Mat& _K=cv::Mat() );

	IMGTOOLS_LIB cv::Mat createGaussianPatch(cv::Size _s, double _sigma=-1);
	IMGTOOLS_LIB double createMovementMask(cv::Mat _img,cv::Mat &_img_mask,int _th);

	IMGTOOLS_LIB void matching (cv::Mat &_img_in, cv::Mat &_template, cv::Mat &_img_out,int _method=0);

	IMGTOOLS_LIB void deleteSmallContours(cv::Mat &_img_in, cv::Mat &_img_out,float area);

	IMGTOOLS_LIB void torgbNorm(cv::Mat &im_in, cv::Mat &im_rgb_out,cv::Mat &img_RGB_out);
	IMGTOOLS_LIB void histogram(cv::Mat &_img_in,std::vector <std::vector<int> > &histogram, cv::Mat &_img_graphic);
	IMGTOOLS_LIB void histogramBGR(const cv::Mat &_img_in,std::vector<std::vector<int> > &histogram, cv::Mat &_img_graphic);


	IMGTOOLS_LIB void plot(std::vector<int> _v, cv::Mat &_img_out);
	IMGTOOLS_LIB void plot(std::vector< std::vector<int> > _v, cv::Mat &_img_out);
	void IMGTOOLS_LIB plotGNUPlot(cv::Mat &_v,std::string _file);
	void IMGTOOLS_LIB plotGNUPlot(std::vector<float> &_v, std::string _file);
	void IMGTOOLS_LIB plotGNUPlot(std::vector<int> &_v, std::string _file);


	IMGTOOLS_LIB void detect_WhiteBlackWhiteStripes(cv::Mat &_img_in,cv::Mat &_map,int _th=100);
	IMGTOOLS_LIB void detect_BlackWhiteSquares(cv::Mat &_img_in, std::vector<cv::RotatedRect> &_v_rects, int _th_min_length=2, int _floodfill_th_up=10, int _floodfill_th_low=10);
	IMGTOOLS_LIB void detectQRSquares(cv::Mat &_img_in,cv::Mat &_pattern , cv::Mat &_map, std::vector< cv::RotatedRect > &_lr, std::vector< cv::RotatedRect > &_lr_not);
	

	IMGTOOLS_LIB void simplederivatives (const cv::Mat &_mat,cv::Mat &_first_derivative,cv::Mat &_second_derivative);

	IMGTOOLS_LIB void minMaxImage (const cv::Mat &_mat,cv::Mat &_first_derivative, cv::Mat _out,bool _t, bool _flag_edges);

	IMGTOOLS_LIB void variancemap(cv::Mat &_img_in,cv::Mat &_img_out,bool _binarize=false);
	IMGTOOLS_LIB void gradientOrientedDistancesHistogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_gx,cv::Mat &_gy, cv::Mat &_imap);
	
	/** \brief: This function compute color correlogram of an image
			- <code> colorCorrelogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_intensityMap, cv::Mat &_contrastMap,int _abs_width,int _abs_height,int _scale_inv=false) </code> .
			\pre
				
				Reference: A. P�rez-Escudero, J. Vicente-Page, R.C. Hinz, S. Arganda, G.G. de Polavieja, idTracker: Tracking individuals in a group by automatic identification of unmarked animals, Nature Methods
			\remark
			\param _img_in		[in]	input image , must be GRAY scale (8bit 1channel)
			\param _mask		[out]	map vector (8bit 1channel) image with high values (255) for borders and low values for flat zones 
			\param _intensityMap	[out]	data matrix (32bit 4channel) matrix with calculated data. channel - 0: angle between edge direction from pixel i and x-axis (-3.1415..3.1415 or -4 if gradient module not long enough) channel-1: normalized gradient module (0..1)
			\																					channel -2: x component of tangent unity vector [-1,1] ,	channel -3: y component of tangent unity vector [-1,1] 						
			\param _contrastMap	[in]	threshold to erase irrelevant gradientes (low value gradients) (values between 0..sqrt(2*255*255) )
			\param _abs_width		[in]	distance of the line which will be followed in edge direction to accumulate in coherent edge map (values between 0..widhtxheight)
			\param _abs_height	[in]	allowed arch of change in the path along the edge (values between 0..3.1415)
			\param _scale_inv	[in]	allowed arch of change in the path along the edge (values between 0..3.1415)
					
			\par Ejemplo: 
				\code										
					viulib::imgTools::stepRowRilter(grayImg, img_out, grayImg.rows/2, grayImg.rows);

				\endcode
			\ingroup viulib_imgTools
	*/
	IMGTOOLS_LIB void colorCorrelogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_intensityMap, cv::Mat &_contrastMap,int _abs_width,int _abs_height,int _scale_inv=false);
	
	//-- acumula en un histograma las distancias maximas desde el punto actual hasta el primer pixel fuera de la mascara en lineas que discurren en 15 direcciones alrededor del pixel --//
	//-- la salida es un histograma de distancias--//
	IMGTOOLS_LIB void maxDistancesHistogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_gx,cv::Mat &_gy,  cv::Mat &_dHistogram,double _ro);
	
	//-- acumula en una matriz  las distancias desde el punto actual hasta el primer pixel fuera de la mascara en lineas que discurren en 15 direcciones alrededor del pixel  en la posicion Ii + Ik--//
	//-- la salida son dos histogramas 2D: --//
	//--  - uno de distancias contra suma de intensidades--//
	//--  - otro de distancias contra valor absoulto de la resta de intensidades--//
	IMGTOOLS_LIB void colorGeometricalCorrelogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_intensityMap, cv::Mat &_contrastMap,double _ro=100);
	//-- en esta funci�n las lineas lanzadas desde cada pixel no acaban fuera de la mascara --//
	IMGTOOLS_LIB void directionalColorCorrelogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_intensityMap, cv::Mat &_contrastMap,int _abs_width,int _abs_height,int _scale_inv=false);

	//-- acumula en un histograma las distancias desde el punto actual hasta el primer pixel fuera de la mascara en lineas que discurren en 15 direcciones alrededor del pixel --//
	//-- la salida es un histograma de distancias--//
	IMGTOOLS_LIB void distancesHistogram(cv::Mat &_img_in,cv::Mat &_mask, cv::Mat &_histogram,double _ro);


	
	/** \brief: This function implements Lic (lineal integral convolution) algorithm
			- <code> apply_LIC(const cv::Mat& _img_in, cv::Mat& _img_out) </code> .
			\pre
				Reference: Brian Cabral and Leith (Casey) Leedom, "Imaging Vector Fields Using Line Integral Convolution," Proceedings of ACM SigGraph 93, Aug 2-6, Anaheim, California, pp. 263-270, 1993. 
			\remark
								
			\par Ejemplo: 
				\code										
					viulib::imgTools::apply_LIC(image, img_out);

				\endcode
	*/
	IMGTOOLS_LIB void	generateLIC(cv::Mat &_img_in,cv::Mat &_vector_field_img, cv::Mat &_img_out);

	/** \brief: This function implements a new edge generator algorithm
		\param _img_in		[in]	input image , must be GRAY scale (8bit 1channel)
		\param _img_out		[out]	map vector (8bit 1channel) image with high values (255) for borders and low values for flat zones 
		\param _img_data	[out]	data matrix (32bit 4channel) matrix with calculated data. channel - 0: angle between edge direction from pixel i and x-axis (-3.1415..3.1415 or -4 if gradient module not long enough) channel-1: normalized gradient module (0..1)
		\																					channel -2: x component of tangent unity vector [-1,1] ,	channel -3: y component of tangent unity vector [-1,1] 						
		\param _th_low_g	[in]	threshold to erase irrelevant gradientes (low value gradients) (values between 0..sqrt(2*255*255) )
		\param _beta		[in]	distance of the line which will be followed in edge direction to accumulate in coherent edge map (values between 0..widhtxheight)
		\param _th_alpha	[in]	allowed arch of change in the path along the edge (values between 0..3.1415)
	
		- <code> applyEdgesEnhancement(cv::Mat &_img_in, cv::Mat &_img_out,float _th_low_g,int _beta,float _th_alpha) </code> .
		\pre
			Reference: 
		\remark
			This algorithm, after smoothing, thresholding and retrieving edge unitary directional vectors, creates an accumulation map
			where coherent edges have a high value whereas noise has a lower value.
		\par Ejemplo: 
			\code										
				viulib::imgTools::applyEdgesEnhancement(image, img_out,100,5,1);

			\endcode
	*/

	IMGTOOLS_LIB void applyEdgesEnhancement(cv::Mat &_img_in, cv::Mat &_img_out,cv::Mat &_img_data,float _th_low_g=100,int _beta=5,float _th_alpha=1.0);
	IMGTOOLS_LIB void applyEdgesEnhancementDiscrete(cv::Mat &_img_in, cv::Mat &_img_out,cv::Mat &_img_data,float _th_low_g=100,int _beta=15,float _th_alpha=1.0);
	IMGTOOLS_LIB void applyEdgesEnhancementDiscreteLineal(cv::Mat &_img_in, cv::Mat &_img_out,cv::Mat &_img_data,float _th_low_g=0.1,int _beta=10000,float _th_alpha=1.0);
	IMGTOOLS_LIB void applyEdgesEnhancementNull(cv::Mat &_img_in, cv::Mat &_img_out,cv::Mat &_img_data,float _th_low_g=100,int _beta=15,float _th_alpha=1.0);
	IMGTOOLS_LIB void applyEdgeSharpening(const cv::Mat &_img_in, std::vector<cv::Mat>& _vimg_in_channels,  std::vector <cv::Mat> &_vgradients, cv::Mat &_grad_data, cv::Mat& _img_data_out,cv::Mat &_labelled_img);
	IMGTOOLS_LIB void applyTraceMaximumGradient(const cv::Mat &_img_in, std::vector<cv::Mat>& _vimg_in_channels, std::vector <cv::Mat> &_vgradients, cv::Mat &_grad_data, cv::Mat& _img_data_out, cv::Mat &_labelled_img);

		/** \brief: This function implements a gradient calculation algorithm for multiple channels 
		\param _img_in		[in]	input image , must be GRAY scale (8bit 1channel)
		
		\param _img_data_in	[out]	data matrix (32bit 3channel) matrix with calculated data. 
		\									channel -0: x component of gradient unity vector [-1,1] ,	channel -1: y component of gradient unity vector [-1,1] 	channel - 2:  normalized gradient module (0..1)					
		\param _th_low_g	[in]	threshold to erase irrelevant gradientes (low value gradients) used in applyLocalNormalizedGradient (values between 0..sqrt(2*255*255) )
	
		- <code> applyMaxGradientMultipleChannels(cv::Mat &_img_in,cv::Mat &_img_data,float _th_low_g=100) </code> .
		\pre
			Reference: 
		\remark
			This algorithm, calculates the most significant gradient(multiple channels) for every pixels.
		\par Ejemplo: 
			\code										
				viulib::imgTools::applyMaxGradientMultipleChannels(img_in, data,100);

			\endcode
	*/
	IMGTOOLS_LIB void applyMaxGradientMultipleChannels(const cv::Mat &_img_in,cv::Mat &_img_data,float _th_low_g=100);
	/** \brief: This function implements a Local gradient calculation algorithm.  applyMaxGradientMultipleChannels is used inside to calculate the gradients
		\param _img_in		[in]	input image , must be GRAY scale (8bit 1channel)
		
		\param _img_data_in	[out]	data matrix (32bit 3channel) matrix with calculated data. 
		\param _img_data_in	[out]	data matrix (32bit 3channel) matrix with calculated data. 
		\									channel -0: x component of gradient unity vector [-1,1] ,	channel -1: y component of gradient unity vector [-1,1] 	channel - 2:  normalized gradient module (0..1)					
		\param _detections  [in]	is a vector<cv::Rect> which contains the sliding windows rois
		\param _lut			[in]	is a pointer to _detections in order to use it as a matrix
		\param _th_low_g	[in]	threshold to erase irrelevant gradientes (low value gradients) used in applyMaxGradientMultipleChannels (values between 0..sqrt(2*255*255) )
	
		- <code> applyLocalNormalizedGradient(cv::Mat& _img_in, cv::Mat& _img_data_out,std::vector<cv::Rect> &_detections,std::vector<std::vector<int>> &_lut,float _th_low_g=100) </code> .
		\pre
			Reference: 
		\remark
			This algorithm, calculates locally the most significant gradient(multiple channels) for every pixels.
		\par Ejemplo: 
			\code										
				viulib::imgTools::applyLocalNormalizedGradient(img_in, data,m_detections_rect,lut,100);

			\endcode
	*/

	IMGTOOLS_LIB void applyLocalNormalizedGradient(const cv::Mat& _img_in, cv::Mat& _img_data_out,std::vector<cv::Rect> &_detections,std::vector<std::vector<int> > &_lut,float _th_low_g=100.0);
	IMGTOOLS_LIB void applyLocalNormalizedGradient(const cv::Mat &_img_in, std::vector<cv::Mat>& _vimg_in_channels,  std::vector <cv::Mat> &_vgradients, cv::Mat& _img_data_out,std::vector<cv::Rect> &_detections,std::vector<std::vector<int> > &_lut,float _th_low_g=100.0);


	/** \brief: This function implements a gradient calculation algorithm for multiple channels 
		\param _img_in		[in]	input image , must be GRAY scale (8bit 1channel)
		
		\param _img_data_in	[out]	data matrix (32bit 3channel) matrix with calculated data. 
		\									channel -0: x component of gradient unity vector [-1,1] ,	channel -1: y component of gradient unity vector [-1,1] 	channel - 2:  normalized gradient module (0..1)					
		\param _th_low_g	[in]	threshold to erase irrelevant gradientes (low value gradients)  (values between 0..sqrt(2*255*255) )
	
		- <code> applyMaxGradientMultipleChannels(cv::Mat &_img_in,cv::Mat &_img_data,float _th_low_g) </code> .
		\pre
			Reference: 
		\remark
			This algorithm, calculates the most significant gradient(multiple channels) for every pixels.
		\par Ejemplo: 
			\code										
				viulib::imgTools::applyMaxGradientMultipleChannels(img_in, data,100);

			\endcode
	*/
	IMGTOOLS_LIB void applyDilateAgainstGradientDirection(cv::Mat &_data_in,cv::Mat &_img_out,float _th_low_g=0.0);
	IMGTOOLS_LIB void applyLocalAverageGradient(const cv::Mat& _img_data_in, cv::Mat& _img_data_out,std::vector<cv::Rect> &_detections);
	IMGTOOLS_LIB void applyLocalAverageGradientDescriptor(const cv::Mat& _img_data_in, cv::Mat& _img_data_out,cv::Size &_size);
	IMGTOOLS_LIB void apply_gaussianBritghtnessCorrection(cv::Mat &_img_in, cv::Mat &_img_out,int _kernel_size=7, int _derivative_x=1, int _derivative_y=0, float _sigmax=2, float _sigmay=2, bool _addresponse=false,float _th_low=1000);
	//applyDoG(cv::Mat &_img_in, cv::Mat &_img_out,int _nlevels);

	IMGTOOLS_LIB void showGradientField(cv::Mat &_img_in, cv::Mat &_img_data, cv::Mat &_img_out,int _gradient_vector_length=5);
	IMGTOOLS_LIB void BGR2CMYK(cv::Mat& img, std::vector<cv::Mat>& cmyk);
	IMGTOOLS_LIB void getMultiChannelGradient(cv::Mat& _img_in,cv::Mat &_data);


  /** \brief: This function helps to visualize a HOG descriptor of an Image
  \param _inputImg		[in]	input image , must be GRAY or BGR
  \param _descriptor	[in]	Input HOG Descriptor, should be a row vector (1 channel)	
  \param _blockSize 	[in]	HOG Parameter: Size of the blocks in the descriptor
  \param _cellSize  	[in]	HOG Parameter: Size of the cells in the descriptor
  \param _blockStride	[in]	HOG Parameter: Size of the blocks stride(step) in the descriptor
  \param _nBins     	[in]	HOG Parameter: Number of bins of HOF histograms.
  \param _nBins     	[in]	HOG Parameter: Number of bins of HOF histograms.
  \param _zoomFactor	[in]	Zoom factor for better visualization of HOG descriptor

  - <code> visualizeHOG(const cv::Mat& _inputImg, const cv::Mat& _descriptor, const cv::Size& _blockSize, const cv::Size& _cellSize,
                        const cv::Size& _blockStride, const int& _nBins, float& _zoomFactor) </code> .
  \pre
  Reference: 
  \remark
  This algorithm, helps to visualize the histograms of an HOG descriptor

  \endcode
  */
  IMGTOOLS_LIB cv::Mat visualizeHOG(const cv::Mat& _inputImg, const cv::Mat& _descriptor, const cv::Size& _blockSize, const cv::Size& _cellSize,
    const cv::Size& _blockStride, const int& _nBins, float& _zoomFactor);

  //*******************************************************************************************
  // HAUSDORFF DISTANCE
  //*******************************************************************************************
  IMGTOOLS_LIB double hausdorffDistance( const std::vector<cv::Point>& _a, const std::vector<cv::Point>& _b );

}

}

#endif
