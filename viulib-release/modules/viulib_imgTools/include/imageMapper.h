#pragma once
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "imgTools.h" 

namespace viulib
{

namespace imgTools
{

	
	/** \brief Functor to apply homography transformations
	*/
	template <typename T,typename T1>
	cv::Point_<T> transform_homography_t (cv::Point_<T> _point, cv::Mat &h)
	{
		cv::Point_<T> ret = cv::Point_<T>( -1, -1 );
		const T u = static_cast<T>( h.at<T1>(0,0) * _point.x ) + static_cast<T>( h.at<T1>(0,1) * _point.y ) + static_cast<T>( h.at<T1>(0,2) );
		const T v = static_cast<T>( h.at<T1>(1,0) * _point.x ) + static_cast<T>( h.at<T1>(1,1) * _point.y ) + static_cast<T>( h.at<T1>(1,2) );
		const T s = static_cast<T>( h.at<T1>(2,0) * _point.x ) + static_cast<T>( h.at<T1>(2,1) * _point.y ) + static_cast<T>( h.at<T1>(2,2) );
		if ( s != 0 )
		{
			ret.x = ( u / s );
			ret.y = ( v / s );
		}

		return ret;	
	};

	/** \brief Base class for all transformations
	*/
	class IMGTOOLS_LIB TransformBase
	{
	    public:
			TransformBase () {};
			~TransformBase() {};
			
			virtual cv::Point2f direct(const cv::Point2f& _point) {return cv::Point2f(0,0);};
			virtual cv::Point2f reverse(const cv::Point2f& _point) {return cv::Point2f(0,0);};

			virtual cv::Rect_<float> getDirectMinimunBoundingRect(const std::vector<cv::Point2f> &_p) {return cv::Rect_<float>(0,0,0,0);};
			virtual cv::Rect_<float> getReverseMinimunBoundingRect(const std::vector<cv::Point2f> &_p){return cv::Rect_<float>(0,0,0,0);};
		
	};
	
	/** \brief Homography transformation class
	*/
	class IMGTOOLS_LIB TransformHomography : public TransformBase
	{
		public:
			TransformHomography(){};
			TransformHomography( const cv::Mat& _H );
			~TransformHomography() {};
			cv::Point2f direct(const cv::Point2f &_point) 
			{
				cv::Point2f ret = cv::Point2f( -1, -1 );

				int type = H.type();
				if (type == CV_8UC1) ret=transform_homography_t<float,unsigned char>(_point,H); //is unsigned 8bit/pixel 0-255. 1-byte unsigned integer (unsigned char).
				else if (type == CV_8SC1)  ret=transform_homography_t<float,char>(_point,H); //CV_8S -> char (min = -128, max = 127)
				else if (type == CV_16UC1) ret=transform_homography_t<float,unsigned short>(_point,H); //CV_16U -> unsigned short (min = 0, max = 65535)
				else if (type == CV_16SC1) ret=transform_homography_t<float,short>(_point,H); //CV_16S -> short (min = -32768, max = 32767)
				else if (type == CV_32SC1) ret=transform_homography_t<float,int>(_point,H); //CV_32S -> int (min = -2147483648, max = 2147483647) CV_32S is a signed 32bit integer value for each pixel - again useful of you are doing integer maths on the pixels, but again needs converting into 8bits to save or display. This is trickier since you need to decide how to convert the much larger range of possible values (+/- 2billion!) into 0-255.  4-byte signed integer (int).
				else if (type == CV_32FC1) ret=transform_homography_t<float,float>(_point,H); //CV_32F is float - the pixel can have any value between 0-1.0 . 4-byte floating point (float).
				else if (type == CV_64FC1) ret=transform_homography_t<float,double>(_point,H); //CV_64F -> double
							
				return ret;
			}
			cv::Point2f reverse(const cv::Point2f &_point) 
			{
				cv::Point2f ret = cv::Point2f( -1, -1 );

				int type = Hinv.type();
				if (type == CV_8UC1) ret=transform_homography_t<float,unsigned char>(_point,Hinv); //is unsigned 8bit/pixel 0-255. 1-byte unsigned integer (unsigned char).
				else if (type == CV_8SC1)  ret=transform_homography_t<float,char>(_point,Hinv); //CV_8S -> char (min = -128, max = 127)
				else if (type == CV_16UC1) ret=transform_homography_t<float,unsigned short>(_point,Hinv); //CV_16U -> unsigned short (min = 0, max = 65535)
				else if (type == CV_16SC1) ret=transform_homography_t<float,short>(_point,Hinv); //CV_16S -> short (min = -32768, max = 32767)
				else if (type == CV_32SC1) ret=transform_homography_t<float,int>(_point,Hinv); //CV_32S -> int (min = -2147483648, max = 2147483647) CV_32S is a signed 32bit integer value for each pixel - again useful of you are doing integer maths on the pixels, but again needs converting into 8bits to save or display. This is trickier since you need to decide how to convert the much larger range of possible values (+/- 2billion!) into 0-255.  4-byte signed integer (int).
				else if (type == CV_32FC1) ret=transform_homography_t<float,float>(_point,Hinv); //CV_32F is float - the pixel can have any value between 0-1.0 . 4-byte floating point (float).
				else if (type == CV_64FC1) ret=transform_homography_t<float,double>(_point,Hinv); //CV_64F -> double
							
				return ret;
			}

			void setHomography(const cv::Mat &H);
			cv::Mat &getHomography(){return H;};

		private:
			cv::Rect_<float> getDirectMinimunBoundingRect(const std::vector<cv::Point2f> &_p);
			cv::Rect_<float> getReverseMinimunBoundingRect(const std::vector<cv::Point2f> &_p);

			std::vector<cv::Point> getDirectTransformedPointList(const std::vector<cv::Point> &_p);
			std::vector<cv::Point> getReverseTransformedPointList(const std::vector<cv::Point> &_p);
			
		private:
			cv::Mat H; 
			cv::Mat Hinv;

		
	};

	/** \brief This class can be used to map an image into another image following a given transformation
		\par Demo Code:
		viulib::imgTools::TransformHomography	homography( h );				// a linear homography is built using a matrix
		viulib::imgTools::TransformRadial		radial ( dist );				// TODO
		viulib::imgTools::TransformMyTransform	myTransform ( myParameter );	// You can do it

		viulib::imgTools::ImageMapper imapper( srcSize, dstSize, &homography );		// Full constructor
		//viulib::imgTools::ImageMapper imapper( srcSize, &homography );			// Same output size as input
		//viulib::imgTools::ImageMapper imapper( srcSize, dstWidth, &homography );	// Keep aspect ratio and define width

		
		Mat myImage ...
		Mat outputImg;

		// Direct map
		imapper.map( myImage, outputImg );

		// Inverse map		
		imapper.inverseMap( outputImg, myImage );

		// Now you can select a given region of your original image defined by a vector of points
		vector<Point> somePoints ...

		imapper.update( somePoints );

		Mat zoomOutputImg;
		imapper.map( myImage, zoomOutputImg );
		imapper.inverseMap( zoomOutputImg, myImage );

		\code		
		\endcode
		\author Vicomtech (2014)
	*/
	class IMGTOOLS_LIB ImageMapper
	{


	public:		
		/** \brief Constructor that creates a destination image with a given Width and mantaining the aspect ratio provided by the transform.
		* Warning! The transform may generate a really large (or really small) size
		*/
		ImageMapper( const cv::Size& _origSize, int _dstWidth, TransformBase* _transform );
		/** \brief Constructor that uses same origSize for dstSize
		*/
		ImageMapper( const cv::Size& _origSize, TransformBase* _transform );
		/** \brief Constructor that gets user-specified origSize and dstSize
		*/
		ImageMapper( const cv::Size& _origSize, const cv::Size& _dstSize, TransformBase* _transform );
		
		ImageMapper( ){m_empty=true;};
		
		~ImageMapper(){};

		void update( const std::vector<cv::Point2f> &_orig_image_region );

		void map( const cv::Mat& _src, cv::Mat& _dst, int _flags = CV_INTER_LINEAR, int _fillval = 0 ) const;
		void inverseMap( const cv::Mat &_img_src, cv::Mat &_img_dst, int _flags = CV_INTER_LINEAR, int _fillval = 0 ) const;

		cv::Point map( const cv::Point& _src) const;		
		cv::Point inverseMap( const cv::Point& _src) const;
		cv::Point_<float> map( const cv::Point_<float>& _src) const;
		cv::Point_<float> inverseMap( const cv::Point_<float>& _src) const;

		bool isEmpty() {return m_empty;}
		//--setters --//
		void set( const cv::Size& _origSize, int _dstWidth, TransformBase* _transform);
		void set( const cv::Size& _origSize, TransformBase* _transform);
		void set( const cv::Size& _origSize, const cv::Size& _dstSize, TransformBase* _transform );

		//--getters --//
		cv::Mat &getMapX() {return m_mapX;}
		cv::Mat &getMapY() {return m_mapY;}
		cv::Mat &getInvMapX() {return m_invMapX;}
		cv::Mat &getInvMapY() {return m_invMapY;}

		cv::Mat getA() {return A;} // A es la matriz (escala y traslacion) que se aplica al punto antes de hacerle la homografia para que se ajuste a la imagen de salida

	private:
		void create(const cv::Size& _origSize,cv::Size _dstSize,cv::Rect_<float> _rdst, TransformBase* _transform);

	private:		
		void createMaps( const cv::Size& _origSize, const cv::Rect_<float>& _dstRect, const cv::Size& _finalDstSize, TransformBase* _transform);

		TransformBase* m_transform;	

		cv::Mat A; // traslation and scale asociated to the visualizacion of the homography


		cv::Size m_origSize;
		cv::Size m_dstSize;

		cv::Mat m_mapX, m_mapY,m_invMapX,m_invMapY;	

		bool m_empty;
		float m_scaleX, m_scaleY;		
		float m_offset_x, m_offset_y;


	};
}
}
