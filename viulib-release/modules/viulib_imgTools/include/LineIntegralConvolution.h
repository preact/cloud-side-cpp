#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#ifdef CV_VERSION_EPOCH
#include "opencv2/legacy/compat.hpp"
#endif
#include "opencv2/highgui/highgui.hpp"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __APPLE__
#   include <malloc/malloc.h>
#else
#   include <malloc.h>
#endif
#include <vector>
#include "filteringTools.h"


void     SyntheszSaddle(int  n_xres,  int     n_yres,  float*   pVectr);
void	 NormalizVectrs(int  n_xres,  int     n_yres,  float*   pVectr);
void     GenBoxFiltrLUT(int  LUTsiz,  float*  p_LUT0,  float*   p_LUT1);
void     MakeWhiteNoise(int  n_xres,  int     n_yres,  unsigned char*  pNoise);
void	 FlowImagingLIC(int  n_xres,  int     n_yres,  float*   pVectr,   unsigned char*  pNoise,  
						unsigned char*  pImage,  float*  p_LUT0,  float*  p_LUT1,  float  krnlen);
void 	 WriteImage2PPM(int  n_xres,  int     n_yres,  unsigned char*  pImage,     char*  f_name);
void	generate_lic(cv::Mat &_img_in,cv::Mat &_vector_field_img, cv::Mat &_img_out);
