/** viulib_dtc (Detection, Tracking and Classification) v13.10
* 
* Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
* (Spain) all rights reserved.
* 
* viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
* Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
* (Spain) all rights reserved.
* 
* As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
* licencing terms of those libraries:
*
* * OpenCV v2.4.6 (http://opencv.org/)
* 
* License Agreement for OpenCV
* -------------------------------------------------------------------------------------
* BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
*
* The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
*
*/


#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef DTC_CLASSIFICATION_H_
#define DTC_CLASSIFICATION_H_
#include <queue>

#include "core.h"
#include "dtc_exp.h"
#include "dtc_features.h"

#ifdef HAVE_TBB
#include "tbb/tbb.h"
#ifdef WIN32
#undef max
#undef min
#endif
#endif


/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/).
*/
namespace viulib
{

  /** \brief This namespace corresponds to the dtc module in VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/)
  * \ingroup viulib_dtc  
  */
  namespace dtc
  {
    /** \brief TrainSample 
    *   This class has tools for definig training samples used in classification training 		
	*	\ingroup viulib_dtc
    */		
    class DTC_LIB TrainSample
    {
    public:
      static const int LABEL_NOTDEF = -1;

    public:
      /** \brief	Constructor. */
      TrainSample();
      /** \brief	Overloaded Constructor. */
      TrainSample(const Descriptor& desc, bool copyData=false);

      // Setters
      void setDescriptor(const Descriptor& desc, bool copyData=false);
      void setLabel(int _label); // use: setLabel ( labelStringMapper( str ) );
      void setFile(std::string &_file) {file = _file;};
      void setImage(cv::Mat &_img) {image = _img;};
      void setDescription(std::string _desc) {description=_desc;};

      // Getters
      const int getLabel() const;
      const std::string getDescription() const {return description;};
      const cv::Mat getImage() const {return image;};
      const std::string getFile() const {return file;};
      const Descriptor& getDescriptor() const;
      Descriptor& getDescriptor();

    private:
      int label;
      std::string description;
      Descriptor descriptor; //cv::Mat // can have multiple rows, to simplify parameter passing
      std::string file;
    public:
      cv::Mat image;
    };

    /** \brief LabelStringMapper
    *  This class has tools for maping numerical and text labels for classification training 
	*	\ingroup viulib_dtc
    */
    // author: pleskovsky@vicomtech.org
    class DTC_LIB LabelStringMapper
    {
    public: 
      //static const int NONE = TrainSample::LABEL_NOTDEF;
       typedef std::map< std::string, int > mapping_type;

    public:
      /// Default constructor
      LabelStringMapper();
      /// Label string initialisation with class names in the form: class_%d, for num classes (starting from 0)
      LabelStringMapper(int num);
      /// Destructor
      ~LabelStringMapper();

      std::string getClassName() const;

      void clear();
      bool empty() const;
      int size() const;
      mapping_type::const_iterator begin() const;
      mapping_type::const_iterator end() const;

      bool exists(const std::string& name) const;
      bool exists(const int& id) const;

      int& add(std::string name); // adds and returns identificator
      int& operator[](std::string name); // adds and returns identificator
      const int operator()(std::string name) const; // only gets the identificator
      const std::string operator()(int id) const; // returns name of the label; "" empty string if label does not exist in the map

      bool load(const cv::FileNode &node); //Load object's parameters from given XML/YAML 
	  bool load(const std::vector<std::string> &_labels);
      bool save(cv::FileStorage &fsPart) const; //Save the object to XML/YAML node

      friend DTC_LIB std::ostream& operator<<(std::ostream& os, const LabelStringMapper& lm);
    private:
      mapping_type nameMap;
    };

    DTC_LIB std::ostream& operator<<(std::ostream& os, const viulib::dtc::LabelStringMapper& lm);

    /** \brief Classification
    *  Wraps the variables of the classification result	
	* \ingroup viulib_dtc 
    */
    class Classification
    {
    public:
      Classification(): confidence(0.0) {};
      Classification(int lab, double conf, int patt_ind = 0): label(lab), confidence(conf), pattern_index(patt_ind)  {};

      //bool operator() (const Classification& lhs, const Classification&rhs) const
      //{
      //   return (lhs.confidence < rhs.confidence);
      //}

      typedef int classification_type;
      classification_type label;
      double confidence; // can hold either likelihood, log likelihood, etc.
      int pattern_index; // exact cluster index (more clusters can have the same label)
    };

    /** \brief Classifier
    * Base class that allows the usage of different classification algorithms.
	* \ingroup viulib_dtc 
    */
    class DTC_LIB Classifier: public virtual VObject
    {
      REGISTER_BASE_CLASS(Classifier,dtc)

        //typedef std::priority_queue<Classification, std::vector<Classification>, Classification> ClassificationVector;
        typedef std::vector<Classification> ClassificationVector;

      /** \brief parallel_processor
      * \brief Implements parallel classification of descriptors, for any classifier, via TBB.
      */
      class parallel_processor
      {
      public:
        parallel_processor (
          const Descriptor& _input, 
          bool _getAllLikelihoods,
          const Classifier* const _classifier,
          std::vector< Classifier::ClassificationVector >* const _output);

#ifdef HAVE_TBB
        void operator() (const tbb::blocked_range<size_t> & output_range) const; 
#endif
        void compute( size_t begin, size_t end ) const;

      private:      
        const Descriptor& input;
        bool getAllLikelihoods;
        const Classifier* const classifier;
        std::vector< Classifier::ClassificationVector >* const output;
      };

    public:
      /** \brief Constructor. */
      Classifier();
      /** \brief Destructor. */
      virtual ~Classifier(){}

      virtual Classifier* clone() const = 0;
    public:
      /** \brief	 Set the feature extraction methodology
      *
      *	\param	_featExt Pointer to the Feature Extractor object
      */
      virtual void setFeatureExtractor(FeatureExtractor* _featExt);

      /** \brief	 Get the feature extraction methodology
      */
      virtual FeatureExtractor* getFeatureExtractor();
      virtual const FeatureExtractor* getFeatureExtractor() const;

      /** \brief	 Set the feature extraction methodology
      *
      *	\param	_featExt Pointer to the Feature Extractor object
      */
      void setLabelMapper(LabelStringMapper _mapper);

      /** \brief	 Get the feature extraction methodology
      */
      LabelStringMapper& getLabelMapper();
      const LabelStringMapper& getLabelMapper() const;

      bool getVerbose() const;
      void setVerbose(bool ver = true);

      /** \brief converts the obtained classification results to simple Descriptor, having at each row either the class value
      *    (by default, if the get_all_likelihoods == false) or the confidence values of all classess.
      */
      static Descriptor classificationToDescriptor(const std::vector<ClassificationVector>& classifiedData, bool get_all_likelihoods = false);

      /** \brief Return the number of currently used classes
      */
      virtual int getClassNumber() const = 0;
      /** \brief Set the number of required classes (i.e. clusters)
       * Implemented only for unsupervised classifiers.
      */
      virtual void setClassNumber(int num) {} // implemented only for unsupervised classification
      // -------------------  learn / train / classify functionality

      /** \brief	Train the classifier using the provided path to an image database
      *
      *	\param	_path Path to the XML file describing the training image database
      */
      void train(const std::string& _path);

      /** \brief	Train the classifier using a given vector of viulib::dtc::TrainSample
      *
      *	\param	_trainData Vector of viulib::dtc::TrainSample containing the training Database
      */
      virtual void train(const std::vector<TrainSample>& _trainData) = 0;

      /** \brief	Train unsupervised classifier using a given vector of features (given on each row of the input Descriptor)
      *
      *	\param	_descriptor Container of feature descriptors; each row is considered as one descriptor;
      * \param   get_all_likelihoods Returns output in form of all likelihoods, ordered by label id. 
      *          Otherwise, several classification hypotheses can be returned (with unspecified order).
      *
      *	\return	A double vector is returned. Outer vector consists of ClassificationVector-s (= vector of Classification-s),
      *    corresponding to each descriptor row. Each ClassificationVector then describes either different hypotheses or 
      *    correspondences to all classese (if get_all_likelihoods was selected).
      *
      *    For each descriptor row a vector of classification results (Classification) is given:
      *		+ result[i].label - calss label
      *		+ result[i].confidence - confidence measure (likelihood/logLikelihood/distance) of the assigned class
      */
      virtual std::vector< ClassificationVector > train(const Descriptor& _descriptor, bool get_all_likelihoods = false) = 0; 

      /** \brief	Train unsupervised classifier given an input image from which multiple descriptors will be extracted and classified.
      *
      *	\param	_img Input image from which several Descriptors will be extracted and call to train(descriptor, bool) will be run.
      */
      std::vector< ClassificationVector > train(const cv::Mat& _img, SamplingStrategy& sampling, bool get_all_likelihoods = false);
      std::vector< ClassificationVector > train(const cv::Mat& _img, bool get_all_likelihoods = false)
      {
         return train(_img, BaseImageSampling::entireImage, get_all_likelihoods);
      }

      /** \brief	Classify descriptors which will be extracted from given input image.
      *  Calls fetureExtractor->run(img) following by classify(descriptor). Both functions are executed in parallel if TBB is on.
      */
      std::vector< ClassificationVector > classify(const cv::Mat& _img, SamplingStrategy& sampling, bool get_all_likelihoods = false) const; 
      std::vector< ClassificationVector > classify(const cv::Mat& _img, bool get_all_likelihoods = false) const
      {
         return classify(_img, BaseImageSampling::entireImage, get_all_likelihoods);
      }

      /** \brief	Classify an input descriptor vectors in parallel.
      *    Only const version of parallel classification available, thus learning must be done apriori.
      *    Use for supervised or pretrained classification only.
      *
      *  Multiple entities are considered for classification. Upon feature extraction, each descriptor row 
      *  will be considered as separate entity to be classified.
      * 
      *  This function is executed in parallel if TBB is on.
      */
      std::vector< ClassificationVector > classify(const Descriptor& _descriptor, bool get_all_likelihoods = false) const;

      /** \brief	Classify single descriptor vector.
      *
      *  Only one entity is considered for classification. Use classify() if more entities 
      *  are extracted from the image and should be followingly classified.
      */
      virtual ClassificationVector classify_single(const Descriptor& singleDescriptor, bool get_all_likelihoods = false) const = 0;

      // ---------------- set / get inner parameters and variables
    protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      virtual void setParam(const ParamBase& value, const std::string& name) = 0;
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      virtual ParamBase getParam(const std::string& name) const = 0;
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      virtual void getParam_(void** param, const std::string& name) {}

      // ------------------------------ load / save to XML/YAML functionality
    protected:
      //! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;

    protected:
      FeatureExtractor* m_featureExt;
      LabelStringMapper m_labelMapper;
      bool m_verbose;
    };


    /** \brief ReferenceSetReductor
    * Optimizes the usage of different classification algorithms by reducing the classifier's internal reference set.
	* \ingroup viulib_dtc 
    */
    class DTC_LIB ReferenceSetReductor : public virtual VObject
    {
      REGISTER_BASE_CLASS(ReferenceSetReductor,dtc)

    public:
      /** \brief Destructor. */
      virtual ~ReferenceSetReductor(){};

      /** \brief	
      *
      *	\param	
      */
      virtual void optimize(viulib::dtc::Classifier *_classifier) = 0;

      // ---------------- set / get inner parameters and variables
    protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      virtual void setParam(const ParamBase& value, const std::string& name)  {};
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      virtual ParamBase getParam(const std::string& name) const { return NullParam(); }
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      virtual void getParam_(void** param, const std::string& name) {}

      // ------------------------------ load / save to XML/YAML functionality
    protected:
      //! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node)  { return false; }
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const  { return false; } 

    };

    /** 
    * \brief Base class providing the general use of Visual Words (VW) dictionaries. It inherits from Classifier in order to learn the VW,
    * but also from FeatureExtractor, in order to directly extract the learned VW from given image and send the 
    * response to further classification (i.e. providing an input for a BOW classification).
    *
    * This class needs a basic FeatureExtractor assigned, whose response is classified into dictionary words.
    * Upon calling FeatureExtraction funcionality, the basic FeatureExtractor is called first and the obtained response 
    * is translated by the dictionary to label numbers. The output of the translator is specified (and reorganized) in the descendent classes.
    *
    * This class is abstract. Create the DictionaryTranslator for basic functionality.
	  * \ingroup viulib_dtc 
    */
    // author: pleskovsky@vicomtech.org
    class DTC_LIB FeatureResponseTranslator: public Classifier, public FeatureExtractor
    {
      REGISTER_BASE_CLASS(FeatureResponseTranslator,dtc)

    public:
      bool load(const cv::FileNode &node) 
      {
        return  Classifier::load(node);
      }
	  
      bool load(const std::string& path) 
      {
         return  Classifier::load(path);
      }
      /*
      bool save(cv::FileStorage &fsPart) const
      { 
      return Classifier::save(fsPart);
      } */

    public:
      /** Constructor */
      FeatureResponseTranslator();
      /** \brief Destructor. */
      virtual ~FeatureResponseTranslator();

      virtual FeatureResponseTranslator* clone() const;

      /** \brief	 Set the inner classificator. Mostly suited for unsupervised classifiers.
      *
      *	\param	_classifier Pointer to the Classifier object
      */
      virtual void setClassifier(Classifier* _classifier);

      /** \brief	 Get the feature extraction methodology
      */
      virtual Classifier* getClassifier();
      virtual const Classifier* getClassifier() const;

      virtual int getClassNumber() const;
      /** \brief	Train the classifier using a given vector of viulib::dtc::TrainSample
      *
      *	\param	_trainData Vector of viulib::dtc::TrainSample containing the training Database
      */
      virtual void train(const std::vector<TrainSample>& _trainData);

      /** \brief	Train unsupervised classifier using a given vector of features (given on each row of the input Descriptor)
      *
      *	\param	_descriptor Container of feature descriptors
      */
      virtual std::vector< ClassificationVector > train(const Descriptor& _descriptor, bool get_all_likelihoods = false); 

      std::vector< ClassificationVector > train(const cv::Mat& _img, SamplingStrategy& sampling, bool get_all_likelihoods = false)
      {
         return Classifier::train(_img, sampling, get_all_likelihoods);
      }

      /** \brief	Classify single descriptor vector.
      *
      *  Only one entity is considered for classification. Use classify() if more entities 
      *  are extracted from the image and should be followingly classified.
      */
      virtual ClassificationVector classify_single(const Descriptor& singleDescriptor, bool get_all_likelihoods = false) const;


	  virtual Descriptor postSampleResponse(std::vector<ClassificationVector> &_classify_result, const cv::Mat &roiImg, const  RegionND& roiRect) const;  

      // ---------------- set / get inner parameters and variables
    protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      //!   The parameter name distinguishes by the prefix "classifier." all parameters which should be forwarded to the inner classifier
      //!   and by the prefix "featureExtractor." those which should be forwarded to the inner feature extractor.
      virtual void setParam(const ParamBase& value, const std::string& name);
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      //!   The parameter name distinguishes by the prefix "classifier." all parameters which should be forwarded to the inner classifier
      //!   and by the prefix "featureExtractor." those which should be forwarded to the inner feature extractor.
      virtual ParamBase getParam(const std::string& name) const;
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      //!   The parameter name distinguishes by the prefix "classifier." all parameters which should be forwarded to the inner classifier
      //!   and by the prefix "featureExtractor." those which should be forwarded to the inner feature extractor.
      virtual void getParam_(void** param, const std::string& name);

      // ------------------------------ load / save to XML/YAML functionality
    protected:
      //! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;
      /* virtual bool saveWrapper(cv::FileStorage &fsPart) const
      {
      return saveInnerData(fsPart);
      }*/

      // Feature Extractor functionality
    protected:
      // runns for whole image; add possible preprocessing
      virtual void precomputeResponse(const cv::Mat &img, SamplingStrategy& sampling);
      // Here, for each roi as given by the sampling strategy:
      // 1) features from ROI are extracted using the base feature extractor (with no inner sampling strategy)
      // 2) the features are classified (class label (ML) or all class likelihoods are be obtained)
      // 3) the class labels or class likelihoods are returned as new Descriptor (correspondingly for each row)
      virtual Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;
      virtual void cleanResponse(const cv::Mat &img, std::vector<Descriptor>& completeDes, SamplingStrategy& sampling);

    public:
      virtual t_data_type getDescriptorType() const;

    protected:
      Classifier* m_classifier;
	  
	  mutable std::vector<int>  m_kpts_2_words;

	  //mutable std::vector<ClassificationVector> tmp_response;
      
	  bool m_use_likelihoods; // uses classifier's likelihoods as output
    };
  

    /** 
    * \brief Base class providing a common framework for training dtc Classifiers with optimized parameters 
    * for a quick and fast evaluation of different classification algorithms implemented in dtc module.
    * response to further classification (i.e. providing an input for a BOW classification).
    *
	  * \ingroup viulib_dtc 
    */
    // author: juandi.ortega@gmail.com



  };
};



#endif // DTC_CLASSIFICATION_H_