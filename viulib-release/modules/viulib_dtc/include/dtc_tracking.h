 /** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

#ifndef DTC_TRACKING_H_
#define DTC_TRACKING_H_

#include "core.h"
#include "dtc_detection.h"
#include "dtc_exp.h"

#define DRAW_COLOR cv::Scalar(0,255,0)
#define DRAW_ROWS cv::Scalar(0,255,0)
#define DRAW_COLS cv::Scalar(0,0,255)

/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/).
*/
namespace viulib
{

	/** \brief This namespace corresponds to the dtc module in VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/)
	* \ingroup viulib_dtc  
	*/
	namespace dtc
	{

		/** \brief Struct containing numbers defining the dimensions of the state vector 
		* \ingroup viulib_dtc
		*/
		struct DTC_LIB StateVectorDimensions
		{			
			int dim_det;						/// Dimensions directly observable
			int deriv_ord;						/// Order of derivatives
			int dim_dyn;						/// Number of dimensions derived
			int dim_total;						/// dim_total = dim_det + dim_dyn*deriv_ord

			/* Default constructor because Tracker needs to have a member of this class */
			StateVectorDimensions()
				: dim_det(0), deriv_ord(0), dim_dyn(0), dim_total(0) {}

			/* Full constructor & default simplified constructor 
			*      e.g. Dimensions( 4, 1, 2 ) like (x,y,w,h,dx,dy) 
			*      e.g. Dimensions( 6, 1, 2 ) like (x,y,w,h,r,s,dx,dy)
			*	   e.g. Dimensions( 2, 2, 2 ) like (x,y,dx,dy,ddx,ddy)
			*	   e.g. Dimensions( 6, 1, 3 ) like (x,y,z,w,l,h,dx,dy,dz)
			*	   e.g. Dimensions( 6, 1, 6 ) like (x,y,z,w,l,h,dx,dy,dz,dw,dl,dh)
			*	   e.g. Dimensions( 2 )		  like (x,y)			
			*/
			StateVectorDimensions( int _dim_det, int _deriv_ord = 0, int _dim_dyn = 0 )			
				: dim_det( _dim_det ), deriv_ord( _deriv_ord ), dim_dyn( _dim_dyn ),	 
				dim_total( dim_det + dim_dyn*deriv_ord ) {}									 			

			void print() const
			{
				printf("StateVectorDimensions: (%d, %d, %d)\n", dim_det, deriv_ord, dim_dyn);
				printf("\tTotal dimension: %d = %d + %d*%d\n", dim_total, dim_det, deriv_ord, dim_dyn);
				printf("\te.g. ("); fflush(stdout);
				for( int i=0; i<dim_det; i++)
				{
					printf("x_%d ", i); fflush(stdout);
				}
				for( int j=0; j<deriv_ord; j++)
				{
					for( int k=0; k<dim_dyn; k++ )
					{
						printf("d%dx_%d ", j+1, k); fflush(stdout);
					}
				}
				printf(")\n");
			}
		};

		/** \brief Track 
		*	Wraps tracking variables
		* \ingroup viulib_dtc  
		*/
		class DTC_LIB Track
		{
			friend class Tracker;							/// Becase Tracker modifies/updates Tracks	

		private:
			// StateVector
			StateVectorDimensions		m_StateVectorDimensions;
			int							m_type;				/// Detection::DETECTION_RECT, ...

		protected:
			// StateVector must be protected to be accesible by Derived classes of Track
			StateVector m_StateVector;					/// StateVector of the Track (not necessarily the same dimension as the Detection's StateVector)
			std::vector<StateVector> m_trajectory;		/// Trajectory: the vector of StateVector

			// Detections & image history
			std::vector<Detection>		m_detections;		/// History of pointers of detections (accesible externally)
			std::vector<cv::Mat>		m_imgPatchHistory;	/// History of images 

			// Life (Tracking-by-detection)
			int m_xNoDet;									/// Number of consecutive frames without associations for this track 
			int m_xDet;									/// Number of consecutive frames with associations for this track
			int m_startFrame;							/// Absolute frame number of initial position
			int m_startFrameNonClutter;					/// Absolute frame number where the Track is no longer clutter
			int m_endFrame;								/// End frame

			// ID
			int							m_ID;		

		public:
			/** \brief Constructor. */
			Track();

			/* General constructor */
			Track( const StateVector& _detSV, const StateVectorDimensions& _dim, int _type );

			/* Simple constructor for (x,y,w,h) without derivatives */
			Track( const cv::Rect& _rect, bool _deriv=true );
			
			/** \brief	Destructor. */
			virtual ~Track(){}

			/** Clone function. */
			//virtual Track* clone() const;

			/** Insert function to concatenate Tracks */
			void insert( const Track* _track );

			// Getters
			int getType() const { return m_type; }
			StateVector getStateVector() const { return m_StateVector; }
			cv::Rect getStateVectorAsRect() const; 

			bool isClutter() const { return (getID()>=0)?(false):(true); }
			int getID() const { return m_ID; }
			int getXNoDet() const { return m_xNoDet; }
			int getXDet() const { return m_xDet; }

			int getStartFrame() const { return m_startFrame; }
			int getStartFrameNonClutter() const { return m_startFrameNonClutter; }
			int getEndFrame() const { return m_endFrame; }

			const std::vector<Detection>&		getRefToDetections()		{ return m_detections; }
			const std::vector<StateVector>&	getRefToTrajectory()		{ return m_trajectory; }
			std::vector<cv::Mat>&		getRefToImgPatchHistory()	{ return m_imgPatchHistory; }		
		};	

		/** \brief Association Function Base class - Implementations must be provided to TrackerDAF
		* \ingroup viulib_dtc  
		*/
		class DTC_LIB AssociationFunction
		{
		public:
			//virtual ~AssociationFunction() {}
			virtual bool operator()( const std::vector<Detection>& _detections, const std::vector<Track*>& _tracks, 
				std::vector<int>& _da ) const = 0;
		};

		/** \brief Tracker 
		* This class has tools for tracking detections on images. Generates and mantain tracks.
		* \ingroup viulib_dtc  
		*/
		class DTC_LIB Tracker: public virtual VObject
		{
			REGISTER_BASE_CLASS(Tracker,dtc)

		public:
			/** \brief	Destructor. */
			virtual ~Tracker(){}	

			virtual std::string getClassName() { return std::string("Tracker");};

			/** \brief	Run the tracker algorithm to track objects		 
			*	\param	_img		Input image 	
			*	\param	_detections	Vector of detections of current time instant
			*	\return	An array of Track objects.
			*/
			virtual std::vector<Track*> track( const cv::Mat& _img, const std::vector<Detection>& _detections ) = 0;

			/** \brief	Draws the current Tracks in a given image
			*	\param	_img_out		Output image 	
			*	\param	_tracks			Vector of tracks to be drawn		 
			*/
			virtual void draw( cv::Mat& _img_out, const std::vector<Track*>& _tracks, const cv::Scalar& _color = DRAW_COLOR, int _thickness = 3 ) = 0;

			/** \brief	Removes a track 
			*
			*	\param	_id Index of the track to remove
			*/
			virtual void rmTrack(int _idx) = 0;

			/** Removes a set of Tracks defined by their idx. */
			virtual void rmTracks( std::vector<int>& _idxToRemove ) = 0;

			/** \brief	Removes a all tracks  
			*/
			virtual void rmAllTracks() = 0;

			/** Filters Small tracks inside larger ones. */
			int filterTracksSmallInsideLarge(std::vector<Track*>& _pTracks); 

			/** Set the current frame number. */
			void setFrameNum( int _frameNum ) { m_frameNum = _frameNum; }

			/** Get a pointer to the tracks */
			virtual std::vector<Track*> getTracks() = 0;
			
		protected:
			/** \brief Set parameters with automatic type conversion. VObject inherrited method.
			 *
			 *	\param value Value of the parameter
			 *	\param	name Name of the parameter
			 *
			 *	 TrackerDAOF: "Data Association with Optical Flow"<br>
			 *		"verbose",					bool,						true to printf traces of the status of the tracker.<br>
			 *		"Proc Size",				cv::Size,					Processing size.<br>	
			 *		"Max Number Objects",		int,						Maximum number of Tracks to mantain (including clutter)	.<br>	 
			 *		"Opflow Mode",				int,						Mode: 0 = OPFLOW_MODE_TRACK: Each Track holds kpts and image patch references, and the optical flow is computed individually inside the Tracker; 1: OPFLOW_MODE_FULLSEARCH: Each Track holds kpts but the image reference (full) is stored in Tracker, the optical flow is computed individually; 2 = OPFLOW_MODE_EXTERN: The optical flow is provided as input to the Tracker, which then manages it for each Track individually.<br>
			 *		"Previous Kpts",			std::vector<cv::KeyPoint>,	Vector of Keypoints for previous image to be provided when Opflow Mode is 2.<br>
			 *		"Current Kpts",				std::vector<cv::KeyPoint>,	Vector of Keypoints for current image to be provided when Opflow Mode is 2.<br>
			 *		"Current Matches",			std::vector<cv::DMatch>,	Vector of matches to be provided when Opflow Mode is 2.<br>
			 *		"Min Detection Size",		cv::Size,					Min. Size of the objects to be detected.<br>
			 *		"Ignore static kpts",		bool,						true for ignoring the kpts that are matched with no motion (useful for moving objects on static background).<br>
			 *		"System Noise",				std::vector<double>,		Vector of double that defines the system noise values to be used in the Kalman Filter (dim = 6) (x,y,w,h,dx,dy).<br>
			 *		"Measurement Noise",		std::vector<double>,		Vector of double that defines the measurement noise values to be used in the Kalman Filter (dim = 4) (x,y,w,h).<br>
			 *		"Upper Bounds",				std::vector<double>,		Vector of double that defines the max. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4) (x,y,w,h).<br>
			 *		"Lower Bounds",				std::vector<double>,		Vector of double that defines the min. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4).<br>
			 *		"Upper Predict",			std::vector<double>,		Vector of double that defines the max. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Lower Predict",			std::vector<double>,		Vector of double that defines the min. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Std Distance",				double,						Value in pixels that defines the standard deviation of the function that associates Detections to Tracks using distance measurement (between top-left corners of the boxes). Low values (like 5) expect Detections close to Tracks; large values (20-50) expect noisy Detections .<br>
			 *		"Std Area",					double,						Standard deviation that defines the function that associates Detections with Tracks according to level of overlap: low values (0.1) expect very similar boxes; large values (0.5) can associate Detections with Tracks even when they do not overlap that much..<br>
			 *		"Max Number Without Detection",			int,			Maximum number of frames without Detection for a Track to survive. If exceeded, the Track is removed.<br>
			 *		"Min Number With Detection",			int,			Minimum number of consecutive frames for which a Track is associated to Detections. Then, the Track is transformed from Clutter to Track.<br>
			 *		"tracking margin",			int,						Value used for default creation of UpperBounds and LowerBounds.<br>
			 *		"Kalman Filter",			bool,						true to activate the smoothing of Tracks with the Kalman Filter.<br>
			 *		"Min Track Confidence",		double,						Minimum confidence value of Tracks to be kept. If the confidence falls below, it is removed. Default is 0.1.<br>
			 *		"draw tracks",				bool,						true to display Tracks in the bottom-left part of the image.<br>
			 *		"draw tracks size",			cv::Size,					Size of the displayed content of Tracks in the bottom-left part of the image.<br>
			 *		"draw trajectory",			bool,						true to display the trajectory of the Tracks.<br>
			 *		"draw clutter",				bool,						true to display also Tracks labelled as Clutter.<br>
			 *		"draw optical flow",		bool,						true to display optical flow of Tracks: WARNING: this will pause video play.<br>
			 *		"detection history",		int,						Number of associated Detections to be stored inside each Track. Default is 1. A larger number can be used for Online Learning.<br>
			 *		"track history",			int,						Number of patch-images stored for each Track (with the position of the Track in the previous images). Default is 1.<br>
			 *		"Remove Track",				int,						Idx of the Track to be removed. This is not the ID, is the idx in the vector of Tracks.<br>			 
			 *
			 *	 TrackerDAAF: "Data Association with Appearance Filter"<br>
			 *		"verbose",					bool,						true to printf traces of the status of the tracker.<br>
			 *		"Proc Size",				cv::Size,					Processing size.<br>	
			 *		"Max Number Objects",		int,						Maximum number of Tracks to mantain (including clutter).<br>
			 *		"Upper Bounds",				std::vector<double>,		Vector of double that defines the max. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4) (x,y,w,h).<br>
			 *		"Lower Bounds",				std::vector<double>,		Vector of double that defines the min. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4).<br>
			 *		"Upper Predict",			std::vector<double>,		Vector of double that defines the max. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Lower Predict",			std::vector<double>,		Vector of double that defines the min. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Std Distance",				double,						Value in pixels that defines the standard deviation of the function that associates Detections to Tracks using distance measurement (between top-left corners of the boxes). Low values (like 5) expect Detections close to Tracks; large values (20-50) expect noisy Detections .<br>
			 *		"Std Area",					double,						Standard deviation that defines the function that associates Detections with Tracks according to level of overlap: low values (0.1) expect very similar boxes; large values (0.5) can associate Detections with Tracks even when they do not overlap that much.<br>
			 *		"Max Number Without Detection",			int,			Maximum number of frames without Detection for a Track to survive. If exceeded, the Track is removed.<br>
			 *		"Min Number With Detection",			int,			Minimum number of consecutive frames for which a Track is associated to Detections. Then, the Track is transformed from Clutter to Track.<br>
			 *		"tracking margin",			int,						Value used for default creation of UpperBounds and LowerBounds.<br>			 			 
			 *		"draw tracks",				bool,						true to display Tracks in the bottom-left part of the image.<br>
			 *		"draw tracks size",			cv::Size,					Size of the displayed content of Tracks in the bottom-left part of the image.<br>
			 *		"draw trajectory",			bool,						true to display the trajectory of the Tracks.<br>
			 *		"draw clutter",				bool,						true to display also Tracks labelled as Clutter	.<br>		 
			 *		"detection history",		int,						Number of associated Detections to be stored inside each Track. Default is 1. A larger number can be used for Online Learning.<br>
			 *		"track history",			int,						Number of patch-images stored for each Track (with the position of the Track in the previous images). Default is 1.<br>
			 *		"detectionsCorrects",		bool,						Activates the correction of the Track with the exact content of associated Detection. Otherwise, template matching is used.<br>
			 *		"inputImg",					cv::Mat,					Input image (color or gray), that can be provided for drawing.<br>
			 *
			 *	 TrackerDAF: "Data Association Filter"<br>
			 *		"Max Number Objects",		int,						Maximum number of Tracks to mantain (including clutter).<br>		 			 			 			 			 
			 *		"Dimensions",				viulib::dtc::StateVectorDimensions,		Struct that defines the dimensions of the problem. See its declaration for details.<br>
			 *		"Linear Gaussian Dynamics",	viulib::dtc::LinearGaussianDynamics*,	Struct that contains the information for the Gaussian Dynamics of the system.<br>
			 *		"Association Function",		viulib::dtc::AssociationFunction*,		Struct that defines the function to be used to match Detections with Tracks.<br> 
			 *																			(Default is viulib::dtc::AssociationFunctionDistance for Detection::DETECTION_RECT types, <br>
			 *																			and viulib::dtc::AssociationFunctionDistance3D for Detection::DETECTION_CUBOID).<br>
			 *																			It can be provided as an derived class of viulib::dtc::AssociationFunction, and passed as:<br>
			 *																				AssociationFunctionCustom myCustomAF( 10.0 );<br>
			 *																				tracker->set(*(viulib::dtc::AssociationFunction*)&myCustomAF, "Association Function");<br>
			 *		"verbose",					bool,						true to printf traces of the status of the tracker.<br>
			 *		"Upper Bounds",				std::vector<double>,		Vector of double that defines the max. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4) (x,y,w,h).<br>
			 *		"Lower Bounds",				std::vector<double>,		Vector of double that defines the min. values of the dimensions of tracks (if exceeded, the track is removed) (dim = 4).<br>
			 *		"Upper Predict",			std::vector<double>,		Vector of double that defines the max. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Lower Predict",			std::vector<double>,		Vector of double that defines the min. values of the prediction of dimensions (dim = 2, (dx,dy)).<br>
			 *		"Max Number Without Detection",			int,			Maximum number of frames without Detection for a Track to survive. If exceeded, the Track is removed.<br>
			 *		"Min Number With Detection",			int,			Minimum number of consecutive frames for which a Track is associated to Detections. Then, the Track is transformed from Clutter to Track.<br>
			 */
			virtual void setParam(const ParamBase& value, const std::string& name) =0;
			//! \brief Get parameters with automatic type conversion. VObject inherrited method.
			virtual ParamBase getParam(const std::string& name) const =0;
			//! \brief direct access to inner parameters, via pointer. VObject inherrited method.
			virtual void getParam(void** param, const std::string& name) =0;

		protected:
			//! \brief	Load and set class parameters. VObject inherrited method.
			virtual bool loadInnerData(const cv::FileNode &node)  { return false; }
			//! \brief	Save class parameters. VObject inherrited method.
			virtual bool saveInnerData(cv::FileStorage &fsPart) const  { return false; } 

		protected:
			// Proxy functions to access Track protected members
			void setTrackID( viulib::dtc::Track& _track, int _ID ) { _track.m_ID =  _ID; }
			void setDetected( viulib::dtc::Track& _track, bool _detected ); 

			void setStateVector( Track& _track, const StateVector& _StateVector );		
			void setStateVector( Track& _track, const cv::Rect& _rect ); 		
			void addDetection( Track& _track, const Detection& _detection ) { _track.m_detections.push_back( _detection ); }
			void rmDetection( Track& _track, const int& _idx ) { _track.m_detections.erase(_track.m_detections.begin() + _idx); }
			void addImgPatchToHistory( Track& _track, const cv::Mat& _img) { _track.m_imgPatchHistory.push_back( _img );}
			void rmImgPatchFromHistory( Track& _track, const int& _idx) 
			{
				if( static_cast<int>(_track.m_imgPatchHistory.size()) > _idx )
					_track.m_imgPatchHistory.erase(_track.m_imgPatchHistory.begin() + _idx);
			}

			int m_detectionType;	/// Detection::DETECTION_RECT, Detection::DETECTION_CUBOID, ...
			int m_frameNum;			/// Current frameNumber
		};

		/** \brief Struct containing all the related variables of Kalman filter
		* \ingroup viulib_dtc  
		*/
		struct DTC_LIB LinearGaussianDynamics
		{
			cv::Mat A;
			cv::Mat H;
			std::vector<double> systemNoise;
			std::vector<double> measurementNoise;

			/* Full constructor */
			LinearGaussianDynamics( const StateVectorDimensions& _svDim,
				const std::vector<double>& _systemNoise, const std::vector<double>& _measurementNoise )
				: systemNoise( _systemNoise ), measurementNoise( _measurementNoise )
			{
				assert( systemNoise.size() == _svDim.dim_total && measurementNoise.size() == _svDim.dim_det );
				buildMatrices( _svDim );			
			}
			/* Shortcout constructor without vectors */
			LinearGaussianDynamics( const StateVectorDimensions& _svDim,
				double _systemNoise, double _measurementNoise )			
			{
				systemNoise = std::vector<double>( _svDim.dim_total, _systemNoise );
				measurementNoise = std::vector<double>( _svDim.dim_det, _measurementNoise );
				buildMatrices( _svDim );			
			}

			void print( const StateVectorDimensions& _svDim )
			{
				std::cout << "A:\n" << A << std::endl;
				std::cout << "H:\n" << H << std::endl;

				printf("System Noise: ("); fflush(stdout);
				for(size_t i=0; i<systemNoise.size(); ++i)
				{
					printf("%f ", systemNoise[i]); fflush(stdout);
				}
				printf(")\n");

				printf("Measurement Noise: ("); fflush(stdout);
				for(size_t i=0; i<measurementNoise.size(); ++i)
				{
					printf("%f ", measurementNoise[i]); fflush(stdout);
				}
				printf(")\n");
			}
		private:
			void buildMatrices( const StateVectorDimensions& _svDim )
			{
				// Set Kalman filter matrices
				A = cv::Mat::eye( _svDim.dim_total, _svDim.dim_total, CV_64F );
				H = cv::Mat::zeros( _svDim.dim_det, _svDim.dim_total, CV_64F );
				for( int j=0; j<_svDim.deriv_ord; j++)	
				{
					for( int k=0; k<_svDim.dim_dyn; k++ )	
					{
						A.at<double>(k, _svDim.dim_det + _svDim.dim_dyn*j + k) = 1 - j*0.5;			
						if( _svDim.deriv_ord == 2 )
							A.at<double>(_svDim.dim_det + k, _svDim.dim_det + _svDim.dim_dyn + k) = 1;			
					}
				}
				for( int i=0; i<_svDim.dim_det; ++i )
					H.at<double>(i,i) = 1;
			}
		};

		typedef enum {TRACKER_OUTPUT_UNCERTAIN, TRACKER_OUTPUT_CORRECT, TRACKER_OUTPUT_OUT_BOUNDS} t_tracker_output;

		/** Function to accumulate Tracks outside Trackers */
		int DTC_LIB storeTracks( const std::vector<viulib::dtc::Track*>& _origTracks, std::vector<viulib::dtc::Track*>& _dstTracks, std::vector<int>& _trackIDVector );

		///** Function to create a single Track from a vector of Tracks.
		// * This function assumes that these tracks have been previously analzyed by their temporal, spatial or appearance similarity.
		// * A single Track is created concatenating several Tracks in order.
		// */
		//void DTC_LIB joinTracks( viulib::dtc::Track* _dstTrack, const std::vector<viulib::dtc::Track*>& _origTracks );

		/** Basic drawing function that draw Track(s) in the given image. */
		void DTC_LIB drawTracks( cv::Mat& _img, const std::vector<viulib::dtc::Track*>& _tracks, const cv::Size2f _scale, cv::Scalar _color, int _thickness );

		/** TODO
		*/
		class DTC_LIB TrackJoiner : public VObject
		{
			REGISTER_GENERAL_CLASS(TrackJoiner,dtc);
		public:
			TrackJoiner(){}
			~TrackJoiner(){}

			std::vector<viulib::dtc::Track*> join( std::vector<viulib::dtc::Track*>& _pTracks );
			
		private:
			/* See the following references for possible implementations of a better algorithm to find the optimum paths:
			* Max flow, min cut theorem: http://en.wikipedia.org/wiki/Max-flow_min-cut_theorem
			* C++ links:
			* http://vision.csd.uwo.ca/code/
			* http://stanford.edu/~liszt90/acm/notebook.html
			* http://www.geeksforgeeks.org/minimum-cut-in-a-directed-graph/
			* http://www.sanfoundry.com/cpp-program-max-flow-min-cut-therem/
			*/

			/* This class creates a Graph from a Matrix containing the weights between nodes and obtains the best paths maximizing the weights.
			*/
			class Graph
			{
			public:
				/* This class represents the Path as a vector of Nodes with associated weights between pairs of Nodes
				*/
				class Path
				{
				public:
					/* This function can be used through addEdge, or directly, to create a Path with a single Node
					*/
					void addNode( int id );
					/* Reverses the order of the nodes inside the vectors.
					*/
					void reversePath();
					/* Adds an edge (i.e. a pair of nodes and the weight between them)
					*/
					void addEdge( int id1, int id2, float w12 );

					/* Computes the weight as the average weight of the path.
					*/
					float getWeight() const;

					std::vector<int> getNodes() const { return nodes; }
					std::vector<float> getWeights() const { return weights; }
					int getNumNodes() const { return static_cast<int>( nodes.size() ); }

					void printf() const;

				protected:		
					void addWeight( float w ) {	weights.push_back( w ); }
				
				private:
					std::vector<int> nodes;
					std::vector<float> weights;	
				};

				Graph( const cv::Mat& _weights );

				std::vector<Path> getBestPaths() const { return bestPaths; }

			private:
				int numNodes;
				std::vector<Path> paths;	
				std::vector<Path> bestPaths;

				void computePaths( const cv::Mat& _weights, std::vector<Path>& _paths );
				void iteration( Path _currentPath, int _currentNode, std::vector<Path>& _pathVector, const cv::Mat& _weights, const cv::Mat& _cols );

			};

			cv::Mat computeLikelihood( std::vector<viulib::dtc::Track*>& _pTracks );
		};
	};
};


#endif //DTC_TRACKING_H_