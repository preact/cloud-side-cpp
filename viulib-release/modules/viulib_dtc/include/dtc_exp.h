/** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

#ifndef DTC_EXP_H_
#define DTC_EXP_H_

// =====================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// =====================================================================================
// When the symbol viulib_dtc_EXPORTS is defined in a project DTC exports functions and 
// classes. In other cases DTC imports them from the DLL.
#ifndef STATIC_BUILD
#ifdef viulib_dtc_EXPORTS
  #if defined _WIN32 || defined _WIN64
    #define DTC_LIB __declspec( dllexport )
  #else
    #define DTC_LIB
  #endif
#else
  #if defined _WIN32 || defined _WIN64
    #define DTC_LIB __declspec( dllimport )
  #else
    #define DTC_LIB
  #endif
#endif
#else
#define DTC_LIB
#endif


// =====================================================================================
// AUTOMATIC DLL LINKAGE
// =====================================================================================
// If the code is compiled under MS VISUAL STUDIO, and viulib_dtc_EXPORTS is not defined
// (i.e. in a DLL client program) this code will link the appropiate lib file, either in 
// DEBUG or in RELEASE
#if defined( _MSC_VER ) && !defined( viulib_dtc_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_dtc_d.lib" )
	#else
		#pragma comment( lib, "viulib_dtc.lib" )
	#endif
#endif



#endif //DTC_EXP_H_
