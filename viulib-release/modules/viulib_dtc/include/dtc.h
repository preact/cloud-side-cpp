
#ifndef _DTC_H_
#define _DTC_H_
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'

/**
 * \defgroup viulib_dtc viulib_dtc
 * viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/legacy/compat.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <list>
#include <typeinfo>
#include <algorithm>

#include "core.h"
#include "dtc_features.h"
#include "dtc_features_filterbanks.h"
#include "dtc_classification.h"
#include "dtc_detection.h"
#include "dtc_tracking.h"
#include "dtc_validation.h"


/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/).
*/
namespace viulib
{

  /** \brief This namespace corresponds to the viulib_dtc module
  * \ingroup viulib_dtc  
  */
  namespace dtc
  {
	
	
	  /** \brief This struct is used to register the factory		
		*	\ingroup viulib_dtc	
		*/
    struct DTC_LIB factory_registration
    {
		  factory_registration();
    };

    static factory_registration dtc_module_init;

	   	

  };
};
/** }@*/
#endif //_DTC_H_
