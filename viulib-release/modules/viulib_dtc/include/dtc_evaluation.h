#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'

/** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

#ifndef DTC_EVALUATION_H_
#define DTC_EVALUATION_H_

#include <iostream>
#include <stdexcept>

// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//Viulib
//#include "system.h"

#include "dtc_exp.h"

namespace viulib
{
namespace dtc
{
	enum GT_ELEM_TYPE{GT_RECT, GT_POINTS, GT_NONE};

    class DTC_LIB GtElement : public cv::Mat
    {		
    public:
      //! Default constructor
      GtElement(): cv::Mat(),m_id(-1), m_type(GT_NONE){}
      //! Matrix 'Copy' constructor; no data conversion is available;
      GtElement(const cv::Mat& _mat, int _id = -1 , GT_ELEM_TYPE _type = GT_POINTS);
      //! Rect 'Copy' constructor
      template <typename T> explicit GtElement(const cv::Rect_<T>& _rect, int _id = -1);
      //! Vector 'Copy' constructor
      template <typename T> explicit GtElement(const std::vector<T>& _coordinates, int _id=-1, GT_ELEM_TYPE _type = GT_POINTS);
      //! Vector of Points 'Copy' constructor
      template <typename T> explicit GtElement(const std::vector<cv::Point_<T> >& _coordinates, int _id=-1);

      //! conversion to another type of structure
      template <typename T> operator cv::Rect_<T>() const;
      template <typename T> operator std::vector<T>() const;
      template <typename T> operator std::vector<cv::Point_<T> >() const;

      /*cv::Rect operator=( GtElement _gt);
      template <typename T>
      std::vector<T> operator=( GtElement _gt);
      std::*/


      // Additional external functions
      int getId(){ return m_id;}

    private:
      GT_ELEM_TYPE m_type;
      int m_id;

      //! Private constructor
      template <typename T> GtElement(const cv::Mat_<T>& _mat, int _id = -1 , GT_ELEM_TYPE _type = GT_POINTS);
    };

    typedef std::vector<std::vector<GtElement> > GtList;
    typedef std::vector<GtElement> PerFrameGtList;

    /** \brief This class provides functions to perform ground tests
    */
    class DTC_LIB GroundTruth
    {
    public:
      GroundTruth();
      //~GroundTruth();


      void setLoadFunction(bool (*gtLoader)(GtList&, const std::string&));

      //! Get GT elements in the specified frame
      std::vector<GtElement> getGtInFrame(const int& _frameNum);

      //! Return the number of different objects
      int getObjectNum(); 
      //! Return the total number of annotations
      int getTotalAnnotationsNum();

      //! Return the number of frames with ground truth annotated
      int getNumFrames() const { return (int) m_gt.size(); }

      bool loadGroundTruth(const std::string& _xmlPath);
      //! Draw GroundTruth in the specified frame
      void drawGtInFrame(cv::Mat& _image, const int& _frameNum, const cv::Scalar _color = cv::Scalar(0, 255, 0)); 
      //! Draw All ground Truth in an image
      void drawAccumutaledGt(cv::Mat& _img, const int& _levels = 0);
      //! Prune Rectangles smaller than the parameter given
      void pruneRects(const cv::Size& _minRectSize);
    private:

      //! Function for Ground Truth loading
      bool defaultGtLoader(GtList& _gt, const std::string& _xmlPath);

      //! List of Ground Truth elements
      GtList m_gt;

      //! Video information
      cv::Size m_imgSize;

      //! Computation Parameters
      cv::Size m_procSize;

      // Pointer to loader function
      bool (*m_gtLoader)(GtList&, const std::string&);
    };

    template <typename T> GtElement::GtElement(const cv::Mat_<T>& _mat, int _id , GT_ELEM_TYPE _type)
    {
      assert(_mat.channels()==1 && "Only 1-channel matrix are suported");
      if (_type == GT_POINTS)
      {
        assert((_mat.rows == 2 || _mat.cols==2) && "Invalid matrix format: 2xN or Nx2 dimentions only");

        cv::Mat m;
        if (_mat.cols == 2)
          m = _mat.t();

        m.copyTo(*this);
        m_type = _type;
      } 
      else if(_type == GT_RECT)
      {
        assert((_mat.rows==1 && _mat.cols==4) || (_mat.rows==4 && _mat.cols==1)  && "Invalid matrix format: 1x4 or 4x1 dimentions only");

        std::vector<T> v;
        _mat.copyTo(v);
        (*this)=GtElement(v,_id,_type);
        m_type = _type;
      }

      m_id = _id;
    }

    //! Rect 'Copy' constructor
    template <typename T> GtElement::GtElement(const cv::Rect_<T>& _rect, int _id)
    {
      // Order of Points
      //    1       2
      //    ---------
      //    |       |
      //    |       |
      //    |       |
      //    ---------
      //    4       3

      std::vector<cv::Point_<T> > vec;
      vec.push_back(_rect.tl());
      vec.push_back(_rect.tl() + cv::Point_<T>(_rect.width,0));
      vec.push_back(_rect.br());
      vec.push_back(_rect.tl() + cv::Point_<T>(0, _rect.height));

      cv::Mat a(vec);
      a = a.reshape(1).t();
      a.copyTo(*this);

      m_id = _id;
      m_type = GT_RECT;
    };

    //! Vector 'Copy' constructor
    template <typename T> inline GtElement::GtElement(const std::vector<cv::Point_<T> >& _coordinates, int _id)
    {
      cv::Mat a(_coordinates);
      a = a.reshape(1).t();
      a.copyTo(*this);
      m_type = GT_POINTS;
      m_id = _id;
    };

    //! Vector of Points 'Copy' constructor
    template <typename T> inline GtElement::GtElement(const std::vector<T>& _coordinates, int _id, GT_ELEM_TYPE _type)
    {
      if (_type == GT_RECT)
      {
        assert(_coordinates.size()==4); 
        cv::Rect_<T> r( _coordinates[0], _coordinates[1], _coordinates[2], _coordinates[3]);

        GtElement g(r, _id);
        g.copyTo(*this);
        m_type = GT_RECT;
      } 
      else if (_type == GT_POINTS)
      {
        assert(_coordinates.size()%2==0);

        std::vector<cv::Point_<T> > points;
        for (size_t i=0; i<_coordinates.size(); i=i+2)
        {
          points.push_back(cv::Point_<T>(_coordinates[i], _coordinates[i+1]));
        }

        GtElement g(points, _id);
        g.copyTo(*this);
        m_type = GT_POINTS;
      }
      m_id = _id;
    };

    //! Definition for automatic transformation to Rect_<type>
    template <typename T> GtElement::operator cv::Rect_<T>() const
    {
      cv::Mat _x((*this)(cv::Rect(0,0, (*this).cols, 1)));
      cv::Mat _y((*this)(cv::Rect(0,1, (*this).cols, 1)));

      double xMin=0, xMax=0, yMin=0, yMax=0;
      minMaxIdx(_x, &xMin, &xMax);
      minMaxIdx(_y, &yMin, &yMax);

      cv::Rect_<T> r( cv::Point_<T>((T)xMin,(T)yMin), cv::Point_<T>((T)xMax,(T)yMax));
      return r;
    }

    //! Definition for automatic transformation to vector<type>
    template <typename T> GtElement::operator std::vector<T>() const
    {
      std::vector<T> v;
      for (int i =0; i<(*this).cols; i++)
      {
        std::vector<T> col;
        cv::Mat m = (*this).col(i).t();
        m.copyTo(col);
        for (size_t j=0; j<col.size(); j++)
          v.push_back(col[j]);
      }
      return v;
    }

    //! Definition for automatic transformation to vector<type>
    template <typename T> GtElement::operator std::vector<cv::Point_<T> >() const
    {
      std::vector<cv::Point_<T> > v;
      for (int i =0; i<(*this).cols; i++)
      {
        std::vector<T> col;
        cv::Mat m = (*this).col(i).t();
        m.copyTo(col);
        cv::Point_<T> pt(col[0], col[1]);
        v.push_back(pt);
      }
      return v;
    }


    /** \brief This class provides tools to evaluate performance of algorithms
    */
    class DTC_LIB Evaluation
    {
    public:
      Evaluation();
      void evaluate( const std::vector<cv::Rect>& _input, const std::vector<GtElement>& _groundTruth, long long _procTimeMs = -1 );

      std::string print()
      {
        if( !m_updated )
          recomputeRP();
        std::stringstream ss;
        ss.precision(3);
        if( m_accProcTimeMs == 0 )
          ss << "(R, P, F)= " << "(" << m_recall << ", " << m_precision << ", " << m_fmeasure << ") TP: " << m_TP << " FP: " << m_FP << " FN: " << m_FN << std::endl;
        else
        {
          ss << "(R, P, F, T)= " << "(" << m_recall << ", " << m_precision << ", " << m_fmeasure << ") TP: " << m_TP << " FP: " << m_FP << " FN: " << m_FN << " T(ms): " << m_avgProcTimeMs << std::endl;
        }
        return ss.str();
      }

    private:
      // Functions
      void increaseTP() { m_TP++; m_updated = false;  }
      void increaseTP( int _tp ) { m_TP += _tp; m_updated = false;}
      void increaseFP() { m_FP++;  m_updated = false;}
      void increaseFP( int _fp ) { m_FP += _fp; m_updated = false;}
      void increaseFN() { m_FN++;  m_updated = false;}
      void increaseFN( int _fn ) { m_FN += _fn; m_updated = false;}

      void setPositivesComputeFP( int _p, int _tp ) { increaseFP( _p - _tp ); m_updated = false;}
      void recomputeRP() 
      { 
        if( m_TP > 0 )
        {
          m_recall = static_cast<double>(m_TP) / static_cast<double>(m_TP + m_FN); 
          m_precision = static_cast<double>(m_TP) / static_cast<double>(m_TP + m_FP); 
          m_fmeasure = 2*(m_precision*m_recall)/(m_precision + m_recall);
        }
        m_updated = true;
      }


      //! Function for comparison between detections and ground truth
      bool comparison(const cv::Rect& _detBox, const cv::Rect& _gtBox);
      bool defaultComparison(const cv::Rect& _detBox, const cv::Rect& _gtBox);

      // Pointer to comparison function		
      bool (*m_comparison)(const cv::Rect& _detBox, const cv::Rect& _gtBox);

      // Variables
      bool m_updated;

      // Qualitative analysis
      int m_TP;		/// True Positives
      int m_FP;		/// False Positives
      int m_FN;		/// False Negatives
      double m_recall;
      double m_precision;
      double m_fmeasure;

      // Time analysis
      int m_timeFrames;
      long long m_accProcTimeMs;
      double m_avgProcTimeMs;		
   };
};
};

#endif // DTC_EVALUATION_H_
