/** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

// author: pleskovsky@vicomtech.org
//
// see http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
//Disable MSVC warning C4482: nonstandard extension used: enum 'xyz' used in qualified name
//It becomes a standard since C++11, and MSVC11 removed the warning.
#pragma warning( disable:4482 )
#endif


#ifndef DTC_FEATURES_FILTERBANKS_H_
#define DTC_FEATURES_FILTERBANKS_H_

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>

#include "dtc_features.h"


namespace viulib
{	
namespace dtc
{
		 	
	//----------------------------------------------------------------------------------------------
	/** \brief Structure for filter bank parameters
	* \ingroup viulib_dtc
	*/
	struct DTC_LIB t_FilterBankParams
	{
		cv::Size winSize;// window size for each filter (2x radius + 1)

		int rotInvScales;// filters should be grouped by scale number
		int rotDepScales;// filters should be grouped by scale number

		enum rotationInvarianceMethod{ NONE, MEAN, MIN_RESPONSE, MAX_RESPONSE, GRADIENT };
		typedef rotationInvarianceMethod t_rotInvMethod;
		int nRotations;// number of rotations considered for each filter, within the rotDepFilters container
		t_rotInvMethod rotInvMethod;//rotationInvarianceMethod 

		bool colorMode;
	};	 

	class FilterBankHelper;
	/** \brief	This class computes the response of a general filter bank, run over a given image.
	*
	*   A filter bank represents a collection of general kernel filters (currently of a equal size).
	*	A response of a filter bank is obtained by filtering the input image by each kernel. 
	*   In fact, the cross-correlation coefficient is obtained for each pixel and each filter applied.
	*
	*   Two sets of filter banks are maintained: rotation invariant and rotation dependent filters.
	*   Functionality for generating rotations or different scales of inserted filters is provided.
	*   
	*   This class uses teh TBB library (if enabled) for accelerating the response computation.
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatFilterBank : public FeatureExtractor
	{
      REGISTER_BASE_CLASS(FeatFilterBank,dtc)

      friend class FilterBankHelper;
	public:
		typedef cv::Mat_<float> t_filterMat;
		typedef std::vector< t_filterMat > t_filterBank;
		typedef t_FilterBankParams::t_rotInvMethod t_rotInvMethod;

	protected:
		//FilterBank Parameters
		t_FilterBankParams m_fb_params;

	public:
		//! Constructor
		//!
		FeatFilterBank();

		//! Constructor
		//!
		//! \param win_size - the window size of each kernel filter (= filter kernel radius * 2 + 1)
		//! \param r_inv_scales - number of scales of rotation invariant filters, used for understanding of the internal feature bank organization
		//! \param r_dep_scales - number of scales of rotation dependent filters, used for understanding of the internal feature bank organization
		//! \param rotations - number of rotations present for each rotation dependent filter, used for understanding of the internal feature bank organization
		//! \param rotation_invariance_method - method for evaluating the different rotations of each filter in the rotation dependent filter bank. Values:
		//!								- NONE: respose for all rot. filters is obtained
		//!								- MEAN: the mean of the responses, obtained for all rotations of a given filter, is returned
		//!								- MIN_RESPONSE: the minimum of the respons, obtained for all rotations of a given filter, is returned
		//!								- MAX_RESPONSE: the maximum of the respons, obtained for all rotations of a given filter, is returned
		//!								- GRADIENT: the gradient of the image is used to select the appropriate rotation of a given filter, and only the response of this is returned
		//! \param color_mode - processing of the input image as multiple channel image or as a gray-scale image (transforms the input image if necessary)
		//!					- true, if we consider the input images in multiple component color mode and processing should be run on each channel separately
		//!                 - false, if the processing should be run on a gray-scale transformed input image.
		FeatFilterBank (const cv::Size& win_size, 
			int r_inv_scales, int r_dep_scales, 
			int rotations, t_rotInvMethod rotation_invariance_method, 
			bool color_mode = false);

		//! Destructor
		virtual ~FeatFilterBank();

		//virtual FeatFilterBank* Copy() = 0;// soft copy - for parallel feature extraction
		//virtual FeatFilterBank* clone() = 0;// const ! hard copy everything

      virtual void generateFB() = 0;

		//! Set FilterBank parameters
		//! \sa FeatFilterBank(...) constructor for parameter description
		void setFilterBankParameters (const cv::Size& win_size, 
			int r_inv_scales, int r_dep_scales, 
			int rotations, t_rotInvMethod rotation_invariance_method,
			bool color_mode = false);
		void setFilterBankParameters (const t_FilterBankParams& params);

		//! Get FilterBank parameters
		t_FilterBankParams getFilterBankParams() const;

		// INHERITED FUNCTIONS
	private:
		//! \brief Precomputing the response of all filters
		//! 
		//! It generates an image of the img.size for each filter, storing its response per pixel.
		//! Only the SamplingStrategy::NONE is used during this process. Other sampling is not considered.
		virtual void precomputeResponse(const cv::Mat& img, SamplingStrategy& sampling) = 0; 
		//! \brief Groups the precomputed filter responses by pixel. Selected Sampling Strategy is considered at this point.
		virtual Descriptor sampleResponse(const cv::Mat& roiImg, const  RegionND& roiRect) const = 0; 
		//! \brief Cleans the unnecessary precomputed data. Internal use only.
		virtual void cleanResponse(const cv::Mat &img, std::vector<Descriptor>& completeDes, SamplingStrategy& sampling) = 0;

	public:
		//! ATTENTION 
		//! \brief Returns the number of filters applied per pixel. 
		//!
		//! Based on the flag m_fb_params.colorMode, it considers either 3 component (RGB) or 1 component (Grayscale)
		//! input. The number of filters is multiplied by this number of components, as the output descriptor will be 
		//! obtained for each color component. The responses are grouped by pixel, the color responses are thus consecutive.
      //! This function is used to reserve memory for parallel processing;
		virtual int  getDescriptorLength(const cv::Mat& img) const = 0;    
		virtual t_data_type getDescriptorType() const = 0;

		// HELPER FUNCTIONS
		//! Insert filter in the bank and return its index number [0-counted]
		virtual int insertFilter( t_filterMat filter, bool is_rotation_invariant = true) = 0;

		//! Generates downscaled versions of filters in the bank; the new filters keep the same window size;
		//! \param r_inv_scales  number of scales to be generated for filters in rotation invariant filter bank
		//! \param r_inv_sigma_scale scaling sigma used for the generation of rot. invariant filters (gaussian function of this sigma is used)
		//! \param r_dep_scales  number of scales to be generated for filters in rotation dependent filter bank
		//! \param r_dep_sigma_scale_x scaling sigma used for the generation of rot. dependent filters in x-direction (gaussian function of this sigma is used)
		//! \param r_dep_sigma_scale_y scaling sigma used for the generation of rot. dependent filters in y-direction (gaussian function of this sigma is used)
		//!                    For default value (0.0 is specified), r_dep_sigma_scale_y = r_dep_sigma_scale_x is used.
		virtual void generateFilterBankScales( int r_inv_scales, double r_inv_sigma_scale,
			int r_dep_scales, double r_dep_sigma_scale_x,
			double r_dep_sigma_scale_y = 0.0) = 0;

		//! \brief Generates rotated variants of filters given in rotDepFilters
		//! \param crop - crops the output to win_size
		//! at the end, the rotations will be grouped by filter
		//! 
		//! Output is CW ordered, equidistantly spaced (wrt angle), filters 
		//! Considering only rotations up to 180 ... after 180 the response magnitudes are equal, but differ only in the sign !
		virtual void generateFilterBankRotations( int number_rotations, bool crop = false, cv::Size crop_size = cv::Size(0,0) ) = 0;

		//! Access the filter bank directly
		virtual t_filterBank& getFilterBank(bool rotation_invariant) = 0;
		virtual const t_filterBank& getFilterBank(bool rotation_invariant) const = 0;

		//! Reprojects the given filter response to the original image space
		virtual Descriptor reprojectResponse(const Descriptor& data) const = 0;

      //INHERRITED functions
   protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      virtual void setParam(const ParamBase& value, const std::string& name)  {}
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      virtual ParamBase getParam(const std::string& name) const    { return NullParam(); }
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      virtual void getParam_(void** param, const std::string& name) {}
   protected:
		//! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;

   public:
		// DEBUG FUNCTIONS
		//! Debugging purpose function - filter bank visualization.
		virtual void saveFilterBankImages(std::string base_name) const = 0;    
		//! Debugging purpose function - filter bank visualization.
		virtual void saveFilterBankImagesAll(std::string base_name) const = 0;
	};

	/** \brief Generates filter bank objects with commonly used filters (like Gabors, Leung-Malik, Schmid and Maximum Response 8).
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FilterBankHelper
	{
	public:
		//! Create empty filter bank object.
		static cv::Ptr<FeatFilterBank> CreateEmptyFB(const cv::Size& win_size = cv::Size(49,49), bool color_mode = false);// empty filter bank
		//! Create empty filter bank object.
		static cv::Ptr<FeatFilterBank> CreateEmptyFB(const cv::Size& win_size, 
			int r_inv_scales, int r_dep_scales, 
			int rotations, FeatFilterBank::t_rotInvMethod rotation_invariance_method,
			bool color_mode = false);// empty filter bank

		//! Create MR8 filter bank object.
		static cv::Ptr<FeatFilterBank> CreateMaximumResponse8FB(const cv::Size& win_size = cv::Size(49,49), bool color_mode = false);//MR8
		//! Create Leung-Malik filter bank object.
		static cv::Ptr<FeatFilterBank> CreateLeungMalikFB(const cv::Size& win_size = cv::Size(49,49), bool color_mode = false);
		//! Create Schmid filter bank object.
		static cv::Ptr<FeatFilterBank> CreateSchmidFB(const cv::Size& win_size = cv::Size(59,59), bool color_mode = false);
		//! Create Gabor filter bank object.
		static cv::Ptr<FeatFilterBank> CreateGaborFB(const cv::Size& win_size = cv::Size(49,49), bool color_mode = false);
		//! Create Piters filter bank object.
		static cv::Ptr<FeatFilterBank> CreatePitersFB(const cv::Size& win_size = cv::Size(49,49), bool color_mode = false);// my selection

		//! Generate MR8 filter bank.
      static void GenerateMaximumResponse8FB( FeatFilterBank* filterBank );
		//! Generate Leung-Malik filter bank.
      static void GenerateLeungMalikFB( FeatFilterBank* filterBank );
		//! Generate Schmid filter bank.
      static void GenerateSchmidFB( FeatFilterBank* filterBank );
		//! Generate Gabor filter bank.
      static void GenerateGaborFB( FeatFilterBank* filterBank );
		//! Generate Piters filter bank.
      static void GeneratePitersFB( FeatFilterBank* filterBank );// my selection
	};

}
}
#endif // DTC_FEATURES_FILTERBANKS_H_

