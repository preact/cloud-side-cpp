/** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

// author: pleskovsky@vicomtech.org

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef DTC_SAMPLING_H_
#define DTC_SAMPLING_H_

#include <opencv2/core/core.hpp>

#include "core.h"
#include "dtc_exp.h"
 
#include "extended_mat.h"
#include "regionND.h"

namespace viulib
{
namespace dtc
{
	/** \brief This class describes the interface for different geometric sampling strategies of an input image 
   * It can be used as base sampling ofr which the regions are inserted from outside (i.e. not via the generate function).
   *
   *  This class is intentionally set abstract. Use the ManualSampling class for using the basic functionality.
   *
	* \ingroup viulib_dtc
	*/
	class DTC_LIB SamplingStrategy
	{
   public: 
      typedef std::vector<RegionND> RegionContainerType;
      typedef RegionContainerType::const_iterator const_iterator;
      typedef RegionContainerType::iterator iterator;

   public:
		SamplingStrategy():extractROIs(true), old_image_size(0,0) {}
      SamplingStrategy(const std::vector<RegionND>& _regions):extractROIs(true),regions(_regions), old_image_size(0,0) {}
      SamplingStrategy(const SamplingStrategy& other): extractROIs(other.extractROIs), regions(other.regions), old_image_size(other.old_image_size) {}
      virtual ~SamplingStrategy() { clear(); }

      virtual SamplingStrategy* clone() const = 0;

      //! \brief Should the sampling strategy have the flag extractImageROIs() on, 
      //!        the regions of interest will be extracted out of the original input image and passed to the feature extractor.
      //!        In other case, the whole image is passed and only the roiRect parameters are forwarded to the feature extractor in order considered the defined ROI rectangles.
      //!        Note -> the option of either passing only the image coordinates or pasing as well the extracted images is implemented for 
      //!        optimization issues, for feature extractors which need to access different data but the basic image.
      bool extractImageROIs() const { return extractROIs; }
      void setExtractImageROIs(bool val = true) { extractROIs = val; }

      virtual void clear() { regions.clear(); }
      virtual bool empty() const { return regions.empty(); }
      virtual size_t size() const { return regions.size(); }
      virtual void resize(size_t sz) { return regions.resize(sz); }

      virtual RegionContainerType& getRegions() { return regions; }
      virtual const RegionContainerType& getRegions() const { return regions; }

      virtual iterator begin() { return regions.begin(); }
      virtual const_iterator begin() const { return regions.begin(); }
      virtual iterator end() { return regions.end(); }
      virtual const_iterator end() const { return regions.end(); }

      void reserve(size_t size) { regions.reserve(size); }
      virtual void push_back(RegionND region) {region.sampling_id = regions.size(); regions.push_back(region); }
      virtual RegionND pop_back() { RegionND r= regions.back(); regions.pop_back(); return r; }
      virtual void erase(size_t i) {
         regions.erase(regions.begin()+i);
      }
      virtual void erase(iterator& it) {
         regions.erase(it);
      }

      virtual RegionND& get(size_t i) { return regions[i]; }
      virtual const RegionND& get(size_t i) const { return regions[i]; }
      virtual RegionND& operator[](size_t i) { return regions[i]; }
      virtual const RegionND& operator[](size_t i) const  { return regions[i]; }
            
      cv::Rect get2D( size_t i ) const {
         if (i<size()) 
            return project2D( get(i) ); 
         return cv::Rect(); 
      }
      // this function should be re-implemented in the descendants
      virtual cv::Rect project2D( const RegionND& region) const { 
         return region; // by default use region's conversion
      } 

      // this functions should be re-implemented in the descendants
      // return number of created regions
      virtual size_t generate(cv::Size image_size) = 0;
      // pure geometrical approach does not teed the image itself
      // default imlplementation - must be redefinedin descendants 
      // virtual size_t generate(cv::Mat image){ return generate(image.size()); }
      virtual size_t generate(cv::Mat image) = 0; 

      virtual int LUT(int i, int j) const { return i+j; } // no sense for base class
      virtual int LUT(int i, int j, int k) const { return i+j+k; } // no sense for base class
      /// expects row vector
      virtual int LUT(const cv::Mat_<int>& indices) const { 
         return -1;

         int out = 0;
         for (cv::MatConstIterator_<int> it = indices.begin(); it != indices.end(); it++)
            out += (int) *it; 
         return out;
      } // no sense for base class

      // return size of a theoretical image, which could represent the LUT in 2D .. for debug/draw functionality
      virtual cv::Size LUTSize2D() const {
         return cv::Size(1, int(regions.size()));
      }
      // return size of a theoretical volume, which could represent the LUT in 3D.. for debug/draw functionality
      virtual cv::Matx<int, 1, 3> LUTSize3D() const {
         return cv::Matx<int, 1,3>(1, 1, int(regions.size()));
      }

   protected: 
      cv::Size old_image_size;
      bool extractROIs;
      RegionContainerType regions;
   };

   //! \brief SamplingStrategy adaptor which annulates the generate() function, i.e. locks the generation step.
   //! Note that the regions can still be added/removed to this object via the push_back/pop_back/erase methods.
   //!  
   //!  Used within MultipleFeatureAdaptor for generating the regions of interest only once for all features.
   //! \ingroup viulib_dtc
	class DTC_LIB LockedSamplingAdaptor : public SamplingStrategy
   {
   public:
      LockedSamplingAdaptor(SamplingStrategy& strategy): inner(&strategy) {}
      virtual LockedSamplingAdaptor* clone() const  { return new LockedSamplingAdaptor(*(this->inner)); }

      void clear() { inner->clear(); }
      bool empty() const { return inner->empty(); }
      size_t size() const { return inner->size(); }
      void resize(size_t sz) { return inner->resize(sz); }

      RegionContainerType& getRegions() { return inner->getRegions(); } // TODO - return copy
      const RegionContainerType& getRegions() const { return inner->getRegions(); }

      iterator begin() { return inner->begin(); } // TODO - return copy
      const_iterator begin() const { return inner->begin(); }
      iterator end() { return inner->end(); } // TODO - return copy
      const_iterator end() const { return inner->end(); }

      void push_back(RegionND region) { } //skip { inner->push_back(region); 
      RegionND pop_back() { return RegionND(); }//skip { return inner->pop_back(); 
      void erase(size_t i) { } //skip { inner->erase(i); }
      void erase(iterator& it) { } //skip { inner->erase(it); }

      RegionND& get(size_t i) { return inner->operator[](i); } // TODO - return copy
      const RegionND& get(size_t i) const { return inner->operator[](i); }
      RegionND& operator[](size_t i) { return inner->operator[](i); } // TODO - return copy
      const RegionND& operator[](size_t i) const  { return inner->operator[](i); }
            
      virtual cv::Rect project2D( const RegionND& region) const { return inner->project2D(region); }

      /** \brief  Skips generate. Does nothing. */
      virtual size_t generate(cv::Size image_size) { return inner->size(); } 
      virtual size_t generate(cv::Mat image) { return inner->size(); } 

      virtual int LUT(int i, int j) const { return inner->LUT(i,j); }
      virtual int LUT(int i, int j, int k) const { return inner->LUT(i,j,k);  }
      virtual int LUT(const cv::Mat_<int>& indices) const { return inner->LUT(indices); }

      virtual cv::Size LUTSize2D() const { return inner->LUTSize2D(); }
      virtual cv::Matx<int, 1, 3> LUTSize3D() const { return inner->LUTSize3D(); }

   private:
      SamplingStrategy* inner;
   };

   /** \brief Generates one sampling region corresponding to the entire image. 
	\ingroup viulib_dtc
	*/
	class DTC_LIB BaseImageSampling:public SamplingStrategy
	{
   public:
      static BaseImageSampling entireImage;

		BaseImageSampling(float scale = 1.0f):imgScale(scale) { extractROIs = false; }
      BaseImageSampling(const BaseImageSampling& other):SamplingStrategy(other), imgScale(other.imgScale) { extractROIs = false; }
      virtual ~BaseImageSampling() {};
               
      virtual BaseImageSampling* clone() const  { return new BaseImageSampling(*this); }
   public:
      virtual size_t generate(cv::Size image_size);
      virtual size_t generate(cv::Mat image){ return generate(image.size()); }

   protected:
      float imgScale;
   };

   /** \brief Here, the generate() method does nothing. The class uses the manually inserted regions, 
   *    via push_back and pop_back, to run the parallel feature extraction on them.
   *    The regions should be inserted either before calling feature(run, manualSampling), or within the 
   *    featureExtractor->precomputeResponse(...) method.
   */
	class DTC_LIB ManualSampling:public SamplingStrategy
	{
   public:
      ManualSampling() {}
      ManualSampling(const std::vector<RegionND>& _regions):SamplingStrategy(_regions) {}
      ManualSampling(const ManualSampling& other):SamplingStrategy(other) {}
      virtual ~ManualSampling() {}
               
      virtual ManualSampling* clone()  const { return new ManualSampling(*this); }
   public:
      virtual size_t generate(cv::Size image_size) { return regions.size(); }
      virtual size_t generate(cv::Mat image){ return generate(image.size()); }
   };
   
    /** \brief Generates sliding window sampling for window of specified size (width, height) and given step (dx, dy). */
	class DTC_LIB SlidingWindows:public SamplingStrategy
	{
   public: 
      typedef SamplingStrategy::RegionContainerType RegionContainerType;
      typedef RegionContainerType::const_iterator const_iterator;
      typedef RegionContainerType::iterator iterator;

   private:
		SlidingWindows() {}
   public:
		SlidingWindows( int win_w, int win_h, int step_w, int step_h): 
         m_size(cv::Vec2i(win_w, win_h)), m_step(cv::Vec2i(step_w, step_h)), LUTsteps( cv::Mat_<int>::zeros(1,2) ) {}
		SlidingWindows( const RegionND& _size, const RegionND& _step): 
         m_size(_size), m_step(_step), LUTsteps( cv::Mat_<int>::zeros(1, int(_size.dimension())) ) { m_size.row(0) *= 0; m_step.row(0) *= 0; }
      template <typename Tp>
		SlidingWindows( const cv::Size_<Tp>& _size, const cv::Size_<Tp>& _step):
         m_size( _size), m_step( _step), LUTsteps( cv::Mat_<int>::zeros(1,2) ) {}
      template <typename Tp, int n>
		SlidingWindows( const cv::Vec<Tp,n>& _size, const cv::Vec<Tp,n>& _step): 
         m_size( _size), m_step( _step), LUTsteps( cv::Mat_<int>::zeros(1,n) ) {}
      SlidingWindows( const SlidingWindows& other):SamplingStrategy(other), 
         m_size( other.m_size), m_step( other.m_step), LUTsteps(other.LUTsteps) {}

      virtual ~SlidingWindows() {};
            
      virtual SlidingWindows* clone() const  { return new SlidingWindows(*this); }
      
      virtual size_t generate(cv::Size image_size);
      virtual size_t generate(cv::Mat image){ return generate(image.size()); }

      virtual int LUT(int i, int j) const;
      virtual int LUT(int i, int j, int k) const;
      virtual int LUT(const cv::Mat_<int>& indices) const;

      // return size of a theoretical image, which could represent the LUT in 2D .. for debug/draw functionality
      virtual cv::Size LUTSize2D() const;
      // return size of a theoretical volume, which could represent the LUT in 3D.. for debug/draw functionality
      virtual cv::Matx<int, 1, 3> LUTSize3D() const;

   protected: 
      RegionND m_size, m_step;
      // TODO - use cv::Size ? - directly only for 2D ?
      viulib::ExtendedMat LUTsteps;
   };


   /** \brief Considering regions specified by center point and characteristic scale.
   *    The corresponding region for each 2D point is: x-(width*scale)/2, y-(height*scale)/2, (width*scale), (height*scale).
   *    In general the width, height are 1,1 - corresponding to 1 pixel. 
   *    If necesasry, the width and height of characteristic region at scale 1 can be specified upon inserting RegionND.
   * 
   * \ingroup viulib_dtc
   */
	class DTC_LIB PointSampling:public ManualSampling
	{
   public:
      PointSampling() {}
      PointSampling(const std::vector<RegionND>& _regions):ManualSampling(_regions) {}
      PointSampling(const PointSampling& other):ManualSampling(other) {}
      virtual ~PointSampling() {}
               
      virtual PointSampling* clone() const  { return new PointSampling(*this); }
   public:
            
      // define rect based on region scale.
      virtual cv::Rect project2D( const RegionND& region) const { 
         cv::Rect out(region);
         out.width *= int(ceil(region.scale));
         out.height *= int(ceil(region.scale));
         out.x -= int(out.width/2.0);
         out.y -= int(out.height/2.0);
         return out; 
      } 
   };

	class DTC_LIB RandomPointSampling:public PointSampling
	{
   public:
      RandomPointSampling(int scales = 1, float _scale_start = 1.0f, float _scale_multiplicator = 2.0f):
                           nr_scales(scales), scale_start(_scale_start), scale_multiplicator(_scale_multiplicator) {}
      RandomPointSampling(const RandomPointSampling& other):PointSampling(other),
                           nr_scales(other.nr_scales), scale_start(other.scale_start), scale_multiplicator(other.scale_multiplicator) {}
      virtual ~RandomPointSampling() {}

      virtual RandomPointSampling* clone() const  { return new RandomPointSampling(*this); }

   protected:               
      RandomPointSampling(const std::vector<RegionND>& _regions):PointSampling(_regions),
         nr_scales(1), scale_start(1.0f), scale_multiplicator(2.0f) {}

   public:
       virtual size_t generate(cv::Size image_size) {return 0;}
       virtual size_t generate(cv::Mat image){ return generate(image.size()); }
   private:
      int nr_scales;
      float scale_start;
      float scale_multiplicator;
   };

	class DTC_LIB RegularPointSampling:public PointSampling
	{
   public:
      RegularPointSampling(int _step_x, int _step_y, int scales = 1, float _scale_start = 1.0f, float _scale_multiplicator = 2.0f):
                           step_x(_step_x), step_y(_step_y), nr_scales(scales), scale_start(_scale_start), scale_multiplicator(_scale_multiplicator) {}

      RegularPointSampling(const RegularPointSampling& other):PointSampling(other),
                           step_x(other.step_x), step_y(other.step_y), nr_scales(other.nr_scales), scale_start(other.scale_start), scale_multiplicator(other.scale_multiplicator) {}
      virtual ~RegularPointSampling() {}

      virtual RegularPointSampling* clone() const  { return new RegularPointSampling(*this); }
   protected:
      RegularPointSampling(const std::vector<RegionND>& _regions):PointSampling(_regions),
                 step_x(0), step_y(0), nr_scales(1), scale_start(1.0f), scale_multiplicator(2.0f) {}
               
   public:
       virtual size_t generate(cv::Size image_size) {return 0;}
       virtual size_t generate(cv::Mat image){ return generate(image.size()); }

   private:
      int step_x, step_y;
      int nr_scales;
      float scale_start;
      float scale_multiplicator;
   };

   // GaussianSamplingAdaptor
}
}
#endif //DTC_SAMPLING_H_