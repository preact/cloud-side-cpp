#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'

//** viulib_dtc (Detection, Tracking and Classification) v13.10
// * 
// * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
// * (Spain) all rights reserved.
// * 
// * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
// * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
// * (Spain) all rights reserved.
// * 
// * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
// * licencing terms of those libraries:
// *
// * * OpenCV v2.4.6 (http://opencv.org/)
// * 
// * License Agreement for OpenCV
// * -------------------------------------------------------------------------------------
// * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
// *
// * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
// *
// */

 // author: pleskovsky@vicomtech.org
 
#ifndef DTC_VALIDATION_H_
#define DTC_VALIDATION_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <string>



// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "core.h"
#include "dtc_exp.h"
#include "dtc_classification.h"
#include "system.h"
#include "imgTools.h"

namespace viulib
{
namespace dtc
{

	typedef struct DTC_LIB EvaluationResult {
   public:
	  EvaluationResult(): TP(0), TN(0), FP(0), FN(0) {}
     EvaluationResult(int _TP, int _TN, int _FP, int _FN):
         TP(_TP), TN(_TN), FP(_FP), FN(_FN) {}

        int TP;		/// True Positives
		  int TN;		/// True Negatives
        int FP;		/// False Positives
        int FN;		/// False Negatives
	}t_evalulation_result;

    /** \brief Validation cost
    * http://en.wikipedia.org/wiki/Precision_and_recall
    * http://en.wikipedia.org/wiki/F1_score
    *
    * http://en.wikipedia.org/wiki/Receiver_operating_characteristic
    * http://people.brandeis.edu/~sekuler/stanislawTodorov1999.pdf
    * http://www.pallier.org/ressources/aprime/aprime.pdf
    * \ingroup viulib_dtc  
	*/
    class DTC_LIB ValidationCost {
      public:
        ValidationCost(): format_short(true), precision(0.0), recall(0.0), FP_rate(0.0), FN_rate(0.0), GE_rate(0.0) { fmeasure = getFMeasure(); }          
        ValidationCost(double _precision, double _recall, double _FP_rate, double _FN_rate, double _GE_rate): 
                       format_short(true), precision(_precision), recall(_recall),FP_rate(_FP_rate), FN_rate(_FN_rate), GE_rate(_GE_rate) { fmeasure = getFMeasure(); }        
        ValidationCost(const ValidationCost& vc, bool _format_short = true): 
                       format_short(_format_short), precision(vc.precision), recall(vc.recall), fmeasure(vc.fmeasure), FP_rate(vc.FP_rate), FN_rate(vc.FN_rate), GE_rate(vc.GE_rate) {}        
        ValidationCost(const EvaluationResult& er):
                       format_short(true), precision(0.0), recall(0.0), fmeasure(0.0), FP_rate(0.0), FN_rate(0.0), GE_rate(0.0)
        {
          if (er.TP > 0) {
            recall = static_cast<double>(er.TP) / static_cast<double>(er.TP + er.FN); 
            precision = static_cast<double>(er.TP) / static_cast<double>(er.TP + er.FP); 
            fmeasure = getFMeasure();
          }

          if (er.FP > 0) 
             FP_rate = static_cast<double>(er.FP)/static_cast<double>(er.FP + er.TN);

          if (er.FN > 0) 
             FN_rate = static_cast<double>(er.FN)/static_cast<double>(er.FN + er.TP);

          if( (er.FN > 0) || (er.FP > 0) )
			   GE_rate = static_cast<double>(er.FP + er.FN)/static_cast<double>(er.TP + er.TN + er.FP + er.FN);			          
        }

    private:
        double getFMeasure() const {
            if (precision + recall > 0)
               return  2*(precision*recall)/(precision + recall);        
            else return 0.0;
        }

    public:                          
        bool format_short;
        double precision; // can be as well sum of Mean Squared Errors, for regression type classification
        double recall;
        double fmeasure;

 		  double FP_rate;
		  double FN_rate;
		  double GE_rate;// global error FP+FN/all

      friend DTC_LIB std::ostream& operator<<(std::ostream& os, const ValidationCost& vc);
        
    };
    DTC_LIB std::ostream& operator<<(std::ostream& os, const viulib::dtc::ValidationCost& vc);
    
    /** \brief Validation cost calculator - keeps and updates counters of TP, TN, FP, FN for multiple classes in form of Confusion Matrix.
	  * \ingroup viulib_dtc
    */
    class DTC_LIB ValidationCostCalculator {
      private:
        ValidationCostCalculator(): format_short(true) {} // forbidden
      public:
        ValidationCostCalculator(int classNumber): format_short(true), confusionMatrix(cv::Mat_<int>::zeros(classNumber,classNumber)) {}

        int getClassNumber() const { return confusionMatrix.cols; }

        void clear(int classNumber = -1)
        { 
            if (classNumber < 0) classNumber = confusionMatrix.rows;
            confusionMatrix = cv::Mat_<int>::zeros(classNumber,classNumber);
        }
        //          Positive  Negative (groud truth result)
        //   True     ...TP       ...FP
        //  False     ...FN       ...TN

        // Functions
        void increase(int classOUT, int classGT, int nr = 1) 
        {
           //confusionMatrix(row, col)
           confusionMatrix(classOUT,classGT) += nr;           
        }
        
        ValidationCost operator()(int classNr = 0) const
        {
           return getValidationCost(classNr);
        }

        ValidationCost getValidationCost(int classNr = 0) const
        {
			EvaluationResult er;
           er.TP = confusionMatrix(classNr,classNr);

           cv::Mat_<int> rowClass = confusionMatrix.row(classNr);
           for (int i=0; i<rowClass.cols; i++)
               if (i != classNr) er.FP += rowClass(i);

           cv::Mat_<int> colClass = confusionMatrix.col(classNr);
           for (int i=0; i<colClass.rows; i++)
               if (i != classNr) er.FN += colClass(i);

           for(int i = 0; i< confusionMatrix.rows; i++)
		   {
			   for(int j = 0; j< confusionMatrix.cols; j++)
			   {
					if(i!=classNr && j!= classNr)
					{
						er.TN += confusionMatrix.at<int>(i,j);
					}
			   }
		   }

           return ValidationCost(er);
        }

        const cv::Mat_<int>& getConfusionMatrix() const { return confusionMatrix; }

        friend class Validation;
        friend DTC_LIB std::ostream& operator<<(std::ostream& os, const ValidationCostCalculator& vcc);
      public:
        bool format_short;
        cv::Mat_<int> confusionMatrix;
	};
   DTC_LIB std::ostream& operator<<(std::ostream& os, const viulib::dtc::ValidationCostCalculator& vcc); 

    /** \brief This class provides functions to perform classifier validation (e.g. regression / cross-validation)      
	* \ingroup viulib_dtc
    */    
    // NEXT STEP - validation algorithm which reduces memory requirements - processes all by parts (only some/necessary training samples at a time; only some parameters at a time)
    class DTC_LIB Validation: public virtual VObject
    {
      REGISTER_BASE_CLASS(Validation,dtc)
    public:
  		typedef struct DTC_LIB error_image_idx
		{
		public:
			error_image_idx(): imgIndex(0,0), GT_label(0), predicted_label(0){}

			std::pair<int,int> imgIndex;
			int GT_label;
			int predicted_label;
		}t_error_image_idx;


    public:
		Validation();

      /** \brief Destructor. */
		virtual ~Validation();
      virtual Validation* clone() const = 0;
      
    public:
	
	/** \brief      Loads validation images from a specified path. All images should be organized in subfolders, these define individual classes used for the evaluation.
                   The same group of images will be used for training as well as for validation by each specific validation algorithm.
	*	\param _path path containing the images.
	*	\param feat_extractor (if given) generates descriptors for each class and prepares the data.            
    */    
      void init( const std::string & _path, FeatureExtractor* feat_extractor= NULL);

	/** \brief  Prepares images given as an array of cv::Mat for the evaluation. The organization is: array of image classes, each including array of images (cv::Mat).
	*
	*   \param _inputImages must be a vector containing the images to train and classify, for each class considered. 
	*
	*	\param feat_ext (if given) generates descriptors for each class and prepares the training/validation set.
	*
    */  
	   void init( const std::vector<std::vector<cv::Mat> >& _inputImages,
				     FeatureExtractor* feat_ext= NULL,
                 LabelStringMapper labelMap = LabelStringMapper());         

    /** \brief  Loads the data as TrainSample objects and prepares the data.
	*
	* \param _trainSamples vector containing training samples for each class considered. The class is specified by TrainSample->label.
   *        Note that several training samples,
   *        i.e. rows of the TrainSample.descriptor, can be included in one TrainSample, as long as they belong to the same class.
   *
	* Carefull, the descriptors must correspond to the classifier's feature type.       
	*/  
	  void init(const std::vector<TrainSample >& _trainSamples,
               LabelStringMapper labelMap = LabelStringMapper()); // option for the above call, if the samples(i.e. descriptors) are already grouped within each class TrainSample     
	 
	  
	  /** \brief   Reset
	  * Resets the information relative to the preparation of the data (e.g. extraction of descriptors and generation of data folds).
    */    
      void reset() { prepared = false; }
	  /** \brief   Clears all the inner data.
	  * 
    */    
      void clear() { 
         reset(); 
         m_imgNames.clear();
         m_inputImages.clear();
         m_trainingFolds.clear(); 
         m_evaluationFolds.clear(); 
 	      
         m_labelMapper.clear();
 	      featClassName.clear();

	      m_trainingSamples.clear(); 
	      m_imgNames.clear();
	      m_inputImages.clear();
	      m_path.clear();          
      }
	  
	  
	  /** \brief          Prepares the data for the evaluation.
     *       It extracts the descriptors from Mat or img files and calls SplitData to 
     *       split the data into training and evaluating sets, for as many rounds as specified.
     *     
     *       Can be used to change the feature extractor;
	  * \param feat_extractor feature extractor object used to extract the descriptors of each Mat or file.
	  */  
      bool prepareData( FeatureExtractor* feat_extractor);
      const std::vector<TrainSample>& getTrainSamples() const { return m_trainingSamples; }

    protected:
		/** \brief   Splits the dataset.
		* Generates different ways to separate the training and evaluation data and stores them for later evaluation.
      * This is a virtual function specific for each validation algorithm.
		*/  
      virtual void splitData() = 0;
                                        // future possibilities -> K-folds, Stratified K-folds, Leave one out, Leave p out,  Leave one label out, Leave p label out, Random, Shuffle
                                        // http://scikit-learn.org/stable/modules/cross_validation.html

										// K is selected by setParam with the parameter "Rounds".
										//The separation is defined by the "splitDataValue" parameter, which contains the percentage of data that will be used for training. 
										//The rest will be used as the evaluating set.
	  
	  


    public:
		/** \brief        Train the classifier on given trainSample and evaluate the learning errors.
		* \param classifier classifier used for training and classifying the data (must have all the inner parameters initialized).
      * \param trainSamples learning samples.
      * \param learn_errors if given, the indexes of error images are returned.
		* \return the validation run on the training set.
		*/  
      ValidationCostCalculator train(Classifier* classifier, const std::vector<TrainSample >& trainSamples) const;
      ValidationCostCalculator train(Classifier* classifier, const std::vector<TrainSample >& trainSamples, std::vector<error_image_idx>& learn_errors) const;


      /** \brief        Evaluate the classifier on the previously specified validation set.
		* \param classifier classifier used for training and classifying the data (must have all the inner parameters initialized).
      * \param learningResults if given, the validation will be run on the training set as well, and the corresponding results will be provided in the given variable.
      *                        These values can be used to generate learning curves.
		* \return vector containing the result of the clasification of the evaluation set classes.
		*/  
       // learn_errors and eval_errors can repeat, since they are collected from multiple evaluation runs
      ValidationCostCalculator evaluate(const Classifier* classifier, ValidationCostCalculator* const learningResults = NULL);// TODO const;
      ValidationCostCalculator evaluate(const Classifier* classifier, ValidationCostCalculator* const learningResults,
                                        std::vector<error_image_idx>& learn_errors, std::vector<error_image_idx>& eval_errors);// TODO const;
          
		/** \brief              Evaluate a given classifier on testing data folds.
		*
      * \param classifier a trained classifier is expected.
      * \param testSamples ground truth samples.
	   * \return result of the clasification.
	   */  
      ValidationCostCalculator evaluate_given(const Classifier* classifier,  const std::vector<TrainSample >& testSamples) const;
      virtual ValidationCostCalculator evaluate_given(const Classifier* classifier,  
                                                      const std::vector<TrainSample >& testSamples,
                                                      std::vector<error_image_idx>& errors) const; 


	  // STEPS OF THE FUNCTION:
			// clean results
			// 1) if train data not ready, run prepareData with classifier's feature extractor
            // 2) check if descriptor [length] corresponds to the one expeced by classifier
            // 3) if cross-validation data not available, run splitData ... TODO - move to init()
            //   3.5) save fold setup in logFile
            // 4) run cross-validation [could be done in parallel - TODO] 
            //         - use std::vector<ValidationCostCalculator> results; for partial validation results      
            //         -> call evaluate_single(classifier->clone()), fill in validation results (results)
			// return getScore(); (mean performance of the results obteined in evaluate_single (number of results determined by Rounds)). 
    // (The ValidationCost object contains the global error, false positive rate, false negative rate, precission and recall.)


	  /** \brief        Train and evaluate multiple classes.
		* \param cv vector containing the predicted label obtained in classify function.
		* \param num_classes Number of classes.
		* \return Results (recall, precision, ..).
		*/  
		//ValidationCost multiclass_evaluation(std::vector<std::vector<Classifier::ClassificationVector> >& cv, int num_classes)const;

      ValidationCost getClassValidation(const ValidationCostCalculator& vcc, const std::string& name) const;
      const cv::Mat getImage(std::pair<int, int> image_index) const
      {
         if (!m_inputImages.empty()) 
            return m_inputImages[image_index.first][image_index.second];         
         return cv::Mat();
      }
      const std::string getImageFilename(std::pair<int, int> image_index) const 
      {
         if (!m_imgNames.empty()) 
            return m_imgNames[image_index.first][image_index.second];         
         return std::string();
      }
      const Descriptor getDescriptor(std::pair<int, int> image_index) const
      {
         if (!m_trainingSamples.empty())
            return m_trainingSamples[image_index.first].getDescriptor().row(image_index.second);
         return Descriptor();
      }
      
    protected: 
		/** \brief              Evaluate a single training data fold.
		*
       * \param classifier should be a clone of the base one.
	   * \return result of the clasification.
	   */  
      ValidationCostCalculator evaluate_single(Classifier* classifier, ValidationCostCalculator* const learningResults = NULL) const; 
      virtual ValidationCostCalculator evaluate_single(Classifier* classifier, ValidationCostCalculator* const learningResults,
                                                       std::vector<error_image_idx>& learn_errors, std::vector<error_image_idx>& eval_errors) const; 
      
      ValidationCostCalculator evaluate_single_LV(Classifier* classifier, ValidationCostCalculator* const learningResults) const; // TODO generates learning curves

											//For each Round:
												// 1) train classifier on training data fold 
												// 2) classify corresponding evaluation fold + calculate TP/FP/...
											// It uses ValidationCostCalculator for asessing precision/recall.
											// After each single evaluation, save state to logFile [for parallelism needs writing lock/mutex - TODO]

	  /** \brief       Mean calculation of results obtained from the evaluation.
	  * \param allScores vector with the results obtained in every evaluate_single iteration.
	  * \return mean performance of the results.
	  */  
      virtual ValidationCostCalculator getScore(const std::vector<ValidationCostCalculator>& allScores) const;
        
    // TODO   additional logging information
    //protected:
    //  virtual bool saveState() const; // save into log-file, log-file name selected via setParam
    //public:
    //  virtual bool loadState(std::string name);
    //  ValidationCost resume(); // resume from loaded state - load initialization/prepared data folds, skip single evaluation already performed, run the rest.

    // additional helper func
    public:
      bool getVerbose() const { return m_verbose; }
      void setVerbose(bool ver = true) { m_verbose = ver; }
      const LabelStringMapper& getLabelMapper() const { return m_labelMapper; }
      const std::vector<std::vector<cv::Mat> >& getInputImages() const { return m_inputImages; }
      const std::vector<std::vector<std::string> >& getImgNames() const { return m_imgNames; }
      const std::string& getPath() const { return m_path; }
      
    // inherrited functions
      // ---------------- set / get inner parameters and variables
    protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      virtual void setParam(const ParamBase& value, const std::string& name);
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      virtual ParamBase getParam(const std::string& name) const;
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      virtual void getParam_(void** param, const std::string& name) {}

      // ------------------------------ load / save to XML/YAML functionality
    protected:
      //! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;

      // ---------- additional helper funcitons
    public:
		/** \brief      Stores the file names of the dataset allocated in a path.
      *              This function also sets the inner LabelMapper to the names of the subfolders containing input images.
		* \param _dir path in which the images are allocated. 
		* \return _imgF vector of images found in the path given. 
		*/  
       // helper function also for outside use
     static int getImages(std::string _dir, std::vector<std::vector<cv::Mat> >& images, 
                          std::vector<std::vector<std::string> >& imgNames, LabelStringMapper& labelMap);

      
    protected:
      //std::string logFile;
      
      // data folds
      // e.g. [ [ TrainSample(s) of class1, TrainSample(s) of class 2, ...], [fold 2 data], ... ] ; TrainSample(s) = { descriptor of multiple rows (cv::Mat), class label };      
      typedef std::vector< std::vector< viulib::dtc::TrainSample > > t_data_folds;
      typedef std::vector< int > t_train_sample_image_indices; // descriptorId within dtc::TrainSample -> <pathId, imageId within the class group
      typedef std::vector< std::vector< t_train_sample_image_indices > > t_data_fold_image_indices;// fold, trainSample class group -> train_sample_image_indices vector
      // training folds
      t_data_folds m_trainingFolds; 
      t_data_fold_image_indices m_trainingFoldsIndices;
      // evaluation folds
      t_data_folds m_evaluationFolds; 
      t_data_fold_image_indices m_evaluationFoldsIndices;
      
      bool m_verbose;
 	   LabelStringMapper m_labelMapper;
 	   std::string featClassName;

	   int rounds; // = number of data folds
	   int training_round; // current round
	    bool prepared; 

	  std::vector<TrainSample> m_trainingSamples; // samples used for training and evaluation - must be defined! (can be generated from m_inputImages upon evaluation execution cia prepareData)
	  std::vector<std::vector<cv::Mat> > m_inputImages; // used to generate m_trainingSamples, can be empty
	  std::vector<std::vector<std::string> > m_imgNames; // only keeping information about the filenames, can be empty   
	  std::string m_path; // only keeping information about the initialization path, can be empty


    public: // for testing
	  bool visualize; // visualize error images

      //classNr, optimize only for this class ... use -1 if a mean of all F-measures will  be used
      // returns optimum parameter found
		double parameter_optimization(const std::string& paramName, double initial_value, double end_value, double stepPlus, double stepTimes, bool show_results, Classifier* classifier, int classNr = -1);      

   };
   
   
}
}

#endif // DTC_Validation_H_
