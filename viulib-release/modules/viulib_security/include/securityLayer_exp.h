/** 
 * viulib_security (Security) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_security is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_security depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenSSL v1.0.1e (http://www.openssl.org/)
 * 
 * License Agreement for OpenSSL
 * -------------------------------------------------------------------------------------
 * Copyright (c) 1998-2011 The OpenSSL Project.  All rights reserved.
 *
 * Apache-style licence (http://www.openssl.org/source/license.html)
 *
 */


#ifndef SECURITYLAYER_EXP_H
#define SECURITYLAYER_EXP_H
// ==============================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// ==============================================================================================
// When the symbol viulib_securitylayer_EXPORTS is defined in a project securityLayer exports functions and classes. 
// In other cases SECL_LIB imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_security_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define SEC_LIB __declspec( dllexport )
	#else
		#define SEC_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define SEC_LIB __declspec( dllimport )
	#else
		#define SEC_LIB
	#endif
#endif
#else
#define SEC_LIB
#endif

// ==============================================================================================
// AUTOMATIC DLL LINKAGE
// ==============================================================================================
// If the code is compiled under MS VISUAL STUDIO, and BOT_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_security_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_security_d.lib" )
	#else
		#pragma comment( lib, "viulib_security.lib" )
	#endif
#endif


namespace viulib
{
	namespace security
	{
		enum CHECK_RESULT {NO_FILE=1, LICENSE_FOOTPRINT_FAIL=2, LICENSE_DATE_FAIL = 3, LICENSE_OK=0};
	}
}
#endif
