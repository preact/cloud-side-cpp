/**  
 * \defgroup viulib_security viulib_security
 * viulib_security (Security) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_security is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_security depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenSSL v1.0.1e (http://www.openssl.org/)
 * 
 * License Agreement for OpenSSL
 * -------------------------------------------------------------------------------------
 * Copyright (c) 1998-2011 The OpenSSL Project.  All rights reserved.
 *
 * Apache-style licence (http://www.openssl.org/source/license.html)
 *
 */

// author: acortes@vicomtech.org, pleskovsky@vicomtech.org

#include "architecture.h" // from viulib_core

#include "securityLayer_exp.h"
//#include "core.h"
#ifndef SECURITY_H
#define SECURITY_H
#include <string>
#include <vector>


namespace viulib
{
	/** \brief Security support functions
	*	\ingroup viulib_security		
	*/
	namespace security
	{

		/** \brief Virtual Class for deriving crytographic algorithms     
		\ingroup viulib_security	
		*/
		class SEC_LIB cryptoAlgorithm //: public VObject
		{
			//REGISTER_BASE_CLASS(cryptoAlgorithm,security);
			
			public:
				cryptoAlgorithm(){};	
				/** \brief	Destructor. */
				virtual ~cryptoAlgorithm(){};	
				
				virtual std::string getClassName() { return std::string("cryptoAlgorithm");};

            virtual bool generateKey(long random = -1){ return true;}
            virtual void clean() { iv = std::string(); }

            std::string generateIV(int len = 32); // 256 bit by default
            void setIV(const std::string& _iv = std::string()) { iv = _iv; }
            std::string getIV() { return iv; }

				virtual std::string encode (const std::string &msg) {return std::string();};
				virtual std::string decode (const std::string &msg) {return std::string();};
			
				///** \brief	Sets the detector's parameters 
				// *
				// *	\param value Value of the parameter
				// *	\param	name Name of the parameter		
				// */
				//virtual void setParam(const ParamBase& _value, const std::string &_name) {}


				///** \brief	Get parameter from Detector
				// *	\param _param void pointer to param content
				// *	\param _name name of the parameter
				// */		
				//virtual void getParam(void** _param, const std::string& _name) {}
				//virtual ParamBase getParam(const std::string& _name) const {return NullParam();}

         protected:
            std::string iv;
		};
	
      void printLastError();

      SEC_LIB std::string strXor( const std::string &str1,  const std::string &str2);
      SEC_LIB std::string base16_encode( const std::string &str );
      SEC_LIB std::string base16_encode( unsigned char const *data, size_t len );
      SEC_LIB std::string base16_decode( const std::string& str );
      SEC_LIB size_t base16_decode(const std::string& str, unsigned char **data );

      /** \brief Encodes given string to Base64 format
      *   \param str string to encode
      *   \return base64 encoded string str
      */
      SEC_LIB std::string base64_encode( const std::string &str );
      SEC_LIB std::string base64_encode( unsigned char const *data, size_t len );
      /** \brief Decodes given string from Base64 format
      *   \param str string to decode
      *   \return decoded base64 string str
      */
      SEC_LIB std::string base64_decode( const std::string &str );
      SEC_LIB size_t base64_decode( const std::string& str, unsigned char **data );



		/** Devuelve el numero de serie de la unidad
			- <code> getSerialNumber(unidad) </code> .
			\pre

			\remark

			\par Ejemplo:
				\code
					// obtener el numero de serie de la unidad c:
					std::string sn = viulib::Security::getSerialNumber ("C:\");
				\endcode
				\ingroup viulib_security	
		*/
			 SEC_LIB std::string getSerialNumber(const std::string &unidad = std::string());
		
		/** Enumera los dispositivos de un tipo
			- <code> EnumDeviceClasses(indice del dispositivo, Nombre de la clase de dispositivos, Descripcion, Existe el dispositivo,...) </code> .
			\pre
			\remark

			\par Ejemplo:
				\code
					//  
					viulib::Security::EnumDeviceClasses (0,...);
				\endcode
				\ingroup viulib_security	
		*/
		 SEC_LIB int EnumDeviceClasses(int index,  std::string &DeviceClassName, std::string &DeviceClassDesc, bool &DevicePresent, int &ClassImage);
	
		
		/** Enumera los dispostivos de una clase.
			- <code> EnumDevices(indice, Nombre de la clase de dispositivo, nombre del dispositivo) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// 
					int res = viulib::Security::EnumDevices (0,...);
				\endcode
				\ingroup viulib_security	
		*/
		 SEC_LIB int EnumDevices(int index, std::string &DeviceClassName, std::string &DeviceName);
		
		/** Funcion que retorna informacion sobre un tipo de unidades
			- <code> getDeviceTypeInfo() </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// 
					std::string info = viulib::Security::getDeviceTypeInfo();
				\endcode
				\ingroup viulib_security	
		*/
		 SEC_LIB std::string getDeviceTypeInfo(std::vector<std::string> ids);

		/** deja en MACData la direccion MAC como un string
			- <code> printMACaddress(direccionMAC) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// 
					viulib::Security::printMACaddress();
				\endcode
				\ingroup viulib_security	
		*/
		 SEC_LIB void printMACaddress(unsigned char MACData[]);

		
		/** devuelve la direccion MAC en un std::string (separada por guiones)
			- <code> getMACaddress(identificador de la tarjeta de red) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// obtener la direccion MAC de la tarjeta de red 0 
					std::string dir_mac = viulib::Security::getMACaddress(0);
				\endcode
				\ingroup viulib_security	
		*/
		 SEC_LIB std::string getMACaddress(int idcard);
		SEC_LIB std::vector<std::string> getMACaddress();

		/** Aplica al string t una algoritmo de encriptacion CESAR utilizando como clave el parametro clave y desplazamiento k
			Algoritmo de cifrado que transforma cada caracter del mensaje original 
			sumando a la posicion del caracter k el valor ascii del caracter correspondiente de la clave

			- <code> sustitucionCESAR(mensaje,trasposicion,clave) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// obtener la lista de voces
					std::string mensaje = "Mensaje a codificar.";
					viulib::Security::sustitucionCESAR(mensaje,10,"clave");
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB void sustitucionCESAR( std::string &t,int k,const std::string clave);
		
		/** Aplica al string t una algoritmo de encriptacion por sustitucion MonoAlfabetica 
		    cada caracter se sustituye por un determinado caracter del alfabeto del texto cifrado 
			
			- <code> sustitucionMA(mensaje) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// aplicar sustitucionMA al mensaje 
					std::string mensaje = "Mensaje a codificar.";
					viulib::Security::sustitucionMA(mensaje);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string sustitucionMA( const std::string &t);
		
		/** Aplica al string t una algoritmo de encriptacion por trasposicion
			
			- <code> trasposicion(mensaje) </code> .
			\pre
			\remark
				Algoritmo de cifrado que consiste en la reordenacion de los elementos
				se distribuye el mensaje en una matriz de nf x k
				"mensaje de prueba" , 3
				nf = ceil(17/3) = 6
				6 x 3
				m e n
				s a j
				e   d
				e   p
				r u e
				b a  
				se seleccionan por columnas "mseerbea  uanjdpe "
			\par Ejemplo:
				\code
					// aplicar trasposicion al mensaje 
					std::string mensaje = "Mensaje a codificar.";
					viulib::Security::trasposicion(mensaje,10);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB void trasposicion( std::string &t,int k);
		
		/** Aplica al string t una algoritmo de encriptacion por trasposicion
			
			- <code> trasposicionI(mensaje) </code> .
			\pre
			\remark
				
				
			\par Ejemplo:
				\code
					// aplicar trasposicion al mensaje 
					std::string mensaje = "Mensaje a codificar.";
					viulib::Security::trasposicionI(mensaje,10);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string trasposicionI( std::string &t,int k);
		
		/** Recupera el mensaje encriptado con el algoritmo de sustitucion
			
			- <code> sustitucionMA(mensaje) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// aplicar sustitucionMAI al mensaje 
					std::string mensaje = "Mensaje a codificar.";
					viulib::Security::sustitucionMAI(mensaje);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string sustitucionMAI( const std::string &t);


		/** Devuelve la codificacion MD5 de un string
			
			- <code> MD5(mensaje) </code> .
			\pre
			\remark
				
			\par Ejemplo:
				\code
					// aplicar MD5 al mensaje 
					std::string mensaje = "Mensaje a codificar.";
					std::string mensaje_md5 = viulib::Security::MD5(mensaje);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string MD5(const std::string &data);

		/** genera un string con la informacion del PC
			- <code> generateComputerIDKey() </code> .
			\pre
				
			\remark
				
				
			\par Ejemplo:
				\code
					// obtener el Key del ordenador
					std::string idKey = viulib::Security::generateComputerIDKey();
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string generateComputerIDKey();
		
		/** genera un string con la informacion del PC encriptada
			- <code> generateEncriptedComputerIDKey() </code> .
			\pre
				
			\remark
				
				
			\par Ejemplo:
				\code
					// obtener el Key encriptada del ordenador
					std::string idKey = viulib::Security::generateEncriptedComputerIDKey();
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string generateEncriptedComputerIDKey();
		
		/** genera un string con la informacion del PC a partir de un string con la informacion del pc encriptada
			- <code> generateDencriptedComputerIDKey(identificador del pc) </code> .
			\pre
				
			\remark
				
				
			\par Ejemplo:
				\code
					// obtener el Key encriptada del ordenador
					std::string idKey = viulib::Security::generateEncriptedComputerIDKey();
					std::string info_pc = viulib::Security::generateDencriptedComputerIDKey(idKey);
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string generateDencriptedComputerIDKey(std::string idkey);

		/** Genera el fichero de peticion de licencia y devuelve lo que se ha escrito en el mismo en un string
			- <code> generateRequestingFile(path al fichero de peticion de licencia) </code> .
			\pre
				
			\remark
					
			\par Ejemplo:
				\code
					// generar el fichero de peticion de licencia
						std::string mensaje = generateRequestingFile("request.cli");
				\endcode
				\ingroup viulib_security	
		*/
		SEC_LIB std::string generateRequestingFile(std::string file);

		/** comprueba si existe el fichero de licencia y si es correcto su contenido
			- <code> checkLicense(path al fichero de licencia) </code> .
			\pre
				
			\remark
				
			\par Ejemplo:
				\code
					// comprobar la licencia
					viulib::security::CHECK_RESULT code;
					code = viulib::Security::checkLicense("license.vli");
					if (code == viulib::security::LICENSE_FAIL ||
			    		code == viulib::security::NO_FILE )
					{
						std::string message;
						message = "Request of client license for:\n";
						message+=viulib::Security::generateEncriptedComputerIDKey();
						if (!viulib::Security::enviarMail("andonicor@gmail.com","acortes@vicomtech.org","New license",message,"andonicor@gmail.com",""))
						{
							viulib::Security::generateRequestingFile("license_request.cli");
							printf("Envie el fichero generado a acortes@vicomtech.org\n");
						}
						exit(0);
					}
					else
					{
						... do something ...
					}
				\endcode
				\ingroup viulib_security	
		*/
	//	SEC_LIB viulib::security::CHECK_RESULT checkLicense(std::string licenseFile);
		SEC_LIB viulib::security::CHECK_RESULT checkLicense(std::string licenseFile, std::string& licence_secret);

		SEC_LIB std::string getComputerName();
	//	SEC_LIB std::string getLicense();

	}
	
}
#endif
