/**  
 * \defgroup viulib_bgfg viulib_bgfg
 * viulib_bgfg (Background & Foreground Detection) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_bgfg is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_bgfg depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_bgfg.
 *
 */ 

#pragma once
#ifndef BGFG_H_
#define BGFG_H_

// =====================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// =====================================================================================
// When the symbol viulib_dtc_EXPORTS is defined in a project DTC exports functions and 
// classes. In other cases DTC imports them from the DLL.
#ifndef STATIC_BUILD
#ifdef viulib_bgfg_EXPORTS
  #if defined _WIN32 || defined _WIN64
    #define BGFG_LIB __declspec( dllexport )
  #else
    #define BGFG_LIB
  #endif
#else
  #if defined _WIN32 || defined _WIN64
    #define BGFG_LIB __declspec( dllimport )
  #else
    #define BGFG_LIB
  #endif
#endif
#else
#define BGFG_LIB
#endif

// =====================================================================================
// AUTOMATIC DLL LINKAGE
// =====================================================================================
// If the code is compiled under MS VISUAL STUDIO, and viulib_dtc_EXPORTS is not defined
// (i.e. in a DLL client program) this code will link the appropiate lib file, either in 
// DEBUG or in RELEASE
#if defined( _MSC_VER ) && !defined( viulib_bgfg_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_bgfg_d.lib" )
	#else
		#pragma comment( lib, "viulib_bgfg.lib" )
	#endif
#endif



#include "core.h"
//#include "dtc_features.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/legacy/compat.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <list>
#include <typeinfo>
#include "public_types.h"

#include <iostream>
#include <string>

#define DRAW_COLOR	cv::Scalar(0,255,0)

/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/).
  */
namespace viulib
{

/** \brief This namespace corresponds to the viulib_bgfg module
 * \ingroup viulib_bgfg  
 */
namespace bgfg
{  

  //Code necessary for automatic registration of bgfg classes within the viulib::Factory
  //
  // The following static variable will be created and its constructor called, once  #include "bgfg.h"  is put into the sourcecode
  // Since the constructor is defined in the bgfg.cpp, the library will be loaded into memory.
  // Upon loading, all static variables of the lib will be initialized and thus all relevant classes registered in the Factory.
  //
	struct BGFG_LIB factory_registration
	{
    factory_registration();
	};
	static factory_registration bgfg_module_init;

	/** \brief BgfgModel class to create background substraction methods
		*	\ingroup viulib_bgfg
		*  This class has tools for the detection of objects on images.
		*
		*	Current available BgfgModel classes:		
		*		- BGModelCodebook
		*		- BGModelColor
		*		- BGModelGMG
		*		- BGModelMBSD
		*		- BGModelMedianApprox
		*		- BGModelMOG
		*		- BGModelRAVG
		*/
	class BGFG_LIB BgfgModel : public VObject
	{
		REGISTER_BASE_CLASS(BgfgModel,bgfg)

	public:
		BgfgModel(){};	
		/** \brief	Destructor. */
		virtual ~BgfgModel(){}
		
		/** \brief Substract
		 *
		 *	\param _img Input Image
		 *  \param [out] _fg Foreground Mask
		 *  \param [out] _bg Background Mask
		 *	\return	void
		 */		
		virtual void compute( const cv::Mat& _img, cv::Mat& _fg, cv::Mat& _bg ) {}
		virtual void compute( const cv::Mat& _img, cv::Mat& _fg ) {}
		virtual void compute( const cv::Mat& _img ) {}
		
		/** \brief	Draw
		 *
		 *	\param [out] _img_out Output Image		 
		 *	\param [in] _color Color		 
		 */
		virtual void draw( cv::Mat& _img_out, const cv::Scalar& _color = DRAW_COLOR) {}

		/** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 *
		 *	BGModelMedianApprox: "Approximation to Median"<br>
		 *		"margin",				int,				Margin (from 0 to 255) to define a foreground pixel. <br>
		 *		"useMorph",				bool,				Use morphology operations. <br>
		 *
		 *	TODO
		 *
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name) {}


		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name) {}
		virtual ParamBase getParam(const std::string& _name) const {return NullParam();}
		

	};

	///** This class computes a 1-D descriptor defining how much fitted is a given Rect with respect
	//*	to underlaying foreground mask (for which the integral image is precompute before each run().
	//*/
	//class BGFG_LIB BGFGFill : public viulib::dtc::FeatureExtractor
	//{
	//public:
	//	BGFGFill():margin(5){}
	//	BGFGFill(int _margin):margin(_margin){}
	//	~BGFGFill() {}

	//protected:	
	//	void precomputeResponse(const cv::Mat &img, viulib::dtc::SamplingStrategy& sampling);
	//	viulib::dtc::Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

	//	BGFGFill* clone() const { return new BGFGFill(); }
	//	t_data_type  getDescriptorType() const { return CV_64FC1;}

	//protected:
	//	cv::Mat integral;
	//	cv::Rect imgRect;
	//	int margin;
	//};

}
}
/** }@*/
#endif