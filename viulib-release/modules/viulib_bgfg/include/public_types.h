/** 
 * viulib_bgfg (Background & Foreground Detection) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_bgfg is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_bgfg depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_bgfg.
 *
 */ 



#pragma once
#ifndef PUBLIC_TYPES_H_
#define PUBLIC_TYPES_H_


/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/).
  */
namespace viulib
{
/** \brief This namespace corresponds to the bgfg module in VIULib library copyrighted by Vicomtech-IK4 (http://www.vicomtech.es/)
 */
namespace bgfg
{  

	typedef enum DISTRIBUTIONS {UNIFORM=0, UNIFORM_ELLIPSOID=1,UNIFORM_IN_RANGE=2, GAUSSIAN=3, BLUE_RATIO=4,BLUE_MAX=5, OTHER=5};
	#define RGB_CS 0
	#define BGR_CS 1
	#define bgr_CS 2
	#define HSV_CS 3
	#define HLS_CS 4
	#define CIELAB_CS 5
	#define YCrCb_CS 6
	#define GRAY_CS 7
	#define UNKNOWN_CS 8
	//typedef enum COLOR_SPACE {RGB=0, BGR = 1,  rgb=2, HSV=3, HSL = 4, CIELAB = 5, YCrCb = 6, GRAY = 7, UNKNOWN = 8};

	inline std::string cstoString(int _cs)
	{
		switch (_cs)
		{
		case RGB_CS: return "RGB";
		case BGR_CS: return "BGR";
		case bgr_CS: return "bgr";
		case HSV_CS: return "HSV";
		case HLS_CS: return "HLS";
		case CIELAB_CS: return "CIELAB";
		case YCrCb_CS: return "YCrCb";
		case GRAY_CS: return "GRAY";
		default: return "UNKNOWN";
		}
	};

	 

	
	struct RANGE_PIXEL
	{
		std::vector<float> pixel;

		int color_space;
		std::string color_space_sz;
		DISTRIBUTIONS type;
		std::vector<float> values;
		
	};


	typedef float (*metric)(std::vector<float> &_pixel); 



}
}
/** }@*/
#endif
