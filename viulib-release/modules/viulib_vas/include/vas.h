/** 
 * \defgroup viulib_vas viulib_vas
 * viulib_vas (Video Analytics for Surveillance) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_vas is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_vas depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_adas.
 *
 */

#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'

#ifndef VAS_H
#define VAS_H

// C++ Standard Library
#include <vector>
#include <string>
#include <numeric>
#include <algorithm>  

#ifdef HAVE_TBB
#include "tbb/tbb.h"
#ifdef WIN32
#undef max
#undef min
#endif
#endif

#include <stdio.h>

// Viulib
#include "core.h"
#include "dtc.h"
#include "opflow.h"
#include "evaluation.h"

#include "vas_exp.h"

// OpenCV
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"

/** \brief This namespace corresponds to the Viulib library
 *	(http://www.vicomtech.es/).
 */
namespace viulib
{	

/** \brief This namespace corresponds to the viulib_vas module
 * \ingroup viulib_vas  
 */
namespace vas
{
	struct VAS_LIB factory_registration
	{
    factory_registration();
	};
	static factory_registration vas_module_init;

	/** This is the main class for PersonTracker (PT) in a video sequence	
	*/
	class PersonTracker : public VObject
	{
		REGISTER_BASE_CLASS(PersonTracker,vas)
	public:
		/** This struct contains the output data provided by a PersonTracker		 
		 */
		struct PersonTracker_Data
		{
			PersonTracker_Data(){}
			PersonTracker_Data( const std::vector<viulib::dtc::Detection>& _detections,
					  const std::vector<viulib::dtc::Track*>& _pTracks ) : detections( _detections ), pTracks( _pTracks ) {}
			std::vector<viulib::dtc::Detection> detections;
			std::vector<viulib::dtc::Track*> pTracks;
		};

		enum {VIEW_CLOSE, VIEW_MID, VIEW_FAR};
		enum {PROFILE_FAST, PROFILE_MID, PROFILE_SLOW};
		enum {THRESHOLD_LOW, THRESHOLD_MID, THRESHOLD_HIGH};

		/** Destructor. */
		virtual ~PersonTracker(){}

		/** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter
		 *
		 *	 PersonTracker_FUB:.<br>
		 *		"Proc. Size",					cv::Size,				Processing size.<br>	 
		 *		"show_debug_images",			bool,					true to show images.<br>		 
		 *		"ub_classifier",				std::string,			Path to the trained Cascade classifier file (XML).<br>
		 *		"face_classifier",				std::string,			Path to the trained Cascade classifier file (XML).<br>
		 *		"ub_minSize",					cv::Size,				Min. Size of UpperBody.<br>
		 *		"ub_maxSize",					cv::Size,				Max. Size of UpperBody.<br>
		 *
		 *	 PersonTracker_BGFG:.<br>
		 *		"frameNum",						int,					Current frame num.<br>
		 *		"Proc. Size",					cv::Size,				Processing size.<br>		 
		 *		"show_debug_images",			bool,					true to show images.<br>		 
		 *		"use_BGFG",						bool,					true to use Background substraction-based detection.<br>
		 *		"detection_threshold",			double,					Detection threshold for the Detector3D.<br>
		 *		"Plane",						viulib::calibration::Plane*,	Pointer to the Plane.<br>
		 *		"grid_start_point",				cv::Point2f,			Coordinates in milimeters of start point of the grid.<br>
		 *		"grid_end_point",				cv::Point2f,			Coordinates in milimeters of end point of the grid.<br>
		 *		"longLevels",					int,					Number of levels in Y direction of the grid.<br>
		 *		"transLevels",					int,					Number of levels in X direction of the grid.<br>
		 *		"vehicle3f",					cv::Vec3f,				Person model (W, L, H).<br>
		 *		"use_SVMHOG",					bool,					true to activate SVM-HOG pedestrian detection.<br>
		 *		"hit_threshold",				double,					Hit threshold for SVM-HOG detector.<br>
		 *		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name)=0;


		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name) =0;
		virtual ParamBase getParam(const  std::string& _name) const =0;

		/** \brief	Executes the algorithm
		*	\param _img Input image		
		*/
		virtual PersonTracker::PersonTracker_Data run( const cv::Mat& _img) = 0;

		/** \brief	Draw
		*	\param _img_out Output Image		
		*	\param _color Main Color		 
		*/
		virtual void draw( cv::Mat& _outputImg, const cv::Scalar& _color = cv::Scalar(0,255,0) ) = 0;
	};	
	
	/** This function converts viulib::dtc::Track into viulib::Object, assigning the specific type
	*/
	void VAS_LIB fromTrackToObject( viulib::dtc::Track* _track, viulib::Object& _object, std::string _type="Person" );

	/** This is the very basic abstract class for Event Detection.
	 * It contains a VideoContentDescription that can be used internally to communicate
	 * intermediate results, and as final result, through the getters.
	*/
	class EventDetector : public VObject
	{
		REGISTER_BASE_CLASS(EventDetector,vas)
	public:
		virtual ~EventDetector(){}		
		//std::vector<viulib::Event*> getEvents() const { return eventVector; }
		const viulib::videoContentDescription::VideoContentDescription& getVideoContentDescription() const { return videoContent; }
		
		/** Shortcut getter for EventList **/
		const viulib::videoContentDescription::EventList& getEventList() const { return videoContent.getEventList(); }		

		/** Shortcut getter for RelationList **/
		const viulib::videoContentDescription::RelationList& getRelationList() const { return videoContent.getRelationList(); }

	protected:		
		/** This structure contains EventList, ObjectList and RelationList **/
		viulib::videoContentDescription::VideoContentDescription videoContent;
		
	};

	/** Base class for Offline Event Detector
	*/
	class OfflineEventDetector : public EventDetector
	{
		REGISTER_BASE_CLASS(OfflineEventDetector,vas)
	public:
		/** Destructor. */
		virtual ~OfflineEventDetector(){}

		/** Main function that processes inputs and generates outputs. It receives a file name of a video to open and process it.
		*/
		virtual void run(std::string _videoFileName) = 0;			

		/** Main function that process the input in form of a VideoContentDescription reference.
		 * This input can be copied into the EventDetector::videoContent.
		*/
		virtual void run(const viulib::videoContentDescription::VideoContentDescription& _videoContentInput) = 0;		
				
		// -- Inherited from VObject -- //
		virtual void setParam(const ParamBase& _value, const std::string &_name)=0;		
		virtual void getParam(void** _param, const std::string& _name) =0;
		virtual ParamBase getParam(const  std::string& _name) const =0;
	};
	
	/** Online Event Detector base class. 
	 *  This is an OfflineEventDetector because it can work Offline, but has additional functionalities
	 *  that allow using it in online mode (e.g. receiving an image).
	 *  It does keep the frame number internally.
	*/
	class OnlineEventDetector : public OfflineEventDetector
	{
		REGISTER_BASE_CLASS(OnlineEventDetector,vas)	
	public:			
		/** Destructor. */
		virtual ~OnlineEventDetector(){}
				
		/** Special run function for Online mode only. Receiving a single image each time.
		*/
		virtual void run( const cv::Mat& _img ) = 0;

		/** Main function that processes inputs and generates outputs. It receives a file name of a video to open and process it.
		*/
		virtual void run( std::string _videoFile );		

		/** Main function that process the input in form of a VideoContentDescription reference.
		 * This input can be copied into the EventDetector::videoContent.
		*/
		virtual void run(const viulib::VideoContentDescription& _videoContentInput) = 0;
		
		/** Draw function available in online mode.
		*/
		virtual void draw( const cv::Mat& _img ) const = 0;
			
		/** Getter of Events that are active with regards the current frameNum stored in the object
		*/
		std::vector<viulib::videoContentDescription::Event> getActiveEvents() const { return videoContent.getActiveEvents(currentFrame); }

		/** Getter of Objects that are active with regards the current frameNum stored in the object
		*/
		std::vector<viulib::videoContentDescription::Object> getActiveObjects() const { return videoContent.getActiveObjects(currentFrame); }
		
		
		/** Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter
		 *
		 *	 OnlineRuleManager:.<br>
		 *		"Rule",							viulib::vas::Rule*,		Adds this Rule to the RuleManager.<br>	 		 
		 *		"frameNum",						int,					Current frame number.<br>
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name)=0;
		
		/** Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name) =0;
		virtual ParamBase getParam(const  std::string& _name) const =0;
		
	protected:		 
		int currentFrame;
	};
		
	/** Base class for Rule execution (using RuleManager)
	 *  It does contain a VideoContentDescription*, that can be a link to an existing VideoContentDescription shared
	 *	between several Rule.
	 *
	 *	The main function is apply(), which takes as input whatever has been set from outside and fills Events and Relations
	 *	inside the VideoContentDescription*.
	*/
	class Rule : public VObject
	{
	REGISTER_BASE_CLASS(Rule,vas)			
	public:		
		virtual ~Rule(){}		

		/** Main execution function.
		*/
		virtual void apply() = 0;

		/** Main getter function to retrieve the result.
		*/
		viulib::videoContentDescription::VideoContentDescription* getVideoContentDescription() { return videoContent; }

		/** Shortcut to get EventList */
		const viulib::videoContentDescription::EventList& getEventList() const { return videoContent->getEventList(); }

		/** Shortcut to get RelationList */
		const viulib::videoContentDescription::RelationList& getRelationList() const { return videoContent->getRelationList(); }

		// -- Inherited from VObject -- //
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const{ return NullParam(); };

	protected:
		/** Pointer to VideoContentDescription provided by the RuleManager. To be used as Input/Output container. */
		viulib::VideoContentDescription* videoContent;

	};

	/** Base class for Rule execution through OnlineRuleManager
	*/
	class OnlineRule : public Rule
	{
	REGISTER_BASE_CLASS(OnlineRule,vas)			
	public:		
		virtual ~OnlineRule(){}
		
		/** Online mode allows to draw something */
		virtual void draw( const cv::Mat& _img ) const = 0;		
	protected:
		int frameNum;		
	};

	/** Basic extension of Rule with no added functions nor members.
	*/
	class OfflineRule : public Rule
	{
	REGISTER_BASE_CLASS(OfflineRule,vas)			
	public:		
		virtual ~OfflineRule(){}		
	};
}
}
#endif
