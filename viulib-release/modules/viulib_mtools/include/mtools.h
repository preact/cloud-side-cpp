/** 
 * \defgroup viulib_mtools viulib_mtools
 * viulib_mtools (Mathematical Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_mtools is a module of 
 * Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_mtools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * SPLINE (http://people.sc.fsu.edu/~jburkardt/cpp_src/spline/spline.html)
 * 
 * License Agreement for SPLINE
 * -------------------------------------------------------------------------------------
 * Author: John Burkardt
 * 
 * The computer code and data files described and made available on this web page are 
 * distributed under the GNU LGPLv3 license.
 */

#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning

#ifndef MATH_TOOLS_H
#define MATH_TOOLS_H

#include <cmath>
#include <iostream>
#include <vector>
#include <limits>

// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// When the symbol viulib_mtools_EXPORTS is defined in a project MTOOLS exports functions and 
// classes. 
// In other cases MTOOLS imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_mtools_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define MTOOLS_LIB __declspec( dllexport )
	#else
		#define MTOOLS_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define MTOOLS_LIB __declspec( dllimport )
	#else
		#define MTOOLS_LIB
	#endif
#endif
#else
#define MTOOLS_LIB
#endif

// AUTOMATIC DLL LINKAGE
// If the code is compiled under MS VISUAL STUDIO, and MTOOLS_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_mtools_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_mtools_d.lib" )
	#else
		#pragma comment( lib, "viulib_mtools.lib" )
	#endif
#endif

namespace viulib
{
/** \brief This namespace contains all the functios and classes about mathematical tools
*/
namespace mtools
{
	//*******************************************************************************************
	// DEFINITIONS AND BASIC OPERATIONS
	//*******************************************************************************************
	#define MT_EPS 1e-6
	#define MT_INF 1e9
	#define MT_PI 3.1415926535897932384626433832795
	#define MT_SMALL_ANGLE 0.05

	MTOOLS_LIB double degreesToRadians( const double &degrees );
	MTOOLS_LIB double radiansToDegrees( const double &radians );
	MTOOLS_LIB float round(float val);

	MTOOLS_LIB double hypotenuse( const double &a, const double &b );

	template <typename T>
	MTOOLS_LIB void smooth (std::vector<T> &v,std::vector<T> &v_out,int kernel_size=2);
#ifdef WIN32
	template MTOOLS_LIB void smooth <double> (std::vector<double> &v,std::vector<double> &v_out,int kernel_size);
	template MTOOLS_LIB void smooth <float> (std::vector<float> &v,std::vector<float> &v_out,int kernel_size);
	template MTOOLS_LIB void smooth <int> (std::vector<int> &v,std::vector<int> &v_out,int kernel_size);
#endif
	
	//-- Taken from --//
	//-- Rafael C. Gonzalez and Richard E. Woods. 1992. Digital Image Processing (2nd ed.). Addison-Wesley Longman Publishing Co., Inc., Boston, MA, USA. --//


	template <typename T>
	MTOOLS_LIB void derivatives (const std::vector<T> &v,std::vector<T> &first_derivative,std::vector<T> &second_derivative);
#ifdef WIN32
	template MTOOLS_LIB void derivatives <double> (const std::vector<double> &v,std::vector<double> &first_derivative,std::vector<double> &second_derivative);
	template MTOOLS_LIB void derivatives <float> (const std::vector<float> &v,std::vector<float> &first_derivative,std::vector<float> &second_derivative);
	template MTOOLS_LIB void derivatives <int> (const std::vector<int> &v,std::vector<int> &first_derivative,std::vector<int> &second_derivative);
#endif
	template <typename T>
	std::vector<T> subvector (const std::vector<T> &v,int index,int size);
#ifdef WIN32
	template MTOOLS_LIB std::vector<double> subvector <double> (const std::vector<double> &v,int index, int size);
	template MTOOLS_LIB std::vector<float> subvector <float> (const std::vector<float> &v,int index,int size);
	template MTOOLS_LIB std::vector<int> subvector <int> (const std::vector<int> &v,int index, int size);
#endif

	template <typename T>
	void minmaxList (const std::vector<T> &v,std::vector<T> &dv,std::vector<int> &_minimos, std::vector<int> &_maximos,bool flag_edges=false);
#ifdef WIN32
	template MTOOLS_LIB void minmaxList <double> (const std::vector<double> &v,std::vector<double> &dv,std::vector<int> &_minimos, std::vector<int> &_maximos,bool flag_edges);
	template MTOOLS_LIB void minmaxList <float> (const std::vector<float> &v,std::vector<float> &dv,std::vector<int> &_minimos, std::vector<int> &_maximos,bool flag_edges);
	template MTOOLS_LIB void minmaxList <int> (const std::vector<int> &v,std::vector<int> &dv,std::vector<int> &_minimos, std::vector<int> &_maximos,bool flag_edges);
	template MTOOLS_LIB void minmaxList <unsigned char> (const std::vector<unsigned char> &v,std::vector<unsigned char> &dv,std::vector<int> &_minimos, std::vector<int> &_maximos,bool flag_edges);
#endif

	template <typename T>
	MTOOLS_LIB std::vector<int> zeroCrossings (const std::vector<T> &v);
#ifdef WIN32
	template MTOOLS_LIB std::vector<int> zeroCrossings <double> (const std::vector<double> &v);
	template MTOOLS_LIB std::vector<int> zeroCrossings <float> (const std::vector<float> &v);
	template MTOOLS_LIB std::vector<int> zeroCrossings <int> (const std::vector<int> &v);
#endif
	template <typename T>
	T maximun (const std::vector<T> &v,int &index);
#ifdef WIN32
	template MTOOLS_LIB double maximun <double> (const std::vector<double> &v,int &index);
	template MTOOLS_LIB int maximun <int> (const std::vector<int> &v,int &index);
	template MTOOLS_LIB float maximun <float> (const std::vector<float> &v,int &index);
	template MTOOLS_LIB short maximun <short> (const std::vector<short> &v,int &index);
#endif
	template <typename T>
	T minimun (const std::vector<T> &v,int &index);
#ifdef WIN32
	template MTOOLS_LIB double minimun <double> (const std::vector<double> &v,int &index);
	template MTOOLS_LIB int minimun <int> (const std::vector<int> &v,int &index);
	template MTOOLS_LIB float minimun <float> (const std::vector<float> &v,int &index);
	template MTOOLS_LIB short minimun <short> (const std::vector<short> &v,int &index);
#endif
	template <typename T>
	void write (std::string file_name, const std::vector<T> &v,bool dir=true, bool write = true);
#ifdef WIN32
	template MTOOLS_LIB void write <double> (std::string file_name, const std::vector<double> &v,bool dir, bool write);
	template MTOOLS_LIB void write <int> (std::string file_name, const std::vector<int> &v,bool dir, bool write);
	template MTOOLS_LIB void write <float> (std::string file_name, const std::vector<float> &v,bool dir, bool write);
#endif
	template <typename T>
	double getSymmetry (int &center,const std::vector<T> &v,int min=-1, int max=-1);
#ifdef WIN32
	template MTOOLS_LIB double getSymmetry <float> (int &center,const std::vector<float> &v,int min, int max);
	template MTOOLS_LIB double getSymmetry <int> (int &center,const std::vector<int> &v,int min, int max);
#endif

template <typename T>
	void cumulativeDistribution(std::vector<T> &p, std::vector<T> &cp,T min=0, T max=100);
#ifdef WIN32
	template MTOOLS_LIB void cumulativeDistribution <double> (std::vector<double> &p, std::vector<double> &cp,double min, double max);
	template MTOOLS_LIB void cumulativeDistribution <int> (std::vector<int> &p, std::vector<int> &cp,int min, int max);
	template MTOOLS_LIB void cumulativeDistribution <float> (std::vector<float> &p, std::vector<float> &cp,float min, float max);
	template MTOOLS_LIB void cumulativeDistribution <unsigned char> (std::vector<unsigned char> &p, std::vector<unsigned char> &cp,unsigned char min, unsigned char max);
#endif

	//*******************************************************************************************
	// SPLINE FUNCTIONALITIES
	//*******************************************************************************************
	// These spline functions are typically used to interpolate data exactly at a set of points; 
	// approximate data at many points, or over an interval.

	// The most common use of this software is for situations where a set of (X,Y) data points 
	// is known, and it is desired to determine a smooth function which passes exactly through 
	// those points, and which can be evaluated everywhere. Thus, it is possible to get a 
	// formula that allows you to "connect the dots".

	// Of course, you could could just connect the dots with straight lines, but that would 
	// look ugly, and if there really is some function that explains your data, you'd expect it 
	// to curve around rather than make sudden angular turns. The functions in SPLINE offer a 
	// variety of choices for slinky curves that will make pleasing interpolants of your data.

	// There are a variety of types of approximation curves available, including:

	// least squares polynomials, 
	// divided difference polynomials, 
	// piecewise polynomials, 
	// B splines, 
	// Bernstein splines, 
	// beta splines, 
	// Bezier splines, 
	// Hermite splines, 
	// Overhauser (or Catmull-Rom) splines.

	// Also included are a set of routines that return the local "basis matrix", which allows 
	// the evaluation of the spline in terms of local function data.

	// The list of routines includes:

	// BASIS_FUNCTION_B_VAL evaluates the B spline basis function.
	// BASIS_FUNCTION_BETA_VAL evaluates the beta spline basis function.
	// BASIS_MATRIX_B_UNI sets up the uniform B spline basis matrix.
	// BASIS_MATRIX_BETA_UNI sets up the uniform beta spline basis matrix.
	// BASIS_MATRIX_BEZIER_UNI sets up the cubic Bezier spline basis matrix.
	// BASIS_MATRIX_HERMITE sets up the Hermite spline basis matrix.
	// BASIS_MATRIX_OVERHAUSER_NONUNI sets up the nonuniform Overhauser spline basis matrix.
	// BASIS_MATRIX_OVERHAUSER_NUL sets up the nonuniform left Overhauser spline basis matrix.
	// BASIS_MATRIX_OVERHAUSER_NUR sets up the nonuniform right Overhauser spline basis matrix.
	// BASIS_MATRIX_OVERHAUSER_UNI sets up the uniform Overhauser spline basis matrix.
	// BASIS_MATRIX_OVERHAUSER_UNI_R sets up the right uniform Overhauser spline basis matrix.
	// BASIS_MATRIX_TMP computes Q = T * MBASIS * P
	// BC_VAL evaluates a parameterized Bezier curve.
	// BEZ_VAL evaluates a Bezier function at a point.
	// BP_APPROX evaluates the Bernstein polynomial for F(X) on [A,B].
	// BP01 evaluates the Bernstein basis polynomials for [0,1] at a point.
	// BPAB evaluates the Bernstein basis polynomials for [A,B] at a point.
	// CHFEV evaluates a cubic polynomial given in Hermite form.
	// D_MAX returns the maximum of two double precision values.
	// D_MIN returns the minimum of two double precision values.
	// D_UNIFORM returns a scaled double precision pseudorandom number.
	// D_UNIFORM_01 is a portable pseudorandom number generator.
	// D3_MXV multiplies a D3 matrix times a vector.
	// D3_NP_FS factors and solves a D3 system.
	// D3_PRINT prints a D3 matrix.
	// D3_PRINT_SOME prints some of a D3 matrix.
	// D3_UNIFORM randomizes a D3 matrix.
	// DATA_TO_DIF sets up a divided difference table from raw data.
	// DIF_VAL evaluates a divided difference polynomial at a point.
	// DVEC_BRACKET searches a sorted array for successive brackets of a value.
	// DVEC_BRACKET3 finds the interval containing or nearest a given value.
	// DVEC_EVEN returns N real values, evenly spaced between ALO and AHI.
	// DVEC_INDICATOR sets a real vector to the indicator vector.
	// DVEC_ORDER_TYPE determines if an array is (non)strictly ascending/descending.
	// DVEC_PRINT prints a real vector.
	// DVEC_UNIFORM fills a double precision vector with scaled pseudorandom values.
	// DVEC_SORT_BUBBLE_A ascending sorts a real vector using bubble sort.
	// I_MAX returns the maximum of two integers.
	// I_MIN returns the smaller of two integers.
	// LEAST_SET constructs the least squares polynomial approximation to data.
	// LEAST_VAL evaluates a least squares polynomial defined by LEAST_SET.
	// PARABOLA_VAL2 evaluates a parabolic function through 3 points in a table.
	// PCHST: PCHIP sign-testing routine.
	// S_LEN_TRIM returns the length of a string to the last nonblank.
	// SPLINE_B_VAL evaluates a cubic B spline approximant.
	// SPLINE_BETA_VAL evaluates a cubic beta spline approximant.
	// SPLINE_CONSTANT_VAL evaluates a piecewise constant spline at a point.
	// SPLINE_CUBIC_SET computes the second derivatives of a piecewise cubic spline.
	// SPLINE_CUBIC_VAL evaluates a piecewise cubic spline at a point.
	// SPLINE_CUBIC_VAL2 evaluates a piecewise cubic spline at a point.
	// SPLINE_HERMITE_SET sets up a piecewise cubic Hermite interpolant.
	// SPLINE_HERMITE_VAL evaluates a piecewise cubic Hermite interpolant.
	// SPLINE_LINEAR_INT evaluates the integral of a piecewise linear spline.
	// SPLINE_LINEAR_INTSET sets a piecewise linear spline with given integral properties.
	// SPLINE_LINEAR_VAL evaluates a piecewise linear spline at a point.
	// SPLINE_OVERHAUSER_NONUNI_VAL evaluates the nonuniform Overhauser spline.
	// SPLINE_OVERHAUSER_UNI_VAL evaluates the uniform Overhauser spline.
	// SPLINE_OVERHAUSER_VAL evaluates an Overhauser spline.
	// SPLINE_PCHIP_SET sets derivatives for a piecewise cubic Hermite interpolant.
	// SPLINE_PCHIP_VAL evaluates a piecewise cubic Hermite function.
	// SPLINE_QUADRATIC_VAL evaluates a piecewise quadratic spline at a point.
	// TIMESTAMP prints the current YMDHMS date as a time stamp.

	//Reference: 

	// 1.	H Brewer and D Anderson,
	//		Visual Interaction with Overhauser Curves and Surfaces,
	//		SIGGRAPH 77. 
	// 2.	E Catmull and R Rom,
	//		A Class of Local Interpolating Splines,
	//		in Computer Aided Geometric Design,
	//		edited by R Barnhill and R Reisenfeld,
	//		Academic Press, 1974. 
	// 3.	Samuel Conte and Carl de Boor,
	//		Elementary Numerical Analysis,
	//		1973. 
	// 4.	Alan Davies and Philip Samuels,
	//		An Introduction to Computational Geometry for Curves and Surfaces,
	//		Clarendon Press, 1996. 
	// 5.	Carl de Boor,
	//		A Practical Guide to Splines,
	//		Springer Verlag, 1978. 
	// 6.	Jack Dongarra, Cleve Moler, Bunch and Stewart,
	//		LINPACK User's Guide,
	//		SIAM, 1978. 
	// 7.	Gisela Engeln-Muellges and Frank Uhlig,
	//		Numerical Algorithms with C,
	//		Springer, 1996.
	// 8.	Foley, van Dam, Feiner, Hughes,
	//		Computer Graphics: Principles and Practice,
	//		Addison-Wesley, 1990. 
	// 9.	Fred Fritsch and R Carlson, 
	//		Monotone Piecewise Cubic Interpolation,
	//		SIAM Journal on Numerical Analysis,
	//		Volume 17, Number 2, April 1980, pages 238-246. 
	// 10.	Kahaner, Cleve Moler, and Nash,
	//		Numerical Methods and Software,
	//		Prentice Hall, 1989.
	// 11.	David Rogers and Alan Adams,
	//		Mathematical Elements of Computer Graphics,
	//		McGraw Hill, 1990, Second Edition.
	MTOOLS_LIB double basis_function_b_val( double tdata[], double tval );
	MTOOLS_LIB double basis_function_beta_val( double beta1, double beta2, double tdata[], 
		double tval );
	MTOOLS_LIB double *basis_matrix_b_uni( void );
	MTOOLS_LIB double *basis_matrix_beta_uni( double beta1, double beta2 );
	MTOOLS_LIB double *basis_matrix_bezier( void );
	MTOOLS_LIB double *basis_matrix_hermite( void );
	MTOOLS_LIB double *basis_matrix_overhauser_nonuni( double alpha, double beta );
	MTOOLS_LIB double *basis_matrix_overhauser_nul( double alpha );
	MTOOLS_LIB double *basis_matrix_overhauser_nur( double beta );
	MTOOLS_LIB double *basis_matrix_overhauser_uni( void);
	MTOOLS_LIB double *basis_matrix_overhauser_uni_l( void );
	MTOOLS_LIB double *basis_matrix_overhauser_uni_r( void );
	MTOOLS_LIB double basis_matrix_tmp( int left, int n, double mbasis[], int ndata, 
		double tdata[], double ydata[], double tval );
	MTOOLS_LIB void bc_val( int n, double t, double xcon[], double ycon[], double *xval, 
		double *yval );
	MTOOLS_LIB double bez_val( int n, double x, double a, double b, double y[] );
	MTOOLS_LIB double bp_approx( int n, double a, double b, double ydata[], double xval );
	MTOOLS_LIB double *bp01( int n, double x );
	MTOOLS_LIB double *bpab( int n, double a, double b, double x );
	MTOOLS_LIB int chfev( double x1, double x2, double f1, double f2, double d1, double d2, 
		int ne, double xe[], double fe[], int next[] );
	MTOOLS_LIB double d_max( double x, double y );
	MTOOLS_LIB double d_min( double x, double y );
	MTOOLS_LIB double d_uniform( double b, double c, int *seed );
	MTOOLS_LIB double d_uniform_01( int *seed );
	MTOOLS_LIB int d3_fs( double a1[], double a2[], double a3[], int n, double b[], 
		double x[] );
	MTOOLS_LIB double *d3_mxv( int n, double a[], double x[] );
	MTOOLS_LIB double *d3_np_fs( int n, double a[], double b[] );
	MTOOLS_LIB void d3_print( int n, double a[], char *title );
	MTOOLS_LIB void d3_print_some( int n, double a[], int ilo, int jlo, int ihi, int jhi );
	MTOOLS_LIB double *d3_uniform( int n, int *seed );
	MTOOLS_LIB void data_to_dif( int ntab, double xtab[], double ytab[], double diftab[] );
	MTOOLS_LIB double dif_val( int ntab, double xtab[], double diftab[], double xval );
	MTOOLS_LIB void dvec_bracket( int n, double x[], double xval, int *left, int *right );
	MTOOLS_LIB void dvec_bracket3( int n, double t[], double tval, int *left );
	MTOOLS_LIB double *dvec_even( int n, double alo, double ahi );
	MTOOLS_LIB double *dvec_indicator( int n );
	MTOOLS_LIB void dvec_order_type( int n, double x[], int *order );
	MTOOLS_LIB void dvec_print( int n, double a[], char *title );
	MTOOLS_LIB double *dvec_uniform( int n, double b, double c, int *seed );
	MTOOLS_LIB void dvec_sort_bubble_a( int n, double a[] );
	MTOOLS_LIB int i_max( int i1, int i2 );
	MTOOLS_LIB int i_min( int i1, int i2 );
	MTOOLS_LIB void least_set( int ntab, double xtab[], double ytab[], int ndeg, 
		double ptab[], double b[], double c[], double d[], double *eps, int *ierror );
	MTOOLS_LIB double least_val( double x, int ndeg, double b[], double c[], double d[] );
	MTOOLS_LIB void parabola_val2( int ndim, int ndata, double tdata[], double ydata[], 
		int left, double tval, double yval[] );
	MTOOLS_LIB double pchst( double arg1, double arg2 );
	MTOOLS_LIB int s_len_trim( char* s );
	MTOOLS_LIB double spline_b_val( int ndata, double tdata[], double ydata[], double tval );
	MTOOLS_LIB double spline_beta_val( double beta1, double beta2, int ndata, double tdata[], 
		double ydata[], double tval );
	MTOOLS_LIB double spline_constant_val( int ndata, double tdata[], double ydata[], 
		double tval );
	MTOOLS_LIB double *spline_cubic_set( int n, double t[], double y[], int ibcbeg, 
		double ybcbeg, int ibcend, double ybcend );
	MTOOLS_LIB double spline_cubic_val( int n, double t[], double tval, double y[], 
		double ypp[], double *ypval, double *yppval );
	MTOOLS_LIB void spline_cubic_val2( int n, double t[], double tval, int *left, double y[], 
		double ypp[], double *yval, double *ypval, double *yppval );
	MTOOLS_LIB double *spline_hermite_set( int ndata, double tdata[], double ydata[], 
		double ypdata[] );
	MTOOLS_LIB void spline_hermite_val( int ndata, double tdata[], double c[], double tval, 
		double *sval, double *spval );
	MTOOLS_LIB double spline_linear_int( int ndata, double tdata[], double ydata[], double a, 
		double b );
	MTOOLS_LIB void spline_linear_intset( int int_n, double int_x[], double int_v[], 
		double data_x[], double data_y[] );
	MTOOLS_LIB void spline_linear_val( int ndata, double tdata[], double ydata[], double tval, 
		double *yval, double *ypval );
	MTOOLS_LIB double spline_overhauser_nonuni_val( int ndata, double tdata[], double ydata[], 
		double tval );
	MTOOLS_LIB double spline_overhauser_uni_val( int ndata, double tdata[], double ydata[], 
		double tval );
	MTOOLS_LIB void spline_overhauser_val( int ndim, int ndata, double tdata[], double ydata[], 
		double tval, double yval[] );
	MTOOLS_LIB void spline_pchip_set( int n, double x[], double f[], double d[] );
	MTOOLS_LIB void spline_pchip_val( int n, double x[], double f[], double d[], int ne, 
		double xe[], double fe[] );
	MTOOLS_LIB void spline_quadratic_val( int ndata, double tdata[], double ydata[], double tval, 
		double *yval, double *ypval );
	MTOOLS_LIB void timestamp( void );

	//*******************************************************************************************
	// ALGEBRA FUNCTIONALITIES
	//*******************************************************************************************
	//*******************************************************************************************
	// Vec3 CLASS
	//*******************************************************************************************
	class MTOOLS_LIB Vec3
	{
	public:
		union
		{
			double vec[3];
			struct
			{
				double x, y, z;
			};
		};
		
		// Constructors
		Vec3();
		Vec3( double x, double y, double z );
		Vec3( const Vec3 &other );
			
		// Operators
		Vec3 operator-() const;
		Vec3 &operator=( const Vec3 &v );
		double &operator[]( int i );
		const double &operator[]( int i ) const;
		bool operator==( const Vec3 &v );
		bool operator!=( const Vec3 &v );
		const Vec3 &operator+=( const Vec3 &v );
		const Vec3 &operator-=( const Vec3 &v );
		const Vec3 &operator*=( double value );
		Vec3 &operator/=( double d );

		// Functions
		void getValue( double &x, double &y, double &z ) const;
		void setValue( double x, double y, double z );
		bool isZero();
		double length() const;
		double squaredLength() const;
		Vec3 &normalize();
		Vec3 &add( const Vec3 &v1, const Vec3 &v2 );
		Vec3 &substract( const Vec3 &v1, const Vec3 &v2 );
		bool isInPlane( double px[], double l );
		Vec3 makePerpendicular();
		void invert();

		// Static functions
		static Vec3 makeZero();
		static Vec3 INF;
	};

	//*******************************************************************************************
	// Quat CLASS
	//*******************************************************************************************
	class MTOOLS_LIB Quat
	{
	public:
		union
		{
			double quat[4];
			struct
			{
				double x, y, z, w;
			};
		};

		// Constructors
		Quat();
		Quat( double x, double y, double z, double w );
		Quat( const Vec3 &dir, double angle );
		Quat( const Quat &q );
		
		// Operators
		double &operator[]( int i );
		double operator[]( int i ) const;
		Quat &operator+=( const Quat &q );
		Quat &operator-=( const Quat &q );
		void operator*=( const Vec3 &q );
		void operator*=( const Quat &q );
		bool operator==( const Quat &q ) const;
		bool operator!=( const Quat &q ) const;

		// Functions
		void getValue( double &x, double &y, double &z, double &w ) const;
		void setValue( double x, double y, double z, double w );
		void setAxisAngle( const Vec3 &dir, double angle );
		void normalize();
		void conjugate();

		// Static functions
		static Quat log( const Quat &q );
		static Quat exp( const Quat &q );
		static Quat lerp( const Quat &q1, const Quat &q2, double t );
		static Quat slerp( const Quat &q1, const Quat &q2, double t );
		static Quat slerpNoInvert( const Quat &q1, const Quat &q2, double t );
		static Quat squad( const Quat &q1, const Quat &q2, const Quat &a, const Quat &b, double t );
		static Quat spline( const Quat &qnm1, const Quat &qn, const Quat &qnp1 );
		static Quat blend( const Quat &q1, const Quat &q2, double d );
	};

	//*******************************************************************************************
	// Mat3 CLASS
	//*******************************************************************************************
	class MTOOLS_LIB Mat3
	{
		union
		{
			double mat[3][3];
			struct
			{
				double m11, m12, m13, m21, m22, m23, m31, m32, m33;
			};
		};

		void copy( const Mat3 &other );

	public:
		// Constructors
		Mat3();
		Mat3( Vec3 row1, Vec3 row2, Vec3 row3 );
		Mat3( double a11, double a12, double a13,
			  double a21, double a22, double a23,
			  double a31, double a32, double a33 );
		Mat3( double a11, double a22, double a33 ); 
		Mat3( const Mat3 &m );

		// Operators
		Mat3 &operator-();
		Mat3 operator=( const Mat3 &other );
		double operator()( int i, int j ) const;
		double &operator()( int i, int j );
		double *operator[]( int i );
		const double *operator[]( int i ) const;
		Mat3 &operator+=( const Mat3 &mat2 );
		Mat3 &operator-=( const Mat3 &mat2 );
		Mat3 &operator*=( const Mat3 &mat2 );
		Mat3 &operator*=( const double &factor );
		Mat3 &operator/=( const double &factor );
		bool operator==( const Mat3 &m );
		bool operator!=( const Mat3 &m );

		// Functions
		int dim1() const;
		int dim2() const;
		Vec3 X();
		Vec3 Y();
		Vec3 Z();
		Vec3 getRow( int i );
		Vec3 getColumn( int i );
		void setRow( const Vec3 &v, int i );
		void setColumn( const Vec3 &v, int i );
		void getQuaternion( double &x, double &y, double &z, double &alpha ) const;
		void getAxisAngle( double &x, double &y, double &z, double &alpha ) const;
		Vec3 getEulerAngles() const;
		Vec3 getZXYEulerAngles() const;
		Vec3 getZYXEulerAngles() const;
		double determinant();
		Mat3 &invert();
		void transpose();
		void orthonormalize( const int row=0 );
		Mat3 identity();
		Mat3 trans() const;
		Mat3 orthonorm() const;
		void setToZero();
		void makeIdentity();
		
		// Static functions
		static Mat3 makeXRot( double alpha );
		static Mat3 makeYRot( double beta );
		static Mat3 makeZRot( double gamma );
		static Mat3 createFromAxisAngle( double x, double y, double z, double psi );
		static Mat3 createFromQuaternion( const Quat &quat );
		static Mat3 createFromQuaternion( double x, double y, double z, double w );
		static Mat3 createFromEulerAngles( const Vec3 &angles );
		static Mat3 createFromEulerAngles( double a1, double a2, double a3 );
		static Mat3 createFromZXYEulerAngles( const Vec3 &angles );
		static Mat3 createFromZXYEulerAngles( double a1, double a2, double a3 );
		static Mat3 createFromZYXEulerAngles( const Vec3 &angles );
		static Mat3 createFromZYXEulerAngles( double a1, double a2, double a3 );
		static Mat3 INF;
		static Mat3 ID;
	};

	//*******************************************************************************************
	// EMap CLASS
	//*******************************************************************************************
	class MTOOLS_LIB EMap
	{
		double module;
		void reparametrize();

	public:
		union
		{
			double vec[3];
			struct
			{
				double x, y, z;
			};
		};

		// Constructors
		EMap();
		EMap( double x, double y, double z );
		EMap( const EMap &other );
		
		// Operators
		EMap operator-() const;
		EMap &operator=( const EMap &v );
		double &operator[]( int i );
		const double &operator[]( int i ) const;
		const EMap &operator+=( const EMap &v );
		const EMap &operator-=( const EMap &v );
		const EMap &operator*=( double value );
		EMap &operator/=( double d );
		bool operator==( const EMap &v );
		bool operator!=(const EMap &v);

		// Functions
		void getValue( double &x, double &y, double &z ) const;
		void setValue( double x, double y, double z );
		bool isZero();
		double getModule() const;
		Mat3 getRotationMatrix() const;
		void getAxisAngle( double &x, double &y, double &z, double &alpha ) const;
		Quat getQuaternion() const;
		bool isInPlane( double px[], double l );
		EMap makePerpendicular();
		EMap &add( const EMap &v1, const EMap &v2 );
		EMap &substract( const EMap &v1, const EMap &v2 );
		void invert();

		// Static functions
		static EMap makeZero();
		static EMap createFromRotationMatrix( const Mat3 &mat );
		static EMap createFromAxisAngle( double x, double y, double z, double alpha );
		static EMap createFromQuaternion( double x, double y, double z, double w );
		static EMap createFromQuaternion( const Quat &q );
		static EMap lerp( const EMap &v1, const EMap &v2, double t );
	};

	//*******************************************************************************************
	// MatN CLASS
	//*******************************************************************************************
	class MTOOLS_LIB MatN
	{
		double **mat;
		double *data;
		int m, n;

		void allocate( int m, int n );
		void freeData();
		void copy( const MatN &other );

	public: 
		// Constructors
		MatN();
		MatN( int n );
		MatN( int m, int n );
		MatN( int m, int n, double val );
		MatN( const MatN &other );
		~MatN();

		// Operators
		MatN &operator-();
		MatN operator=( const MatN &other );
		double operator()( int i, int j ) const;
		double &operator()( int i, int j );
		double *operator[]( int i );
		const double *operator[]( int i ) const;
		MatN &operator+=( const MatN &mat2 );
		MatN &operator-=( const MatN &mat2 );
		MatN &operator*=( const MatN &factor );
		MatN &operator*=( const double &factor );
		MatN &operator/=( const double &factor );
		bool operator==( const MatN &m );
		bool operator!=( const MatN &m );

		// Functions
		int dim1() const;
		int dim2() const;
		double *getData();
		const double *getData() const;
	};

	//*******************************************************************************************
	// LU CLASS
	//*******************************************************************************************
	class MTOOLS_LIB LU
	{
		MatN LU_;
		int m, n, pivsign;
		MatN piv;

		MatN permute_copy( const MatN &A, const MatN &piv, int j0, int j1 );
		MatN permute_copy( const MatN &A, const MatN &piv );

	public:
		LU( const MatN &A );
		int isNonsingular();
		MatN getL();
		MatN getU();
		MatN getPivot();
		double det();
		MatN solve( const MatN &B );
	};

	//*******************************************************************************************
	// QR CLASS
	//*******************************************************************************************
	class MTOOLS_LIB QR
	{
		MatN QR_;
		int m, n;
		MatN Rdiag;

	public:
		QR( const MatN &A );
		int isFullRank() const;
		MatN getHouseholder() const;
		MatN getR() const;
		MatN getQ() const;
		MatN solve( const MatN &B ) const;
	};

	//*******************************************************************************************
	// EIG CLASS
	//*******************************************************************************************
	class MTOOLS_LIB EIG
	{
		// Row and column dimension (square matrix)
		int n;
		int issymmetric;

		// Arrays for internal storage of eigenvalues
		MatN d;         // real part
		MatN e;         // img part

		// Array for internal storage of eigenvectors
		MatN V;

		// Array for internal storage of nonsymmetric Hessenberg form
		MatN H;

		// Working storage for nonsymmetric algorithm
		MatN ort;

		// Symmetric Householder reduction to tridiagonal form
		void tred2();

		// Symmetric tridiagonal QL algorithm
		void tql2();

		// Nonsymmetric reduction to Hessenberg form
		void orthes();

		// Complex scalar division
		double cdivr, cdivi;
		void cdiv( double xr, double xi, double yr, double yi );

		// Nonsymmetric reduction from Hessenberg to real Schur form
		void hqr2();

	public:
		// Check for symmetry, then construct the eigenvalue decomposition
		EIG( const MatN &A );

		// Return the eigenvector matrix
		void getV( MatN &V_ );

		// Return the real parts of the eigenvalues
		void getRealEigenvalues( MatN &d_ );

		//Return the imaginary parts of the eigenvalues
		//in parameter e_.
		void getImagEigenvalues( MatN &e_ );

		//Computes the block diagonal eigenvalue matrix.
		//If the original matrix A is not symmetric, then the eigenvalue 
		//matrix D is block diagonal with the real eigenvalues in 1-by-1 
		//blocks and any complex eigenvalues,
		//a + i*b, in 2-by-2 blocks, [a, b; -b, a].  That is, if the complex
		//eigenvalues look like

		//u + iv     .        .          .      .    .
		//.      u - iv     .          .      .    .
		//.        .      a + ib       .      .    .
		//.        .        .        a - ib   .    .
		//.        .        .          .      x    .
		//.        .        .          .      .    y
		
		//then D looks like

		//u        v        .          .      .    .
		//-v        u        .          .      .    . 
		//.        .        a          b      .    .
		//.        .       -b          a      .    .
		//.        .        .          .      x    .
		//.        .        .          .      .    y

		//This keeps V a real matrix in both symmetric and non-symmetric
		//cases, and A*V = V*D.
		void getD( MatN &D );
	};

	//*******************************************************************************************
	// SVD CLASS
	//*******************************************************************************************
	class MTOOLS_LIB SVD 
	{
		MatN U, V;
		MatN s;
		int m, n;

	public:
		// Constructor
		SVD( const MatN &Arg );

		// Return the left singular vectors
		void getU( MatN &A );

		// Return the right singular vectors
		void getV( MatN &A );

		// Return the one-dimensional array of singular values
		void getSingularValues( MatN &x );

		// Return the diagonal matrix of singular values
		void getS( MatN &A );

		// Two norm  (max(S))
		double norm2();

		// Two norm of condition number ( max(S)/min(S) )
		double cond();

		// Effective numerical matrix rank
		int rank();
	};

	// Vec3 operators
	MTOOLS_LIB Vec3 operator+( const Vec3 &v1, const Vec3 &v2 );
	MTOOLS_LIB Vec3 operator-( const Vec3 &v1, const Vec3 &v2 );
	MTOOLS_LIB Vec3 operator*( const Vec3 &v1, double f );
	MTOOLS_LIB Vec3 operator*( double f, const Vec3 &v1 );
	MTOOLS_LIB Vec3 operator/( double f, const Vec3 &v1 );
	MTOOLS_LIB Vec3 operator/( const Vec3 &v1, double f );
	MTOOLS_LIB std::ostream &operator<<( std::ostream &os, const Vec3 &v );

	// Quat operators
	MTOOLS_LIB Quat operator+( const Quat &q1, const Quat &q2 );
	MTOOLS_LIB Quat operator-( const Quat &q1, const Quat &q2 );
	MTOOLS_LIB Quat operator*( const Quat &q1, const Quat &q2 );
	MTOOLS_LIB Quat operator*( const Quat &q, double v );
	MTOOLS_LIB Quat operator*( double v, const Quat &q );
	MTOOLS_LIB Quat operator/( const Quat &q, double v );
	MTOOLS_LIB std::ostream &operator<<( std::ostream &os, const Quat &q );

	// Mat3 operators
	MTOOLS_LIB Mat3 operator+( const Mat3 &m, const Mat3 &n );
	MTOOLS_LIB Mat3 operator-(const Mat3 &m, const Mat3 &n );
	MTOOLS_LIB Mat3 operator*( const Mat3 &mat1, const Mat3 &mat2 );
	MTOOLS_LIB Vec3 operator*( const Mat3 &mat, const Vec3 &vec );
	MTOOLS_LIB Mat3 operator*( double a, const Mat3 &m );
	MTOOLS_LIB Mat3 operator*( const Mat3 &m, double a );
	MTOOLS_LIB std::ostream &operator<<( std::ostream &os, const Mat3 &m );
	
	// EMap operators
	MTOOLS_LIB EMap operator+( const EMap &v1, const EMap &v2 );
	MTOOLS_LIB EMap operator-( const EMap &v );
	MTOOLS_LIB EMap operator-( const EMap &v1, const EMap &v2 );
	MTOOLS_LIB EMap operator*( const EMap &v1, double f );
	MTOOLS_LIB EMap operator*( double f, const EMap &v1 );
	MTOOLS_LIB EMap operator/( double f, const EMap &v1 );
	MTOOLS_LIB EMap operator/( const EMap &v1, double f );
	MTOOLS_LIB std::ostream &operator<<( std::ostream &os, const EMap &v );

	// MatN operators
	MTOOLS_LIB MatN operator+( const MatN &m1, const MatN &m2 );
	MTOOLS_LIB MatN operator-( const MatN &m1, const MatN &m2 );
	MTOOLS_LIB MatN operator*( const MatN &m1, const MatN &m2 );
	MTOOLS_LIB MatN operator*( const MatN &m1, double f );
	MTOOLS_LIB MatN operator*( double f, const MatN &m1 );
	MTOOLS_LIB MatN operator/( const MatN &m1, double f );
	MTOOLS_LIB std::ostream &operator<<( std::ostream &os, const MatN &m );

	// Vec3 functions
	MTOOLS_LIB double dot( const Vec3 &v1, const Vec3 &v2 );
	MTOOLS_LIB Vec3 cross( const Vec3 &v1, const Vec3 &v2 );

	// Mat3 functions
	MTOOLS_LIB Mat3 transpose( const Mat3 &m );
	MTOOLS_LIB Mat3 inv( const Mat3 &a );
	MTOOLS_LIB Vec3 sum( const Mat3 &m1, bool columnwise );

	// EMap functions
	MTOOLS_LIB double dot( const EMap &v1, const EMap &v2 );
	MTOOLS_LIB EMap cross( const EMap &v1, const EMap &v2 );
	MTOOLS_LIB MatN dQdV( const Quat &q );
	MTOOLS_LIB Vec3 eMapVelocity( EMap expMap, Vec3 w );

	// MatN functions
	MTOOLS_LIB MatN transpose( const MatN &m );
	MTOOLS_LIB Vec3 sumByRows( const MatN &m1 );
	MTOOLS_LIB double norm( const MatN &m1 );
	MTOOLS_LIB MatN invQR( const MatN &a );
    MTOOLS_LIB MatN invLU( const MatN &a );
	MTOOLS_LIB double det( const MatN &a );
	MTOOLS_LIB void zeros( MatN &m );
	MTOOLS_LIB void ones( MatN &m );
	MTOOLS_LIB void eye( MatN &m );

	//*******************************************************************************************
	// RANSAC CLASS
	//*******************************************************************************************
	class MTOOLS_LIB RANSAC 
	{
	private:
		// RANSAC parameters
		double m_epsilon;
		double m_P_inlier;
		double m_sigma;
		double m_T_noise_squared;
		
		int m_min_iters;
		int m_max_iters;

		bool m_reestimate;
		bool m_update_T_iter;
		bool m_notify;
		

		// RANSAC precalculated parameters
		int m_MSS_dim;
		int m_N_I_best;
		double m_J_best;

		// Minimal Sample Set (MSS)
		std::vector<int> m_MSS;
		std::vector<int> m_MSS_best;

		// Consensus Set (CS)
		std::vector<int> m_ind_CS;				// current
		std::vector<int> m_CS_best;
		std::vector<int> m_ind_CS_best;

		// Result
		std::vector<std::vector<double> > m_result;	// Vector of vector because of _numModes		
		std::vector<int> m_numInliers;				// vector of number of inliers

	protected:
		std::vector<int> m_CS_idx;
		bool m_verbose;
		
	public:
		RANSAC();

		// Main function to run and find best solution
		void run(std::vector<double>& _params, void* _data, int _numData, int _numModes = 1);

		// Set parameter
		void setParameter(void* value, const std::string &name);	

		// Get parameter
		void getParameter(void* _value, const std::string& _name) const;

		void removeOutliers( void* _data, std::vector<int>& _ind_CS ) {}

		// ---------------------
		// Virtual functions
		// ---------------------
		// Desstructor		
		virtual ~RANSAC() {}

		// Error function		
		virtual double evaluateCS(const std::vector<double>& _params, const void* _data, int _numData, std::vector<double>& _fvec, int* _CS_count, int _modeNum) = 0;

		// Estimate MSS function
		virtual void estimateMSS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _MSS) = 0;

		// Estimate with all inliers (CS)
		virtual void estimateCS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _ind_CS) = 0;
	
				
		// Estimate with all inliers (CS)
		virtual void fillMinimalSampleSet(const void* _data, int _numData, std::vector<int>& _MSS);

	
	private:
		void getMinimalSampleSet(std::vector<double>& _params, const void* _data, int _numData, std::vector<int>& _MSS);
		double getConsensusSet(const std::vector<double>& _params, const void* _data, int _numData, std::vector<int>& _CS_idx, std::vector<double>& _fvec, int* _N_I, int _modeNum);
	};

	//*******************************************************************************************
	// CIRCUMFERENCE OPERATIONS
	//*******************************************************************************************
	class MTOOLS_LIB Circumference
	{		
	private:
		double cx, cy;

		double r;
		double r_2;
		
		double M11, M12, M13, M14;

	public:

		static Circumference getCircumference ( double p1x, double p1y, double p2x, double p2y, double p3x, double p3y );
		bool isCircle() const { return ( std::abs(M11) > 1e-5 )?(true):(false); }

		double getCenterX() const { return cx; }
		double getCenterY() const { return cy; }
		double getRadius() const { return r; }
	};
    
    
    //*******************************************************************************************
    // OPTIMIZATION LEVENMENT MENKWARD METHOD
    //*******************************************************************************************
#define LM_USERTOL 30*__DBL_EPSILON__

	/* Collection of control (input) parameters. */
	typedef struct {
		double ftol;      /* relative error desired in the sum of squares. */
		double xtol;      /* relative error between last two approximations. */
		double gtol;      /* orthogonality desired between fvec and its derivs. */
		double epsilon;   /* step used to calculate the jacobian. */
		double stepbound; /* initial bound to steps in the outer loop. */
		int maxcall;      /* maximum number of iterations. */
		int scale_diag;   /* UNDOCUMENTED, TESTWISE automatical diag rescaling? */
		int printflags;   /* OR'ed to produce more noise */
	} lm_ctrl;

	/* Collection of status (output) parameters. */
	typedef struct {
		double fnorm;     /* norm of the residue vector fvec. */
		int nfev;         /* actual number of iterations. */
		int info;         /* status (index for lm_infmsg and lm_shortmsg). */
	} lm_status;
    
    class MTOOLS_LIB LmMin
    {
    public:
        enum control_config{
            LM_USER,
            LM_DOUBLE,
            LM_FLOAT
        };

    protected:
        lm_ctrl m_control;
        lm_status  m_status;
        
    public:
        LmMin();
        LmMin( const control_config& cc );
        virtual ~LmMin(){}
        
        void optimize(int n_par, double *par, int m_dat, const void *data,
                      void (*evaluate) (const double *par, int m_dat, const void *data,
                                        double *fvec, int *info),
                      void (*printout) (int n_par, const double *par, int m_dat,
                                        const void *data, const double *fvec,
                                        int printflags, int iflag, int iter, int nfev));
        
        void setPredefinedControlStruct( const control_config& sel, const int& verbose = 0 );
		lm_ctrl getPredefinedControlStruct( const control_config& sel, const int& verbose = 0 );
        void setControlStruct( const lm_ctrl& cntrl ){ m_control = cntrl; }
        void setStatusStruct( const lm_status& sts ){ m_status = sts; }
        
        lm_ctrl getControlStruct(){ return m_control; }
        lm_status  getStatusStruct(){ return m_status; }
        
    };
}
}

#endif
