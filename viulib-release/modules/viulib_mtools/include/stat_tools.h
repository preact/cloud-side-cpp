/**
 * viulib_mtools (Mathematical Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_mtools is a module of 
 * Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_mtools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * SPLINE (http://people.sc.fsu.edu/~jburkardt/cpp_src/spline/spline.html)
 * 
 * License Agreement for SPLINE
 * -------------------------------------------------------------------------------------
 * Author: John Burkardt
 * 
 * The computer code and data files described and made available on this web page are 
 * distributed under the GNU LGPLv3 license.
 */

#ifndef STAT_TOOLS_H
#define STAT_TOOLS_H

#include "mtools.h"

namespace viulib
{
namespace mtools
{

/*********************************************************************
  Returns the value ln(Gamma(xx)) for xx>0. Full accuracy is obtained
  for xx > 1. For 0<xx<1, the reflection formula can be used first:

     Gamma(1-z) = pi/Gamma(z)/sin(pi*z) = pi*z/Gamma(1+z)/sin(pi*z)
*********************************************************************/ 
MTOOLS_LIB double lngamma(double xx);
MTOOLS_LIB double gamma(double xx);

/**********************************************
  Returns the incomplete beta function Ix(a,b)
**********************************************/
MTOOLS_LIB double betai(double a,double b,double x);

/*****************************************************************
  Continued fraction for incomplete beta function (used by BETAI)
*****************************************************************/
MTOOLS_LIB double betacf(double a,double b,double x);

/**************************************************
*          Statistical distributions              *
* ----------------------------------------------- *
*   This program allows computing several distri- *
*   butions:                                      *
*     1.  binomial distribution                   *
*     2.  Poisson distribution                    *
*     3.  normal distribution                     *
*     4.  normal distribution (2 variables)       *
*     5.  chi-square distribution                 *    
*     6.  Student T distribution                  *
*     7.  F-distribution                          *
* ----------------------------------------------- *
* REFERENCE: "Mathematiques et statistiques By H. *
*             Haut, PSI Editions, France, 1981"   *
*             [BIBLI 13].                         *
*                                                 *
*                    C++ version by J-P Moreau.   *
*                        (www.jpmoreau.fr)        *
* ----------------------------------------------- *
* SAMPLE RUN:                                     *
*                                                 *
*       Statistical distributions                 *
*                                                 *
* Tutorial                                        *
*                                                 *
* 1. Input distribution number:                   *
*                                                 *
*    1: binomial distribution                     *
*    2: Poisson distribution                      *
*    3: normal distribution                       *
*    4: normal distribution (2 variables)         *
*    5: chi-square distribution                   *
*    6: Student T distribution                    *
*                                                 *
* 2. Define the parameters of chosen distribution *
*                                                 *
* 3. Input value of random variable               *
*                                                 *
*                                                 *
* Input distribution number (1 to 6): 3           *
*                                                 *
*   Normal distribution                           *
*                                                 *
*   MU=mean                                       *
*   S =standard deviation                         *
*   X =value of random variable                   *
*                                                 *
*   MU = 2                                        *
*   S  = 3                                        *
*   X  = 5                                        *
*                                                 *
*   Probability of random variable  = X: .0806569 *
*   Probability of random variable <= X: .8413447 *
*   Probability of random variable >= X: .1586552 *
*                                                 *
**************************************************/

/***************************************************
*            BINOMIAL  DISTRIBUTION                *
* ------------------------------------------------ *
* INPUTS:                                          *
*          p:  probability of success              *
*          n:  number of trials                    *
*          x:  value of random variable            *
* OUTPUTS:                                         *
*          fx: probability of random variable  = X *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Binomial(double p,int n,double x,
                         double *fx,double *px, double *qx);

/***************************************************
*              POISSON  DISTRIBUTION               *
* ------------------------------------------------ *
* INPUTS:                                          *
*          xm:  mean                               *
*           x:  value of random variable           *
* OUTPUTS:                                         *
*          fx: probability of random variable  = X *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Poisson(double xm, double x,
                        double *fx,double *px, double *qx);

/***************************************************
* INPUTS:                                          *
*          xm: mean                                *
*           s: standard deviation                  *
*           x: value of random variable            *
* OUTPUTS:                                         *
*          fx: probability of random variable  = X *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Normal(double xm,double s,double x,
                       double *fx,double *px, double *qx);

/***************************************************
*        Normal distribution (2 variables)         *
* ------------------------------------------------ *
* INPUTS:                                          *
*          xm: mean of x                           *
*          ym: mean of y                           *
*          sx: standard deviation of x             *
*          sy: standard deviation of y             *
*          ro: correlation coefficient             *
*           x: value of 1st random variable        *
*           y: value of 2nd random variable        *
* OUTPUTS:                                         *
*         fxy: probability of random variables     *
*              = (X,Y)                             *
***************************************************/
MTOOLS_LIB void Normal2(double xm,double ym,double sx,double sy,double ro,
                        double x,double y,double *fxy);

/***************************************************
*             Chi-square distribution              *
* ------------------------------------------------ *
* INPUTS:                                          *
*          nu: number of degrees of freedom        *
*           x: value of random variable            *
* OUTPUTS:                                         *
*          fx: probability of random variable = X  *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Chi2(int nu, double x, 
          double *fx, double *px, double *qx);

/***************************************************
*            Student's T distribution              *
* ------------------------------------------------ *
* INPUTS:                                          *
*          nu: number of degrees of freedom        *
*           x: value of random variable            *
* OUTPUTS:                                         *
*          bt: probability of random variable      *
*              between -X and +X                   *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Student(int nu,double x,
             double *bt,double *px,double *qx);

/***************************************************
*            F distribution                        *
* ------------------------------------------------ *
* INPUTS:                                          *
*          nu1: number of degrees of freedom       *
*          nu2: number of degrees of freedom       *
*           x: value of random variable            *
* OUTPUTS:                                         *
*          bt: probability of random variable = X .. not implemented!!! *
*          px: probability of random variable <= X *
*          qx: probability of random variable >= X *
***************************************************/
MTOOLS_LIB void Fdistr(int nu1,int nu2,double x,
            double *bt,double *px,double *qx);
  
}
}

#endif   //STAT_TOOLS_H

