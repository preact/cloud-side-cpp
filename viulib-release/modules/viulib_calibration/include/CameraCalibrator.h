/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_CAMERA_CALIBRATOR_H
#define VIULIB_CAMERA_CALIBRATOR_H

#include <vector>
#include "Camera.h"


namespace viulib
{

namespace calibration
{
	enum PATTERNTYPE { SYMMETRIC_CIRCLEGRID, ASYMMETRIC_CIRCLEGRID, CHESSBOARD, NONE };

/*! \brief Can be used for the intrinsic calibration of a camera using chess, circle grid patterns or any other pattern 
\par Usage

  Step 1 : Create a CameraCalibrator object\n
  Step 2 : set the chess pattern porperties\n
  Step 3 : Call CameraCalibrator::newPatternImage with each new captured image\n
  Step 3 : If you are using a different pattern you can call CameraCalibrator::newPatternDetectedCorners\n
  Step 4 : Call CameraCalibrator::finishCalibration to finish the calibration\n
  Step 5 : You can continue adding new images or points and call again CameraCalibrator::finishCalibration in order to improve the calibration\n

\par Demo Code:

\code
	
\endcode
*	\ingroup viulib_calibration
*/
class CAL_LIB CameraCalibrator : public VObject
{

	

	REGISTER_GENERAL_CLASS(CameraCalibrator,calibration);

public:
    //! \name Constructor
    //@{
    /** \brief CameraCalibrator constructor
     \param width [in] Resolution width of the camera
     \param height [in] Resolution height of the camera
     */
    CameraCalibrator( );
    CameraCalibrator( const int &width, const int &height );
    CameraCalibrator( Camera *cam );

	//@}
	virtual ~CameraCalibrator(void);

	/** \brief Set the chess pattern properties
		\param numRows [in] The number of inner corners of ther pattern per column
		\param numColumns [in] The number of inner corners of ther pattern per row
		\param quadsize [in] The size in mm of the each quad side
		\return True if the motion vector was calculated
	*/
	void setPatternProperties(PATTERNTYPE type, const int &anumRows, const int &anumColumns,const float &pointDistance = 1,cv::Size margin=cv::Size(0,0) );


	/** \brief Set type of pattern that will be use
		\param 
		\return void
	*/
	void setPatternProperties(PATTERNTYPE type,cv::Size s,const float &pointDistance=1,cv::Size margin=cv::Size(0,0));


	/** \brief Set the pointer of the camera related to this cameracalibration class
		\param cam [in] Pointer to the camera
		\return void
	*/
	void setCamera(Camera *cam);
	Camera *getCamera();
	
	/** \brief Set size of image used for calibration
		\param awidth [in] image width
		\param aheight [in] image height
		\return void
	*/
	void setImageSize( const int &awidth, const int &aheight);

	/** \brief get the number of images that have been added to the point vectors
		\return void
	*/
	int getNumberOfUsedImages() {return m_numImages;};
	
	/** \brief Add a new image
		\param img [in] The image
		\param corners [out] The corners of the pattern that have been found
		\param usePointsForCalibration [in] If the detected points must be used for the calibration
		\return True if pattern was detected
	*/	
	bool newPatternImage( const cv::Mat &img, std::vector<cv::Point2f> &corners, bool usePointsForCalibration );
	
	/** \brief Add detected corners data for the calibration
		\param img [in] The source image to refine corners position. Could be empty if the corners position is already correct.
		\param corners [in] The list of corners
		\param refineCornersPosition [in] If the corners position should be refined using the source img
	*/
	void newPatternDetectedCorners( const cv::Mat &img, std::vector<cv::Point2f> &corners, bool refineCornersPosition);

	/** \brief Perform fast pattern pre detection (the corners position is not refined)
		\brief img [in] The source image
		\param corners [out] List of detected corners
		\return true if the patter was detected
	*/
	bool preDetectPattern( const cv::Mat &img, std::vector<cv::Point2f> &corners );

	/** \brief Finish the calibration process
		\param distortionCoeffs[out] The distortion coefficients (must be a 4x1 matrix)
		\param distortionCoeffs[out] The intrinsics parameters ( must be a 3x3 matrix)
		\return RMS error, -1 if calibration went wrong
	*/	
	double finishCalibration(  cv::Mat &distortionCoeffs, cv::Mat &intrinsics, std::vector<cv::Mat> &rotVectors, std::vector<cv::Mat> &transVectors );

	/** \brief Finish the calibration process		
		\return RMS error, -1 if calibration went wrong
	*/	
	double finishCalibration();

	/** \brief Draw the detected pattern corners in the image
		\param corners [in] the list of corners
		\param img [in] the dst image
	*/
	void drawPatternPoints( std::vector<cv::Point2f> &corners,cv::Mat &img);

	/** \brief Save intrinsics parameters of a camera
		\param filenameIntrinsics[in] The name of the file
		\return true if writing was correct
	*/
	bool saveIntrinsics( const std::string& filenameIntrinsics);
//	bool saveExtrinsics( const std::string& filenameExtrinsics);
	
	/** \brief Diferent functions to draw Axis and Ground Grids after extrinsics are calculated
		\return void
	*/
	//void paintGroundPlaneGrid(cv::Mat &img, cv::Point2f min_pt, cv::Point2f max_pt, float step,cv::Mat &rvec, cv::Mat &tvec);
	//void paintGroundPlaneGridDistortion(cv::Mat &img, cv::Point2f min_pt, cv::Point2f max_pt, float step, cv::Mat &rvec, cv::Mat &tvec);
	//void paintCoordinateSystem(cv::Mat &img,cv::Mat rvec,cv::Mat tvec);


	bool getExtrinsicsFromView(const cv::Mat &img, std::vector<cv::Point2f> &corners,bool usePointsForCalibration , cv::Mat &rvec,cv::Mat &tvec,bool _applyDistortion=false);

	void reset()
	{
		m_numImages = 0;
		m_objectPointsMatrix.clear();
		m_imagePointsMatrix.clear();
		m_cam->setCalibrated(false);
	};

	void getReprojectionErrors( std::vector<std::vector < cv::Point2f > > &errors );

	void setFindCornerSubpixParameters( const int &winSize, const int &zeroZone, const int &iterations, const float &epsilon );

	void getFindCornerSubpixParameters( int &winSize, int &zeroZone, int &iterations, float &epsilon ) const;

	void setCalibrateParameters( const bool &fixPrincipalPoint, const bool &fixAspectRatio,	const bool &zeroTangentialDistortion,
		const bool &rationalModel, const int &iterations, const float &epsilon );

	void getCalibrateParameters( bool &fixPrincipalPoint, bool &fixAspectRatio, bool &zeroTangentialDistortion,
		bool &rationalModel, int &iterations, float &epsilon ) const;

	void setFindPatternParameters( const bool &adaptiveThreshold, const bool &normalizeImage, const bool &filterQuads, const bool &fastCheck );
	
	void getFindPatternParameters( bool &adaptiveThreshold, bool &normalizeImage, bool &filterQuads, bool &fastCheck ) const;

	static void generateModelPoints( std::vector<cv::Point3f> &objectPoints, 
		int numRows, int numCols, PATTERNTYPE patternType, float pointsDistance );

private:
	void initialize( );

	/** \brief Generate 3-D points of the symmetric circle grid pattern
		\param objectPoints[out] The points of the pattern		
	*/	
	static void generateSymmetricCircleGridModelPoints( std::vector<cv::Point3f> &objectPoints,
		int numRows, int numCols, PATTERNTYPE patternType, float pointsDistance);

	/** \brief Generate 3-D points of the asymmetric circle grid pattern
		\param objectPoints[out] The points of the pattern		
	*/	
	static void generateAsymmetricCircleGridModelPoints( std::vector<cv::Point3f> &objectPoints,
		int numRows, int numCols, PATTERNTYPE patternType, float pointsDistance);

	/** \brief Generate 3-D points of the chess board pattern
		\param objectPoints[out] The points of the pattern		
	*/	
	static void generateChessModelPoints( std::vector<cv::Point3f> &objectPoints,
		int numRows, int numCols, PATTERNTYPE patternType, float pointsDistance);

	static bool findPattern( const cv::Mat &img, PATTERNTYPE patternType, cv::Size patternSize, std::vector<cv::Point2f> &corners, int flags );

	/** \brief Save parameters and matrix into a file
		\param filename[in] file name
		\param imageSize[in] file name
		\param squareSize[in] file name
		\param aspectRatio[in] file name
		\param flags[in] file name
		\param cameraMatrix[in] file name
		\param distCoeffs[in] file name
		\param rvecs[in] file name
		\param tvecs[in] file name
		\param rvec[in] file name
		\param tvec[in] file name
		\param reprojErrs[in] file name
		\param imagePoints[in] file name
		\param totalAvgErr[in] file name
		\return RMS void
	*/
	void saveCameraParamsIntrinsics(	const std::string& filename,
													cv::Size imageSize, 
													cv::Size boardSize,
													float squareSize, 
													int flags,
													const cv::Mat& cameraMatrix, 
													const cv::Mat& distCoeffs,
													const std::vector<std::vector<cv::Point2f> >& imagePoints,
													double totalAvgErr );

	//void saveCameraParamsExtrinsics( const std::string& filename,
	//					cv::Size imageSize, cv::Size boardSize,
	//					float squareSize, float aspectRatio, int flags,
	//					const Mat& cameraMatrix, const Mat& distCoeffs,
	//					const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	//					const Mat& rvec, const Mat& tvec,
	//					const vector<float>& reprojErrs,
	//					const vector<vector<Point2f> >& imagePoints,
	//					double totalAvgErr );

protected:
	int m_width; // Resolution width of the camera
	int m_height; // Resolution height of the camera

private:
	
	//-- pattern description --//
	int m_numRows;					//The number of inner corners of the pattern per column
	int m_numColumns;				//The number of inner corners of the pattern per row
	int m_numCorners;				//The total number of inner corners of the pattern
	float m_pointsDistance;			//The size in mm of the each quad side or the distance between two circles centers	
	int  m_margin_height;			//margin from the last in y square of the pattern to the bottom border
	int  m_margin_width;			//margin from the first square in x of the pattern to the left border
	PATTERNTYPE m_pattern_type;				//Patterns type 
	cv::Size m_pattern_size;		//Patterns size rows x cols (internal points)
	//--------------------------//
	
	int m_flags; //flags for cameracalibration
	int m_numImages; //The number of captured images	
	std::vector<std::vector< cv::Point3f > > m_objectPointsMatrix;	//World 3d Points
	std::vector<std::vector < cv::Point2f > > m_imagePointsMatrix;
	std::vector<std::vector < cv::Point2f > > m_reprojectionError;
	double m_rms;
	Camera *m_cam;
	
	//find pattern parameters
	int m_findPatternFlags;

	//refine cornersubpix parameters
	cv::TermCriteria m_refineCornersCriteria;
	cv::Size m_winSize;
	cv::Size m_zeroZone;

	//calibrate parameters
	int m_calibrateFlags;
	cv::TermCriteria m_calibrateCriteria;

};

};

};

#endif
