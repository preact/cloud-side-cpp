/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_STEREO_PAIR_H
#define VIULIB_STEREO_PAIR_H

#include "opencv2/core/core.hpp"
#include "calibration_exp.h"
#include "Camera.h"

namespace viulib
{

namespace calibration
{

/** \brief Repesents a stereo pair of cameras
*/
class CAL_LIB StereoPair : public VObject
{
	
	REGISTER_GENERAL_CLASS(StereoPair,calibration);

public:

    StereoPair();
	virtual ~StereoPair(void);

	/** \brief Initialize the pair
		\param cameraA [in] The first camera
		\param cameraB [in] The second camera
		\param verticalStereo [in] If one of the camera is located on top of the other one
	*/
	void initialize( Camera *cameraA, Camera *cameraB, bool verticalStereo = false );
	
	/** \brief Calculate the corresponding epiline (a,b,c) to the point (x,y)
		\param camera [in] The camera (1 or 2) where the point was detected
		\param x,y [in] The 2D points coordinates
		\param a,b,c [out] The calculated parameters of the epiline equation in the other camera image	
	*/
	void getEpiline( const int &camera, const float &x, const float &y, float &a, float &b, float &c );

	/** \brief Rectify the images so that the epipoles of the 2 images are at infinity
		\param img [in/out] The images
	*/
	void rectifyImages( cv::Mat *img[2] ) const;
	void rectifyImage( int index, cv::Mat &img ) const;

	/** \brief Triangulate the 3D position of two matched 2D points
		\param p0x, p0y [in] The 2D coordinates of the point int camera 0
		\param p1x, p1y [in] The 2D coordinates of the point int camera 1
		\param x,y,z [out] The calculated 3D coordinates		
	*/
	void triangulate3DPoint( const float &p0x, const float &p0y, const float &p1x, const float &p1y, 
		float &x, float &y, float &z );

	/** \brief Calculate the projection of a 3D point
		\param camera [in] The camera to make the projection
		\param p3Dx,p3Dy,p3Dz [in] The 3D coordinates of the point
		\param p2Dx,p2Dy [out] The calculated 2D coordinates		
	*/
	void project3DPoint( const int &camera, const double &p3Dx, const double &p3Dy, const double &p3Dz, 
						double &p2Dx, double &p2Dy );	

	cv::Mat1f getCameraMatrixA() const;
	void setCameraMatrixA( const cv::Mat1f &intrinsics );
	cv::Mat1d getCameraMatrixRectifiedA() const;
	cv::Mat1f getCameraMatrixB() const;
	void setCameraMatrixB( const cv::Mat1f &intrinsics );
	cv::Mat1d getCameraMatrixRectifiedB() const;
	cv::Mat1f getDistortionCoeffsA();
	void setDistortionCoeffsA( const cv::Mat1f &coeffs );
	cv::Mat1f getDistortionCoeffsB();
	void setDistortionCoeffsB( const cv::Mat1f &coeffs );

	cv::Mat1f getUndistortXMapA() const;
	cv::Mat1f getUndistortYMapA() const;
	cv::Mat1f getUndistortXMapB() const;
	cv::Mat1f getUndistortYMapB() const;

	cv::Matx33d getFundamentalMatrix() const;
	void setFundamentalMatrix( cv::Matx33d f ){ m_fundamentalMatrix = f; }
	cv::Matx33d getEssentialMatrix() const;
	void setEssentialMatrix( cv::Matx33d e){ m_essentialMatrix = e; }

	cv::Matx33d getRotation() const { return m_rotation; }
	void setRotation( cv::Matx33d r ) { m_rotation = r; }	
	cv::Matx31d getTranslation() const { return m_translation; }
	void setTranslation( cv::Matx31d t) { m_translation = t; }
	cv::Mat1d  getProjectionMatrixA() const { return m_projection[0]; }
	cv::Mat1d  getProjectionMatrixRectifiedA() const { return m_projectionRectified[0]; }
	void setProjectionMatrixA( cv::Mat1d p ) { p.copyTo( m_projection[0] ); }
	cv::Mat1d  getProjectionMatrixB() const { return m_projection[1]; }
	cv::Mat1d  getProjectionMatrixRectifiedB() const { return m_projectionRectified[1]; }
	void setProjectionMatrixB( cv::Mat1d  p ) { p.copyTo( m_projection[1] ); }
	cv::Mat1d getTranslationRectified() const { return m_translationRectified; }
	cv::Mat1d getDisparityToDepthMatrix() const { return m_disparityToDepth; }
	cv::Rect getValidDisparityROIA() const { return m_validROIs[0]; }
	cv::Rect getValidDisparityROIB() const { return m_validROIs[1]; }

	void setProjectionRectifiedMatrixA( cv::Mat1d  p ) { p.copyTo( m_projectionRectified[0] ); }
	void setProjectionRectifiedMatrixB( cv::Mat1d  p ) { p.copyTo( m_projectionRectified[1] ); }

	void calibrateFromProjectionRectifiedMatrices();

	cv::Size getImageSize() const;

	void writeToFile( const char *fileName ) const;

	void writeToFile( cv::FileStorage &fs ) const;

	void loadFromFileNode( const cv::FileNode &fs );	

	bool isCalibrated() const;
	void setCalibrated( bool calibrated );

	bool isRectified() const { return m_rectified; }
	void setRectified( bool rectified ) { m_rectified = rectified; }
	
	void setCalibrationError( double calibrationError );

	double getCalibrationError() const;

	bool loadExtrinsics( const std::string& filename );

	void calculateRectification( float alphaScale = 0.5f );

	void buildProjectionMatrices();

private:	
	void initUndistortMaps( const cv::Mat &R1, const cv::Mat &R2 );

	cv::Mat1d  m_projection[2];
	cv::Mat1d  m_projectionRectified[2];
	cv::Mat1d m_cameraMatrixRectified[2];
	cv::Matx33d m_fundamentalMatrix;
	cv::Matx33d m_essentialMatrix;
	cv::Matx33d m_rotation;
	cv::Matx31d m_translation;
	cv::Mat1f m_MapXL;
	cv::Mat1f m_MapYL;
	cv::Mat1f m_MapXR;
	cv::Mat1f m_MapYR;
	cv::Mat1b m_rectifyTemp;		
	cv::Rect m_rectificationROIs[2];
	cv::Mat1d m_translationRectified;
	cv::Mat m_disparityToDepth;
	cv::Rect m_validROIs[2];
	
	bool m_verticalStereo;
	bool m_calibrated;
	double m_calibrationError;
	bool m_rectified;

	Camera *m_cameraA;
	Camera *m_cameraB;
};

};

};

#endif
