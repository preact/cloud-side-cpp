/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_CAMERAS_SETUP_H
#define VIULIB_CAMERAS_SETUP_H

#include "opencv2/core/core.hpp"
#include "Camera.h"
#include "StereoPair.h"
#include "calibration_exp.h"

namespace viulib
{

namespace calibration
{

/** \brief A setup of single cameras and stereo pairs
*	\ingroup viulib_calibration
*/
	class CAL_LIB CamerasSetup : public VObject
{
	
	REGISTER_GENERAL_CLASS(CamerasSetup,calibration);

public:
    CamerasSetup();
	~CamerasSetup();
	
	void addCamera( Camera *camera );

	int cameraCount() const;

	Camera *getCamera( int c );

	void addStereoPair( StereoPair *stereoPair );

	int getStereoPairCount() const;

	StereoPair * getStereoPair( int s );

	void clear();

private:

	std::vector<Camera*> *m_cameras;
	std::vector<StereoPair*> *m_stereoPairs;
	
};

};

};
#endif