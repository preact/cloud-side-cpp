/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_CAMERA_H
#define VIULIB_CAMERA_H

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "calibration_exp.h"
#include "core.h"

/** \brief viulib namespace
*/
namespace viulib
{

/** \brief Cameras calibration namespace
 *	\ingroup viulib_calibration
*/
namespace calibration
{



/** \brief funcion que aplica una matriz de extrinsecos sobre un vector de puntos en el sistema de referencia de un objeto
	\param points_3d_object [in] puntos en el sistema de referencia del objeto
	\param points_3d_cam [out] puntos en el sistema de referencia del camara
	\param rvec [in] vector de rotacion (angulos de euler)
	\param tvec [in] matriz de traslacion
	\f$ \left( \begin{array}{c} 
				x_{c}  \\
				y_{c}  \\
				z_{c}  \\
				w 
				\end{array}\right)
				=  \left( \begin{array}{c|r} 
				R & t \\
				0 & 1
				\end{array}\right) 
				\left( \begin{array}{c} 
				x_{w}  \\
				y_{w}  \\
				z_{w}  \\
				1 
				\end{array}\right)
				=
				\left( \begin{array}{ccc|r} 
				r_{0,0} & r_{0,1} & r_{0,2} & t_{x} \\
				r_{1,0} & r_{1,1} & r_{1,2} & t_{y} \\
				r_{2,0} & r_{2,1} & r_{2,2} & t_{z} \\
				0 & 0 & 0 & 1
				\end{array}\right)
				\left( \begin{array}{c} 
				x_{w}  \\
				y_{w}  \\
				z_{w}  \\
				1 
				\end{array}\right) 
	\f$
	
	\return void
	*	\ingroup viulib_calibration
*/
CAL_LIB void  applyRT( std::vector<cv::Point3f> &_vpOut, const std::vector<cv::Point3f> &_vpIn, const cv::Mat &_rvec,const cv::Mat &_tvec);




/** \brief funcion que aplica una matriz de extrinsecos sobre un punto en el sistema de referencia de un objeto
	\param pt3D [in] punto en el sistema de referencia del objeto
	\param pt3Dsc [out] punto en el sistema de referencia del camara
	\param rvec [in] vector de rotacion (angulos de euler)
	\param tvec [in] matriz de traslacion
	\return void
	*	\ingroup viulib_calibration
*/

CAL_LIB	void applyRT(const cv::Point3f &_pIn, cv::Point3f &_pOut, cv::Mat &_rvec,cv::Mat &_tvec);

/** \brief Funcion que devuelve a partir de tres puntos en el espacio los coeficientes de la ecuacion del plano asociado
	\\      Ax + By + Cz + D = 0
	\\    p1 ------------  p2
	\\      |            | 
	\\      |            | 
	\\      |            | 
	\\      |            | 
	\\       ------------  p3
	\\
	\param p1 [in] primer punto del plano
	\param p2 [in] segundo punto del plano
	\param p3 [in] tercer punto del plano
	\param A [out] coeficiente x de la ecuacion del plano
	\param B [out] coeficiente y de la ecuacion del plano
	\param C [out] coeficiente z de la ecuacion del plano
	\param D [out] coeficiente libre de la ecuacion del plano
	\return void
	*	\ingroup viulib_calibration
*/
CAL_LIB cv::Point3f points2Plane(cv::Point3f p1, cv::Point3f p2, cv::Point3f p3, double &A, double &B, double &C, double &D);

/** \brief Funcion que devuelve a partir la normal de un plano y de un punto  coplanario los coeficientes de la ecuacion del plano asociado
	\\      Ax + By + Cz + D = 0
	\\     
	\\      |p1            
	\\      |             
	\\      |--> N            
	\\      |      
	\\      |
	\\      
	\\
	\param p1 [in] primer punto del plano
	\param p2 [in] segundo punto del plano
	\param p3 [in] tercer punto del plano
	\param A [out] coeficiente x de la ecuacion del plano
	\param B [out] coeficiente y de la ecuacion del plano
	\param C [out] coeficiente z de la ecuacion del plano
	\param D [out] coeficiente libre de la ecuacion del plano
	\return void
	*	\ingroup viulib_calibration
*/
CAL_LIB void normal2Plane(cv::Point3f normal,cv::Point3f p, double &A, double &B, double &C, double &D);

/** \brief Dado un vector v1 en el sistema de coordenadas de la camara devuelve la interseccion con el plano a b c d
	\\     el vector v1 tiene su origen en 0,0,0 
	\param point_ccs [in] punto p1 en el sistema de coordenadas de la camara
	\param K [in] Matriz de la camara
	\param a [in] coeficiente a de la formula general del plano
	\param b [in] coeficiente b de la formula general del plano
	\param c [in] coeficiente c de la formula general del plano
	\param d [in] coeficiente d de la formula general del plano
	\return punto de interseccion en el sistema de coordenadas de la camara
	*	\ingroup viulib_calibration
*/
CAL_LIB cv::Point3f cameraPointAndPlaneIntersection(const cv::Mat &point_ccs, const cv::Mat &K,double a, double b, double c, double d);
/**
	*	\ingroup viulib_calibration
*/
CAL_LIB cv::Point3f cameraPointAndPlaneIntersection(const cv::Point3f &point_ccs, const cv::Mat &K,double a, double b, double c, double d);
/**
	*	\ingroup viulib_calibration
*/
CAL_LIB cv::Point3f imagePoint2PlaneIntersection(const cv::Point2f &image_point, const cv::Mat &K,double a, double b, double c, double d);

/** \brief Projecta un punto desde el sistema de coordenadas del mundo al plano de imagen de la camara
	\param K [in] Matriz de la camara
	\param rvec [in] vector de rotacion
	\param tvec [in] vector de traslacion
	\return 
	\code \f$
				\left( \begin{array}{c} 
				x_{imagen}  \\
				y_{imagen}  \\
				w 
				\end{array}\right) 
				= K  I_{o}  \left( \begin{array}{c|r} 
				R & t \\
				0 & 1
				\end{array}\right) 
				\left( \begin{array}{c} 
				x_{w}  \\
				y_{w}  \\
				y_{w}  \\
				1 
				\end{array}\right)
				=\left( \begin{array}{ccc} 
				f_{x} & 0 & c_{x}  \\
				0 & f_{y} & c_{y} \\
				0 & 0 & 1 
				\end{array}\right)
				\left( \begin{array}{ccc|r} 
				1 & 0 & 0 & 0 \\
				0 & 1 & 0 & 0 \\
				0 & 0 & 1 & 0 \\
				\end{array}\right)
				\left( \begin{array}{ccc|r} 
				r_{0,0} & r_{0,1} & r_{0,2} & t_{x} \\
				r_{1,0} & r_{1,1} & r_{1,2} & t_{y} \\
				r_{2,0} & r_{2,1} & r_{2,2} & t_{z} \\
				0 & 0 & 0 & 1
				\end{array}\right)
				\left( \begin{array}{c} 
				x_{w}  \\
				y_{w}  \\
				y_{w}  \\
				1 
				\end{array}\right)
				\f$
	\endcode
	*	\ingroup viulib_calibration

*/

CAL_LIB cv::Point2d project(const cv::Point3d &pt3D, cv::Mat &rvec,cv::Mat &tvec, cv::Mat &K,bool infinite=false);
/**
*	\ingroup viulib_calibration
*/
CAL_LIB std::vector<cv::Point2d> project(const std::vector<cv::Point3d> &vpt3D, cv::Mat &rvec,cv::Mat &tvec, cv::Mat &K,bool infinite);

/**
*	\ingroup viulib_calibration
*/
CAL_LIB void drawCoordinateSystem(cv::Mat img_out,cv::Point3f s, int l, cv::Mat rvec, cv::Mat tvec, cv::Mat K, cv::Mat D);

/*! \brief Stores the intrinsic calibration of a camera
\par Usage

  Step 1 : Create a Camera object\n
  Step 2 : Load intrinsics yml file with loadIntrinsics \n
  Step 3 : Get information\n

\par Demo Code:

\code
	
\endcode
*	\ingroup viulib_calibration
*/
class CAL_LIB Camera : public VObject
{
	

	REGISTER_GENERAL_CLASS(Camera,calibration);

public:
    //! \name Constructor
    //@{
    /** \brief Camera constructor
     \param imageSize [in] Resolution of the camera
     \param sensorSize [in] Resolution  of the sensor
     */
    Camera( );
    Camera( cv::Size imageSize, cv::Size2f sensorSize );
    //@}
    
	~Camera();
	
	
	//! \name Get Functions
	//@{
	/** \brief get image size
		\return image size
	*/
	cv::Size getImageSize() const;

	/** \brief get sensor size
		\return sensor size
	*/
	cv::Size2f getSensorSize() const;

	/** \brief retrieves a pointer to K matrix (camera matrix)
		\return pointer to the K matrix with intrinsics
		\    |fx   0  cx|
		\K = | 0  fy  cy|
		\    | 0   0   1|
	*/
	cv::Mat1f getCameraMatrix32() const;
	const cv::Mat1d *getCameraMatrix64() const;
	const cv::Mat1d *getCameraMatrix() const;

	
	/** \brief get focalLength in milimeters
		\return focal length
	*/
	double getFocalLengthMm() const;
	
	/** \brief get both components in pixels of focal lenght (fx and fy)
		\return 2d point with the two fields of the focal length in each point coordinate
	*/
	cv::Point2d getFocalLength( ) const;	
	
	/** \brief retrieves x component of focal length in pixels 
		\return focal length's x component
	*/
	double getFovX() const;
	
	/** \brief retrieves y component of focal length in pixels
		\return focal length's y component
	*/
	double getFovY() const;
	
	/** \brief retrieves a distortion coefficients of the calibration
		\return 1D mat with the distortion coefficients
	*/
	cv::Mat1f getDistortionCoeffs32() const;
	const cv::Mat1d *getDistortionCoeffs64() const;

	void setDistortionCoeffs( const cv::Mat &coeffs );

	/** \brief gets the principal point  (cx, cy) after de calibration in millimeters
		\return principal point of calibration
	*/
	cv::Point2d getPrincipalPointMm() const;
	
	/** \brief gets principal point (cx, cy) in pixels
		\return principal point in pixels
	*/
	cv::Point2f getPrincipalPoint() const;

	/** \brief sets principal point (cx, cy) in pixels
		\param pp [in] principal point in pixels
	*/
	void setPrincipalPoint( const cv::Point2f &pp );
	
	/** \brief retrieves camera matrix in opengl format to create the frustrum
		\param _nearPlane  [in] Z-clipping plane near the camera
		\param _farPlane  [in]  Z-plane beyond nearplane where the space of projection finishs 
		\param _projectionMatrix [out] apropiate projection matrix for opengl to create the projection frustrum
		\return void
	*/
	void getOpenGlProjectionMatrix(int _nearPlane, int _farPlane,double *_projectionMatrix);

	/** \brief retrieves calibration error
		\return calibration error
	*/
	double getCalibrationError(  ) const ;

	//@}

	//! \name Set Functions
	//@{
	/** \brief set image size
		\param imageSize [in] Size of the image
		\return void
	*/
	void setImageSize(cv::Size imageSize);
	
	/** \brief set sensor size
		\param sensorSize [in] Size of the sensor
		\return void
	*/
	void setSensorSize(cv::Size2f sensorSize);
	
	/** \brief set camera matrix
		\param camera_matrix [in] input camera matrix
		\return void
	*/
	void setCameraMatrix( const cv::Mat &camera_matrix );
	
	/** \brief sets the calibrated flag to calibrated
		\param calibrated [in] calibrated value
		\return void
	*/
	void setCalibrated( bool calibrated );
	
	/** \brief sets focal length
		\param focal [in] focal length in millimiters
		\return void
	*/
	void setFocalLengthMm( double focal );
	
	/** \brief sets calibration error
		\param error [in] error of the calibration
		\return void
	*/
	void setCalibrationError( double error);
	
	/** \brief sets focal length
		\param focal [in] focal lenght
		\return void
	*/	
	void setFocalLength( cv::Point2f focal);
	
	/** \brief sets focal lenght in x 
		\param fov [in] focal lenght in x
		\return void
	*/
	void setFovX( double fov );
	
	/** \brief set focal lenght in y
		\param fov [in] focal lenght in y
		\return void
	*/
	void setFovY( double fov );	
	//@}

	/** \brief this function shows if the calibration is done or it is not
		\return bool indicating if the camera is calibrated
	*/
	bool isCalibrated() const;


	/** \brief this function undistorts a point 
		\param px [in] x-coordinate of the point to be undistorted
		\param py [in] y-coordinate of the point to be undistorted
		\return void
	*/
	void undistortPoint( float &px, float &py );

	/** \brief this function distorts a point 
		\param px [in] x-coordinate of the point to be distorted
		\param py [in] y-coordinate of the point to be distorted
		\return void
	*/
	void distortPoint( float &px, float &py );
	
	/** \brief this function undistorts a set of points
		\param points [in out] vector of points to be undistorted
		\return void
	*/
	void undistortPoints( std::vector<cv::Point2f> &points );	

	/** \brief this function undistorts all the points of an image
		\param src [in] image to be undistorted
		\param dst [out] the undistorted image
		\return void
	*/
	void undistortImage( const cv::Mat &src, cv::Mat &dst );

	/** \brief this function loads  a file with calibration information into this class
		\param filename [in] file from which the data will be read
		\return void
	*/
	bool loadIntrinsics( const std::string& filename);	
	
	/** \brief this function loads  a node with calibration information into this class
		\param fs [in] file node from which the data will be read
		\return void
	*/
	bool loadIntrinsicsFromFileNode( cv::FileNode &fs );

	/** \brief this function loads the Camera object with default parameters
		\param size [in] size of image
		\return bool
	*/
	bool loadIntrinsicsDefault( const cv::Size size );
	
	/** \brief this function writes a file with calibration information into this class
		\param filename [in] file where the data will be stored
		\return void
	*/
	void writeIntrinsics( const std::string& filename) const;
	/** \brief this function writes  a node with calibration information into this class
		\param filename [in] node where the data will be stored
		\return void
	*/
	void writeIntrinsics( cv::FileStorage &fs ) const;
	
	/** \brief draw camera info in an image
		\param dst [in out] image where the information will be drawn
		\param x [in] x position of the info
		\param y [in] y position of the info
		\return void
	*/
	void drawCamInfo( cv::Mat &dst,int x, int y );
  
  /** \brief copy the current object to other
		\param _c [out] Plane object where the content will be copied to
		\return void
	*/
	void copyTo( Camera &c) const;

private:
	cv::Size m_imageSize;
	cv::Size2f m_sensorSize;
	bool m_calibrated;
	double m_focalLength;
	double m_fovx, m_fovy;	
	cv::Point2d	m_principalPoint;
	double m_pixel_aspectRatio;
	double m_calibrationError;

	cv::Mat1d m_cameraMatrix;  // intrinsic parameters of the camera
	cv::Mat1d m_distCoeffs;
	cv::Mat1f m_mapX;
	cv::Mat1f m_mapY;
};

};

};
#endif
