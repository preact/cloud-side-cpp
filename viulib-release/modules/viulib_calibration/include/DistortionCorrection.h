/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_DISTORTION_CORRECTION_H
#define VIULIB_DISTORTION_CORRECTION_H

#include "calibration_exp.h"
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "core.h"

namespace viulib
{

namespace calibration
{

/** \brief Esta clase se utiliza para el calculo y correcion de la distorsion de lente
	\par Demo Code:
		DistortionCorrection distCorrector;
		//Inicializamos las lineas (cada linea es un vector de puntos)
		std::vector<std::vector<cv::Point2f> > lines;
		lines.push_back( points );
		m_distCorrector.setData( lines );
		//Generar los mapas de transformacion, no modifica las imagenes
		m_distCorrector.compute( imgSrc, imgDst );
		//Corregir la imagen aplicando los mapas de transformacion
		m_distCorrector.correct( imgSrc, imgDst );
		//Dibujar los puntos originales
		m_distCorrector.drawPoints( imgSrc );
		//Dibujar los puntos corregidos
		m_distCorrector.drawCorrectedLines( imgDst );
		//Dibujar las lineas corregidas
		m_distCorrector.drawCorrectedLines( imgDst ); 
	\code		
	\endcode
\author Vicomtech (2010)
*/
class CAL_LIB DistortionCorrection : public VObject
{

	REGISTER_GENERAL_CLASS(DistortionCorrection,calibration);

public:
    
    DistortionCorrection();

	/** \brief Set the number of distortion coefficients
		\param numCoeffs number of distortion coefficients
	*/
	void setCoeffsNumber( int numCoefs );

	~DistortionCorrection(void);

	/** \brief Computes the distortion parameters and generates the undistortion maps
		\brief imgSrc [in] Size of the distorted image
		\brief imgDst [in] Size of the undistorted image
	*/
	void compute( const cv::Size &imgSrc, const cv::Size &imgDst, const cv::Mat& _K = cv::Mat(), int _verbose=0 );

	/** \brief Correct the distortion of an image
		\brief imgSrc [in] The distorted image
		\brief imgDst [out] The undistorted image
	*/
	void correct( const cv::Mat &imgSrc, cv::Mat &imgDst );
	
	/** \brief Apply the undistortion to a point
		\param point [in] the point in the distorted image coordinates
		\return point the point in the undistorted image coordinates
	*/
	cv::Point2f correctPoint( const cv::Point &point );
	
	/** \brief Apply the undistortion to a point
		\param point [in] the point in the distorted image coordinates
		\return point the point in the undistorted image coordinates
	*/
	cv::Point2f correctPoint( const cv::Point2f &point );
	
	/*	\brief Get the correction map for X coordinates
		\return the Map 
	*/
	cv::Mat1f getMapX() const
	{
		return m_mapX;
	}

	/*	\brief Get the correction map for Y coordinates
		\return the Map 
	*/
	cv::Mat1f getMapY() const
	{
		return m_mapY;
	}

	/*	\brief Get the distortion coeffs
		\return the coeffs
	*/
	cv::Mat1d getDistCoeffs() const { return m_distortCoeffs; }

	/** \brief Set the data for computing the distortion
		\param lines [in] Array of lines composed by arrays of points
	*/
	void setData( const std::vector<std::vector<cv::Point2f> > &lines );

	/** \brief Get the data used for computing the distortion
		\return Array of lines composed by arrays of points
	*/
	std::vector<std::vector<cv::Point2f> > &getData();

	void getCorretedData( std::vector<std::vector<cv::Point2f> > & );

	/** \brief Delete the stored data used for computing the distortion		
	*/
	void resetData();
	
	/** \brief Check if the distortion is already computed
		\return true is computed
	*/
	bool isComputed()
	{
		return m_computed;
	}

	/** \brief Draw in an image the points used for computing the correction
		\param img [in] The image
	*/
	void drawCorrectedPoints( cv::Mat &img );

	/** \brief Draw in an image the lines used for computing the correction
		\param img [in] The image
	*/
	void drawCorrectedLines( cv::Mat &img );

	/** \brief Draw in an image the lines used for computing the correction
		\param img [in] The image
	*/
	void drawPoints( cv::Mat &img );

	/** \brief this function loads a file with distortion information into this class
		\param filename [in] file from which the data will be read
		\return void
	*/
	bool loadDistortionCoeffs( const std::string& filename);	

	/** \brief this function writes a file with distortion information from this class
		\param filename [in] file where the data will be stored
		\return void
	*/
	void writeDistortionCoeffs(const std::string& filename  ) const;
	
	/** \brief this function writes  a node with distortion information into this class
		\param filename [in] node where the data will be stored
		\return void
	*/
	void writeDistortionCoeffs( cv::FileStorage& fs ) const;

private:	
	std::vector<std::vector<cv::Point2f> > m_lines;
	std::vector<std::vector<cv::Point2f> > m_correctedLines;

	cv::Size m_imOrigSize, m_imDstSize;
		
	cv::Mat1f m_mapX, m_mapY;	
	cv::Mat1d m_distortCoeffs;
	int m_numCoeffs;
	bool m_computed;
	cv::Mat1f m_cameraMatrix;
};
};
};
#endif