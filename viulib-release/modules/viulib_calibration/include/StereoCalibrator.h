/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_STEREO_CALIBRATOR_H
#define VIULIB_STEREO_CALIBRATOR_H

#include <iostream>
#include <vector>
#include "opencv2/core/core.hpp"
#include "StereoPair.h"
#include "calibration_exp.h"

namespace viulib
{

namespace calibration
{

/*! \brief this class can be used for the calibration of a stereo pair using s chess pattern
\par Usage

  Step 1 : Create a StereoCalibrator object\n
  Step 2 : set the chess pattern porperties\n
  Step 3 : Call newImages with each new captured image\n
  Step 4 : Call finishCalibration to finish the calibration\n
  Step 5 : You can continue adding new images and call again finishCalibration in order to improve the calibration\n

\par Demo Code:

\code
	
\endcode
*	\ingroup viulib_calibration
*/

class CAL_LIB StereoCalibrator : public VObject
{

REGISTER_GENERAL_CLASS(StereoCalibrator,calibration);

public:
    
    StereoCalibrator();

	/** \brief Initialize the calibrator
		\param stereoPair [in] The stereo pair to be calibrated		
		\param numRows [in] The number of inner corners of ther pattern per column
		\param numColumns [in] The number of inner corners of ther pattern per row
		\param quadsize [in] The size in mm of the each quad side
	*/
	void initialize( StereoPair *stereoPair, const int &_numRows, const int &_numColumns, const float &_quadsize, bool verticalStereo = false );

	virtual ~StereoCalibrator(void);

	/** \brief Add a new pair of images for calibration
		\param img [in] The images
		\param dataCollected [out] Each of these are set to true if calibration data was collected in each of the images
	*/
	void newImages( cv::Mat *img[2], bool dataCollected[2], bool drawCorners );

	void newCorners( std::vector<cv::Point2f> *corners[2], int cornerCount );

	/** \brief Finish the calibration process
		\return True if the stereo pair was calbrated
	*/
	bool finishCalibration();	

	void setCalibrateParameters( const bool &optimizeIntrinsics, const bool &fixPrincipalPoint, const bool &fixAspectRatio, const bool &sameFocal,
		const bool &zeroTangentialDistortion, const bool &rationalModel, const int &iterations, const float &epsilon );

	void getCalibrateParameters( bool &optimizeIntrinsics, bool &fixPrincipalPoint, bool &fixAspectRatio, bool &sameFocal,
		bool &zeroTangentialDistortion, bool &rationalModel, int &iterations, float &epsilon ) const;
	
	void setFindCornerSubpixParameters( const int &winSize, const int &zeroZone, const int &iterations, const float &epsilon );

	void getFindCornerSubpixParameters( int &winSize, int &zeroZone, int &iterations, float &epsilon ) const;

	void setFindPatternParameters( const bool &adaptiveThreshold, const bool &normalizeImage, const bool &filterQuads, const bool &fastCheck );
	
	void getFindPatternParameters( bool &adaptiveThreshold, bool &normalizeImage, bool &filterQuads, bool &fastCheck ) const;

private:
	void deletePoints( );

protected:
	bool verticalStereo;
	int width; // Resolution width of the camera
	int height; // Resolution height of the camera

	//Pattern properties
	int numRows;
	int numColumns;
	float quadsize;
	int numCorners;

	int numImages;
	std::vector<std::vector<cv::Point2f> > m_imagePoints[2];//points detected in each camera	
	std::vector<std::vector<cv::Point3f> > m_objectPoints;	//World 3d Points 
	StereoPair *m_stereoPair;
	
	//find pattern parameters
	int m_findPatternFlags;

	//calibrate parameters
	int m_calibrateFlags;
	cv::TermCriteria m_calibrateCriteria;

	//refine cornersubpix parameters
	cv::TermCriteria m_refineCornersCriteria;
	cv::Size m_winSize;
	cv::Size m_zeroZone;
};

};

};

#endif