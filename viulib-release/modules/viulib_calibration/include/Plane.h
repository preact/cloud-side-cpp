/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_PLANE_H
#define VIULIB_PLANE_H

#include "Camera.h"
#include "imageMapper.h"
#include <iostream>
#include <stdio.h>

namespace viulib
{
	namespace calibration
	{
		/*! \brief Defines a plane with respect to a camera
		\par Usage
		  Step 1 : Create a Plane using a PlaneCalibrator\n  
		  *	\ingroup viulib_calibration
		*/
		class CAL_LIB Plane : public VObject
		{
			friend class PlaneCalibrator;
	
			REGISTER_GENERAL_CLASS(Plane,calibration);

		public:
            Plane();
			~Plane();	
			
			// ----------------------------------------------
			// Camera
			// ----------------------------------------------
			/** \brief Get a pointer to the camera				
					\return Camera object
			*/
			Camera* getCamera() const { return m_camera; }			
			/** \brief Set the pointer of the camera related to this planecalibration class
				\param cam [in] Pointer to the camera
				\return void
			*/
			void setCamera(Camera* _cam) { m_camera = _cam; }

		
			// -----------------------------------------------
			// Load
			// -----------------------------------------------	
			bool loadPlane( const std::string& _filename );
			void savePlane( const std::string& _filename);
			bool loadPlaneFromFileNode( const cv::FileNode& _fs);
			bool savePlaneFromFileNode( const cv::FileNode& _fs);
			void resetValues();

			// -----------------------------------------------
			// Getters			
			// -----------------------------------------------
			cv::Mat getP()					const { cv::Mat Paux = m_P.clone(); return Paux; }
			cv::Mat getP32()				const { cv::Mat Paux(3,4,CV_32FC1); m_P.convertTo(Paux,CV_32FC1); return Paux; }
			cv::Mat getRvec()				const { cv::Mat rvecAux = m_rvec.clone();	return rvecAux;	}
			cv::Mat getTvec()				const { cv::Mat tvecAux = m_tvec.clone(); return tvecAux;	}
			cv::Mat getR()					const { cv::Mat RAux = m_R.clone(); return RAux; }
			cv::Mat getC()					const { cv::Mat CAux = m_C.clone(); return CAux; }
			cv::Mat getCPlane()				const { cv::Mat CPlaneAux = m_CPlane.clone(); return CPlaneAux; }
			double getCameraYaw()			const { return m_yaw; }
			cv::Mat getHomographyMetric()	const { cv::Mat HAux = m_homographyMetric.clone(); return HAux; }
			cv::Mat getGeneralForm()		const { cv::Mat GF = (cv::Mat_<double>(4,1) << m_a, m_b, m_c, m_d); return GF; }
			cv::Point3d getNormal();
			double getTransMM() const {return m_trans_mm;};
			double getLongMM() const {return m_long_mm;};
 
			std::vector<cv::Point2f> getOrigPoints() const { return m_origPoints; }
			std::vector<cv::Point2f> getDstPointsMetric() const { return m_dstPointsMetric; }
			std::vector<cv::Point2f> getCoveragePointsMetricCam() const { return m_boundDstPointsMetricCam; }

			bool isLoaded() const { return m_isLoaded; }
			
			// ----------------------------------------------
			// Warping (Bird's-eye view)
			// ----------------------------------------------
			void setWarpCorrespondences( const std::vector<cv::Point2f>& _origPts, const std::vector<cv::Point2f>& _dstPtsMetric );			
			void applyWarp( cv::Mat& _imWarp, const cv::Mat& _inputImg, bool _useMaps=true, bool _flipVertical=true );			
			void applyInvWarp( cv::Mat& _imUnWarped, const cv::Mat& _inputWarpImg, bool _useMaps=true, bool _flipVertical=true );
			void fromMetric2Warp( const cv::Point2d& _ptMetric, cv::Point2d& _ptWarp ) const;
			void fromMetric2Warp( const cv::Point2f& _ptMetric, cv::Point2f& _ptWarp ) const;
			void fromMetric2Warp( const std::vector<cv::Point2d>& _vptMetric, std::vector<cv::Point2d>& _vptWarp ) const;
			void fromMetric2Warp( const std::vector<cv::Point2f>& _vptMetric, std::vector<cv::Point2f>& _vptWarp ) const;
			void fromWarp2Metric( const cv::Point2f& _ptWarp, cv::Point2f& _ptMetric ) const;

			// ----------------------------------------------
			// Geometry, information map, and coverage area
			// ----------------------------------------------
			void getLineAtInfinity( cv::Mat& _lineInf );
			void getInformationRatioMap( cv::Mat& _infMap, double _min_pixels_per_m2=0 );	// if _min_pixels_per_m2 = 0 -> not-thresholded image is returned
			double getInformationRatio( const cv::Point& _pt ) const;

			/** \brief Retrieves a 8U image which contains the inverse depth corresponding to each pixel reprojected into the plane
				\param _invDepthMap [out] Output 8U Mat containing the inverse depth for each pixel, and scaled according to _scale
				\param _pAlpha [out] Output scale that converts from the 8U values in _invDepthMap into milimeters^-1;  x(MM^-1) = ( x(8U)*_pAlpha ) + _pBeta;
				\param _pBeta [out] Output shift value x(MM^-1) = ( x(8U)*_pAlpha ) + _pBeta;
				\return void
			*/
			void getPlaneInvDepthMap( cv::Mat& _invDepthMap8U, double* _pAlpha, double* _pBeta );

			std::vector<cv::Point> computeCoverageAreaIR( double _min_pixels_per_m2 );
			std::vector<cv::Point> computeCoverageAreaDist( double _distance_to_cam_mm );
			void computeBoundOrigPoints( std::vector<cv::Point>& _points, double _min_pixels_per_m2=0 );			

			bool isInsideCoverageArea( const cv::Point& _point );
			

			// ----------------------------------------------
			// Homography functions 
			// ----------------------------------------------
			/** \brief Apply a homography to a Point2f
				\param[in] point The Point2f
				\param[in] homography The homography

				\return The transformed Point2f
			*/
			static cv::Point2f applyHomography(const cv::Point2f& _point, const cv::Mat1f& _homography);

			/** \brief Apply a homography to a Point2d
				\param[in] point The Point2d
				\param[in] homography The homography
				\return The transformed Point2d
			*/
			static cv::Point2d applyHomography(const cv::Point2d& _point, const cv::Mat1d& _homography);
			
			/** \brief Apply a homography to a Point3d
				\param[in] point The Point3d
				\param[in] homography The homography
				\return The transformed Point3d
			*/
			static cv::Point3d applyHomography( const cv::Point3d& _point, const cv::Mat1d& _homography);
			
			/** \brief Apply a homography to a vector of points
				\param[in] origPts Points in the original domain
				\param[out] dstPts Points in the destination domain				
				\return void
			*/
			static void applyHomography( const std::vector<cv::Point2f>& _origPts, std::vector<cv::Point2f>& _dstPts, const cv::Mat1f& _homography);
			static void applyHomography( const std::vector<cv::Point2d>& _origPts, std::vector<cv::Point2d>& _dstPts, const cv::Mat1d& _homography);
			
			// ----------------------------------------------
			// Drawing functions
			// ----------------------------------------------
			/** \brief Draws a vector of points from the original image into another image of any size
				\param[out] img Image
				\param[in] points Vector of points to draw				
				\return void
			*/
			void drawPointsFromOrigView( const std::vector<cv::Point2f> &_points, cv::Mat &_img ) const;
			void drawControlPoints( const std::vector<cv::Point> &_points, cv::Mat &_img ) const;
			void drawControlPoints( const std::vector<cv::Point2f>& _points, cv::Mat& _img ) const;

			/** \brief Draw a grid that depicts the world plane
				\param[out] img Image
				\param[in] sideSize Size (in mm) of the side of cells of the grid
				\param[in] offset Offset with respect to the origin of coordinates
				\param[in] applyDistortion Determines whether the distortion coefficients shall be applied or not				
				\param[in] margin Point that determines the offset from where the painting will start 
				\return void
			*/
			void getGrid(cv::Size _size,bool _applyDistortion, int _hcell, int _wcell, cv::Point_<float> &_orig,int _w, int _h,  std::vector < std::vector<cv::Point_<float> > > &_vhlines,std::vector< std::vector< cv::Point_<float> > > &_vvlines);
			void getWorldCoordinatesPoints( cv::Size _size, bool _applyDistortion,int _lenght, cv::Point_<float> &_orig, cv::Point_<float> &_orig_x, cv::Point_<float> &_orig_y, cv::Point_<float> &_orig_z );
			void getMetricRule( cv::Size _size, cv::Point _img_point , int _length,bool _applyDistortion,int _axis,std::vector< std::vector <cv::Point_<float> > > &vmeters,std::vector< std::vector <cv::Point_<float> > > &vcentimeters );
			void drawGrid( cv::Mat& _img, int _sideSize = 1000, int _offset = 0, bool _applyDistortion=false,cv::Scalar color = cv::Scalar(0,0,255),cv::Point3f margin = cv::Point3f(0,0,0),int width=0, int length=0);
			void drawWorldCoordinates( cv::Mat& _img, bool _applyDistortion=false, int _thickness = 1 );
			void drawTopView( cv::Mat& _outputImg, cv::Size _outputSize = cv::Size(600, 600), const cv::Mat& _inputImg = cv::Mat() );
			void drawMetricRule( cv::Mat &_img, cv::Point _img_point , int _length,bool _applyDistortion=false,int _axis=2, bool _drawCm=true);
			void drawCoordinateSystem(cv::Mat &img_out , int size=100,int thickness=1);
			void drawCoordinateSystem(cv::Mat &img_out,int sizeX, int sizeY, int sizeZ,int thickness = 1);
			void drawVP(cv::Mat &_img);

			/** \brief Draws a line (a,b,c) in the image
				\param[out] img The image
				\param[in] line The line in general form (a,b,c)
				\param[in] color The color of the line
				\return Vector of Points (2) intersection of the line with the image
			*/			
			std::vector<cv::Point> drawLine( cv::Mat &img, const cv::Mat& _line, cv::Scalar color, int thickness=3);
			void drawCoverage( cv::Mat& _img, const std::vector<cv::Point>& _points, cv::Scalar color=CV_RGB(255,0,0), int thickness=3);
							
			// ----------------------------------------------
			// Transformation
			// ----------------------------------------------
			/** \brief This function transforms the coordinates of a given 3D point from the world to the camera coordinate system
				\param _points3DCam [out] points in the camera coordinate frame
				\param _points3DWorld [in] points in the world coordinate frame				
				\param _rvec [in] rotation vector (Euler angles)
				\param _tvec [in] traslation vector
				\return void
			*/
			static void fromWorld2Cam(std::vector<cv::Point3f>& _points3DCam, const std::vector<cv::Point3f>& _points3DWorld, const cv::Mat& _rvec, const cv::Mat& _tvec);

			/** \brief This function transforms the coordinates of a given 3D point from the world to the camera coordinate system
				\param _point3DCam [out] point in the camera coordinate frame
				\param _point3DWorld [in] point in the world coordinate frame				
				\param _rvec [in] rotation vector (Euler angles)
				\param _tvec [in] traslation vector
				\return void
			*/
			static void fromWorld2Cam(cv::Point3f& _point3DCam, const cv::Point3f& _point3DWorld, const cv::Mat& _rvec, const cv::Mat& _tvec);
			static void fromCam2World(cv::Point3f& _point3DWorld, const cv::Point3f& _point3DCam, const cv::Mat& _rvec, const cv::Mat& _tvec);

			static bool fromRT2GeneralForm( double* _a, double* _b, double* _c, double* _d, const cv::Mat& _rvec, const cv::Mat& _tvec, const viulib::calibration::Camera* _camera);

			/** \brief This function unprojects an image point to the plane
				\param ipoint [in] point in the image (x,y)
				\return The 3D point in the plane in camera coordinates system
			*/
			cv::Point3d fromImage2Plane( cv::Point3d im_point ) const;
			
			/** \brief This function unprojects an image point to the plane

				If a distance to the plane is specified, the image point will unprojected to 
				a plane parallel to the original and located at that distance in the Z axis

				\param image_point_2d [in] point in the image (x,y)
				\param image_point_ccs [out] 3D point in the plane in camera coordinates system
				\param distanceToPlane [in] Distance in the plane Z Axis
				\return The 3D point in the plane in plane coordinates system
			*/
			cv::Point3f fromImage2Plane( const cv::Point2f &image_point_2d,cv::Point3f &image_point_ccs, float distanceToPlane = 0.0f ) const;

			/** \brief This function projects a plane point to the image
				\param _image_point_pcs [in] 3D plane point
				\param _applyDistortion [in] If the lens distortion must be applied or not to the projected image point
				\return The projected 2D image point
			*/
			cv::Point2f fromPlane2Image( const cv::Point3f &_image_point_pcs,bool _applyDistortion=false ) const;

			/** \brief This function calculates the interseccion between a vector which passes trough A point in the image and the plane with normal dir and that stands in the point which is the intersection between the vector that passes through B point in the image and this plane
				\param ipoint [in] point in the image (x,y)
				\param dir [in] normal vector of the vertical plane in plane coordinates
				\param ibasepoint [in] point that belongs to both planes , the vertical one and this one in image coordinates
				\return distance between ibasepoint in space and intersection_point in space
			*/
			double fromImage2PerpendicularPlane(const cv::Point2f &ipoint,const cv::Point3d &dir,const cv::Point2f &ibasepoint, cv::Point3d &ibasepoint_3d, cv::Point3d &ipoint_3dc);


			/** \brief Return vanishing point in _dir direction
				\param _dir [in] direction of convergence
				\return vanishing poing
			*/
			cv::Point getVanishingPoint( cv::Point3f _dir);

			/** \brief Set Plane origin in the position (dX,dY) , 
			    \ that position is referenced to the plane origin coordinate system, 
				  \ previously translated to the point of the vertical projection of the camera in the plane
				\param dX [in] X position in Plane System in mm
				\param dY [in] Y position in Plane System in mm
				\return void
			*/
			void setPlaneOriginPosition(int dX, int dY);

			// ----------------------------------------------
			// Copy functions
			// ----------------------------------------------
			void copyTo(Plane &p) const;

			/** \brief Generate an homography for a plane above the ground plane at that height
				\param height [in] The height of the plane
				\return the homography warp for the new plane
			*/
			cv::Mat generateHomographyAtHeight( float height ) const;

		protected:		

			/** \brief Computes the projection matrix P from intrinsics (K) and extrinsics (R,t)
				\param _K [in] Calibration matrix 3x3
				\param _R [in] Rotation matrix 3x3
				\param _tvec [in] Tralsation vector 3x1
				\return P Projection matrix
			*/
			cv::Mat computeP( const cv::Mat& _K, const cv::Mat& _R, const cv::Mat& _tvec );

			// ----------------------------------------------
			// Homography
			// ----------------------------------------------
			void setHomography( const cv::Mat& _H ) { m_homographyMetric = _H; }//std::cout << m_homographyMetric << std::endl; }

			// ----------------------------------------------
			// RT
			// ----------------------------------------------
			/*void setR( const cv::Mat& _R ) { m_R = _R; }
			void setTvec( const cv::Mat& _tvec ) { m_tvec = _tvec; }
			void setRvec( const cv::Mat& _tvec ) { m_tvec = _tvec; }*/
			void setRT( const cv::Mat& _R, const cv::Mat& _rvec, const cv::Mat& _tvec );

		private:
			// ----------------------------------------------
			// RT
			// ----------------------------------------------
			void computeCameraPosition(const cv::Mat& _R, const cv::Mat& _tvec);
		private:
			// Camera
			Camera* m_camera;							// Reference camera that is watching the plane

			// Status
			bool m_isLoaded;

			// Homography (metric)			
			cv::Mat m_homographyMetric;					// Metric homography

			// Extrinsics (coordinate system in some point of the plane with respect to the camera)
			cv::Mat m_P;								// Projection matrix
			cv::Mat m_rvec, m_tvec, m_R;				// RT of world coordinate frame (on the plane) with respect to m_camera
			cv::Mat m_C, m_CPlane;
			double m_yaw;
			
			// Canonical form of the plane with respect to the camera's coordinate system (a, b, c, d)^T
			double m_a, m_b, m_c, m_d;

			// Warping
			viulib::imgTools::ImageMapper m_mapper;
			viulib::imgTools::TransformHomography m_transform;
			cv::Size m_warpSize;							// Last warp size.
			std::vector<cv::Point2f> m_origPoints, m_dstPointsWarp;
			std::vector<cv::Point2f> m_dstPointsMetric;
			double m_trans_mm, m_long_mm;
			double m_scaleX, m_scaleY;						// For quick transform between metric and warped

			// Line at infinity
			cv::Mat m_lineInf;
			std::vector<cv::Point> m_lineInfPoints;

			// Information ratio and Coverage area
			cv::Mat m_invDepthMap;
			double m_alphaFrom8UToInvMM, m_betaFrom8UToInvMM;

			cv::Mat m_infMap;		
			//double m_min_pixels_per_m2;
			cv::Point m_pointFarthest;
			cv::Mat m_lineFarthest;
			std::vector<cv::Point> m_boundOrigPoints;
			//std::vector<cv::Point2f> __boundDstPoints;
			std::vector<cv::Point2f> m_boundDstPointsMetric;
			std::vector<cv::Point2f> m_boundDstPointsMetricCam;	

		};
	};
};


#endif
