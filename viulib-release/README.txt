
Viulib 2.0.X Oct 2014

Copyright (c) 2011-2014 Vicomtech-IK4
All rights reserved.

CREDITS
---------------
Author: Vicomtech-IK4
Contact: www.vicomtech.org
E-mail: viulib@vicomtech.org
Date: 22/10/2014

DESCRIPTION
---------------
Viulib (R) (Vision and Image Utility Library) is a set of precompiled libraries developed by Vicomtech that simplifies the building 
of complex computer vision, machine learning and artificial intelligence solutions. It reduces time in prototyping stages, facilitates 
technology validation, enables transparent licensing schemes and allow for multi-platform integration.
 
Viulib (R) is composed of a set of SW modules each of them deployed for particular tasks. 
Below is a list of them with details about its nature, the current development status and the platforms in which they have been tested.
 
	Application modules
 
	viulib_adas			- Advanced Driver Assistance Systems
	viulib_evaluation	- Evaluation
	viulib_hbp			- Human
	viulib_vas			- Video Analytics for Surveillance
 
	Development modules
 
	viulib_bgfg			- Background & Foreground Detection
	viulib_calibration	- Calibration
	viulib_dtc			- Detection, Tracking and Classification
	viulib_imgTools		- Image Tools
	viulib_mtools		- Mathematical Tools
	viulib_omf			- Object Model Fitting
	viulib_opflow		- Optical Flow
	viulib_ter			- Temporal Event Recognition
 
	Support modules
 
	viulib_core			- Core
	viulib_security		- Security
	viulib_system		- System
	viulib_utils			- Utils

REQUIREMENTS
---------------
Viulib 2.0.X has been tested for Windows 7 32 bits, and Ubuntu 12.04.5 LTS.
It can be likely work as well in Windows 7 64 bits, Windows 8 32 bits, and Windows 8 64 bits, although it has not been tested.

Viulib 2.0.X requires the following SW to be installed and configured properly in your system:

	OpenCV 2.4.9
		http://opencv.org/downloads.html
		https://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.9/opencv-2.4.9.exe/download

	OpenSSL 1.0.1e
		http://slproweb.com/products/Win32OpenSSL.html
		
Additionally, some modules have individual dependencies that are described in the corresponding header files (e.g. hbp.h).
		
RELEASE NOTES
---------------
This version of Viulib is the first one to be used as an SDK and delivered upon request. Previous versions (Viulib 1.X.X) are not available and include all the preliminar work done by Vicomtech-IK4. This is the reason the first version of Viulib starts as Viulib 2.0.X.
		
INSTALLATION
---------------
Please refer to the INSTALLATION.txt file that can be found in the main installation folder.