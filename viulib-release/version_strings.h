//===================================================================================
//  viulib CMake configuration file
//
//  ** File generated automatically, do not modify **
//
//  Usage from an external project:
//
//    #include "version_strings.h"
//    std::string version = viulib::VersionStrings::Version;
//
// ===================================================================================
#include "git_version.h" // loads #define GIT_COMMIT_HASH
#include <string>
#include <sstream>
// This two macros are set to check if the variable 'GIT_COMMIT_HASH' is defined empty
#define DO_EXPAND(VAL)  VAL ## 1
#define EXPAND(VAL)     DO_EXPAND(VAL)

#if !defined(GIT_COMMIT_HASH)
   #define GIT_COMMIT_HASH "000000"
#endif
namespace viulib
{
  template<typename T>
  struct Info_
  {
 	//static std::string const Version;
	static int const Version_Major = 2;
	static int const Version_Minor = 0;
	static std::string const Version_Patch;
	
	
	std::string getVersion()
	{
		std::ostringstream oss;
		oss << Version_Major << "." << Version_Minor << "." << Version_Patch;
		return oss.str();
	}
  };

  template<typename T> std::string const Info_<T>::Version_Patch=GIT_COMMIT_HASH;
  //template<typename T> std::string const Info_<T>::Version="Viulib version: 2.0.(GIT_COMMIT_HASH)";

  typedef Info_<void> VersionInfo;
}


