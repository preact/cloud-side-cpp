#include "process.h"

using namespace std;
using namespace cv;
using namespace viulib;
using namespace viulib::vas;
using namespace viulib::videoContentDescription;

//////////////////////////////////////////////
// ProcessMotion
//////////////////////////////////////////////
void ProcessMotion::setType( const std::string& _type )
{
    if( !_type.compare("MOG" ) )
    {
        bgfgModel = viulib::bgfg::BgfgModel::create("BGModelMOG");
        bgfgModel->set( false, "useGPU" );
        bgfgModel->set( true, "useMorph" );
    }
    else
    {
        cout << "Error: Not recognized Motion model: " << _type << endl;
    }
}

std::vector<std::string> ProcessMotion::compute( const std::string& _videoFileName )
{
    VideoContentDescription videoContent;

    // Open video code
    // Size
    Size origSize, procSize;
    int procWidth = 320;

    // Load video
    cv::VideoCapture video;
    video.open(_videoFileName);

    int width = 0, height = 0, fps = 0, fourcc = 0;
    int frameCount = 0;
    if( !video.isOpened() )
    {
        cout << "Cannot open video: " << _videoFileName << endl;
        return std::vector<string>();
    }
    else
    {
        // Show video information
        width = (int) video.get(CV_CAP_PROP_FRAME_WIDTH);
        height = (int) video.get(CV_CAP_PROP_FRAME_HEIGHT);
        fps = (int) video.get(CV_CAP_PROP_FPS);
        fourcc = (int) video.get(CV_CAP_PROP_FOURCC);
        frameCount = (int) video.get(CV_CAP_PROP_FRAME_COUNT);

        if(verbose)
            printf("Input video: (%d x %d) at %d fps, fourcc = %d, numFrames = %d\n", width, height, fps, fourcc, frameCount);
    }
    origSize = Size( width, height );
    int procHeight = (int)(origSize.height*((double)procWidth/origSize.width));
    procSize = Size( procWidth, procHeight );

    if(verbose)
        printf("Resize to: (%d x %d)\n", procSize.width, procSize.height);


    Mat inputImg, inputImgGray, inputImgGrayRes;
    Mat outputImg;
    int startFrame = -1;
    int endFrame = -1;
    int frameNum = 0;
    for( ;; )
    {
        // Prepare code
        if( frameNum > frameCount )
            break;

        video >> inputImg;
        if(inputImg.empty())
            break;

        if( verbose )
            if( frameNum % 100 == 0 )
                cout << "Frame: " << frameNum << endl;

        if( verbose )
            outputImg = inputImg;
        if( inputImg.type() != CV_8UC1 )
            cvtColor( inputImg, inputImgGray, CV_BGR2GRAY );
        else
            inputImg.copyTo( inputImgGray );

        resize( inputImgGray, inputImgGrayRes, procSize );

        // Process
        Mat fgMask;
        bgfgModel->compute(inputImgGray, fgMask);
        int c = countNonZero( fgMask );
        double val = static_cast<double>(c) / ( fgMask.cols * fgMask.rows );

        if( val == 1.0) // Full white fgMask is due to incorrect initialization
        {
            frameNum++;
            continue;
        }

        double thres = 0.02;
        if( val > thres )
        {
            if( startFrame == -1 )
                startFrame = frameNum;
            endFrame = frameNum;
        }
        else if( startFrame != -1 )
        {
            videoContent.addEvent( Event( -1, "Motion", startFrame, endFrame ) );
            if( verbose )
                cout << "Added: Motion (" << startFrame << ", " << endFrame << ")" << endl;
            startFrame = -1;
            endFrame = -1;
        }

        if( verbose )
        {
            cv::putText( outputImg, "video_cl_1", Point(0, outputImg.rows-5), CV_FONT_NORMAL, 0.8, CV_RGB(0,0,255), 1, CV_AA);
            bgfgModel->draw( outputImg);
            viulib::imgTools::drawProgressBar(outputImg, Rect(5,5,outputImg.cols/3,20),(val>thres)?(1.0):(val/thres), "Motion level");
            imshow("video_cl_1", outputImg);
            waitKey(1);
        }

        frameNum++;
    }

    // Convert into event type
    return videoContent.getEventTypes();
}
//////////////////////////////////////////////
// ProcessPersonsActions2
//////////////////////////////////////////////
void ProcessPersonsActions2::setType( const std::string& _type )
{
    if( !_type.compare("video_cl_2") )
        algorithm = viulib::vas::PersonTracker::VIEW_FAR;
    else if( !_type.compare("video_cl_3") )
        algorithm = viulib::vas::PersonTracker::VIEW_MID;
    else if( !_type.compare("video_cl_4") )
        algorithm = viulib::vas::PersonTracker::VIEW_CLOSE;
    else
    {
        cout << "Error: algorithm type not recognized: " << _type << endl;
    }
}
std::vector<std::string> ProcessPersonsActions2::compute( const std::string& _videoFileName )
{
    VideoContentDescription videoContent;

    // Open video code
    // Size
    Size origSize, procSize;
    int procWidth = 640;

    // Load video
    cv::VideoCapture video;
    video.open(_videoFileName);

    int width = 0, height = 0, fps = 0, fourcc = 0;
    int frameCount = 0;
    if( !video.isOpened() )
    {
        cout << "Cannot open video: " << _videoFileName << endl;
        return std::vector<string>();
    }
    else
    {
        // Show video information
        width = (int) video.get(CV_CAP_PROP_FRAME_WIDTH);
        height = (int) video.get(CV_CAP_PROP_FRAME_HEIGHT);
        fps = (int) video.get(CV_CAP_PROP_FPS);
        fourcc = (int) video.get(CV_CAP_PROP_FOURCC);
        frameCount = (int) video.get(CV_CAP_PROP_FRAME_COUNT);

        if(verbose)
            printf("Input video: (%d x %d) at %d fps, fourcc = %d, numFrames = %d\n", width, height, fps, fourcc, frameCount);
    }
    origSize = Size( width, height );
    int procHeight = (int)(origSize.height*((double)procWidth/origSize.width));
    procSize = Size( procWidth, procHeight );

    if(verbose)
        printf("Resize to: (%d x %d)\n", procSize.width, procSize.height);


    // Configure detectors (using algorithm type)
    personTracker = viulib::vas::PersonTracker::create("PersonTracker_BGFG");
    int viewType = algorithm;
    if(personTracker != 0)
    {
        // Config
        
        int profileType = viulib::vas::PersonTracker::PROFILE_MID;
        int thresholdType = viulib::vas::PersonTracker::THRESHOLD_LOW;

        if( verbose )
        {
            cout << "----------------------\n"
                 << "PersonTracker_BGFG config.\n"
                 << "\tviewType : " << viewType << "(0:CLOSE, 1:MID, 2:FAR)\n"
                 << "\tprofileType : " << profileType << "(0:FAST, 1:MID, 2:SLOW)\n"
                 << "\tthresholdType : " << thresholdType << "(0:LOW, 1:MID, 2:HIGH)" << endl;
        }

        personTracker->set(verbose, "verbose");
        personTracker->set(false, "showDebugImages");

        personTracker->set( viewType, "viewType");
        personTracker->set( profileType, "profileType");
        personTracker->set( thresholdType, "thresholdType");

        personTracker->set( procSize, "procSize");

        if( viewType != viulib::vas::PersonTracker::VIEW_CLOSE )
            personTracker->set("hogcascade_pedestrians.xml", "fb_classifier");
        else
        {
            personTracker->set("lbpcascade_frontalface.xml", "face_classifier");
            personTracker->set("haarcascade_mcs_upperbody.xml", "ub_classifier");
        }
    }

    // Set video data
    videoContent.setVideoData( VideoData(_videoFileName, frameCount, origSize.height, origSize.width, fps, "") );

    // Set Rules
    rulePersonMotion = viulib::vas::OfflineRule::create("OfflineRulePersonMotion");
    rulePersonsMotion = viulib::vas::OfflineRule::create("OfflineRulePersonsMotion");
    rulePersonsFighting = viulib::vas::OfflineRule::create("OfflineRulePersonsFighting");

    // -------------------------------
    // 1.- Get tracks
    // -------------------------------
    PROFILE_BEGIN(1.-Tracks)
    vector<int> trackIDVector;
    vector<cv::Ptr<Track> > trackContainer;

    Mat inputImg, inputImgGray, inputImgGrayRes;
    Mat outputImg;
    int frameNum = 0;
    for( ;; )
    {
        // Prepare code
        if( frameNum > frameCount )
            break;

        video >> inputImg;
        if(inputImg.empty())
            break;

        if( verbose )
            if( frameNum % 100 == 0 )
                cout << "Frame: " << frameNum << endl;

        outputImg = inputImg;
        if( inputImg.type() != CV_8UC1 )
            cvtColor( inputImg, inputImgGray, CV_BGR2GRAY );
        else
            inputImg.copyTo( inputImgGray );

        resize( inputImgGray, inputImgGrayRes, procSize );

        // Run detectors
        PROFILE_BEGIN(PERSON_TRACKER)
        // Detect persons
        personTracker->set(frameNum, "frameNum");
        PersonTracker::PersonTracker_Data data = personTracker->run( inputImgGrayRes );

        // Accumulate tracks
        storeTracks(data.pTracks, trackContainer, trackIDVector);

        // Feed fighting detection
        Mat fgMask = personTracker->get( "fgMask" );
        rulePersonsFighting->set( fgMask, "fgMask" );
        rulePersonsFighting->set( frameNum, "frameNum" );

        PROFILE_END(PERSON_TRACKER)

        if( verbose)
        {
            personTracker->draw( outputImg, CV_RGB(0,0,255));
            imshow("Tracking", outputImg);
            waitKey(1);
        }

        // Iterate
        frameNum++;
    }
    PROFILE_END(1.-Tracks)

    // -------------------------------
    // 2.- Refine Tracks
    // -------------------------------
    PROFILE_BEGIN(2.-REFINE)
    viulib::dtc::TrackJoiner* trackJoiner = new viulib::dtc::TrackJoiner();
    vector<cv::Ptr<Track> > tracks;
	if(!trackContainer.empty())
		tracks = trackJoiner->join( trackContainer );

    for( size_t i=0; i<tracks.size(); ++i )
    {
	if( !tracks[i]->isClutter() )
	{
        viulib::videoContentDescription::Object object;
        fromTrackToObject( tracks[i], object);
	    if( viewType == viulib::vas::PersonTracker::VIEW_CLOSE )
	        fromObjectFaceToObjectFullBody( object );
        videoContent.addObject( object );
	}
    }
    PROFILE_END(2.-REFINE)

    // -------------------------------
    // 3.- RuleManager
    // -------------------------------
    PROFILE_BEGIN(3.-RuleManager)
    // Create RuleManager
    offRM = OfflineEventDetector::create("OfflineRuleManager");
    if( offRM == 0 )
    {
        cout << "ERROR: Cannot create OfflineRuleManager";
        exit(-1);
    }

    // Set Rules
    offRM->set( *rulePersonMotion, "Rule");
    offRM->set( *rulePersonsMotion, "Rule");
    offRM->set( *rulePersonsFighting, "Rule");

    // Launch
    offRM->run( videoContent );

    // Get results
    videoContent = offRM->getVideoContentDescription();

    if(verbose)
    {
        vector<Event> events = videoContent.getEventList().getEvents();
        vector<Relation> relations = videoContent.getRelationList().getRelations();

        for( size_t e=0; e<events.size(); ++e )
            cout << "Event[" << events[e].getID()
                 << "](" << events[e].getFrameStart() << ","
                 << events[e].getFrameEnd() << "): "
                 << events[e].getType() << endl;
        for( size_t r=0; r<relations.size(); ++r)
        {
            int rhID = relations[r].getRHID();
            bool active = false;
            for( size_t e=0; e<events.size(); ++e)
            {
                if( events[e].isID( rhID ))
                {
                    active = true;
                    break;
                }
            }
            if(active)
                relations[r].print();
        }
    }
    PROFILE_END(3.-RuleManager)

    // Convert into event type
    return videoContent.getEventTypes();

}
