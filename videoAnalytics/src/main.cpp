// -- C++ -- //
#include <iostream>

// -- OpenCV -- //
#include "opencv2/opencv.hpp"

// -- Viulib -- //
#include "core.h"

// -- Project -- //
#include "args.h"
#include "clip.h"
#include "algorithmLauncher.h"

using namespace std;

void help()
{
	 cout << "/*\n"
         << " **************************************************************************************************\n"
		 << " * Cloud-side Video Analytics \n"
         << " * ----------------------------------------------------\n"		 
		 << " * \n"
		 << " * Author:Vicomtech-IK4\n"
		 << " * www.vicomtech.es\n"
		 << " * Version v" << APP_VERSION_MAJOR << "." << APP_VERSION_MINOR << "." << APP_VERSION_PATCH << "." << APP_VERSION_STATUS << "\n"		 		 
         << " * Using Viulib v" << viulib::getVersion() << "\n"
		 << " * \n"
         << " * Date:08/09/2015\n"
		 << " **************************************************************************************************\n"
		 << " * \n"
         << " * Usage: cloud_video_analytics (-r | --release ) (-j | --json) <JSON serialized message>\n"
         << " *     (-r | --release) When specified, no debug infor is shown.\n"
         << " * Example: ./cloud_video_analytics -r -j \"{ \\\"clipID\\\" : 1, \\\"videoFilePath\\\" : \"\\\"/home/mnieto/p-react/test001.avi\"\\\", \\\"algorithmName\\\" : \\\"video_cl_1\\\" }\"\n"
	         << " */\n" << endl;
}

int main(int _argc, char** _argv)
{
	/* INPUT
		
		>JSON message containing:
			the path of the video file locally downloaded by the communications module 
			the name/parameters of the algorithm to run
		>Additional arguments (verbose, etc)

        e.g. >./cloud_video_analytics -r -j "{ \"clipID\" : 1,
			\"videoFilePath\" : "\"/home/mnieto/Videos/test001.avi"\", 
            \"algorithmName\" : \"video_cl_1" }"
	*/	

	// -- Parse arguments -- //
	Args myArgs( _argc, _argv );	// internally parses the JSON message
    if( !myArgs.inputMessageExists() )
		return -1;

    if(!myArgs.isReleaseMode())
        help();

	// -- Create Clip (both metadata and content) -- //
	Clip inputClip;

	// -- Load data from JSON and feed into Clip Object-- //
	inputClip.setClipObjectID( myArgs.getJSONInputMessage().getData().getClipID() );
	inputClip.setVideoFilePath( myArgs.getJSONInputMessage().getData().getVideoFilePath() );
	// TBC: other params
		
	// -- Create and setup the algorithm execution -- //
	AlgorithmLauncher algorithmLauncher( myArgs.getJSONInputMessage().getData().getAlgorithmName() );
	algorithmLauncher.setVideoFile( myArgs.getJSONInputMessage().getData().getVideoFilePath() );
    algorithmLauncher.setVerbose(!myArgs.isReleaseMode());
	
	// -- Execute and retrieve data -- //
    std::vector<std::string> eventsDetected = algorithmLauncher.run();

    // -- Retrieve result and generate output metadata -- //
    /* OUTPUT
        >JSON message returned to the console containing:
            the metadata fields of the Clip Object updated by the process
        e.g. "{ \"analyticsVideoResults\" : [ \"PersonRunning\", \"Chasing\" ] }"
    */
    std::stringstream oss;
    oss << "[ { \"analyticsVideoResults\" : [ ";
    for(size_t i=0; i<eventsDetected.size(); ++i)
    {
        if(i==eventsDetected.size()-1)
            oss << "\"" << eventsDetected[i] << "\"" << " ] } ]";
        else
            oss << "\"" << eventsDetected[i] << "\"" << ", ";
    }
    if( eventsDetected.size() == 0 )
        oss << " ] } ]";

	// -- Return back to the output defined by the argument -- //
    if( !myArgs.isReleaseMode() )
        cout << "== START OF OUTPUT MESSAGE ==" << endl;    
     cout << oss.str() << endl;
    if( !myArgs.isReleaseMode() )
        cout << "== END OF OUTPUT MESSAGE == " << endl;

}
