#ifndef _ALGORITHMLAUNCHER_H_
#define _ALGORITHMLAUNCHER_H_

#include <string>
#include <vector>


class AlgorithmLauncher
{
public:
	/* Constructors
	*/
	AlgorithmLauncher(const std::string& _algorithmName);

    std::vector<std::string> run();
	
	// -- Setters -- //
	void setVideoFile( const std::string& _videoFileName ) { videoFileName = _videoFileName; }
    void setVerbose( bool _verbose ) { verbose = _verbose; }
	
private:
	std::string algorithmName;
	std::string videoFileName;

	bool validAlgorithm;
    bool verbose;
};

#endif //_ALGORITHM_H_
