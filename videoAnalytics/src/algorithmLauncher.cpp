#include "algorithmLauncher.h"
#include "process.h"

#include <iostream>

using namespace std;



/*int openCVRun( const std::string& _videoFileName )
{
	cv::VideoCapture video( _videoFileName );
	int width = 0, height = 0, fps = 0, fourcc = 0;
	int frameCount = 0;
	if( !video.isOpened() )
	{
		cout << "Video " << _videoFileName << " cannot be opened" << endl;
		return -1;
	}
	else
	{
		// Show video information
		width = (int) video.get(CV_CAP_PROP_FRAME_WIDTH);
		height = (int) video.get(CV_CAP_PROP_FRAME_HEIGHT);
		fps = (int) video.get(CV_CAP_PROP_FPS);
		fourcc = (int) video.get(CV_CAP_PROP_FOURCC);
		frameCount = (int) video.get(CV_CAP_PROP_FRAME_COUNT);
		
		cout 	<< "Input video: (" << width << ", " << height << ")"
			<< " at " << fps << " fps, fourcc = " << fourcc
			<< ", numFrames = " << frameCount << endl;
	}	

	Mat inputImg;
	int frameNum = 0;
	for(;;)
	{
		video >> inputImg;
		if( inputImg.empty() )
		{
			break;
		}

		cout << "Frame " << frameNum << "/" << frameCount << "\r";

		// -- Some processing -- //
		Mat edges;
		cv::Canny( inputImg, edges, 50, 120 );

		frameNum++;
	}
	
	return 0;
}*/

AlgorithmLauncher::AlgorithmLauncher(const std::string& _algorithmName):validAlgorithm(false)
{
	if( !_algorithmName.empty() )
	{
		algorithmName = _algorithmName;
        if( !algorithmName.compare("video_cl_1") || !algorithmName.compare("video_cl_2") || !algorithmName.compare("video_cl_3") || !algorithmName.compare("video_cl_4"))
        {
			validAlgorithm = true;
		}
		else
		{
			cout << "Algorithm: " << algorithmName << " is not recognized.\n" << endl;
		}
	}
	else
	{
		cout << "Warning: No algorithmName provided to AlgorithmLauncher." << endl;
	}


}
std::vector<std::string> AlgorithmLauncher::run()
{
    vector<string> result;
	if( validAlgorithm )
	{
		// Launch algorithm
        if( !algorithmName.compare("video_cl_1") )
        {
            if(verbose)
                cout << "\nAlgorithmLauncher: Launching algorithm: " << algorithmName << " on video: " << videoFileName << "..." << endl;


            Process* process = new ProcessMotion();
            process->setType( "MOG" );
            process->setVerbose( verbose );
            result = process->compute( videoFileName );

            if(verbose)
                cout << "Video processed." << endl;
            return result;
		}
        else if( !algorithmName.compare("video_cl_2") || !algorithmName.compare("video_cl_3") || !algorithmName.compare("video_cl_4") )
        {
            if(verbose)
                cout << "\nAlgorithmLauncher: Launching algorithm: " << algorithmName << " on video: " << videoFileName << "..." << endl;


            Process* process = new ProcessPersonsActions2();
            process->setType( algorithmName );
            process->setVerbose( verbose );
            result = process->compute( videoFileName );

            if(verbose)
                cout << "Video processed." << endl;
            return result;
        }
		else
		{
            if(verbose)
                cout << "Algorithm: " << algorithmName << " is not recognized.\n" << endl;
            return result;
		}		
	}
	else
	{
        if(verbose)
            cout << "Warning: no valid algorithm has been loaded." << endl;
        return result;
	}
}
