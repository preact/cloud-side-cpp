#ifndef _CLIP_H_
#define _CLIP_H_

#include <string>
#include <vector>

/* Container of Clip object information
*/
class Clip
{
	/* Binary content with the actual video content.
	*/
	struct Video // known as the "Clip Data"
	{	
		std::string videoFilePath;
		// TODO: Perhaps some OpenCV container
	};
	/* Attributes defined as in https://redmine.p-react.eu/projects/p-react/wiki/Orchestration
	*/
	struct Attributes // known as "Clip Object"
	{
		int clipObjectID;
		int parentClipObjectID;
		int childrenClipObjectID;

		std::string name;

		int width;
		int height;
		std::string aspectRatio;
		int frames;
		int duration;

		std::string videoFormat;
		int videoBitrate;

		int audioTrackID;

		std::string codec;
		int bitrate;
		int samplingRate;

		int priority;

		std::string location;
		int embeddedSystemID;
		long clipStartTime;		// date, time
		long clipEndTime;
		long systemUploadTime;

		std::vector<int> analyticsVideoObjectID;
		std::vector<int> analyticsAudioObjectID;
		std::vector<std::string> detectionEventReason;
		std::vector<std::string> analyticsAlgoResults;

		std::string clipDataType;
		std::string clipData;
		std::string digitalSignature;
		std::string clipObjectNotes;

		long timeToLive;
		long timeToDisplay;
		long timeToDelete;
		long timeToPurge;

		/* Default constructor to initialize Attributes
		*/
		Attributes():clipObjectID(0),parentClipObjectID(0),childrenClipObjectID(0),
			name(""),
			width(0), height(0),aspectRatio(""),frames(0),duration(0),
			videoFormat(""),videoBitrate(0),
			audioTrackID(0),
			codec(""), bitrate(0),samplingRate(0),
			priority(0),
			location(""),embeddedSystemID(0),clipStartTime(0),clipEndTime(0),systemUploadTime(0),
			clipDataType(""),clipData(""),digitalSignature(""),clipObjectNotes(""),
			timeToLive(0),timeToDisplay(0),timeToDelete(0),timeToPurge(0)
			{}				
		
	};
public:
	// -- Setter functions -- //
	// -- -- Attributes -- -- //
	void setClipObjectID( int i ) 		{ attributes.clipObjectID = i; }
	// TBC

	// -- -- Video -- -- //
	void setVideoFilePath( const std::string& _videoFilePath ) { video.videoFilePath = _videoFilePath; }
	// TBC

private:
	Attributes attributes;
	Video video;
};

#endif // _CLIP_H_
