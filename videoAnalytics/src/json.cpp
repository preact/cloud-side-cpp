#include "json.h"

using namespace std;

/*JSONOutputMessage::Data::Data(const std::string& _serializedMessage)
{
	// Parse into data
    //cout << "Parsing:\n" << _serializedMessage << endl;

	// From: https://wiki.gnome.org/action/show/Projects/JsonGlib?action=show&redirect=JsonGlib
	g_type_init();
	JsonParser* parser = json_parser_new();
	const gchar* str = _serializedMessage.c_str();
	json_parser_load_from_data( parser, str, -1, NULL );

	JsonReader* reader = json_reader_new( json_parser_get_root( parser ) );

	// -- Read data -- //
    json_reader_read_member( reader, "analyticsVideoResults" );
    this->analyticsVideoResults = string( json_reader_get_string_value( reader ) );
	json_reader_end_member( reader );

	g_object_unref( reader );
	g_object_unref( parser );

}
JSONOutputMessage::JSONOutputMessage(const std::string& _serializedMessage)
	:serializedMessage(_serializedMessage),data(_serializedMessage)
{	
}*/

JSONInputMessage::Data::Data(const std::string& _serializedMessage)
{
	// Parse into data
    //cout << "Parsing:\n" << _serializedMessage << endl;

	// From: https://wiki.gnome.org/action/show/Projects/JsonGlib?action=show&redirect=JsonGlib
	g_type_init();
	JsonParser* parser = json_parser_new();
	const gchar* str = _serializedMessage.c_str();
	json_parser_load_from_data( parser, str, -1, NULL );

	JsonReader* reader = json_reader_new( json_parser_get_root( parser ) );

	// -- Read data -- //
	json_reader_read_member( reader, "clipID" );
	this->clipID = json_reader_get_int_value( reader );	
	json_reader_end_member( reader );

	json_reader_read_member( reader, "videoFilePath" );   
	this->videoFilePath = string( json_reader_get_string_value( reader ) );
	json_reader_end_member( reader );

	json_reader_read_member( reader, "algorithmName" );
	this->algorithmName = string( json_reader_get_string_value( reader ) );
	json_reader_end_member( reader );

	g_object_unref( reader );
	g_object_unref( parser );

}
JSONInputMessage::JSONInputMessage(const std::string& _serializedMessage)
	:serializedMessage(_serializedMessage),data(_serializedMessage)
{	
}
