#ifndef _ARGS_H_
#define _ARGS_H_

#include <vector>

// -- PROJECT -- //
#include "json.h"

class Args
{
public:	
	/* Constructor 
	*/
	Args(int _argc, char** _argv);

	// -- Getters -- //
	const JSONInputMessage& getJSONInputMessage() const { return jsonInputMessage; }
	bool inputMessageExists() const { return !jsonInputMessage.getSerializedMessage().empty(); }

    bool isReleaseMode() const { return releaseMode; }

private:
	JSONInputMessage jsonInputMessage;	// Container of JSON input message
    bool releaseMode;

};

#endif // _ARGS_H_
