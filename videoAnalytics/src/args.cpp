#include <iostream>
#include <stdlib.h>
#include "args.h"

using namespace std;

Args::Args(int _argc, char** _argv)
{
    // <app> (-r | --release) (-j | --json) <JSONstring>

    releaseMode = false;
    string JSONString;

	if( _argc < 2 )
	{
		cout << "Warning: not enough parameters." << endl;
		return;
	}
	for(int i=1; i<_argc; i++)
	{				
		if(string(_argv[i]) == "--json" || string(_argv[i]) == "-j")
		{	
            JSONString = string(_argv[++i]);
            //cout << "JSON string received:\n" << JSONString << endl;

			// Read JSON message and read values			
			jsonInputMessage = JSONInputMessage( JSONString );
			//cout << "Args::jsonMessage: " << jsonMessage.getSerializedMessage() << endl;
		}		
        else if(string(_argv[i]) == "--release" || string(_argv[i]) == "-r")
        {
            releaseMode = true;
        }
		else cout << "Unknown argument: " << string(_argv[i]) << endl;
	}

    if( !releaseMode )
        cout << "JSON string received:\n" << JSONString << endl;
}


