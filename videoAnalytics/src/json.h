#ifndef _JSON_H_
#define _JSON_H_

#include <iostream>

// -- JsonGlib -- //
#include <glib-2.0/glib.h>
#include <glib-2.0/glib/gstdio.h>
#include <glib-2.0/gio/gio.h>
#include <json-glib/json-glib.h>

/*
class JSONOutputMessage
{
public:	
	class Data
	{
	public:
        // Constructors

		Data():detectedEvent("") {}
		Data(const std::string& _serializedMessage);

        // Getters
		std::string 	getDetectedEvent() const { return detectedEvent; }
		
		void print() const 
		{
			// -- Print -- //
			std::cout 	<< "{\n"
                    << "\tanalyticsVideoResults: " << detectedEvent << "\n"
					<< "}"	
					<< std::endl;
		}
	private:		
        std::string analyticsVideoResults;
	};

    // Constructors

	JSONOutputMessage(){}
	JSONOutputMessage(const std::string& _serializedMessage);
	
	// -- GETTERS -- //
	void printSerializedMessage() const { std::cout << "Serialized JSON message:\n" << serializedMessage << std::endl; }
	void printMessage() const { data.print(); }
	std::string getSerializedMessage() const { return serializedMessage; }

	const Data& getData() const { return data; }

private:
	Data data;
	std::string serializedMessage;
};*/

/* The message from the Analytics module (the communications interface)
*/
class JSONInputMessage
{
public:	
	class Data
	{
	public:
		/* Constructors 
		*/
		Data():clipID(0),videoFilePath("") {}
		Data(const std::string& _serializedMessage);

		/* Getters
		*/
		int 		getClipID() const { return clipID; }
		std::string 	getVideoFilePath() const { return videoFilePath; }
		std::string 	getAlgorithmName() const { return algorithmName; }
		
		void print() const 
		{
			// -- Print -- //
			std::cout 	<< "Input Message read:\n" << std::endl;
			std::cout 	<< "{\n"
					<< "\tclipID: " << clipID << ",\n"
					<< "\tvideoFilePath: " << videoFilePath << "\n"
					<< "\talgorithmName: " << algorithmName << "\n"
					<< "}"	
					<< std::endl;
		}
	private:
		int clipID;
		std::string videoFilePath;
		std::string algorithmName;
		// TBC
	};

	/* Constructors
	*/
	JSONInputMessage(){}
	JSONInputMessage(const std::string& _serializedMessage);
	
	// -- GETTERS -- //
	void printSerializedMessage() const { std::cout << "Serialized JSON message:\n" << serializedMessage << std::endl; }
	void printMessage() const { data.print(); }
	std::string getSerializedMessage() const { return serializedMessage; }

	const Data& getData() const { return data; }

private:
	Data data;
	std::string serializedMessage;
};

#endif // _JSON_H_
