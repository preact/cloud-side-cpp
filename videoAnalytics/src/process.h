#ifndef __PROCESS_H__
#define __PROCESS_H__

#include "opencv2/opencv.hpp"

#include "core.h"
#define DO_PROFILING 1
#include "profiling.h"
#include "bgfg.h"
#include "vas.h"
#include "videoContentDescription.h"

/** Base class for processing at the cloud side.
*/
class Process
{
public:
    virtual ~Process(){}
    void setVerbose( bool _verbose ) { verbose = _verbose; }

    virtual void setType( const std::string& _type ) {}

    virtual std::vector<std::string> compute(const std::string& _videoFileName) = 0;

protected:
    bool verbose;
};

/** Class for computing motion: video_cl_1
*/
class ProcessMotion : public Process
{
public:
    virtual std::vector<std::string> compute( const std::string& _videoFileName );
    virtual void setType( const std::string& _type );
private:
    viulib::bgfg::BgfgModel* bgfgModel;
    cv::Mat motionMask;
};

class ProcessPersonsActions2 : public Process
{
public:
    virtual std::vector<std::string> compute( const std::string& _videoFileName );
    virtual void setType( const std::string& _type );
private:
    int algorithm;

    // Detectors
    viulib::vas::PersonTracker* personTracker;
    viulib::vas::Rule* rulePersonMotion;
    viulib::vas::Rule* rulePersonsMotion;
    viulib::vas::Rule* rulePersonsFighting;
    viulib::vas::OfflineEventDetector* offRM;
};

#endif // __PROCESS_H__
