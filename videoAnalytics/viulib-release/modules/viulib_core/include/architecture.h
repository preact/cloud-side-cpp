/**  
 *  
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef VIULIB_ARCHITECTURE_H
#define VIULIB_ARCHITECTURE_H

//-- System architecture defines --//

#if defined _WIN32 || defined _WIN64
   // _WIN32 is also defined for 64bit environments
   #define __VIULIB_WINDOWS__
#endif 
#if defined __linux__ || defined __GNUC__
   // __GNUC__ for gnu on linux
   // __gnu_linux__
   #define __VIULIB_LINUX__
#endif 
#if defined __APPLE__ || defined __MACH__
   // __APPLE__ && __MACH__
   #include <TargetConditionals.h>
   #ifdef TARGET_OS_IPHONE 
      #define __VIULIB_IOS__
   #else
      #define __VIULIB_APPLE__
   #endif
#endif 
#if defined __ANDROID__
   #define __VIULIB_ANDROID__
#endif


#endif //VIULIB_ARCHITECTURE_H