/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */


#ifndef RECT_H
#define RECT_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "point.h"
#include "core.h"

namespace viulib
{
	/** \brief Rect <template>
	*	This class represents a rectangle, fully compatible with cv::Rect
	*   This class provides tools for multiple conversion types
	*	<code>
	*		viulib::Rect<float> r(10.0,20.0,315.0,230.4);
	*		r = cv::Rect(5,1,14,20);
	*	</code>
	*	\ingroup viulib_core
	*/

	template <class T>
	class  Rect
	{	
	public:

		//! \name Constructors
		//@{
		/** \brief Rect constructors
			\param _x, _y , _widht,_height [in] coordenadas en tipo T
			\param _r [in] rectangulo f (float)
			\param _r [in] rectangulo d (double)
		*/
		Rect()								{ x = y =0 ;  width = height = 0; };
		Rect(T _x, T _y, T _w,T _h)			{ x = _x; y= _y; width = _w; height = _h;};
		Rect(const cv::Rect_<float> &_r)	{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height;};
		Rect(const cv::Rect_<double> &_r)	{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height;};
		Rect(const cv::Rect_<int> &_r)		{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height;};
		//
		////! \name Assignment Operator 
		////@{
		///** \brief Allow assignment between Mat, Point3f, Point3d, vector and Point3
		//	\return image size
		//*/
		Rect<T>& operator=( const cv::Rect_<float>& _r )	{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height; return *this; };
		Rect<T>& operator=( const cv::Rect_<double>& _r )	{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height; return *this; };
		Rect<T>& operator=( const cv::Rect_<int>& _r )		{ x = (T)_r.x; y= (T)_r.y; width = (T)_r.width; height = (T)_r.height;  return *this; };
		//
		////! \name Mathemathical Operators
		////@{
		///** \brief Allow operations between Point3 objects
		//	\return image size
		//*/
		///*Rect operator+( const Point3<T>& _p )	{Point3<T> p; p.x = x + _p.x; p.y = y + _p.y; p.z = z + _p.z;  return p; };
		//Rect operator-( const Point3<T>& _p )	{Point3<T> p; p.x = x - _p.x; p.y = y - _p.y; p.z = z - _p.z;  return p; };
		//Rect operator*( const Point3<T>& _p )	{Point3<T> p; p.x = x * _p.x; p.y = y * _p.y; p.z = z * _p.z;  return p; };
		//Rect operator/( const Point3<T>& _p )	{Point3<T> p; if (_p.x!=0)  p.x = x / _p.x; 
		//											  if (_p.y!=0) p.y = y / _p.y; 
		//											  if (_p.z!=0) p.z = z / _p.z;  
		//											  return p; };*/
		////! \name Casting operatoors
		////@{
		///** \brief Allow casting  between Point3 and Mat, Point3d, Point3f and Vector<T>
		//	\return image size
		//*/
		operator cv::Rect(){return cv::Rect(x,y,width,height);};
		operator cv::Rect_<float>(){return cv::Rect_<float>(x,y,width,height);};
		operator cv::Rect_<double>(){return cv::Rect_<double>(x,y,width,height);};
		operator cv::Rect_<short>(){return cv::Rect_<short>(x,y,width,height);};

		//operator Rect<double>(){double x1=x,y1=y,w1=widht,h1=height;return Rect<double>(x1,y1,w1,h1);};
		//operator Rect<float>(){float x1=x,w1=widht,h1=height; return Rect<float>(x1,y1,w1,h1);};
		//operator Rect<int>(){int x1=x,y1=y,w1=widht,h1=height; return Rect<int>(x1,y1,w1,h1);};
		//operator Rect<short>(){short x1=x,y1=y,w1=widht,h1=height; return Rect<short>(x1,y1,w1,h1);};

		///** \brief calculates the module of the vector between (0,0,0) and this point
		//	\return module
		//*/
		T area() { return width*height;};

		///** \brief translate the point adding the vector (0,0,0)-_p
		//	\return 
		//*/
		T translate(Point _p) {  x +=_p.x; y+=_p.y;};

		bool intersection(cv::Rect &_r )
		{
			return	  !(_r.x+_r.width <x ||
						_r.x > x + width ||
						_r.y+_r.height<y ||
						_r.y > y + height);
		};

		bool inside(viulib::Point2<int> &_p)
		{
			return !(_p.x <x ||
					_p.x > x + width ||
					_p.y<y ||
					_p.y > y + height);
		}
		float intersection(cv::Rect &_r,std::vector<cv::Point> &contour)
		{

			int w,h;
			
			cv::Rect r1,r2;
			r1 = *this;
			r2 = _r;

			

			w= max (_r.width+_r.x,width+x);
			h= max (_r.height+_r.y,height+y);
      w = w-x;
      h=h-y;

      int mx, my;
			mx = std::min (_r.x, x);
			my = std::min (_r.y, y);

      r1.x -=mx;
			r2.x -=mx;
			r1.y -=my;
			r2.y -=my;

			cv::Mat img_r1,img_r2;
			img_r1.create(h,w,CV_8UC1);
			img_r2.create(h,w,CV_8UC1);
			img_r1.setTo(0);
			img_r2.setTo(0);

			cv::rectangle(img_r1,_r,cv::Scalar(255,255,255),CV_FILLED);
			cv::rectangle(img_r2,*this,cv::Scalar(255,255,255),CV_FILLED);

			cv::Mat res;
			res.create(h,w,CV_8UC1);
			cv::multiply(img_r1,img_r2,res);

			std::vector< std::vector< cv::Point > > contours;
			std::vector<cv::Vec4i> hierarchy;
			cv::findContours( res, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

			if (contours.empty()) return 0.0;

			contour = contours[0];

			double area =  cv::boundingRect(cv::Mat(contour)).area();
			return (float)this->area()/area;	

		};

public:
		T x;
		T y;
		T width;
		T height;
		

	};
	
}
#endif