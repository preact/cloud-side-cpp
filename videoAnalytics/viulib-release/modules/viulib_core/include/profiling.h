/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

#ifndef PROFILING_H_
#define PROFILING_H_

#include "core.h"
#include <map>
#include <list>
#include <string>
#include <iostream>


namespace viulib
{

	/** \brief Segment info for profiling
	 *	\ingroup viulib_core
	 */
	struct SEGMENT_INFO
	{	
		double first_time;
		double t0,t1;
		int ocurrences;
		float total_time;
		float last_time;
		std::string alias;
		SEGMENT_INFO()
		{
			first_time=t0=t1=0.0;
			ocurrences=0;
			total_time=last_time=0.0f;
		};
	};
	/** \brief Profiler info for profiling
	 *	\ingroup viulib_core
	 */
	struct PROFILER_INFO
	{
		std::map<std::string,SEGMENT_INFO> info_map;

	};

	 /** \brief Profiler
		*	This class provides methods for using a profiling tool. It is a 
		*	singleton, and this single instance can be accessed by using the instance() method:
		*   Use the macros in your program as shown below
		*	<code>
		*
		*   .h
		*
		*		#define DO_PROFILING 1
		*		#include "profiling.h"
		*
		*
		*
		*	in each segment of code you want to analize
		*
		*		PROFILE_BEGIN(main::prueba)
		*              your code here
		*		PROFILE_END(main::prueba)
		*
		*
		*
		*   when you want to see the info
		*
		*		PROFILE_OUT()
		*
		*	</code>
		*	\ingroup viulib_core
	*/

	 class Profiler
	 {
	 public:
		/** \brief	Destructor. */
		virtual ~Profiler(){};
		
		static CORE_LIB Profiler *instance();
		
		virtual void update(std::string _segmentAlias) = 0;

		virtual void printOut(std::string _segmentAlias,bool execTime=false) = 0;
		virtual void printOut(bool execTime=false,int _ordered=1) = 0;
		virtual void printString(std::string _segmentAlias,std::string &_output_string) = 0;

		virtual SEGMENT_INFO& get(std::string _segmentAlias) = 0;
		virtual PROFILER_INFO& get() = 0;

	 };



}


//#define DO_PROFILING 1
#if DO_PROFILING == 1

	#define PROFILE_BEGIN(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->update(std::string(#SEGMENT_ALIAS));

	#define PROFILE_END(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->update(std::string(#SEGMENT_ALIAS));

	#define PROFILE_OUT_SINGLE(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->printOut(std::string(#SEGMENT_ALIAS),false);

	#define PROFILE_OUT_SEGMENT(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->printOut(std::string(#SEGMENT_ALIAS),false);
	
  #define PROFILE_OUT() \
		viulib::Profiler::instance()->printOut(false,1);

	#define PROFILE_OUT_ORDERED(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->printOut(false,1);

	#define PROFILE_OUT_SINGLE_EXEC_TIME(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->printOut(std::string(#SEGMENT_ALIAS),(bool)true);
	
	#define PROFILE_OUT_EXEC_TIME(SEGMENT_ALIAS_EXEC_TIME) \
		viulib::Profiler::instance()->printOut(true);

	#define PROFILE_EXEC_TIME_STRING(SEGMENT_ALIAS,OUTPUT_TEXT) \
		viulib::Profiler::instance()->printString(std::string(#SEGMENT_ALIAS),OUTPUT_TEXT);

	#define PROFILE_GET_LAST_TIME(SEGMENT_ALIAS) \
		viulib::Profiler::instance()->get(std::string(#SEGMENT_ALIAS)).last_time;
	
#else 
	#define PROFILE_BEGIN(SEGMENT_ALIAS) \
	{}

	#define PROFILE_END(SEGMENT_ALIAS) \
	{}

	#define PROFILE_OUT_SINGLE(SEGMENT_ALIAS) \
	{}
	
	#define PROFILE_OUT()\
	{}

	#define PROFILE_OUT_SEGMENT(SEGMENT_ALIAS) \
	{}
	
	#define PROFILE_OUT_ORDERED(SEGMENT_ALIAS) \
		{}

	#define PROFILE_OUT_SINGLE_EXEC_TIME(SEGMENT_ALIAS) \
	{}
	
	#define PROFILE_OUT_EXEC_TIME(SEGMENT_ALIAS_EXEC_TIME) \
	{}

	#define PROFILE_EXEC_TIME_STRING(SEGMENT_ALIAS,OUTPUT_TEXT) \
	{}

	#define PROFILE_GET_LAST_TIME(SEGMENT_ALIAS,OUTPUT_FLOAT) \
	{}
#endif


#endif //PROFILING_H_

