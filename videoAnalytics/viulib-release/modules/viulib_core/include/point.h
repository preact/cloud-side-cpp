/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */


#ifndef POINT_H
#define POINT_H
//#include "core_exp.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace viulib
{
	/** \brief Point3 <template>
	*	This class represents a point in 3d space, fully compatible with cv::Point
	*   This class provides tools for multiple conversion types
	*	<code>
	*		viulib::Point<float> p(10.0,20.0,15.0);
	*		p = cv::Point3d(0.3,10.2,14.0);
	*	</code>
	*	\ingroup viulib_core
	*/
	template <typename T>
//	class CORE_LIB Point3
	class Point3
	{	
	public:

		//! \name Constructors
		//@{
		/** \brief Point constructors
			\param _x, _y , _z [in] coordenadas en tipo T
			\param _m [in] matriz columna o fila
			\param _p [in] punto 3f (float)
			\param _p [in] punto 3d (double)
		*/
		Point3()							{ x = y = z =0;  w = 1; };
		Point3(T _x, T _y, T _z)			{ x = _x; y= _y; z = _z; w = 1;};
		Point3(const cv::Mat &_m)			{ set(_m); };
		Point3(const cv::Point3f &_p)		{ set(_p); w = 1;};
		Point3(const cv::Point3d &_p)		{ set(_p); w = 1;};
		Point3(std::vector<T> &_p)			{ set(_p); };
		

		//! \name Set Functions
		//@{
		/** \brief set a point from a Mat, cv::Point3f, cv::Point3d, std::vector
		*/
		void set(const cv::Mat &_m)
			{
				w = 1;
				if ((_m.cols==3 || _m.cols==4)&& _m.rows ==1) 
				{
					if (_m.type()==CV_64F)
					{
						x = _m.at<double>(0,0);
						y = _m.at<double>(0,1);
						z = _m.at<double>(0,2);
						if (_m.cols==4) w = _m.at<double>(0,3);
					}
					else if (_m.type()==CV_32F)
					{
						x = _m.at<float>(0,0);
						y = _m.at<float>(0,1);
						z = _m.at<float>(0,2);
						if (_m.cols==4) w = _m.at<float>(0,3);

					}
				}
				else if (_m.cols == 1 && (_m.rows==3 || _m.rows==4))
				{
					if (_m.type()==CV_64F)
					{
						x = _m.at<double>(0,0);
						y = _m.at<double>(1,0);
						z = _m.at<double>(2,0);
						if (_m.rows==4) w = _m.at<double>(3,0);
					}
					else if (_m.type()==CV_32F)
					{
						x = _m.at<float>(0,0);
						y = _m.at<float>(1,0);
						z = _m.at<float>(2,0);
						if (_m.rows==4) w = _m.at<float>(3,0);
					}

				}
			};
		void set(const cv::Point3f &_p)
			{
				x =_p.x;  y = _p.y; z = _p.z;
			};
		void set(const cv::Point3d &_p)
			{
				x =_p.x;  y = _p.y; z = _p.z;
			};

		void set(const std::vector<T> &_v)
			{
				if (_v.size()==3 || _v.size()==4)
				{
					x = _v[0]; y = _v[1]; z =_v[2];
					if (_v.size()==4) w = _v[3];
				}
			};

		
		//! \name Assignment Operator 
		//@{
		/** \brief Allow assignment between Mat, Point3f, Point3d, vector and Point3
			\return image size
		*/
		Point3& operator=( const cv::Point3f& _p )		{ x = _p.x; y = _p.y; z = _p.z; w = 1; return *this; };
		Point3& operator=( const cv::Point3d& _p )		{ x = _p.x; y = _p.y; z = _p.z; w = 1; return *this; };
		Point3& operator=( const cv::Mat& _m )			{ set(_m);  return *this; };
		Point3& operator=( const std::vector<T>& _v )	{ set(_v);  return *this; };

		//! \name Mathemathical Operators
		//@{
		/** \brief Allow operations between Point3 objects
			\return image size
		*/
		Point3 operator+( const Point3<T>& _p )	{Point3<T> p; p.x = x + _p.x; p.y = y + _p.y; p.z = z + _p.z;  return p; };
		Point3 operator-( const Point3<T>& _p )	{Point3<T> p; p.x = x - _p.x; p.y = y - _p.y; p.z = z - _p.z;  return p; };
		Point3 operator*( const Point3<T>& _p )	{Point3<T> p; p.x = x * _p.x; p.y = y * _p.y; p.z = z * _p.z;  return p; };
		Point3 operator/( const Point3<T>& _p )	{Point3<T> p; if (_p.x!=0)  p.x = x / _p.x; 
													  if (_p.y!=0) p.y = y / _p.y; 
													  if (_p.z!=0) p.z = z / _p.z;  
													  return p; };
		//! \name Casting operatoors
		//@{
		/** \brief Allow casting  between Point3 and Mat, Point3d, Point3f and Vector<T>
			\return image size
		*/
		operator cv::Mat(){return toMat();};
		operator cv::Point3f(){return cv::Point3f((float)x,(float)y,(float)z);};
		operator cv::Point3d(){return cv::Point3d((double)x,(double)y,(double)z);};
		operator std::vector<T>(){return toVector();};
		operator Point3<double>(){double x1=x,y1=y,z1=z;return Point3<double>(x1,y1,z1);};
		operator Point3<float>(){float x1=x,y1=y,z1=z; return Point3<float>(x1,y1,z1);};
		operator Point3<int>(){int x1=x,y1=y,z1=z; return Point3<int>(x1,y1,z1);};
		operator Point3<short>(){short x1=x,y1=y,z1=z; return Point3<short>(x1,y1,z1);};

		//! \name Convert functions
		//@{
		/** \brief Allow casting  between Point3 and Mat, Point3d, Point3f and Vector<T>
			\return image size
		*/
		
		/** \brief Convert to cv::Mat which type is type, in homogeneous (4x1 or 1x4) or inhomogeneous (3x1 or 1x3) and with colum representation (3x1) or row representation (1x3)
			\return the cv::Mat  with the point
		*/
		cv::Mat			toMat(int type = -1,bool homogeneous=false, bool column = true)
			{
					int tipo = cv::Mat_<T>().type();
					cv::Mat point;
					if (column)
					{
						if (homogeneous)
						{
							if (type == -1)
							{
								point.create(4,1,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(1,0) = y;
								point.at<T>(2,0) = z;
								point.at<T>(3,0) = w;
							}
							else
							{
								point.create(4,1,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = (double)x;
									point.at<double>(1,0) = (double)y;
									point.at<double>(2,0) = (double)z;
									point.at<double>(3,0) = (double)w;
								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = (float)x;
									point.at<float>(1,0) = (float)y;
									point.at<float>(2,0) = (float)z;
									point.at<float>(3,0) = (float)w;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = (int)x;
									point.at<int>(1,0) = (int)y;
									point.at<int>(2,0) = (int)z;
									point.at<int>(3,0) = (int)w;

								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = (uchar)x;
									point.at<uchar>(1,0) = (uchar)y;
									point.at<uchar>(2,0) = (uchar)z;
									point.at<uchar>(3,0) = (uchar)w;
								}
							}
						}
						else 
						{
							if (type ==-1)
							{
								point.create(3,1,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(1,0) = y;
								point.at<T>(2,0) = z;
							}
							else 
							{
								point.create(3,1,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = (double)x;
									point.at<double>(1,0) = (double)y;
									point.at<double>(2,0) = (double)z;
								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = (float)x;
									point.at<float>(1,0) = (float)y;
									point.at<float>(2,0) = (float)z;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = (int)x;
									point.at<int>(1,0) = (int)y;
									point.at<int>(2,0) = (int)z;
								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = (uchar)x;
									point.at<uchar>(1,0) = (uchar)y;
									point.at<uchar>(2,0) = (uchar)z;
								}
							}
						}
					}
					else 
					{
						if (homogeneous)
						{
							if (type==-1)
							{
								point.create(1,4,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(0,1) = y;
								point.at<T>(0,2) = z;
								point.at<T>(0,3) = w;
							}
							else
							{
								point.create(1,4,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = (double)x;
									point.at<double>(0,1) = (double)y;
									point.at<double>(0,2) = (double)z;
									point.at<double>(0,3) = (double)w;

								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = (float)x;
									point.at<float>(0,1) = (float)y;
									point.at<float>(0,2) = (float)z;
									point.at<float>(0,3) = (float)w;

								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = (int)x;
									point.at<int>(0,1) = (int)y;
									point.at<int>(0,2) = (int)z;
									point.at<int>(0,3) = (int)w;
								}
								else  if (type == CV_8U)
								{
									point.at<uchar>(0,0) = (uchar)x;
									point.at<uchar>(0,1) = (uchar)y;
									point.at<uchar>(0,2) = (uchar)z;
									point.at<uchar>(0,3) = (uchar)w;
								}

							}
						}
						else 
						{
							if (type==-1)
							{
								point.create(1,3,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(0,1) = y;
								point.at<T>(0,2) = z;
							}
							else
							{
								point.create(1,3,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = (double)x;
									point.at<double>(0,1) = (double)y;
									point.at<double>(0,2) = (double)z;
								}
								else if (type==CV_32F)
								{
									point.at<float>(0,0) = (float)x;
									point.at<float>(0,1) = (float)y;
									point.at<float>(0,2) = (float)z;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = (int)x;
									point.at<int>(0,1) = (int)y;
									point.at<int>(0,2) = (int)z;
								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = (uchar)x;
									point.at<uchar>(0,1) = (uchar)y;
									point.at<uchar>(0,2) = (uchar)z;
								}


							}
						}
					}
					return point;

			};
		std::vector<T>	toVector(bool homogeneous=false)
			{
				std::vector<T> v;
				if (homogeneous)
				{
					v.resize(4);
					v[0] = x;
					v[1] = y;
					v[2] = z;
					v[3] = w;
					return v;

				}
				else
				{
					v.resize(3);
					v[0] = x;
					v[1] = y;
					v[2] = z;
					return v;

				}
			};
		cv::Point3f		toPoint3f()		{return cv::Point3f(x,y,z);};
		cv::Point3d		toPoint3d()		{return cv::Point3d(x,y,z);};
		
		
		//! \name Information functions
		//@{
		/** \brief returns the type of the data of this Point3 object in opencv format
			\return CV_FORMAT
		*/
		int type() {return cv::Mat_<T>().type();}

		/** \brief calculates the module of the vector between (0,0,0) and this point
			\return module
		*/
		double module() { return sqrt( x * x +  y * y +  z * z);};

		/** \brief translate the point adding the vector (0,0,0)-_p
			\return 
		*/
		void translate(Point3 _p) {  x +=_p.x; y+=_p.y; z+=_p.z;};

		/** \brief Function that calculates the unit vector 
			\return the unit vector
		*/
		Point3 unit() { T mod =module(); return Point3(x = x / mod, y = y / mod, z= z / mod);};

		/** \brief Function that calculates the dot product between (0,0,0)-this  and (0,0,0)-_p vectors
			\return dot product
		*/
		double dot(Point3 _p){return x * _p.x + y * _p.y + z*_p.z;};
		
		/** \brief Function that calculates the cross vector 
			\return the cross vector
		*/
		Point3 cross(Point3 _p){ return Point3(_p.z*y-z*_p.y, _p.x*z-x*_p.z, _p.y*x-y*_p.x); }

		/** \brief Function that calculates euclidean distance between two points
			\return the cross vector
		*/
		double euclidean (Point3 &_p)
		{
			return sqrt((double)((x-_p.x)*(x-_p.x) + (y-_p.y)*(y-_p.y) + (z-_p.z)*(z-_p.z)));
		};

		T x;
		T y;
		T z;
		T w;
		

	};


	/** \brief Point2 <template>
	*	This class represents a point in a plane, fully compatible with cv::Point
	*   This class provides tools for multiple conversion types
	*	<code>
	*		viulib::Point<float> p(10.0,20.0);
	*		p = cv::Point2d(0.3,10.2);
	*	</code>
	 *	\ingroup viulib_core
	*/
	template <typename T>
	class Point2
	{	
	public:

		//! \name Constructors
		//@{
		/** \brief Point constructors
			\param _x, _y , _z [in] coordenadas en tipo T
			\param _m [in] matriz columna o fila
			\param _p [in] punto 3f (float)
			\param _p [in] punto 3d (double)
		*/
		Point2()							          { x = y =0;  w = 1; };
		Point2(T _x, T _y)					    { x = _x; y= _y;  w = 1;};
		Point2(const cv::Mat &_m)			  { set(_m); };
		Point2(const cv::Point2f &_p)		{ set(_p); w = 1;};
		Point2(const cv::Point2d &_p)		{ set(_p); w = 1;};
		Point2(const cv::Point &_p)			{ set(_p); w = 1;};
		Point2(std::vector<T> &_p)			{ set(_p); };
		

		//! \name Set Functions
		//@{
		/** \brief set a point from a Mat, cv::Point3f, cv::Point3d, std::vector
		*/
		void set(const cv::Mat &_m)
			{
				w = 1;
				if ((_m.cols==2 || _m.cols==3)&& _m.rows ==1) 
				{
					if (_m.type()==CV_64F)
					{
						x = _m.at<double>(0,0);
						y = _m.at<double>(0,1);
						if (_m.cols==3) w = _m.at<double>(0,2);
					}
					else if (_m.type()==CV_32F)
					{
						x = _m.at<float>(0,0);
						y = _m.at<float>(0,1);
						if (_m.cols==3) w = _m.at<float>(0,2);

					}
				}
				else if (_m.cols == 1 && (_m.rows==2 || _m.rows==3))
				{
					if (_m.type()==CV_64F)
					{
						x = _m.at<double>(0,0);
						y = _m.at<double>(1,0);
						if (_m.rows==3) w = _m.at<double>(2,0);
					}
					else if (_m.type()==CV_32F)
					{
						x = _m.at<float>(0,0);
						y = _m.at<float>(1,0);
						if (_m.rows==3) w = _m.at<float>(2,0);
					}

				}
			};
		void set(const cv::Point &_p)
		{
			x =_p.x;  y = _p.y;
		};

		void set(const cv::Point2f &_p)
			{
				x =_p.x;  y = _p.y;
			};
		void set(const cv::Point2d &_p)
			{
				x =_p.x;  y = _p.y;
			};

		void set(const std::vector<T> &_v)
			{
				if (_v.size()==2 || _v.size()==3)
				{
					x = _v[0]; y = _v[1]; 
					if (_v.size()==3) w = _v[2];
				}
			};

		
		//! \name Assignment Operator 
		//@{
		/** \brief Allow assignment between Mat, Point3f, Point3d, vector and Point3
			\return image size
		*/
		Point2<T>& operator=( const Point2<T>& _p )			{ x = _p.x; y = _p.y;  w = 1; return *this; };
		Point2<T>& operator=( const cv::Point2f& _p )		{ x = _p.x; y = _p.y;  w = 1; return *this; };
		Point2<T>& operator=( const cv::Point2d& _p )		{ x = _p.x; y = _p.y;  w = 1; return *this; };
		Point2<T>& operator=( const cv::Point& _p )			{ x = _p.x; y = _p.y;  w = 1; return *this; };
		Point2<T>& operator=( const cv::Mat& _m )			{ set(_m);  return *this; };
		Point2<T>& operator=( const std::vector<T>& _v )	{ set(_v);  return *this; };

		//! \name Mathemathical Operators
		//@{
		/** \brief Allow operations between Point3 objects
			\return image size
		*/
		Point2<T> operator+( const Point2<T>& _p )	{Point2<T> p; p.x = x + _p.x; p.y = y + _p.y;   return p; };
		Point2<T> operator-( const Point2<T>& _p )	{ Point2<T> p; p.x = x - _p.x; p.y = y - _p.y;   return p; };
		Point2<T> operator*( const Point2<T>& _p )	{ Point2<T> p; p.x = x * _p.x; p.y = y * _p.y;   return p; };
		Point2<T> operator/( const Point2<T>& _p )	{ Point2<T> p; if (_p.x!=0)  p.x = x / _p.x; else x=0;
													  if (_p.y!=0) p.y = y / _p.y; else y =0;
													  return p; };
		//! \name Casting operatoors
		//@{
		/** \brief Allow casting  between Point3 and Mat, Point3d, Point3f and Vector<T>
			\return image size
		*/
		operator cv::Mat(){return toMat();};
		operator cv::Point2f(){return cv::Point2f(x,y);};
		operator cv::Point2d(){return cv::Point2d(x,y);};
		operator cv::Point_<int>(){ int x1=(int)x;int y1=(int)y; return cv::Point2i(x1,y1);};
		
		operator std::vector<T>(){return toVector();};
		operator Point2<double>(){double x1=(double) x, y1= (double)y;return Point2<double>(x1,y1);};
		operator Point2<float>(){float x1=(float) x, y1=(float) y; return Point2<float>(x1,y1);};
		operator Point2<int>(){int x1=(int) x, y1=(int) y; return Point2<int>(x1,y1);};
		operator Point2<short>(){short x1=(short) x, y1=(short) y; return Point2<short>(x1,y1);};

		//! \name Convert functions
		//@{
		/** \brief Allow casting  between Point3 and Mat, Point3d, Point3f and Vector<T>
			\return image size
		*/
		
		/** \brief Convert to cv::Mat which type is type, in homogeneous (4x1 or 1x4) or inhomogeneous (3x1 or 1x3) and with colum representation (3x1) or row representation (1x3)
			\return the cv::Mat  with the point
		*/
		cv::Mat			toMat(int type = -1,bool homogeneous=false, bool column = true)
			{
					int tipo = cv::Mat_<T>().type();
					cv::Mat point;
					int ncols, nrows;
					if (column)
					{
						if (homogeneous)
						{
							if (type == -1)
							{
								point.create(3,1,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(1,0) = y;
								point.at<T>(2,0) = w;
							}
							else
							{
								point.create(3,1,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = x;
									point.at<double>(1,0) = y;
									point.at<double>(2,0) = w;
								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = x;
									point.at<float>(1,0) = y;
									point.at<float>(2,0) = w;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = x;
									point.at<int>(1,0) = y;
									point.at<int>(2,0) = w;

								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = x;
									point.at<uchar>(1,0) = y;
									point.at<uchar>(2,0) = w;
								}
							}
						}
						else 
						{
							if (type ==-1)
							{
								point.create(2,1,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(1,0) = y;
							}
							else 
							{
								point.create(2,1,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = x;
									point.at<double>(1,0) = y;
								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = x;
									point.at<float>(1,0) = y;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = x;
									point.at<int>(1,0) = y;
								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = x;
									point.at<uchar>(1,0) = y;
								}
							}
						}
					}
					else 
					{
						if (homogeneous)
						{
							if (type==-1)
							{
								point.create(1,3,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(0,1) = y;
								point.at<T>(0,2) = w;
							}
							else
							{
								point.create(1,3,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = x;
									point.at<double>(0,1) = y;
									point.at<double>(0,2) = w;

								}
								else if (type == CV_32F)
								{
									point.at<float>(0,0) = x;
									point.at<float>(0,1) = y;
									point.at<float>(0,2) = w;

								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = x;
									point.at<int>(0,1) = y;
									point.at<int>(0,2) = w;
								}
								else  if (type == CV_8U)
								{
									point.at<uchar>(0,0) = x;
									point.at<uchar>(0,1) = y;
									point.at<uchar>(0,2) = w;
								}

							}
						}
						else 
						{
							if (type==-1)
							{
								point.create(1,2,tipo);
								point.at<T>(0,0) = x;
								point.at<T>(0,1) = y;
							}
							else
							{
								point.create(1,2,type);
								if (type == CV_64F)
								{
									point.at<double>(0,0) = x;
									point.at<double>(0,1) = y;
								}
								else if (type==CV_32F)
								{
									point.at<float>(0,0) = x;
									point.at<float>(0,1) = y;
								}
								else if (type == CV_32S)
								{
									point.at<int>(0,0) = x;
									point.at<int>(0,1) = y;
								}
								else if (type == CV_8U)
								{
									point.at<uchar>(0,0) = x;
									point.at<uchar>(0,1) = y;
								}


							}
						}
					}
					return point;

			};
		std::vector<T>	toVector(bool homogeneous=false)
			{
				std::vector<T> v;
				if (homogeneous)
				{
					v.resize(3);
					v[0] = x;
					v[1] = y;
					v[3] = w;
					return v;

				}
				else
				{
					v.resize(2);
					v[0] = x;
					v[1] = y;
					return v;

				}
			};
		cv::Point2f		toPoint2f()		{return cv::Point2f(x,y);};
		cv::Point2d		toPoint2d()		{return cv::Point2d(x,y);};
		
		
		//! \name Information functions
		//@{
		/** \brief returns the type of the data of this Point3 object in opencv format
			\return CV_FORMAT
		*/
		int type() {return cv::Mat_<T>().type();}

		/** \brief calculates the module of the vector between (0,0,0) and this point
			\return module
		*/
		double module() { return sqrt( (double)(x * x +  y * y ));};

		/** \brief translate the point adding the vector (0,0,0)-_p
			\return 
		*/
		void translate(Point2 _p) {  x +=_p.x; y+=_p.y; };

		/** \brief Function that calculates the unit vector 
			\return the unit vector
		*/
		Point2 unit() { double mod =module(); return Point2(x = x / mod, y = y / mod );};

		/** \brief Function that calculates the dot product between (0,0,0)-this  and (0,0,0)-_p vectors
			\return dot product
		*/
		double dot(Point2 _p){return x * _p.x + y * _p.y ;};
		
		/** \brief Function that calculates the cross vector 
			\return the cross vector
		*/
		Point3<T> cross(Point2 &_p){ return Point3<T>(0,0,_p.y*x - y*_p.x); }

		/** \brief Function that calculates euclidean distance between two points
			\return the cross vector
		*/
		double euclidean (Point2 &_p)
		{
			return sqrt((double)((x-_p.x)*(x-_p.x) + (y-_p.y)*(y-_p.y)));
		};

		T x;
		T y;
		T w;
		

	};

	
}
#endif