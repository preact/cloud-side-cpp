/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

#ifndef SAMPLING_H
#define SAMPLING_H

#include "rect.h"

namespace viulib
{
	
	/** \brief Function for naive sampling
	*	\ingroup viulib_core
	*/
	std::vector<viulib::Rect<int> >  sampling_naive(cv::Size _img_size, int _win_cols,int _win_rows,cv::Point _step)
	{
		// Format into Detection
		std::vector<viulib::Rect<int> > samples;
		
		for (int i=0;i< _img_size.height; i+=_step.y)
		{
			for (int j=0;j< _img_size.width; j+=_step.x)
			{
				Rect<int> r;
				r.x = j;
				r.y = i;
				r.width= _win_cols;
				r.height = _win_rows;
	
				if (r.x + r.width >=_img_size.width) continue;
				if (r.y + r.height >= _img_size.height) continue;
				samples.push_back(r);
			}
		}	
		return samples;
	};
	/** \brief Function for naive rel sampling
	*	\ingroup viulib_core
	*/
	std::vector<viulib::Rect<int> >  sampling_naive_rel(cv::Size _img_size, double _win_cols,double _win_rows,cv::Point_<float> _step)
	{

		return	sampling_naive(_img_size, (int)(_img_size.width * _win_cols),_img_size.height * _win_rows,cv::Point(_img_size.width* _step.x,_img_size.height* _step.y));
	};


}
#endif