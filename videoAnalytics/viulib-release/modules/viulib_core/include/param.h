/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

// author: pleskovsky@vicomtech.org

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#pragma warning( disable:4800 ) // 'const XXXX' : forcing value to bool 'true' or 'false' (performance warning)
#endif

#ifndef PARAM_H
#define PARAM_H

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>  // for cv::BRISK

#include <string>
#include <sstream>

#include "point.h"
#include "extended_mat.h"

// test explicit instatiation
//namespace viulib{
//	  template<> class Point2<short>;
//	  template<> class Point2<int>;
//	  template<> class Point2<float>;
//	  template<> class Point2<double>;
//}

///
/// basic idea: 
///
/// class ParamBase{
///  virtual operator T() { 
///     if (inner) return inner->operator T();
///     return T(); // default 
///  } 
///  ParamBase* inner;
/// }
///
/// class ParamSpecification<T>:public ParamBase{
///  virtual operator T() { return value; }
///  T value;
/// }
///
/// Note that templates can not be virtual, therefore we use macros to create the necessary variations for known types.
///
/// USE: the class ParamBase keeps pointer to any inner Parameter specification. The returned inner Parameter instances 
/// must be created dynamically, to not to lose any temporary inner data (i.e. the value).
///

// casting operator T() .. returns copy of the stored data;
#define DEFAULT_CAST(TYPE) \
   virtual operator TYPE() const {	\
      if (inner) return inner->operator TYPE(); \
      return TYPE(); \
	}

// casting operator T*() .. returns pointer to  data and const data
#define DEFAULT_PTR_CAST(TYPE) \
   virtual operator TYPE*() const {	\
      if (inner) return inner->operator TYPE*(); \
      return NULL; \
	} \
   virtual operator const TYPE*() const  {	\
      if (inner) return inner->operator const TYPE*(); \
      return NULL; \
	}    

// casting operator const T*() .. returns pointer to const data
/*#define DEFAULT_PTR_CONST_CAST(TYPE) \
   virtual operator const TYPE*() const  {	\
      if (inner) return inner->operator const TYPE*(); \
      return NULL; \
	}    
   */

// PARAM SPECIFICATIONs for fully defined types 
// - temporary parameters allowed
// Param, ConstParam and TempParam
#define PARAM_CLASS_SPECIFICATION(TYPE) \
      \
	   template < >\
		   class Param_< TYPE >: public ParamBase\
		   {\
		   public:\
			   Param_(TYPE& v): value(&v) { \
			   } \
			   Param_(TYPE* v): value(v) { \
			   } \
		   \
         virtual operator TYPE() const {	return *value; } \
         virtual operator TYPE*() const {	return value; } \
         virtual operator const TYPE*() const {	return value; } \
		   \
		   private: \
			   TYPE* value; \
		   };\
      \
		template < > \
         class ConstParam_<TYPE>: public ParamBase\
         {\
         public:\
            ConstParam_(const TYPE& v): value(&v) { \
            } \
            ConstParam_(const TYPE* v): value(v) { \
            } \
			\
         virtual operator TYPE() const { return *value; } \
         virtual operator TYPE*() const {	return const_cast<TYPE*>(value); } \
         virtual operator const TYPE*() const {	return value; } \
			\
         private:\
              const TYPE* value;\
         };\
      \
	   template < >\
		   class TempParam_< TYPE >: public ParamBase\
		   {\
		   public:\
			   TempParam_(const TYPE& v): value(v) { \
			   } \
		   \
         virtual operator TYPE() const {	return value; } \
	      \
		   private:\
			   const TYPE value;\
		   };

#define PARAM_CLASS_SPECIFICATION_2(TYPE,TYPE2) \
      \
	   template < >\
		   class Param_< TYPE >: public ParamBase\
		   {\
		   public:\
			   Param_(TYPE& v): value(&v) { \
			   } \
			   Param_(TYPE* v): value(v) { \
			   } \
		   \
         virtual operator TYPE() const {	return *value; } \
         virtual operator TYPE*() const {	return value; } \
         virtual operator const TYPE*() const {	return value; } \
         virtual operator TYPE2() const {	return TYPE2(*value); } \
         virtual operator TYPE2*() const {	return (TYPE2*)value; } \
         virtual operator const TYPE2*() const {	return (TYPE2*)value; } \
		   \
		   private: \
			   TYPE* value; \
		   };\
      \
		template < > \
         class ConstParam_<TYPE>: public ParamBase\
         {\
         public:\
            ConstParam_(const TYPE& v): value(&v) { \
            } \
            ConstParam_(const TYPE* v): value(v) { \
            } \
			\
         virtual operator TYPE() const { return *value; } \
         virtual operator TYPE*() const {	return const_cast<TYPE*>(value); } \
         virtual operator const TYPE*() const {	return value; } \
         virtual operator TYPE2() const {	return TYPE2(*value); } \
         virtual operator TYPE2*() const {	return (TYPE2*)const_cast<TYPE*>(value); } \
         virtual operator const TYPE2*() const {	return (TYPE2*)value; } \
			\
         private:\
            const TYPE* value;\
         };\
      \
	   template < >\
		   class TempParam_< TYPE >: public ParamBase\
		   {\
		   public:\
			   TempParam_(const TYPE& v): value(v) { \
			   } \
		   \
         virtual operator TYPE() const {	return value; } \
         virtual operator TYPE2() const {	return TYPE2(value); } \
	      \
		   private:\
			   const TYPE value;\
		   };

// PARAM SPECIFICATIONs for pointer types only 
// - forward declarated classes with unknown interface and abstract classes
// - cannot return by instance
// Param and ConstParam
#define PARAM_CLASS_SPECIFICATION_PTR(TYPE) \
      \
	   template < >\
		   class Param_< TYPE >: public ParamBase\
		   {\
		   public:\
			   Param_(TYPE& v): value(&v) { \
			   } \
			   Param_(TYPE* v): value(v) { \
			   } \
		   \
         virtual operator TYPE*() const { return value; } \
         virtual operator const TYPE*() const {	return value; } \
		   \
		   private:\
			   TYPE* value;\
		   }; \
      \
		template < > \
         class ConstParam_<TYPE>: public ParamBase\
         {\
         public:\
            ConstParam_(const TYPE& v): value(&v) { \
            } \
            ConstParam_(const TYPE* v): value(v) { \
            } \
			\
         virtual operator TYPE*() const {	return const_cast< TYPE* >(value); } \
         virtual operator const TYPE*() const {	return value; } \
			\
         private:\
            const TYPE* value;\
         }; 

/*      \
	   template < >\
		   class TempParam_< TYPE >: public ParamBase\
		   {\
		   public:\
			   TempParam_(const TYPE& v)  { \
               value = new TYPE(v); \
			   } \
		   \
         virtual operator TYPE*() const {	return value; } \
	      \
		   private:\
			   TYPE* value;\
		   };
         */

// PARAM SPECIFICATIONs for pointer types only, with 2 type conversions
#define PARAM_CLASS_SPECIFICATION_PTR_2(TYPE, TYPE2_NO_PTR) \
      \
	   template < >\
		   class Param_< TYPE >: public ParamBase\
		   {\
		   public:\
			   Param_(TYPE& v): value(&v) { \
			   } \
			   Param_(TYPE* v): value(v) { \
			   } \
		   \
         virtual operator TYPE*() const {	return const_cast< TYPE* >(value); } \
         virtual operator const TYPE*() const {	return value; } \
         virtual operator TYPE2_NO_PTR() const { return *const_cast<TYPE2_NO_PTR*>(value); } \
         virtual operator const TYPE2_NO_PTR() const {	return *(TYPE2_NO_PTR*)value; } \
		   \
		   private:\
			   TYPE* value;\
		   }; \
      \
		template < > \
         class ConstParam_<TYPE>: public ParamBase\
         {\
         public:\
            ConstParam_(const TYPE& v): value(&v) { \
            } \
            ConstParam_(const TYPE* v): value(v) { \
            } \
			\
         virtual operator TYPE*() const { return value; } \
         virtual operator const TYPE*() const {	return value; } \
         virtual operator TYPE2_NO_PTR() const { return *(TYPE2_NO_PTR*)value; } \
         virtual operator const TYPE2_NO_PTR() const { return *(TYPE2_NO_PTR*)value; } \
			\
         private:\
            const TYPE* value;\
         }; 

/*      \
	   template < >\
		   class TempParam_< TYPE >: public ParamBase\
		   {\
		   public:\
			   TempParam_(const TYPE& v)  { \
               value = new TYPE(v); \
			   } \
		   \
         virtual operator TYPE*() const {	return value; } \
         virtual operator TYPE2_NO_PTR() const {	return (TYPE2_NO_PTR) *value; } \
	      \
		   private:\
			   TYPE* value;\
		   };
         */

// FORWARD DECLARATION OF OpenCV TYPES
#ifdef CV_VERSION_EPOCH
namespace cv{
   class Feature2D;
   class FeatureDetector;
   class DescriptorExtractor;
   class DescriptorMatcher;
}
#endif

namespace viulib
{
	// following shortcuts are provided for linux 
	
	typedef signed char s_char;
	typedef signed short s_short;
	typedef long int l_int;
	typedef unsigned long int u_l_int;
	typedef long long int l_l_int;
	typedef long double l_double;
	#ifdef WIN32

	  typedef unsigned char u_char;
	#ifndef u_short
	  typedef unsigned short u_short;
	#endif
	#ifndef u_int
	  typedef unsigned int u_int;
	#endif
	#endif
	
	// when defining new type conversion do 1 (of 3):
	//#include "MyNewClass.h"; // if interface is simple, or:
	//class MyNewClass; // forward declaration

	// Forward declarations:

	//class ExtendedMat;
	namespace dtc{
		class LabelStringMapper;
		class Detector;
		class DetectionFunctor;		
		class AssociationFunction;
		struct LinearGaussianDynamics;
		struct StateVectorDimensions;
		class Track;
		class Classifier;
	}
	using dtc::LabelStringMapper;
	using dtc::Detector;
	using dtc::DetectionFunctor;
	using dtc::AssociationFunction;
	using dtc::LinearGaussianDynamics;	
	using dtc::StateVectorDimensions;
	using dtc::Track;
	using dtc::Classifier;

	namespace calibration{
		class Camera; 
		class Plane;
	}
	using calibration::Camera;
	using calibration::Plane;

	namespace vas{
		class Rule;		
	}
	using vas::Rule;

	namespace videoContentDescription{
		class Object;
		class ObjectData;
		class VideoContentDescription;
	}
	using viulib::videoContentDescription::Object;
	using viulib::videoContentDescription::ObjectData;
	using viulib::videoContentDescription::VideoContentDescription;

	/** \brief Evaluate functor class
	*	\ingroup viulib_core
	*/
	class evaluate_functor
	{
	public:
		evaluate_functor();
		virtual double operator()(  cv::Rect& _rect ){return 0.0;};	
	};

	class VObject;

	/** \brief This class is used for passing arguments.
	*	\ingroup viulib
	*/
	class ParamBase {
	public:
		ParamBase ():inner(NULL) { }
		ParamBase(ParamBase* _inner):inner(_inner) { }
		virtual ~ParamBase() {
			if (inner) delete inner;
			inner = NULL;
		}
		
	      // explicit helper functions
	      std::string str() const { return operator std::string();}
	      cv::Mat mat() const { return operator cv::Mat();}
	      
	public:
		// when defining new type conversion do 2 (of 3):
		//DEFAULT_CAST(MyNewClass) ... if MyNewClass interface is known, otherwise do:
		//DEFAULT_PTR_CAST(MyNewClass) ... to allow only for passing pointer to forward declarated class

		DEFAULT_CAST(bool)
		DEFAULT_CAST(char)
		DEFAULT_CAST(s_char)
		DEFAULT_CAST(u_char)
		DEFAULT_CAST(s_short)
		DEFAULT_CAST(u_short)
		DEFAULT_CAST(int)
		DEFAULT_CAST(u_int)
		DEFAULT_CAST(l_int)
		DEFAULT_CAST(u_l_int)
		DEFAULT_CAST(l_l_int)
		DEFAULT_CAST(float)
		DEFAULT_CAST(double)
		DEFAULT_CAST(l_double)
		DEFAULT_CAST(std::string) // confuses with char* ? .. uses by 'to string' conversions

		DEFAULT_PTR_CAST(void)
		DEFAULT_PTR_CAST(bool)
		DEFAULT_PTR_CAST(char)
		DEFAULT_PTR_CAST(s_char)
		DEFAULT_PTR_CAST(u_char)
		DEFAULT_PTR_CAST(s_short)
		DEFAULT_PTR_CAST(u_short)
		DEFAULT_PTR_CAST(int)
		DEFAULT_PTR_CAST(u_int)
		DEFAULT_PTR_CAST(l_int)
		DEFAULT_PTR_CAST(u_l_int)
		DEFAULT_PTR_CAST(l_l_int)
		DEFAULT_PTR_CAST(float)
		DEFAULT_PTR_CAST(double)
		DEFAULT_PTR_CAST(l_double)


		//-- std::vectors --//
		//------------------//
		//vectors in general
		DEFAULT_CAST(std::vector<void*>)
		DEFAULT_CAST(std::vector<bool>)
		DEFAULT_CAST(std::vector<char>)
		DEFAULT_CAST(std::vector<signed char>)
		DEFAULT_CAST(std::vector<unsigned char>)
		DEFAULT_CAST(std::vector<signed short>)
		DEFAULT_CAST(std::vector<unsigned short>)
		// Used in viulib::dtc::ClassifierKNN
		DEFAULT_CAST(std::vector<int>)
		DEFAULT_CAST(std::vector<unsigned int>)
		DEFAULT_CAST(std::vector<long int>)
		DEFAULT_CAST(std::vector<unsigned long int>)
		DEFAULT_CAST(std::vector<long long int>)
		DEFAULT_CAST(std::vector<float>)
		DEFAULT_CAST(std::vector<double>)
		DEFAULT_CAST(std::vector<long double>)


		//DEFAULT_CAST(std::vector<std::string>) // confuses with std::vector<char*> ?

		// Used in viulib::dtc::ClustererGMM
		DEFAULT_CAST(std::vector< cv::Mat >)
		// Used in viulib::dtc::DetectorNWD
		DEFAULT_CAST(std::vector< std::vector<int> >)
		// Used in viulib::dtc::TrackerDAOF
		DEFAULT_CAST(std::vector< cv::KeyPoint >)
		// Used in viulib::dtc::TrackerDAOF
		DEFAULT_CAST(std::vector< cv::DMatch >)
		// Used in viulib::dtc::TrackerDAAF
		DEFAULT_CAST(std::vector< Track* >)
		// Used in viulib::dtc::ClassifierMultiGenerative
		DEFAULT_CAST(std::vector< Classifier* >)
		// Used in dtc_features
		DEFAULT_CAST(std::vector< std::vector<cv::KeyPoint> >)
		
		
		//-- Opencv types --//
		//------------------//
		DEFAULT_CAST( cv::Mat )
		DEFAULT_CAST( cv::TermCriteria )
		DEFAULT_CAST( cv::Point )
		DEFAULT_CAST( cv::Size )
		DEFAULT_CAST( cv::Rect )
		DEFAULT_CAST( cv::Vec2f )
		DEFAULT_CAST( cv::Vec3f )
		DEFAULT_CAST( cv::Point2f )
		#ifdef CV_VERSION_EPOCH
			DEFAULT_PTR_CAST( cv::Feature2D )
			DEFAULT_PTR_CAST( cv::FeatureDetector )
			DEFAULT_PTR_CAST( cv::DescriptorExtractor )
			DEFAULT_PTR_CAST( cv::DescriptorMatcher)
		#endif

		//-- Viulib types --//
		//------------------//
		DEFAULT_CAST( viulib::Point2<short> )
		DEFAULT_CAST( viulib::Point2<int> )
		DEFAULT_CAST( viulib::Point2<float> )
		DEFAULT_CAST( viulib::Point2<double> )
		DEFAULT_CAST( viulib::Point3<short> )
		DEFAULT_CAST( viulib::Point3<int> )
		DEFAULT_CAST( viulib::Point3<float> )
		DEFAULT_CAST( viulib::Point3<double> )
		DEFAULT_CAST( ExtendedMat )

		DEFAULT_PTR_CAST(  VObject )
		DEFAULT_PTR_CAST(  LabelStringMapper )
		DEFAULT_PTR_CAST(  Camera )
		DEFAULT_PTR_CAST(  Plane )
		DEFAULT_PTR_CAST(  evaluate_functor )
		DEFAULT_PTR_CAST(  Detector )
		DEFAULT_PTR_CAST(  DetectionFunctor )
		DEFAULT_PTR_CAST(  AssociationFunction )
		DEFAULT_PTR_CAST(  LinearGaussianDynamics )
		DEFAULT_PTR_CAST(  StateVectorDimensions )
		DEFAULT_PTR_CAST(  Classifier )
		DEFAULT_PTR_CAST(  Rule )
		DEFAULT_PTR_CAST(  Object )
		DEFAULT_PTR_CAST(  ObjectData )
		DEFAULT_PTR_CAST(  VideoContentDescription )


	protected:
		ParamBase* inner;
	};         


	//-- TEMPLATE PARAM_ --//
	//---------------------//
	/** \brief Template class for passing arguments.
	*	\ingroup viulib_core
	*/
	template < typename T > // inner value type 
	class Param_: public ParamBase
	{
	public:

		Param_( T& v): value(&v) { 
		} 
		Param_(T* v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ParamBase_ template with new type, 
		// for which these conversions are not defined. Use either new Param_ specialization or include the conversions to your new type class.
		//
		virtual operator bool() const { return (bool) *value; } 
		virtual operator char() const  { return (char) *value; }
		virtual operator signed char() const  {return (signed char) *value; }
		virtual operator unsigned char() const  { return (unsigned char) *value; }
		virtual operator signed short() const  { return (signed short) *value; }
		virtual operator unsigned short() const  { return (unsigned short) *value; }
		virtual operator int() const { return (int) *value; }
		virtual operator unsigned int() const { return (unsigned int) *value; }
		virtual operator long int() const { return (long int) *value; }
		virtual operator unsigned long int() const { return (unsigned long int) *value; }
		virtual operator long long int() const { return (long long int) *value; }
		virtual operator float() const { return (float) *value; }
		virtual operator double() const { return (double) *value; }
		virtual operator long double() const { return (long double) *value; }

		// TODO - here the automatic type conversion is not applied
		virtual operator void*() const { return (void*) value; }
		virtual operator bool*() const { return (bool*) value; }
		virtual operator char*() const { return (char*) value; }
		virtual operator signed char*() const { return (signed char*) value; }
		virtual operator unsigned char*() const { return (unsigned char*) value; }
		virtual operator signed short*() const { return (signed short*) value; }
		virtual operator unsigned short*() const { return (unsigned short*) value; }
		virtual operator int*() const { return (int*) value; }
		virtual operator unsigned int*() const { return (unsigned int*) value; }
		virtual operator long int*() const { return (long int*) value; }
		virtual operator unsigned long int*() const { return (unsigned long int*) value; }
		virtual operator long long int*() const { return (long long int*) value; }
		virtual operator float*() const { return (float*) value; }
		virtual operator double*() const { return (double*) value; }
		virtual operator long double*() const { return (long double*) value; }

		virtual operator std::string() const 
		{ 
			std::stringstream ss;
			ss << *value;
			return ss.str();
		}

      virtual operator const char*() const
      {	
         return operator std::string().c_str();
	   } 
      //virtual operator char*() const {	
	   //} 

	private:
		T* value; // inner value - stored by ptr
	};

	//-- TEMPLATE CONST_PARAM_ --//
	//---------------------------//
	/** \brief Template class for passing constant arguments.
	*	\ingroup viulib_core
	*/
	template < typename T >  // inner value type 
	class ConstParam_: public ParamBase
	{
	public:

		ConstParam_(const  T& v): value(&v) { 
		} 
		ConstParam_(const T* v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ConstParam_ template with new type, 
		// for which these conversions are not defined. Use either new ConstParam_ specialization or include the conversions to your new type class.
		//
		virtual operator bool() const { return (bool) *value; } 
		virtual operator char() const  { return (char) *value; }
		virtual operator signed char() const  {return (signed char) *value; }
		virtual operator unsigned char() const  { return (unsigned char) *value; }
		virtual operator signed short() const  { return (signed short) *value; }
		virtual operator unsigned short() const  { return (unsigned short) *value; }
		virtual operator int() const { return (int) *value; }
		virtual operator unsigned int() const { return (unsigned int) *value; }
		virtual operator long int() const { return (long int) *value; }
		virtual operator unsigned long int() const { return (unsigned long int) *value; }
		virtual operator long long int() const { return (long long int) *value; }
		virtual operator float() const { return (float) *value; }
		virtual operator double() const { return (double) *value; }
		virtual operator long double() const { return (long double) *value; }

		// TODO - here the automatic type conversion is not applied
		virtual operator const void*() const { return (void*) value; }
		virtual operator const bool*() const { return (bool*) value; }
		//virtual operator const char*() const { return (char*) value; }
		virtual operator const signed char*() const { return (signed char*) value; }
		virtual operator const unsigned char*() const { return (unsigned char*) value; }
		virtual operator const signed short*() const { return (signed short*) value; }
		virtual operator const unsigned short*() const { return (unsigned short*) value; }
		virtual operator const int*() const { return (int*) value; }
		virtual operator const unsigned int*() const { return (unsigned int*) value; }
		virtual operator const long int*() const { return (long int*) value; }
		virtual operator const unsigned long int*() const { return (unsigned long int*) value; }
		virtual operator const long long int*() const { return (long long int*) value; }
		virtual operator const float*() const { return (float*) value; }
		virtual operator const double*() const { return (double*) value; }
		virtual operator const long double*() const { return (long double*) value; }

		virtual operator std::string() const
		{ 
			std::stringstream ss;
			ss << *value;
			return ss.str();
		}
      virtual operator const char*() const
      {	
         return operator std::string().c_str();
	   } 
      //virtual operator char*() const {	
	   //} 

	private:
		const T* value; // inner value - stored by ptr
	};


	//-- TEMPLATE TEMP_PARAM_ --//
	//--------------------------//
	// Temp param keeps copy of inner value
	/** \brief Template class for passing temporal arguments.
	*	\ingroup viulib_core
	*/
	template < typename T > // inner value type 
	class TempParam_: public ParamBase
	{
	public:

		TempParam_(const T& v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ParamBase_ template with new type, 
		// for which these conversions are not defined. Use either new Param_ specialization or include the conversions to your new type class.
		//
		virtual operator bool() const { return (bool) value; } 
		virtual operator char() const  { return (char) value; }
		virtual operator signed char() const  {return (signed char) value; }
		virtual operator unsigned char() const  { return (unsigned char) value; }
		virtual operator signed short() const  { return (signed short) value; }
		virtual operator unsigned short() const  { return (unsigned short) value; }
		virtual operator int() const { return (int) value; }
		virtual operator unsigned int() const { return (unsigned int) value; }
		virtual operator long int() const { return (long int) value; }
		virtual operator unsigned long int() const { return (unsigned long int) value; }
		virtual operator long long int() const { return (long long int) value; }
		virtual operator float() const { return (float) value; }
		virtual operator double() const { return (double) value; }
		virtual operator long double() const { return (long double) value; }

		virtual operator std::string() const
		{ 
			std::stringstream ss;
			ss << value;
			return ss.str();
		}
      virtual operator const char*() const
      {	
         return operator std::string().c_str();
	   } 
      //virtual operator char*() const {	
	   //} 

	private:
		T value; // inner value - stored by copy
	};



   //------------------------------------------------------------------------------------------------

	//-- TEMPLATE CREATION HELPER FUNCTIONS --//
	//----------------------------------------//

	template <typename T>
	ParamBase CreateParam(T& val)
	{
		return new Param_<T>(val);
	}
	template <typename T>
	ParamBase CreateParam(T* val)
	{
		return new Param_<T>(val);
	}
	template <typename T>
	ParamBase CreateParamRef(T* val)
	{
		return new Param_<T>(val);
	}

	template <typename T>
	ParamBase CreateConstParam(const T& val)
	{
		return new ConstParam_<T>(val);
	}
	template <typename T>
	ParamBase CreateConstParam(const T* val)
	{
		return new ConstParam_<T>(val);
	}
	template <typename T>
	ParamBase CreateConstParamRef(const T* val)
	{
		return new ConstParam_<T>(val);
	}

	template <typename T>
	ParamBase CreateTempParam(const T& val)
	{
		return new TempParam_<T>(val);
	}

	static ParamBase NullParam()
	{
		return ParamBase();
	}

	//-- PARAM_ SPECIALIZATIONS --//
	//-----------------------------//
	/** \brief Specialization <void> for Param_ class
	*	\ingroup viulib_core
	*/
	template < > // inner type specialization for void*
	class Param_<void>: public ParamBase
	{
	public:
		Param_(void* v): value(v) { 
		} 

		virtual operator void*() const { return (void*) value; }

	private:
		void* value;
	};
	/** \brief Specialization <std::string> for Param_ class
	*	\ingroup viulib_core
	*/
	template < > // inner type std::string specialization
	class Param_<std::string>: public ParamBase
	{
	public:
		Param_(std::string& v): value(&v) { 
		} 
		Param_(std::string* v): value(v) { 
		} 
		virtual operator char() const { return (char) (*value)[0]; } // support only char
		virtual operator char*() const { return (char*) value->c_str(); } // todo .. confuses with operator std::string()  ?
		virtual operator const char*() const { return (char*) value->c_str(); } // todo .. confuses with operator std::string()  ?

		int getInt() const { return atoi( (*value).c_str() ); }
		double getDouble() const { return atof( (*value).c_str() ); }
		virtual operator unsigned short() const { return (unsigned short) getInt(); }
		virtual operator signed short() const { return (signed short) getInt(); }
		virtual operator int() const { return (int) getInt(); }
		virtual operator float() const { return (float) getDouble(); }
		virtual operator double() const { return (double) getDouble(); }

		virtual operator std::string() const { return *value; }

	private:
		std::string* value;
	};


	//-- CONSTPARAM_ SPECIALIZATIONS --//
	//----------------------------------//
	/** \brief Specialization <void> for ConstParam_ class
	*	\ingroup viulib_core
	*/
	template < > // inner type specialization for void*
	class ConstParam_<void>: public ParamBase
	{
	public:
		ConstParam_(const void* v): value(v) { 
		} 

		virtual operator void*() const { return (void*) value; }

	private:
		const void* value;
	};
	/** \brief Specialization <std::string> for ConstParam_ class
	*	\ingroup viulib_core
	*/
	template < > // inner type std::string specialization
	class ConstParam_<std::string>: public ParamBase
	{
	public:
		ConstParam_(const std::string& v): value(&v) { 
		} 
		ConstParam_(const std::string* v): value(v) { 
		} 
		virtual operator char() const { return (char) (*value)[0]; } // support only char
		virtual operator char*() const  { return (char*) value->c_str(); } // todo .. confuses with operator std::string()  ?
		virtual operator const char*() const  { return (char*) value->c_str(); } // todo .. confuses with operator std::string()  ?

		int getInt() const { return atoi( (*value).c_str() ); }
		double getDouble() const { return atof( (*value).c_str() ); }
		virtual operator unsigned short() const { return (unsigned short) getInt(); }
		virtual operator signed short() const { return (signed short) getInt(); }
		virtual operator int() const { return (int) getInt(); }
		virtual operator float() const { return (float) getDouble(); }
		virtual operator double() const { return (double) getDouble(); }

		virtual operator std::string() const { return *value; }

	private:
		const std::string* value;
	};


	//-- TEMPPARAM_ SPECIALIZATIONS --//
	//----------------------------------//
	/** \brief Specialization <std::string> for TempParam_ class
	*	\ingroup viulib_core
	*/
	template < > // inner type std::string specialization
	class TempParam_<std::string>: public ParamBase
	{
	public:
		TempParam_(const std::string& v): value(v) { 
		} 
		virtual operator char() const { return (char) value[0]; } // support only char
		virtual operator char*() const { return (char*) value.c_str(); } // todo don't support .. does not guarantee a copy 
		virtual operator const char*() const { return (char*) value.c_str(); } // todo don't support .. does not guarantee a copy 

		int getInt() const { return atoi( value.c_str() ); }
		double getDouble() const { return atof( value.c_str() ); }
		virtual operator unsigned short() const { return (unsigned short) getInt(); }
		virtual operator signed short() const { return (signed short) getInt(); }
		virtual operator int() const { return (int) getInt(); }
		virtual operator float() const { return (float) getDouble(); }
		virtual operator double() const { return (double) getDouble(); }

		virtual operator std::string() const { return value; }

	private:
		const std::string value;
	};

   //------------------------------------------------------------------------------------------------
	//-- TEMPLATE PARAM_  for std::vector --//
	//---------------------//  
	namespace vector_helper{
	  
	  template <typename T, typename T2> 
	  std::vector<T2> convert(const std::vector<T>& a) 
	  {
	    //if (typeid( T2 ) == typeid( T )) return a;
	    std::vector<T2> out;
	    if (a.empty()) return out;
	    out.reserve(a.size());
	    for (typename std::vector<T>::const_iterator it = a.begin(); it != a.end(); ++it)
		 out.push_back( T2(*it) );
	    return out;
	  }

	  template<typename T> // specification for the same type T2 = T
	  std::vector<T> convert(const std::vector<T>& a) 
	  { 
	    return a; 
	  }

	  template<typename T>
	  std::string str(const std::vector<T>* value)
	  {
		std::stringstream ss;
		ss << "[" ;
		for (typename std::vector<T>::const_iterator it= value->begin(); it != value->end(); )
		{
		    ss << (*it);
		    ++it;
		    if (it != value->end()) 
		      ss << ", ";
		}
		ss <<"]";
		return ss.str();
	  }
	}
	
   //template< >
	//template <template<class> typename TVEC, typename T>// inner value type 
   template <typename T>
	class Param_< std::vector<T> >: public ParamBase
	{
	public:

		Param_(std::vector<T>& v): value(&v) { 
		} 
		Param_(std::vector<T>* v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ParamBase_ template with new type, 
		// for which these conversions are not defined. Use either new Param_ specialization or include the conversions to your new type class.
		//
		virtual operator std::vector<bool>() const { return vector_helper::convert<T,bool>(*value); } 
		virtual operator std::vector<char>() const  { return vector_helper::convert<T,char>(*value); } 
		virtual operator std::vector<signed char>() const  { return vector_helper::convert<T,signed char>(*value); } 
		virtual operator std::vector<unsigned char>() const  { return vector_helper::convert<T,unsigned char>(*value); } 
		virtual operator std::vector<signed short>() const  { return vector_helper::convert<T,signed short>(*value); } 
		virtual operator std::vector<unsigned short>() const  { return vector_helper::convert<T,unsigned short>(*value); } 
		virtual operator std::vector<int>() const { return vector_helper::convert<T,int>(*value); } 
		virtual operator std::vector<unsigned int>() const { return vector_helper::convert<T,unsigned int>(*value); } 
		virtual operator std::vector<long int>() const { return vector_helper::convert<T,long int>(*value); } 
		virtual operator std::vector<unsigned long int>() const { return vector_helper::convert<T,unsigned long int>(*value); } 
		virtual operator std::vector<long long int>() const { return vector_helper::convert<T,long long int>(*value); } 
		virtual operator std::vector<float>() const { return vector_helper::convert<T,float>(*value); } 
		virtual operator std::vector<double>() const { return vector_helper::convert<T,double>(*value); } 
		virtual operator std::vector<long double>() const { return vector_helper::convert<T,long double>(*value); } 

		virtual operator std::string()  const
		{ 
		  return vector_helper::str(value);
		}
						
      virtual operator bool() const
      {
         return ((value)&& (!value->empty()));
      }

	   private:
		   std::vector<T>* value; // inner value - stored by ptr
	};
	
	
   //-- TEMPLATE CONST_PARAM_  for std::vector --//
	//---------------------------//
   // TODO get vector as ExtendedMat / as Mat

   //template< >
	//template <template<class> typename TVEC, typename T>// inner value type 
   //template <typename T, typename A>
	//class ConstParam_< std::vector< T, A > >: public ParamBase
   template <typename T>
	class ConstParam_< std::vector< T > >: public ParamBase
	{
	public:

		ConstParam_(const  std::vector<T>& v): value(&v) { 
		} 
		ConstParam_(const std::vector<T>* v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ParamBase_ template with new type, 
		// for which these conversions are not defined. Use either new Param_ specialization or include the conversions to your new type class.
		//
		virtual operator std::vector<bool>() const { return vector_helper::convert<T,bool>(*value); } 
		virtual operator std::vector<char>() const  { return vector_helper::convert<T,char>(*value); } 
		virtual operator std::vector<signed char>() const  { return vector_helper::convert<T,signed char>(*value); } 
		virtual operator std::vector<unsigned char>() const  { return vector_helper::convert<T,unsigned char>(*value); } 
		virtual operator std::vector<signed short>() const  { return vector_helper::convert<T,signed short>(*value); } 
		virtual operator std::vector<unsigned short>() const  { return vector_helper::convert<T,unsigned short>(*value); } 
		virtual operator std::vector<int>() const { return vector_helper::convert<T,int>(*value); } 
		virtual operator std::vector<unsigned int>() const { return vector_helper::convert<T,unsigned int>(*value); } 
		virtual operator std::vector<long int>() const { return vector_helper::convert<T,long int>(*value); } 
		virtual operator std::vector<unsigned long int>() const { return vector_helper::convert<T,unsigned long int>(*value); } 
		virtual operator std::vector<long long int>() const { return vector_helper::convert<T,long long int>(*value); } 
		virtual operator std::vector<float>() const { return vector_helper::convert<T,float>(*value); } 
		virtual operator std::vector<double>() const { return vector_helper::convert<T,double>(*value); } 
		virtual operator std::vector<long double>() const { return vector_helper::convert<T,long double>(*value); } 

		virtual operator std::string()  const
		{ 
		  return vector_helper::str(value);
		}

      virtual operator bool() const
      {
         return ((value)&& (!value->empty()));
      }
   
	private:
		const std::vector<T>* value; // inner value - stored by ptr
	};


	//-- TEMPLATE TEMP_PARAM_ for std::vector --//
	//--------------------------//

	// Temp param keeps copy of inner value
	template < typename T > // inner value type 
	class TempParam_ < std::vector<T> >: public ParamBase
	{
	public:

		TempParam_(const std::vector<T>& v): value(v) { 
		} 

		// outer type conversions
		// Should error on compiling appear at these functions, it means that you are using the ParamBase_ template with new type, 
		// for which these conversions are not defined. Use either new Param_ specialization or include the conversions to your new type class.
		//
		virtual operator std::vector<bool>() const { return vector_helper::convert<T,bool>(value); } 
		virtual operator std::vector<char>() const  { return vector_helper::convert<T,char>(value); } 
		virtual operator std::vector<signed char>() const  { return vector_helper::convert<T,signed char>(value); } 
		virtual operator std::vector<unsigned char>() const  { return vector_helper::convert<T,unsigned char>(value); } 
		virtual operator std::vector<signed short>() const  { return vector_helper::convert<T,signed short>(value); } 
		virtual operator std::vector<unsigned short>() const  { return vector_helper::convert<T,unsigned short>(value); } 
		virtual operator std::vector<int>() const { return vector_helper::convert<T,int>(value); } 
		virtual operator std::vector<unsigned int>() const { return vector_helper::convert<T,unsigned int>(value); } 
		virtual operator std::vector<long int>() const { return vector_helper::convert<T,long int>(value); } 
		virtual operator std::vector<unsigned long int>() const { return vector_helper::convert<T,unsigned long int>(value); } 
		virtual operator std::vector<long long int>() const { return vector_helper::convert<T,long long int>(value); } 
		virtual operator std::vector<float>() const { return vector_helper::convert<T,float>(value); } 
		virtual operator std::vector<double>() const { return vector_helper::convert<T,double>(value); } 
		virtual operator std::vector<long double>() const { return vector_helper::convert<T,long double>(value); } 

		virtual operator std::string()  const
		{ 
		  return vector_helper::str(&value);
		}
		
      virtual operator bool() const
      {
         return (!value.empty());
      }
	private:
		std::vector<T> value; // inner value - stored by copy
	};




	//-- REST OF PARAM_ CONST_PARAM and TEMPPARAM_ SPECIALIZATIONS --//
	//---------------------------------------------------------------//

	// when defining new type conversion do 3 (of 3):
	//PARAM_CLASS_SPECIFICATION(MyNewClass) ... if MyNewClass interface is known, otherwise do:
	//PARAM_CLASS_SPECIFICATION_PTR(MyNewClass) ... to allow only for passing pointer to forward declarated class

	//-- std::vector types --//
	//-----------------------//

	PARAM_CLASS_SPECIFICATION(std::vector<void*>)
	//PARAM_CLASS_SPECIFICATION(std::vector<bool>)
	//PARAM_CLASS_SPECIFICATION(std::vector<char>)
	//PARAM_CLASS_SPECIFICATION(std::vector<signed char>)
	//PARAM_CLASS_SPECIFICATION(std::vector<unsigned char>)
	//PARAM_CLASS_SPECIFICATION(std::vector<signed short>)
	//PARAM_CLASS_SPECIFICATION(std::vector<unsigned short>)
	// Used in viulib::dtc::ClassifierKNN
	//PARAM_CLASS_SPECIFICATION(std::vector<int>)
	//PARAM_CLASS_SPECIFICATION(std::vector<unsigned int>)
	//PARAM_CLASS_SPECIFICATION(std::vector<long int>)
	//PARAM_CLASS_SPECIFICATION(std::vector<unsigned long int>)
	//PARAM_CLASS_SPECIFICATION(std::vector<long long int>)
	//PARAM_CLASS_SPECIFICATION(std::vector<float>)
	//PARAM_CLASS_SPECIFICATION(std::vector<double>)
	//PARAM_CLASS_SPECIFICATION(std::vector<long double>)

	PARAM_CLASS_SPECIFICATION(std::vector<std::string>)

	PARAM_CLASS_SPECIFICATION( std::vector< cv::Mat > )
	PARAM_CLASS_SPECIFICATION( std::vector< std::vector<int> > ) 
	PARAM_CLASS_SPECIFICATION( std::vector<cv::KeyPoint> ) 
	PARAM_CLASS_SPECIFICATION( std::vector<cv::DMatch> ) 
	PARAM_CLASS_SPECIFICATION( std::vector<const Track*> ) 
	PARAM_CLASS_SPECIFICATION( std::vector<Track*> ) 
	PARAM_CLASS_SPECIFICATION( std::vector< Classifier* >)
	PARAM_CLASS_SPECIFICATION( std::vector< std::vector<cv::KeyPoint> > )


	//-- Opencv types --//
	//------------------//

	PARAM_CLASS_SPECIFICATION_2( cv::Mat, ExtendedMat )
	PARAM_CLASS_SPECIFICATION( cv::TermCriteria )
	PARAM_CLASS_SPECIFICATION( cv::Point )
	PARAM_CLASS_SPECIFICATION( cv::Size )
	PARAM_CLASS_SPECIFICATION( cv::Rect )	
	PARAM_CLASS_SPECIFICATION( cv::Vec2f )
	PARAM_CLASS_SPECIFICATION( cv::Vec3f )
	PARAM_CLASS_SPECIFICATION( cv::Point2f )
	
	#ifdef CV_VERSION_EPOCH
		PARAM_CLASS_SPECIFICATION_PTR( cv::Feature2D )
		PARAM_CLASS_SPECIFICATION_PTR( cv::FeatureDetector  )
		PARAM_CLASS_SPECIFICATION_PTR( cv::DescriptorExtractor )
		PARAM_CLASS_SPECIFICATION_PTR(	cv::DescriptorMatcher)
	#endif

	//-- Viulib types --//
	//------------------//

	PARAM_CLASS_SPECIFICATION( viulib::Point2<short> )
	PARAM_CLASS_SPECIFICATION( viulib::Point2<int> )
	PARAM_CLASS_SPECIFICATION( viulib::Point2<float> )
	PARAM_CLASS_SPECIFICATION( viulib::Point2<double> )
	PARAM_CLASS_SPECIFICATION( viulib::Point3<short> )
	PARAM_CLASS_SPECIFICATION( viulib::Point3<int> )
	PARAM_CLASS_SPECIFICATION( viulib::Point3<float> )
	PARAM_CLASS_SPECIFICATION( viulib::Point3<double> )

	PARAM_CLASS_SPECIFICATION_2( ExtendedMat, cv::Mat )

	PARAM_CLASS_SPECIFICATION_PTR( VObject )
	PARAM_CLASS_SPECIFICATION_PTR( LabelStringMapper )
	PARAM_CLASS_SPECIFICATION_PTR( Camera )
	PARAM_CLASS_SPECIFICATION_PTR( Plane )

	PARAM_CLASS_SPECIFICATION_PTR(  evaluate_functor )
	PARAM_CLASS_SPECIFICATION_PTR(  Detector )
	PARAM_CLASS_SPECIFICATION_PTR(  DetectionFunctor )
	PARAM_CLASS_SPECIFICATION_PTR(  AssociationFunction )
	PARAM_CLASS_SPECIFICATION_PTR(  LinearGaussianDynamics )
	PARAM_CLASS_SPECIFICATION_PTR(  StateVectorDimensions )
	PARAM_CLASS_SPECIFICATION_PTR(  Classifier )
	PARAM_CLASS_SPECIFICATION_PTR(	Rule )
	PARAM_CLASS_SPECIFICATION_PTR(	Object )
	PARAM_CLASS_SPECIFICATION_PTR(	ObjectData )
	PARAM_CLASS_SPECIFICATION_PTR(	VideoContentDescription )

}
using viulib::ParamBase;
#endif //PARAM_H
