/** 
* viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

#ifndef _VIDEO_CONTENT_DESCRIPTION_H_
#define _VIDEO_CONTENT_DESCRIPTION_H_

#ifdef WIN32
  #define NOMINMAX
	#include <windows.h>
	#include "conio.h"
	#include <time.h>
#endif

#ifdef linux
	#include <stdio.h>
	#include <sys/time.h>
	#include <time.h>
#endif
	
#include <stdlib.h>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <fstream>
#include <vector>
#include <map>

#include "core.h"
#include "system.h"
#include "core_exp.h" 

//#include "tinyXML/tinyxml2.h"

namespace tinyxml2
{
	class XMLElement;
};

namespace viulib 
{

namespace videoContentDescription
{
//namespace core
//{
	

	/** This is a simple metadata class that contains information about the video itself	 
	 */
	struct CORE_LIB VideoData
	{	
		std::string name;
		int num_frames;
		int height;
		int width;
		double frame_rate;
		std::string annotate;

		VideoData():name(""), num_frames(0), height(0), width(0), frame_rate(0.0), annotate("") {}
		VideoData(const std::string& _name, int _num_frames, int _height, int _width, double _frame_rate, const std::string& _annotate)
			:name(_name), num_frames(_num_frames), height(_height), width(_width), frame_rate(_frame_rate), annotate(_annotate) {}

	};

	/** This is the base class from which any object data type can be derived
	 * It has a clone() function to perform dynamic content copy	 
	 */
	class CORE_LIB ObjectData : public virtual viulib::VObject
	{
		REGISTER_BASE_CLASS(ObjectData,videoContentDescription)
		
	public:
		ObjectData():frameStart(0), frameEnd(0), description(""), annotate("") {}
		/*ObjectData(int _fs, int _fe, const std::string& _description, const std::string& _annotate)
			:frameStart(_fs), frameEnd(_fe), description(_description), annotate(_annotate) {}*/
		
		/* Clone function to copy content of the Dynamic object */
		virtual ObjectData* clone() const = 0;//{ return new ObjectData( *this ); }
		virtual void loadData( tinyxml2::XMLElement* _element ) = 0;
		virtual void saveData( tinyxml2::XMLElement* _element ) = 0;
		
		int getFrameStart() const { return frameStart; }
		int getFrameEnd() const { return frameEnd; }
		std::string getDescription() const { return description; }
		std::string getAnnotate() const { return annotate; }

		void setFrameStart(int _frameStart) { frameStart = _frameStart; }
		void setFrameEnd(int _frameEnd) { frameEnd = _frameEnd; }
		void setDescription(std::string _description) { description = _description; }
		void setAnnotate(std::string _annotate) { annotate = _annotate; }

	private:
		int frameStart;
		int frameEnd;
		std::string description;
		std::string annotate;
	};

	/** This is the class representing an object in a video description
	 * It contains functions to add more data to the object	 
	 */
	class CORE_LIB Object
	{
		friend class ObjectList;
	public:
		Object():id(-1), type(""), frameStart(0), frameEnd(0), annotate(""), objectDataType(OBJECTDATA_UNKNOWN) {}
		Object(int _id, const std::string& _type, int _frameStart, int _frameEnd, int _objectDataType, const std::string& _annotate=std::string(""))
			:id(_id), type(_type), frameStart(_frameStart), frameEnd(_frameEnd), annotate(_annotate), objectDataType(_objectDataType){}

		ObjectData* addObjectData( ObjectData* _objectData );
		ObjectData* findObjectData( const std::string& _objectType, int _frame ) const;
		void clear () 
		{
			id=-1; type.clear();frameStart=0;frameEnd=0;objectDataType=OBJECTDATA_UNKNOWN;annotate.clear(); 		               
		    objectData.clear();
		};

		enum {OBJECTDATA_UNKNOWN, OBJECTDATA_BBOX, 
			  OBJECTDATA_CIRCLE, OBJECTDATA_POINT,
			  OBJECTDATA_STATUS, OBJECTDATA_INTERVALINTEGER, 
			  OBJECTDATA_INTERVALINTEGEREXTENDED, OBJECTDATA_POLYGON };		

		const std::vector<ObjectData*>& getObjectData() const { return objectData; }
		int getObjectDataType() const { return objectDataType; }
		int getID() const { return id; }		
		std::string getType() const { return type; }
		int getFrameStart() const { return frameStart; }
		int getFrameEnd() const { return frameEnd; }
		std::string getAnnotate() const { return annotate; }

	private:		
		void setID( int _id ) { id = _id; }

		std::vector<ObjectData*> objectData;

		int id;
		std::string type;
		int frameStart;
		int frameEnd;
		int objectDataType;
		std::string annotate;

	};

	/** This class represents the list of objects
	 * It contains a vector of Objects	
	 */
	class CORE_LIB ObjectList
	{
	public:
		ObjectList(): annotate(""), nextIDToAssign(0){}

		void addObject( const Object& _object );
		void rmObject( int _id );
		void updateObject( int _id, const Object& _object )
		{
			// So, we have an incoming Object and we have to substitute
			// the content of the existing Object with id _id
			bool found = false;
			for( std::size_t i=0; i<objects.size(); ++i )
			{
				if( objects[i].getID() == _id )
				{
					objects[i] = _object;
					objects[i].setID( _id ); // Just in case the provided Object does not have the same ID
					found = true;
					break;
				}
			}
			if( !found )
				addObject( _object );
		}

		void rmAllObjects()
		{
			objects.clear();
			objectTypes.clear();
		}
		void clear()
		{
			rmAllObjects();
			nextIDToAssign = 0;
			annotate = std::string("");
		}

		std::vector<int> rmAllObjectsOlderThan( int _frameNum );

		void addObjectData( int _id, ObjectData* _objectData )
		{
			if( _id >= 0 && _id < static_cast<int>( objects.size() ) )
				objects[_id].addObjectData( _objectData );
		}

		std::vector<ObjectData*> findObjectData( const std::string& _objectType, int _frame ) const;

		int getNumObjects() const { return static_cast<int>( objects.size() ); }
		std::vector<Object> getOneTypeObjectList(std::string _type)  
		{ 
			std::vector<Object> olist;
			for (size_t i=0;i<objects.size();i++)
			{
				if (!objects[i].getType().compare(_type))
					olist.push_back(objects[i]);
			}

			return olist; 
		}
		std::vector<std::string> getObjectTypes() const { return objectTypes; }
		const std::vector<Object>& getObjects() const { return objects; }

		int getLastAssignedID() const { return nextIDToAssign - 1; }

	private:
		std::vector<Object> objects;
		std::vector<std::string> objectTypes;
		std::string annotate;

		int nextIDToAssign;
	};

	/** \brief This is the class that describe an event (from a semantic point of view)
	 * It only contains data
	 * \ingroup viulib_core
	 */
	class CORE_LIB Event
	{	
		friend class EventList;
	public:
		Event():id(-1), type(""), frameStart(0), frameEnd(0), annotate(""){}
		Event(int _id, const std::string& _type, int _fs, int _fe, const std::string& _annotate=std::string(""))
			:id(_id), type(_type), frameStart(_fs), frameEnd(_fe), annotate(_annotate){}
		virtual ~Event(){};

		void reset()
		{
			id = -1;
			type = std::string("");
			frameStart = 0;
			frameEnd = 0;		
			annotate = std::string("");
		}

		bool isID( int _id ) const { return (_id==id); }
		int getID() const { return id; }
		std::string getType() const { return type; }
		int getFrameStart() const { return frameStart; }
		int getFrameEnd() const { return frameEnd; }
				
		std::string getAnnotate() const { return annotate; }

	private:
		void setID( int _id ) { id = _id; }
		void setFrameEnd( int _frameEnd ) { frameEnd = _frameEnd; }		
		
		int id;
		std::string type;
		int frameStart;
		int frameEnd;		
		std::string annotate;	
			
	};

	/** \brief This class represents the container of all events in a single video content descriptor
	 * It does contain a vector of Events
	 * \ingroup viulib_core
	 */
	class CORE_LIB EventList
	{
	public:
		EventList():annotate(""), nextIDToAssign(0){}
		EventList(const std::string& _annotate):annotate(_annotate){}

		void addEvent( const Event& _event );
		void rmEvent( int _id );
		void updateEvent( int _id, int _frameEnd );

		void rmAllEvents() 
		{ 
			events.clear();
			eventTypes.clear();			
		}
		void clear() 
		{
			rmAllEvents();
			nextIDToAssign = 0;			
			annotate = std::string("");
		}

		std::vector<int> rmAllEventsOlderThan( int _frameNum );

		std::vector<Event> findEvents( const std::string& _eventType, int _frame ) const;
		const std::vector<Event>& getEvents() const { return events; }
		std::vector<std::string> getEventTypes() const { return eventTypes; }
		int getNumEvents() const { return static_cast<int>( events.size() ); }			
		int getLastAssignedID() const { return nextIDToAssign - 1; }

		const Event& getEvent( int _ID ) const 
		{
			std::map<int, std::size_t>::const_iterator it = IDtoIdx.find(_ID);
			return events[ (*it).second ];
		}
		
		//int getEventIdx(int _ID) const
		//{ 
		//	std::map<int, std::size_t>::const_iterator it = IDtoIdx.find(_ID);
		//	if( it != IDtoIdx.end() )
		//	{
		//		std::size_t a = (*it).second;			
		//		return a;
		//	}
		//	else
		//		return -1;
		//}

	private:
		std::vector<Event> events;
		std::vector<std::string> eventTypes;
		std::string annotate;

		std::map<int, std::size_t> IDtoIdx;

		int nextIDToAssign;
	};

	/** This class can create Relations between Objects and Events:
	 *
	 * Possible relations:
	 *	- Object "contains" {Objects} - "CONTAINER"
	 *	- Object "participates in" {Events} -"ACTOR"
	 *	- Event "composed by" {Events} - "COMPOSITION"
	 *	- Event "has participants" {Objects} - "ACTION"
	*/
	class CORE_LIB Relation
	{
	public:
		Relation():id(-1), type(""){}
		Relation(std::string _type):id(-1)
		{
			setType( _type );
		}
		Relation( std::string _type, int _lhID, const std::vector<int>& _rhIDVector ):id(-1), lhID(_lhID), rhIDVector(_rhIDVector)
		{
			setType( _type );			
		}
		Relation( std::string _type, int _lhID, int _rhID ):id(-1), lhID(_lhID)
		{
			setType( _type );
			rhIDVector.push_back( _rhID );
		}

		void setLHID( int _ID ) { lhID = _ID; }
		void addRHID( int _ID ) { rhIDVector.push_back( _ID ); }
		void setRHID( const std::vector<int>& _IDVector ) { rhIDVector = _IDVector; }

		int getLHID() const { return lhID; }
		int getRHID() const 
		{
			if( rhIDVector.size() >= 1 )
				return rhIDVector[0];
			else
			{
				viulib::system::error("Relation::getRHID", "rhIDVector is empty.");
				return -1;
			}
		}
		std::vector<int> getRHIDVector() const { return rhIDVector; }

		void setID( int _id ) { id = _id; }
		int getID() const { return id; }

		void setType(std::string _type)
		{
			if( !_type.compare("CONTAINER") || !_type.compare("ACTOR") 
				|| !_type.compare("COMPOSITION") || !_type.compare("ACTION") )
				type = _type;
			else
			{
				viulib::system::error("Relation::Relation", "ERROR: unrecognized type. Use CONTAINER, ACTOR, COMPOSITION or ACTION");
				type = std::string("");
			}
		}
		std::string getType() const { return type; }

		void print() const 
		{			
			std::cout << "Relation[" << id << "] " << type << ": ";
			if( !type.compare("CONTAINER") || !type.compare("ACTOR") )
				std::cout << "Object[" << lhID << "] - ";
			else
				std::cout << "Event[" << lhID << "] - ";
			
			if( !type.compare("CONTAINER") || !type.compare("ACTION") )
			{
				std::cout << "{";
				for( size_t i=0; i<rhIDVector.size(); ++i )
				{
					std::cout << "Object[" << rhIDVector[i] << "] ";
				}
				std::cout << "}" << std::endl;
			}
			else
			{
				std::cout << "{";
				for( size_t i=0; i<rhIDVector.size(); ++i )
				{
					std::cout << "Event[" << rhIDVector[i] << "] ";
				}
				std::cout << "}" << std::endl;
			}

		}

	private:
		std::string type;
		int id;

		int lhID;
		std::vector<int> rhIDVector;
	};	

	/** This class has utility functions for groups of Relations
	*/
	class CORE_LIB RelationList
	{
	public:
		RelationList():annotate(""), nextIDToAssign(0){}

		void addRelation( const Relation& _relation );
		void rmRelation( int _id );
		void rmAllRelations() 
		{			
			relations.clear();
			relationTypes.clear();			
		}

		void clear()
		{
			rmAllRelations();
			nextIDToAssign = 0;
			annotate = std::string("");
		}

		void rmRelationsWithEvent( int _id );
		void rmRelationsWithObject( int _id );

		void updateRelationAddRHID( int _rid, int _rhid );

		const std::vector<Relation>& getRelations() const { return relations; }		
		std::vector<std::string> getRelationTypes() const { return relationTypes; }
		int getNumRelations() const { return relations.size(); }

		std::vector<Relation> getRelationsOfEvent(int _id ) const;
		std::vector<Relation> getRelationsOfObject(int _id ) const;

		bool isEmpty() const { return (relations.empty()); }
		int getLastAssignedID() const { return nextIDToAssign - 1; }
	
	private:
		std::vector<Relation> relations;
		std::vector<std::string> relationTypes;
		std::string annotate;
		int nextIDToAssign;
	};

	

	/** Main class for description of video content
	 *
	 * This class contains all the structures required to describe the content of a video file
	 * It does agree with a common decription format with external semantic analysis.
	 * The format is read from a schema.xsd that must be provided in compile time
	 *
	 * This class can be filled with content from a valid XML file using the load() function
	 * The content of this class can be written into XML files using the save() function
	 * Applications including viulib_core can create this data container and fill
	 * it using the addObject() and addEvent() functions.
	 *
	 * Annotation applications can use this class to create video content description
	 * \ingroup viulib_core
	 */
	class CORE_LIB VideoContentDescription
	{
	public:		
		VideoContentDescription()
			:empty(true), hasVideoData(false), annotate(""),videoData(),objectList(),eventList() {}
		VideoContentDescription(const VideoData& _vd, const ObjectList& _ol, const EventList& _el)
			:empty(false), hasVideoData(true), annotate(""),videoData(_vd),objectList(_ol),eventList(_el) {}

		VideoContentDescription(const std::string& _fileName)
			:empty(true), hasVideoData(false), annotate(""),videoData(),objectList(),eventList()
		{			
			if( load(_fileName) ) std::cout << "Loaded: " << _fileName << std::endl;
		}
				
		bool load( const std::string& _fileName ); 
		bool save( const std::string& _fileName ) const;

		void print() const
		{
			// TODO: add Object, Event and Relation prints
			if( hasVideoData )
			{
				std::cout << "VideoData:\n"
						  << "\tName: " << videoData.name << "\n"
						  << "\tNum. frames: " << videoData.num_frames << "\n"
						  << "\tSize: " << videoData.width << " x " << videoData.height << "\n"
						  << "\tFps: " << videoData.frame_rate << std::endl;
			}
			if( !videoData.annotate.empty() )
				std::cout << "\tAnnotate: " << videoData.annotate << std::endl;
		}
		void saveAsTxt( const std::string & descriptor) const;
				
		void addObject( const Object& _object ) { objectList.addObject( _object ); empty = false; }
		void addEvent( const Event& _event ) { eventList.addEvent( _event ); empty = false; }
		void addRelation( const Relation& _relation ) { relationList.addRelation( _relation ); empty = false; }
		void rmObject( int _id ) 
		{ 
			objectList.rmObject( _id ); 

			// Remove all Relations of this Object
			if( !relationList.isEmpty() )
			{
				std::vector<Relation> relations = relationList.getRelationsOfObject( _id );
				for( size_t r=0; r<relations.size(); ++r )
				{
					rmRelation( relations[r].getID() );
				}
			}
		}
		void rmEvent( int _id ) 
		{ 
			eventList.rmEvent( _id ); 

			// Remove all Relations of this Event
			if( !relationList.isEmpty() )
			{
				std::vector<Relation> relations = relationList.getRelationsOfEvent( _id );
				for( size_t r=0; r<relations.size(); ++r )
				{
					rmRelation( relations[r].getID() );
				}
			}
		}
		void rmRelation( int _id ) { relationList.rmRelation( _id ); }
		void updateEvent( int _id, int _frameEnd ) { eventList.updateEvent( _id, _frameEnd ); }
		void updateObject( int _id, const Object& _object ) { objectList.updateObject( _id, _object ); }
		void updateRelationAddRHID( int _rid, int _rhid ) { relationList.updateRelationAddRHID( _rid, _rhid ); }
		
		void addObjectData( int _id, ObjectData* _objectData )
		{
			objectList.addObjectData( _id, _objectData );
			//if( _id >= 0 && _id < static_cast<int>( objectList.objects.size() ) )
			//	objectList.objects[_id].addObjectData( _objectData );
		}
		void setVideoData( const VideoData& _videoData ) 
		{
			videoData = _videoData; hasVideoData = true; 
		}

		void clear();
		void rmAllOlderThan( int _frameNum );
		void rmAll();

		const VideoData& getVideoData() const { return videoData; }
		const ObjectList& getObjectList() const { return objectList; }
		const EventList& getEventList() const { return eventList; }
		const RelationList& getRelationList() const { return relationList; }

		bool isEmpty() const { return empty; }
		int getNumEvents() const { return eventList.getNumEvents(); }
		int getNumObjects() const { return objectList.getNumObjects(); }

		int getLastObjectIDAssigned() const { return objectList.getLastAssignedID(); }
		int getLastEventIDAssigned() const { return eventList.getLastAssignedID(); }
		int getLastRelationIDAssigned() const { return relationList.getLastAssignedID(); }

		std::vector<std::string> getObjectTypes() const { return objectList.getObjectTypes(); }
		std::vector<std::string> getEventTypes() const { return eventList.getEventTypes(); }

		std::vector<Event> getActiveEvents( int _frameNum ) const;
		std::vector<Object> getActiveObjects( int _frameNum ) const;

		std::string annotate;

	private:
		VideoData videoData;
		ObjectList objectList;
		EventList eventList;
		RelationList relationList;

		bool empty;
		bool hasVideoData;

	};


	// -------------------------------------------------------------------- //
	// ------------------ DYNAMIC TYPES OF ObjectData			----------- //
	// -------------------------------------------------------------------- //


	/** Bounding box object	
	*/
	class CORE_LIB bbox : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(bbox)		
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 
		virtual bbox* clone() const { return new bbox( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		int x;
		int y;
		int width;
		int height;
	};

	/** Circle object	
	*/
	class CORE_LIB circle : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(circle)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 
		virtual circle* clone() const { return new circle( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		int x;
		int y;
		int radius;
	};

	/** Point object	
	*/
	class CORE_LIB point : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(point)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 
		virtual point* clone() const { return new point( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		int x;
		int y;
	};

	/** Status object	
	*/
	class CORE_LIB status : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(status)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 
		virtual status* clone() const { return new status( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		bool value;
	};

	/** IntervalInteger object	
	*/
	class CORE_LIB intervalInteger : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(intervalInteger)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 	
		virtual intervalInteger* clone() const { return new intervalInteger( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		int min;
		int max;
	};

	/** Interval integer extended object	
	*/
	class CORE_LIB intervalIntegerExtended : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(intervalIntegerExtended)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 	
		virtual intervalIntegerExtended* clone() const { return new intervalIntegerExtended( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		int left;
		int right;
		bool leftFlag;
		bool rightFlag;
		std::string leftType;
		std::string rightType;
	};

	/** Polygon (Closed polyline)	
	*/
	class CORE_LIB polygon : public ObjectData
	{
		REGISTER_CLASS_DECLARATION(polygon)
	protected:
		virtual void setParam(const ParamBase& value, const std::string& name) {}
		virtual ParamBase getParam(const std::string& name) const { return NullParam(); }

	public: 	
		virtual polygon* clone() const { return new polygon( *this ); }
		void loadData( tinyxml2::XMLElement* _element );
		void saveData( tinyxml2::XMLElement* _element );

		std::vector<cv::Point> toOpenCVContour() const
		{
			std::vector<cv::Point> openCVPointVector;
			for(size_t i=0; i<pointVector.size(); ++i)
			{
				cv::Point p( pointVector[i].x, pointVector[i].y ); 
				openCVPointVector.push_back( p );
			}		
			return openCVPointVector;
			//inputData.roiBBox = cv::boundingRect( inputData.roi );
		}

		std::vector<viulib::videoContentDescription::point> pointVector;
	};

};
};
#endif // _VIDEO_CONTENT_DESCRIPTION_H_