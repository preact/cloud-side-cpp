/** 
 * viulib_core (Core) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_core is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_core depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_core.
 *
 */

#ifndef PROFILER_IMPL_H_
#define PROFILER_IMPL_H_

#include "profiling.h"
#include <map>
#include <list>
#include <string>
#include <iostream>

namespace viulib
{

	/** \brief This class provides methods for performance profiling
	 	\ingroup viulib_core
	*/
	class ProfilerImp : public Profiler
	{
	public:
		/** \brief	Constructor. */
		ProfilerImp();
	
		/** \brief	Destructor. */
		~ProfilerImp();
	
		void update(std::string _segmentAlias);

		void printOut(std::string _segmentAlias,bool execTime=false);
		void printOut(bool execTime=false,int _ordered=0);
		void printString(std::string _segmentAlias,std::string &_output_string);

		SEGMENT_INFO& get(std::string _segmentAlias);
		PROFILER_INFO& get();
    private:
		PROFILER_INFO m_profiling_map;
	
    };

}
#endif //FACTORY_IMPL_H_