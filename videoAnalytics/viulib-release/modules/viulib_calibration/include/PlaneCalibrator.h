/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#ifndef VIULIB_PLANE_CALIBRATOR_H
#define VIULIB_PLANE_CALIBRATOR_H

// C++ Standard Library
#include <vector>

#include "Camera.h"
#include "CameraCalibrator.h"		

#include "Plane.h"


/** \namespace viulib
*  This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 
*	(http://www.vicomtech.es/).
*/
namespace viulib
{

	namespace calibration 
	{
		//typedef enum RECTIFICATIONTYPE { LANDSCAPE=0, VERTICAL=1, VERTICAL_FLIP=2};

		/** \brief 
			 *  This class has tools for the computation of image-plane to world-plane homographies,
			 *	and extrinsic camera parameters.
			 *
			 *  Suggested usage:
			 *	Step 1: 
			 *	Step 2: 
			 *	Step 3: 
			 *		- 
			 *	Step 4: 
			 *	Step 5: 
			 *		- 
			 *		
			 *
			 *	Additionally:
			 *	Step i: Draw points and grid - drawPoints(), drawGrid()
			 *	Step j: Generate rectified image - applyHomography()
			 *	Step k: Generate information map - generateInformationMap()
			 *
			 *	\ingroup viulib_calibration
			 */
		class CAL_LIB PlaneCalibrator : public VObject
		{

			REGISTER_GENERAL_CLASS(PlaneCalibrator,calibration);

		public:
            PlaneCalibrator();	
			virtual ~PlaneCalibrator(void);

			/** \brief Set the pointer of the camera related to this planecalibration class
				\param cam [in] Pointer to the camera
				\return void
			*/
			void setCamera(Camera* _cam) { m_cam = _cam; }

			/** \brief Get a pointer to the camera
					\return Camera object
			*/
			Camera* getCamera() { return m_cam; }
			
			/** \brief Set the pointer of the plane related to this planecalibration class
				\param plane [in] Pointer to the plane
				\return void
			*/
			void setPlane(Plane* _plane);

			/** \brief Get a pointer to the plane				
					\return Plane object
			*/
			Plane* getPlane();

			// ----------------------------------------------
			// Calibration of Camera and Plane through homography
			// ----------------------------------------------
			/** \brief Determine the homography between the _origPoints and a rectangle defined by trans_mm and long_mm
				\      and determine Rt and m_a m_b m_c and m_d from it
				\param _origPoints the 4-points of the original image
				\param _trans_mm transversal distance of the rectangle in the plane
				\param _long_mm longitudinal distance of the rectangle in the plane
			*/
			double calibrateCameraPlane( const std::vector<cv::Point2f>& _origPoints, double _trans_mm, double _long_mm, const cv::Size& _imSize );

			/** \brief Calibrate the camera intrinsics and the plane pose matrix [R,t] with 3D points of a known object and their 2D image projection

					The points with Z==0 are considered to be in the plane. The homography is estimated using only those points.

				\param _origPoints [in] 2D image projection of the points
				\param _objectPoints [in] 3D points (The points with Z==0 are considered to be in the plane)
				\param _imSize [in] The camera image size
				\return The reprojection error obtained with the estimated pose matrix 
			*/			
			double calibrateCameraPlane( const std::vector<cv::Point2f>& _origPoints, const std::vector<cv::Point3f>& _objectPoints, const cv::Size& _imSize );

		
			/** \brief Calibrate the plane pose matrix [R,t] with 3D points of a known object and their 2D image projection

					The points with Z==0 are considered to be in the plane. The homography is estimated using only those points.

				\param _origPoints [in] 2D image projection of the points
				\param _objectPoints [in] 3D points (The points with Z==0 are considered to be in the plane)
				\param forceMatchHomographyAndPose [in] If the matching betweem homography and pose should be forced or not
				\return The reprojection error obtained with the estimated pose matrix 
			*/			
			double calibratePlane( const std::vector<cv::Point2f>& _origPoints, const std::vector<cv::Point3f>& _objectPoints, bool forceMatchHomographyAndPose = false );

			/** \brief Calibrate the plane pose matrix [R,t] and Homography with 2D points in the plane and their 2D image projection
				\param _origPoints [in] 2D image projection of the points
				\param _planePoints [in] 2D plane points
				\return The reprojection error obtained with the estimated pose matrix 
			*/
			double calibratePlane( const std::vector<cv::Point2f>& _origPoints, const std::vector<cv::Point2f>& _planePoints );
			
			// ----------------------------------------------
			// Calibration through homography
			// ----------------------------------------------
			/** \brief Determine the homography between the _origPoints and a rectangle defined by trans_mm and long_mm
				\      and determine Rt and m_a m_b m_c and m_d from it
				\param _origPoints the n-points of the original image
				\param _planePoints the n-points of the plane
			*/
			double calibratePlane( const std::vector<cv::Point2f>& _origPoints, double _trans_mm, double _long_mm );


			// ----------------------------------------------
			// Calibration through homography
			// ----------------------------------------------
			/** \brief Determine the homography between the _origPoints and the _planePoints
				\      and determine Rt and m_a m_b m_c and m_d from it
				\param _origPoints the n-points of the original image
				\param _planePoints the n-points of the plane
			*/
			double calibrateCameraPlane( const std::vector<cv::Point2f>& _origPoints, const std::vector<cv::Point2f>& _planePoints, const cv::Size& _imSize );
			
			/** \brief Determine the homography and general form from R and t using a measured pattern 
				\param _inputImg [in] A (8bit-1channel) gray-scale input image
				\param number_of_squares [in] number of points (width, height) in the pattern
				\param square_mm  [in] size in mm (width, height) of the squares
				\param pattern_margin [in] margin in mm (width, height) can be found at the boundaries of the pattern
				\param pattern_base_to_ground [in] distance in mm from the botton of the pattern to the ground
				\param _outputImg [out] output (8bit-3channel) bgr image with information related to this calibration process
				\param showPoints [in] flag which indicates if pattern's points will be shown
				\return  calibration error
				\image latex G:/Projects/Viulib/extras/doc/viulib_calibration/planeCalibration.eps "Plane Calibration" width=10cm
	
			*/
			double calibratePlane(const cv::Mat& _inputImg, 
								cv::Size number_of_squares, 
								cv::Size square_mm, 
								cv::Size pattern_margin, 
								int pattern_base_to_ground,
								cv::Mat& _outputImg, 
								bool showPoints=false);
			
			/** \brief Determine the R t and  homography 
				\param _a plane general form a coefficient from camera system
				\param _b plane general form a coefficient from camera system
				\param _c plane general form a coefficient from camera system
				\param _d plane general form a coefficient from camera system
				\ Origin will be placed where central axis intersects with plane
			*/
			double calibratePlane( const int &_a, const int &_b, const int &_c, const int &_d);

			/** \brief Computes the extrinsic parameters of the camera (RT) from homography
				\param applyDistortion Determines whether the distortion parameters shall be applied or not (false means that the image has been undistorted externally)				
				\return Reprojection error
			*/
			double computeCameraRT( cv::Mat &rvec, cv::Mat &tvec, bool _applyDistortion=false );
			
			/** \brief Sets the homography from image plane to world plane in mm
				\param[out] homographyMetric the homography that will contain				
				\return void
			*/
			void getHomographyMetric( cv::Mat& _homographyMetric ) { _homographyMetric = m_homographyMetric; }	

			

			/** \brief Load original 4-points position from vector
				\param[in] Vector of points
				\return Success
			*/
			bool loadOrigPoints(const std::vector<cv::Point2f>& _points);

			/** \brief Load original 4-points position from file
				\param[in] filename Filename				
				\return Success
			*/
			bool loadOrigPoints( const std::string& _filename);		

			/** \brief Load original 4-points position from OpenCV FileNode
				\param[in] filename FileNode				
				\return Success
			*/
			bool loadOrigPointsFromFileNode( const cv::FileNode& _fs );

			/** \brief Write original 4-points position to file
				\param[in] filename Filename				
				\return void
			*/
			void writeOrigPoints( const std::string& _filename) const;	

			/** \brief Write original 4-points position to OpenCV FileStorage
				\param[in] filename FileStorage				
				\return void
			*/
			void writeOrigPoints( cv::FileStorage& _fs ) const;

			/** \brief Get the 4-points in the original domain
				\param[out] origPoints 4-points in the original domain				
				\return void
			*/
			void getOrigPoints( std::vector<cv::Point2f>& _origPoints ) const { _origPoints = m_origPoints; }
			std::vector<cv::Point2f> getOrigPoints() const { return m_origPoints; }

			/** \brief Get Points in the planein mm
				\return list of plane metric points
			*/
			std::vector<cv::Point2f> getPointsMetric( ) { return m_dstPointsMetric;}

			// ----------------------------------------------
			// Calibration through chessboard
			// ----------------------------------------------
				
					
			// ----------------------------------------------
			// Save
			// ----------------------------------------------
			bool savePlane( const std::string& _filenamePlane );		
			void savePlaneParams( const std::string& _filename, viulib::calibration::Plane* _plane );

		

			
			

			
			// ----------------------------------------------
			// TO BE UPDATED...
			// ----------------------------------------------
			/** \brief Devuelve en vu el vector unitario del vector v
				\param v [in] vector de entrada
				\param modulo [out] modulo del vector de entrada
				\param vu [out] vector unitario de m\F3dulo 1
				\return void
			*/
			void vectorUnitario (cv::Point3f v,double &modulo,cv::Point3f &vu);

			/** \brief Devuelve en vu el vector columna unitario del vector v
				\param v [in] vector de entrada
				\param modulo [out] modulo del vector de entrada
				\param vu [out] vector columna unitario de m\F3dulo 1
				\return void
			*/
			void vectorUnitario (cv::Point3f v,double &modulo,cv::Mat &vu);

			
			/** \brief This function calculates the interseccion between a vector which passes trough A point in the image and the plane defined by A,B,C,D coeficients
				\param ipoint [in] point in the image (x,y)
				\param A [out] coeficiente x de la ecuaci\F3n del plano
				\param B [out] coeficiente y de la ecuaci\F3n del plano
				\param C [out] coeficiente z de la ecuaci\F3n del plano
				\param D [out] coeficiente libre de la ecuaci\F3n del plano
				\return intersection point in 3d space
			*/
			static cv::Point3f imagePointPlaneIntersection(cv::Point2f ipoint, double A, double B, double C, double D,Camera &cam);
	
			/** \brief This function calculates the interseccion between a vector (p1p2) in 3d-space defined by points p1 and p2 and the plane defined by A,B,C,D coeficients storaged in a column vector
				\param p1 [in] point in 3d-space (x,y,z)
				\param p2 [in] point in 3d-space (x,y,z)
				\param plane [in] plane's coeficients [A,B,C,D]t
				\return intersection point in 3d space
			*/
			cv::Point3f linePlaneIntersection(const cv::Point3f &p1,const cv::Point3f &p2,const cv::Mat &plane);

			

			/** \brief 
				\param _psn_x [in] : pattern squares number x
				\param _psn_y [in] : pattern squares number y
				\param _pss [in] : pattern squares size (height)
				\param _hpp [in] : distance from pattern base to plane in mm
				\param _pm [in] : pattern margin lenght in mm
				\return void
			*/
			static cv::Point getVP(cv::Mat &im_in, cv::Mat &im_out, CameraCalibrator &_cam_c, int _psn_x, int _psn_y, int _pss, int _hpp, int _pm,double &_dib,cv::Mat &rvec, cv::Mat &tvec, double &a, double &b, double &c, double &d ,std::vector<cv::Point3f> &v3dpoints);
			

			double setRT( cv::Mat rvec, cv::Mat tvec );

			void fromRT2H( const cv::Mat& _K, const cv::Mat& _R, const cv::Mat& _tvec, cv::Mat& _H );
		

		private:			

			void calibrateCameraPlane();

			// ----------------------------------
			// Plane
			// ----------------------------------
			Plane* m_plane;	

			// ----------------------------------
			// Camera and calibrator
			// ----------------------------------
			Camera* m_cam;			
			
			// ----------------------------------
			// Calibration through Homography
			// ----------------------------------			
			std::vector<cv::Point2f> m_origPoints;
			std::vector<cv::Point2f> m_dstPointsMetric;			// in (mm)
			std::vector<cv::Point3f> m_dstPointsMetric3D;		// in (mm)
			cv::Mat m_homographyMetric, m_homographyMetricInv;
		};

	};

};

#endif //VIULIB_PLANE_CALIBRATOR_H
