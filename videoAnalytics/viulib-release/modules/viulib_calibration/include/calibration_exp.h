/**
 * viulib_calibration (Calibration) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_calibration is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_calibration depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://apps.jcns.fz-juelich.de/doku/sc/lmfit)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 *
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 *
 * Third party copyrights are property of their respective owners.
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_calibration.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * Copyright (C) 1980-1999, University of Chicago
 *
 * Copyright (C) 2004-2013, Joachim Wuttke, Forschungszentrum Juelich GmbH 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php) 
 */
#pragma warning( disable:4251 )

#ifndef VIULIB_CALIBRATION_EXP_H
#define VIULIB_CALIBRATION_EXP_H

// ==============================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// ==============================================================================================
// When the symbol viulib_calibration_EXPORTS is defined in a project calibration exports functions and classes. 
// In other cases calibration imports them from the DLL
#ifndef STATIC_BUILD
#ifdef viulib_calibration_EXPORTS
	#if defined _WIN32 || defined _WIN64
		#define CAL_LIB __declspec( dllexport )
	#else
		#define CAL_LIB
	#endif
#else
	#if defined _WIN32 || defined _WIN64
		#define CAL_LIB __declspec( dllimport )
	#else
		#define CAL_LIB
	#endif
#endif
#else
#define CAL_LIB
#endif

// ==============================================================================================
// AUTOMATIC DLL LINKAGE
// ==============================================================================================
// If the code is compiled under MS VISUAL STUDIO, and RM_EXPORTS is not defined (i.e.
// in a DLL client program) this code will link the appropiate lib file, either in DEBUG or in
// RELEASE
#if defined( _MSC_VER ) && !defined( viulib_calibration_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_calibration_d.lib" )
	#else
		#pragma comment( lib, "viulib_calibration.lib" )
	#endif
#endif

#include <stdio.h>

#endif
