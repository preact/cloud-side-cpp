/** viulib_dtc (Detection, Tracking and Classification) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_dtc is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_dtc depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_dtc.
 *
 */

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma warning( disable:4251 ) // std::vector needs to have dll-interface to be used by clients of class 'X<T> warning
#pragma warning( disable:4275 ) // non dll-interface class 'cv::Mat' used as base for dll-interface class 'Z'
#endif

#ifndef DTC_FEATURES_H_
#define DTC_FEATURES_H_


#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/features2d/features2d.hpp>  // for cv::BRISK
//#include <opencv2/nonfree/nonfree.hpp>
#ifdef HAVE_TBB
  #include "tbb/tbb.h"
  #include "tbb/mutex.h"
  #ifdef WIN32
    #undef max
    #undef min
  #endif
#endif

#include "core.h"
#include "dtc_exp.h"

#include "extended_mat.h"
#include "regionND.h"
#include "dtc_sampling.h"

namespace viulib
{
namespace dtc
{
	//! \brief The Descriptor class stores and manages 'descriptors' on separate rows of ExtendedMat. The length of stored 'descriptors' must be equal.
   //!
	//! This class encapsulates 'descriptor' vectors in cv::Mat structure, preferably using the inner data type of float.
	//! This is done to ease the use of general 'descriptors' in other OpenCV modules, like classification/training,
	//! where the default input is a matrix of data of the type CV_32FC1 (float), with single 'descriptor' represented at each row of the ExtendedMat.
   //!
   typedef ExtendedMat Descriptor;   

   // **************** FeatureExtractor Class *********************
	/** \brief This class provides methods for computing image feature descriptors for use in classification training 
	* \ingroup viulib_dtc
	*/
	// author: pleskovsky@vicomtech.org ++
	class DTC_LIB FeatureExtractor: public virtual VObject
	{
        REGISTER_BASE_CLASS(FeatureExtractor,dtc)
 	    //REGISTER_GENERAL_CLASS(FeatureExtractor,dtc)

         /** \brief parallel_processor
         * \brief Implements parallel extraction of descriptors from an image (via TBB).
         */
	      class parallel_processor
	      {
	      public:
            parallel_processor(FeatureExtractor* const _extractor, const cv::Mat& _input, 
   			                   const SamplingStrategy& _sampling, std::vector<Descriptor>* const _outputs);
         #ifdef HAVE_TBB
			   void operator() (const tbb::blocked_range<size_t> & roi_range) const;
         #endif
			   void compute( size_t begin, size_t end ) const;

		   private:
			   FeatureExtractor* const extractor;
			   const cv::Mat& input;
			   std::vector<Descriptor>* const outputs;
			   const SamplingStrategy& sampling;
	      };

	public:
		FeatureExtractor(){}
		FeatureExtractor(std::string _type){}

		virtual std::string getFullClassName() const 
		{ 
			return getClassName(); 
		}

      //! \brief Creates new FeatureExtractor* object and sets the inner parameters equal to the current object ones.
      //! The deinitialization of the created object is left to the caller.
      //
      virtual FeatureExtractor* clone() const = 0;  // Does HARD copy of ALL parameters; The copy constructor, instead, may reuse some inner pointers

		//! \brief  Runs the feature descriptor sampling according to given sampling strategy.
		//!
		//! First, the FeatureExtractor::precomputeResponse(..) is called with the entire image on input.
		//! In this function all image preprocessing can be done. If necessary, the sampling strategy chosen
		//! can be considered.
		//! During the precomputation of the response, the ROI's can be generated and included in the sampling 
		//! strategy. This can be beneficial for adapting the use of salient feature detectors like SIFT.
		//!
		//! Then, the SamplingStrategy object is used to call FeatureExtractor::sampleResponse(..).
		//! According to the sampling strategy the sampleResponse function only obtains the necessary image ROIs,
		//! extracted from the input image. In the sampleResponse function thus either the precomputed
		//! responses are used to group the descriptors for the requested region, or simply the extractor is 
		//! run on the input ROI to obtain the corresponding descriptor (the latter mostly for cases where no precomputing was done).
		//! The location of the ROI within the whole image can be obtained via the OpenCV img->locateRoi(wholeSize, ofset) function.
		//!
		//! At last, FeatureExtractor::cleanResponse(..) is called to clean all unnecessary precomputed data.
		//!
		virtual std::vector<Descriptor> run(const cv::Mat &img, SamplingStrategy& sampling );
		virtual Descriptor run(const cv::Mat& img )
		{
			BaseImageSampling samplingBase;
			return Descriptor(this->run(img, samplingBase));
		}    


	protected:
		//! \brief Optionally, precomputes necessary feature response on the whole image. Consider the use of this function if precomputation on the
		//! whole image would be faster than obtaining the descriptor per selected ROI's. 
		//!
		//! \param img	 input image 
		//! \param sampling	 sampling strategy of the basic feature extractor
		//!
		//! \sa FeatureExtractor::run()
		virtual void precomputeResponse(const cv::Mat &img, SamplingStrategy& sampling); // ... can generate new ROI's for sampling strategy
		//! \brief Generate filter response for given image. 
		//!
		//! \param roiImg -  If the sampling strategy has the flag extractImageROIs() on, 
      //!                  the region of interest extracted out of the orriginal input image, according to the sampling strategy, is returned.
      //!                  Otherwise the whole image is passed and only the roiRect parameters forwards the information of the currently considered ROI rectangle.
      //!                  Note -> the option of either passing only the image coordinates or pasing as well the extracted images is implemented for 
      //!                  optimization issues, for feature extractors which need to access different data but the basic image.
		//! \param roiRect - region of interest in form of cv::Rect as given by the sampling strategy
		//!
		//! In case Sampling strategy was used, this function will be called for each input ROI defined.
		//! If no precomputation was used, this function can simply process the whole descriptor extraction.
		//!
		//! ATT! this function should consider the type of descriptor (FeatureExtractor::getDescriptorType()) used by this extractor, 
		//!      when generating the output.
		//!
		//! \sa FeatureExtractor::run()
		virtual Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const = 0; // grouping response or simply executing the whole extraction process
		//! \brief Optionally, cleans any precomputed response.
		//!
		//! \param img - input image 
		//! \param completeDes - united set of descriptors obtained from the ROIs of the sampling strategy
		//! \param sampling - sampling strategy of the basic feature extractor (NONE, IMG_BASED, RUNNING_WINDOWS, PREDEFINED_WINDOWS)
		//!
		//! \sa FeatureExtractor::run()
		virtual void cleanResponse(const cv::Mat &img, std::vector<Descriptor>& completeDes, SamplingStrategy& sampling);

	public:
		//! \brief Returns the type of the descriptor returned by the FeatureExtractor, considering its current properties.
		//! Overload this function to allow for other descriptor's precision (e.g. double, char...).
		//!
		//! This function is used during the sampleResponse function, when allocating the memory for output descriptors.
		virtual t_data_type getDescriptorType() const = 0; // TODO - how to manage Multiple features of different types ? -> convert descriptors upon retrieval !

   protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      virtual void setParam(const ParamBase& value, const std::string& name)  {}
      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      virtual ParamBase getParam(const std::string& name) const    { return NullParam(); }
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      virtual void getParam_(void** param, const std::string& name) {}
   protected:
		//! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node)  { return true; }
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const  { return true; } 
	};


	// **************** Naive Sampling Feature Adaptor Class *********************
	/** \brief	This class provides methods for sampling specific feature extractor at predefined windows.
	*   This apporoach differs from the sampling strategy of generic Feature extraction run in that
	*	The  precomputeResponse, sampleResponse and cleanResponse will be called for each ROI.
	*
	* \ingroup viulib_dtc
	*/
	// author: pleskovsky@vicomtech.org
	class DTC_LIB NaiveSamplingFeatureAdaptor : public FeatureExtractor 
	{
      // the following is only necessary for loading, in general the user should create the object directly via its constructor (specifying the inner feature extractor).
      REGISTER_CLASS_DECLARATION(NaiveSamplingFeatureAdaptor)

	private:
        //access to the following is only necessary for loading
        NaiveSamplingFeatureAdaptor(): extractor(NULL) {} // intentionally hidden
        friend class RegisterFactoryClass<NaiveSamplingFeatureAdaptor>; // allowing access for the factory

   public:
		  NaiveSamplingFeatureAdaptor(FeatureExtractor* _extractor): extractor(_extractor) {}
		  ~NaiveSamplingFeatureAdaptor() {}

        virtual NaiveSamplingFeatureAdaptor* clone() const 
        {
           return new NaiveSamplingFeatureAdaptor( extractor->clone() );
        }

        const FeatureExtractor* getFeatureExtractor() const
        {
			  return extractor;
        }

		  // INHERITED FUNCTIONS
		  virtual Descriptor sampleResponse(const cv::Mat& roiImg, const  RegionND& roiRect) const 
		  {
			  if (extractor != NULL)
				  return extractor->run(roiImg);
			  return Descriptor(this->getDescriptorType());
		  }

		  virtual t_data_type getDescriptorType() const 
		  {
			  if (extractor != NULL)
				  return extractor->getDescriptorType();
			  else return t_data_type(CV_32FC1);
		  }

		  std::string getFullClassName() const 
		  { 
			  if (extractor != NULL)
				  return getClassName() +":" + extractor->getClassName();
			  return getClassName(); 
		  }

   protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
      //! This method forwards the call directly to the internal extractor.
      virtual void setParam(const ParamBase& value, const std::string& name) 
		{
			if (extractor != NULL) extractor->set(value, name);
		}

      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
      //! This method forwards the call directly to the internal extractor.
      virtual ParamBase getParam(const std::string& name) const 
		{
			if (extractor != NULL) return extractor->get(name);
         return NullParam(); 
		}
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
      //! This method forwards the call directly to the internal extractor.
      virtual void getParam_(void** param, const std::string& name)
		{
			if (extractor != NULL) extractor->get(param, name);
		}
   protected:
		//! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node); 

      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;
      
      //virtual bool saveWrapper(cv::FileStorage &fsPart) const
      //{
      //   return saveInnerData(fsPart);
      //}

	private:
		FeatureExtractor* extractor;
	};

	// **************** Multiple Feature Extractor Class *********************
	/** \brief This class provides methods for computing image multiple feature descriptors for use in classification training
	*
	*  Note: The type of the common descriptor should be specified in the constructor of the MultiFeatureExt!
	*  At the moment of extracting the distinct descriptors, all types will be converted to the selected one automatically.
	* 
	*  The programmer is responsible for mixing only feature extractors which return equal number of descriptors (rows).
	* \ingroup viulib_dtc
	*/
	class DTC_LIB MultipleFeatureAdaptor : public FeatureExtractor 
	{
      REGISTER_CLASS_DECLARATION(MultipleFeatureAdaptor)

   public:
      typedef std::vector<FeatureExtractor*> FeatureExtCollector;

	public:
		  MultipleFeatureAdaptor(t_data_type _descriptorType = t_data_type(CV_32FC1)): 
		    descriptorType(_descriptorType) {}
		  ~MultipleFeatureAdaptor() {}

		  std::string getFullClassName() const; 
		  std::string  getClassName(size_t i) const;

		  void addFeatureExtractor(FeatureExtractor* featExt);
        FeatureExtractor* getFeature(size_t i);
        const FeatureExtractor* getFeature(size_t i) const;
        unsigned int size() const;
        
        //! \brief Release memory taken by internal feature extractors. Use this function in case load on empty extractor was used.
        void  destroy();

		  // INHERITED FUNCTIONS
        virtual MultipleFeatureAdaptor* clone() const;

        //! Note that each extractor has to return equal number of rows in the output descriptor, in order to match them together.
		  virtual std::vector<Descriptor> run(const cv::Mat &img, SamplingStrategy& sampling );
		  virtual Descriptor sampleResponse(const cv::Mat& roiImg, const RegionND& roiRect) const;

		  virtual t_data_type getDescriptorType() const;

   protected:
      //! \brief Set parameters with automatic type conversion. VObject inherrited method.
        //! The parameter name should should be a concatenation of corresponding feature extractor name, dot and parameter name. E.g. FeatPixelValues.color
      virtual void setParam(const ParamBase& value, const std::string& name);

      //! \brief Get parameters with automatic type conversion. VObject inherrited method.
        //! The parameter name should should be a concatenation of corresponding feature extractor name, dot and parameter name. E.g. FeatPixelValues.color
      virtual ParamBase getParam(const std::string& name) const;
      //! \brief direct access to inner parameters, via pointer. VObject inherrited method.
        //! The parameter name should should be a concatenation of corresponding feature extractor name, dot and parameter name. E.g. FeatPixelValues.color
      virtual void getParam_(void** param, const std::string& name);
   protected:
		//! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;

	private:
		FeatureExtCollector featureExtractors;
		t_data_type descriptorType;
	};

   
   // **************** Concatenate Feature Adaptor Class *********************
	/** \brief	This class provides methods for concatenating the output of single PIXEL BASED feature to another feature extractor.
   *   It overwrites the FeatureExtractor::run() method to first extract internal feature descriptors and generate an intermediate
   *   response in the form of an image (it reshapes the descriptor to fit the input image size).
   *   Afterwards, on this image, the external feature extractor is called.
   *
   *   A good use example is the computation of histograms, by setting the HistogramFeatureExtractor as the external part and any other
   *   feature extractor (operating per pixel) as the inner extractor.
	*/
	// author: pleskovsky@vicomtech.org
	class DTC_LIB ConcatenateFeatureAdaptor : public FeatureExtractor 
	{
      // the following is only necessary for loading, in general the user should create the object directly via its constructor (specifying the inner feature extractor).
      REGISTER_CLASS_DECLARATION(ConcatenateFeatureAdaptor)

	private:
        //access to the following is only necessary for loading
        ConcatenateFeatureAdaptor(): intern(NULL), external(NULL) {} // intentionally hidden
        friend class RegisterFactoryClass<ConcatenateFeatureAdaptor>; // allowing access for the factory

	public:
		  ConcatenateFeatureAdaptor(FeatureExtractor* _intern, FeatureExtractor* _external ): intern(_intern), external(_external) {}
        ~ConcatenateFeatureAdaptor() { internImage.release(); }

        virtual ConcatenateFeatureAdaptor* clone() const 
        {
           return new ConcatenateFeatureAdaptor( intern->clone(), external->clone() );
        }
        
        std::string getFullClassName() const; 

        const FeatureExtractor* getInternalFeatureExtractor() const
        {
			  return intern;
        }
        const FeatureExtractor* getExternalFeatureExtractor() const
        {
			  return external;
        }
        cv::Mat getInternImage() const // for debug purposes
        {
            return internImage;
        }

		  // INHERITED FUNCTIONS
		  virtual std::vector<Descriptor> run(const cv::Mat &img, SamplingStrategy& sampling );
        //! \brief Sample response is not used.
        // dummy function
        virtual Descriptor sampleResponse(const cv::Mat& roiImg, const RegionND& roiRect) const 
        { 
            return Descriptor(getDescriptorType());
        }

		  virtual t_data_type getDescriptorType() const 
		  {
			  if (external != NULL)
				  return external->getDescriptorType();
			  else return t_data_type(CV_32FC1);
		  }

   protected:
      //! \breif Set parameters with automatic type conversion. VObject inherrited method.
      //! Access to the internal extractors is possible using prefixes "internal." & "external." 
      virtual void setParam(const ParamBase& value, const std::string& name); 

      //! \breif Get parameters with automatic type conversion. VObject inherrited method.
      //! Access to the internal extractors is possible using prefixes "internal." & "external." 
      virtual ParamBase getParam(const std::string& name) const;
      //! \breif direct access to inner parameters, via pointer. VObject inherrited method.
      //! Access to the internal extractors is possible using prefixes "internal." & "external." 
      virtual void getParam_(void** param, const std::string& name);

   protected:
		//! \brief	Load and set class parameters. VObject inherrited method.
      virtual bool loadInnerData(const cv::FileNode &node);
      //! \brief	Save class parameters. VObject inherrited method.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const;

	private:
		FeatureExtractor* intern;
		FeatureExtractor* external;
      ExtendedMat internImage;
	};


	//***************** Explicit Features **********************************
	/** \brief This class returns pixel values of given image in CV_32F format.
	* \ingroup viulib_dtc
	*/
	// author: pleskovsky@vicomtech.org
	class  DTC_LIB FeatPixelValues : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatPixelValues)

	public:
		FeatPixelValues() {}
		~FeatPixelValues() {}

		// INHERITED FUNCTIONS
      FeatPixelValues* clone() const;
      //! Extracts image pixel values and converts them to CV_32F format (often used by classification algorithms).
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

   protected:
		//! \brief	Empty - has no inner variables.
      virtual bool loadInnerData(const cv::FileNode &node)  { return true; }
      //! \brief	Empty - has no inner variables.
      virtual bool saveInnerData(cv::FileStorage &fsPart) const 
      {        
          fsPart << "data_type" << this->getDescriptorType().type;
          return true; 
      } 

		t_data_type  getDescriptorType() const { return CV_32F;}
	};


	//***************** Explicit Features **********************************
	/** \brief This class computes moments from image (the type of the moments is set through setParam function
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatMoments : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatMoments)

   public:
		FeatMoments() {m_type="ALL";}
		FeatMoments(const std::string& _type): m_type(_type) {}
		~FeatMoments() {}

		// INHERITED FUNCTIONS
      FeatMoments* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;
      ParamBase getParam(const std::string& _name) const;

		t_data_type  getDescriptorType() const { return CV_32FC1;}
      void setParam(const ParamBase& value, const std::string& name);

   protected:
      bool loadInnerData(const cv::FileNode &node);
      bool saveInnerData(cv::FileStorage &fsPart) const;

	protected:
		std::string m_type;
	};

	//***************** Explicit Features **********************************
	/** \brief This class computes normalized image histogram. Pixels should be of ordinary type (char, int, float, double), one component.
	* \ingroup viulib_dtc
	*/
	// author: pleskovsky@vicomtech.org
	class  DTC_LIB FeatImageHistogram : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatImageHistogram)

	public:
		FeatImageHistogram();
      FeatImageHistogram(double _min, double _max, int _size, bool _normalize):
         m_min(_min),m_max(_max),m_size(_size),m_normalize(_normalize) {}
		~FeatImageHistogram() {}

		// INHERITED FUNCTIONS
      FeatImageHistogram* clone() const;
		void precomputeResponse(const cv::Mat &img, SamplingStrategy& sampling);
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;
		void cleanResponse(const cv::Mat &img, std::vector<Descriptor>& completeDes, SamplingStrategy& sampling);

		t_data_type  getDescriptorType() const { return CV_32FC1;}

   protected:
		void setParam(const ParamBase& value, const std::string& _name);
      ParamBase getParam(const std::string& _name) const;
      void getParam_(void** param, const std::string& name);

   protected:
      bool loadInnerData(const cv::FileNode &node);
      bool saveInnerData(cv::FileStorage &fsPart) const;

	protected:
		double m_min; /// minimum bin value (inclusive)
		double m_max; /// maximum bin value (not inclusive)
		int m_size; /// number of bins
		bool m_normalize; /// normalize
	};




	//***************** Explicit Features **********************************
	/** \brief	This class computes feature a histogram of the gradients angle of the image
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatGradientsHistogram : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatGradientsHistogram)

	public:
		FeatGradientsHistogram() {
			m_th=10;
         m_w=false;
		}
		FeatGradientsHistogram(int _th, bool _w): m_th(_th), m_w(_w) {}	
		~FeatGradientsHistogram() {}

		// INHERITED FUNCTIONS
      FeatGradientsHistogram* clone() const;
		void precomputeResponse(const cv::Mat &img, SamplingStrategy& sampling);
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		t_data_type  getDescriptorType() const { return CV_32FC1;}
   protected:
		void setParam(const ParamBase& value, const std::string& _name);
      ParamBase getParam(const std::string& _name) const;
      void getParam_(void** param, const std::string& name) {};

	protected:
		int m_th;
		bool m_w;
	};



	//***************** Explicit Features **********************************
	/** \brief	This class computes feature points from type m_type from image (the type of the features is set through setParam function)
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatFeaturePoints : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatFeaturePoints)
	  //REGISTER_GENERAL_CLASS(FeatFeaturePoints,dtc)

	public:
		FeatFeaturePoints() {
			m_featureEngine=NULL;
			m_detector=NULL;
			m_extractor=NULL;
			m_adapter=NULL;
		}
		~FeatFeaturePoints() {}

		// INHERITED FUNCTIONS
      FeatFeaturePoints* clone() const;
		void precomputeResponse(const cv::Mat &img, SamplingStrategy& sampling);
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;
		void cleanResponse(const cv::Mat &img, std::vector<Descriptor>& completeDes, SamplingStrategy& sampling);

		t_data_type  getDescriptorType() const { return CV_32FC1;}

   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const;

		bool loadInnerData(const cv::FileNode &node);
		bool saveInnerData(cv::FileStorage &fsPart) const;

	protected:
		int m_size;

		int m_threshold;
		int m_octaves;
		float m_pattern_scales;
		std::string m_name;
		cv::Ptr<cv::Feature2D>           m_featureEngine;

		cv::FeatureDetector *     m_detector;
		cv::DescriptorExtractor * m_extractor;
		cv::FeatureDetector *     m_adapter;
		int m_adapter_type;
		int m_adapter_min_num;
		int m_adapter_max_num;
		//cv::Ptr<cv::DescriptorMatcher>   m_matcher;

		//cv::Mat m_kp_image;

		mutable std::vector< std::vector<cv::KeyPoint> >	m_roiKeypoints;
		

		ManualSampling keypointSampling;
	};


	//***************** Explicit Features **********************************
	/** \brief	This class computes the percentage of Horizontal and Vertical Borders
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatBorder : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatBorder)

	public:
		FeatBorder() {}
		~FeatBorder() {}

		// INHERITED FUNCTIONS
      FeatBorder* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		virtual t_data_type getDescriptorType() const { return t_data_type(CV_32FC1);}
   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const {return NullParam();}
	};

	/** \brief	This class computes the mean and stdDev of Horizontal and Vertical Borders
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatEdge : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatEdge)

	public:
		FeatEdge() {}
		~FeatEdge() {}

		// INHERITED FUNCTIONS
      FeatEdge* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		virtual t_data_type getDescriptorType() const { return t_data_type(CV_32FC1);}
   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const {return NullParam();}
	};

	/** \brief This class computes the symmetry of an image in the vertical axis
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatSymmetry : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatSymmetry)

	public:
		FeatSymmetry() {}
		~FeatSymmetry() {}

		// INHERITED FUNCTIONS
      FeatSymmetry* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		virtual t_data_type getDescriptorType() const { return t_data_type(CV_32FC1);}
   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const {return NullParam();}
	};

	/** \brief	This class computes the amount of shadow in an image 
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatShadow : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatShadow)

	public:
		FeatShadow() {}
		~FeatShadow() {}
		// INHERITED FUNCTIONS
      FeatShadow* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		virtual t_data_type getDescriptorType() const { return t_data_type(CV_32FC1);}
   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const {return NullParam();}
	};

	//***************** Implicit Features **********************************



	/** \brief	This class computes the Histogram of Oriented Gradients descriptor from an image
	* \ingroup viulib_dtc
	*/
	class  DTC_LIB FeatHOG : public FeatureExtractor
	{
      REGISTER_CLASS_DECLARATION(FeatHOG)

	public:
		/// Internal HOG parameters
		struct Params
		{
			cv::Size winSize;
			cv::Size blockSize;
			cv::Size blockStride;
			cv::Size cellSize;
			int nBins;
			int derivAperture;
			double winSigma;
			int histogramNormType;
			double L2HysThreshold;
			bool gammaCorrection;
		};

	private:
		//Feature Extractor Parameters
		bool m_useColorImg;

		//HOG Parameters
		Params m_hog_params;

		//HOG object
		cv::HOGDescriptor m_hog;
	public:
		//Constructors
		FeatHOG(): m_hog(cv::HOGDescriptor()), m_hog_params(Params()), m_useColorImg(true){};

		FeatHOG (const cv::Size &win_size, const cv::Size &block_size, const cv::Size &block_stride,
			const cv::Size &cell_size, const int &n_bins, const int deriv_aperture = 1, 
			const double &win_sigma = -1, const int &histogram_normy_type = cv::HOGDescriptor::L2Hys, 
			const double &L2_hys_threshold = 0.2, const bool &gamma_correction = false, const bool &use_color_img = false);

		// Set HOG parameters

		void setHOGParameters (const cv::Size &win_size, const cv::Size &block_size, const cv::Size &block_stride,
			const cv::Size &cell_size, const int &n_bins, const int deriv_aperture = 1, 
			const double &win_sigma = -1, const int &histogram_normy_type = cv::HOGDescriptor::L2Hys, 
			const double &L2_hys_threshold = 0.2, const bool &gamma_correction = false, const bool &use_color_img = false);

		// Get HOG parameters
		Params getHOGParams();

		// INHERITED FUNCTIONS
    FeatHOG* clone() const;
		Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		virtual t_data_type getDescriptorType() const { return t_data_type(CV_32FC1);}
   protected:
		 /** \brief	Sets the detector's parameters 
		 *
		 *	\param value Value of the parameter
		 *	\param	name Name of the parameter		
		 */
		virtual void setParam(const ParamBase& _value, const std::string &_name);

		/** \brief	Get parameter from Detector
		 *	\param _param void pointer to param content
		 *	\param _name name of the parameter
		 */		
		virtual void getParam(void** _param, const std::string& _name){};
		virtual ParamBase getParam(const  std::string& _name) const;
				
		bool loadInnerData(const cv::FileNode &node);
		bool saveInnerData(cv::FileStorage &fsPart) const;
	};


	/** This class computes a 1-D descriptor defining how much fitted is a given Rect with respect
	*	to underlaying foreground mask (for which the integral image is precompute before each run().
	*/
	class DTC_LIB BGFGFill : public viulib::dtc::FeatureExtractor
	{
		 REGISTER_CLASS_DECLARATION(BGFGFill)
	public:
		BGFGFill():margin(5){}
		BGFGFill(int _margin):margin(_margin){}
		~BGFGFill() {}

	protected:	
		void precomputeResponse(const cv::Mat &img, viulib::dtc::SamplingStrategy& sampling);
		viulib::dtc::Descriptor sampleResponse(const cv::Mat &roiImg, const  RegionND& roiRect) const;

		BGFGFill* clone() const { return new BGFGFill(); }
		t_data_type  getDescriptorType() const { return CV_64FC1;}
		
	protected:
		cv::Mat integral;
		cv::Rect imgRect;
		int margin;
	};

}
}
#endif //DTC_FEATURES_H_
