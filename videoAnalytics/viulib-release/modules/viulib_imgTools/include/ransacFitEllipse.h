/** 
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#include "imgTools.h"

namespace viulib
{

namespace imgTools
{

/** \brief RansacFitEllipse class
\par Demo Code:
\code
	CvBox2D ellipse;
	CvPoint2D32f *inliers = new CvPoint2D32f[points.size()];
	imgTools::RansacFitEllipse ransacFit;
	int num_inliers = ransacFit.fit( &points[0], points.size(), inliers, ellipse );
	delete inliers;
\endcode
*/
class IMGTOOLS_LIB RansacFitEllipse
{
public:

	RansacFitEllipse();

	virtual ~RansacFitEllipse(void);	

	/** \brief Fit an ellipse to the points using RANSAC algorithm
		\param [in] points Input points
		\param [out] Inlier points. The memory for at least pointCount points must be already reserved
		\param [out] Ellipse
		\param [in] minRadius Minimum length of the ellipse 
		\param [in] maxRadius Minimum length of the ellipse 
		\param [in] minAspect Minimum aspect (width/height) of the ellipse 
		\param [in] maxAspect Minimum aspect (width/height) of the ellipse 		
		\param [in] useIsolatedPoints Use points that are isolated from the rest as possible inliers
		\param [in] inlierThreshold Distance threshold 
		\param [in] finalEstimationWithInliers Estimate the final ellipse with all the inliers data
		\return the inlier count. -1 if no ellipse was found
	*/
	int fit( const std::vector<cv::Point2f> &points, cv::Point2f *inliers, cv::RotatedRect &elipse, int minRadius, int maxRadius, float minAspect, float maxAspect, bool useIsolatedPoints, float inlierThreshold, bool finalEstimationWithInliers );

};

}

}