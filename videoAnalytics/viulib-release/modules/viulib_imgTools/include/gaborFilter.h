/** 
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>

namespace viulib
{
namespace imgTools
{

struct GaborFilterBank
{
	// Variable members
	std::vector< std::vector<cv::Mat> > spatialDomain;	// the Gabor filter bank in the spatial domain, of size (nScales X nOrientations)
	std::vector< std::vector<cv::Mat> > freqDomain;		// the Gabor filter bank in the frequency domain, of size (nScales X nOrientations)
	int nScales;
	int nOrientations;

	// Constructor
	GaborFilterBank()
	{
		nScales = 0;
		nOrientations = 0;
	}

	// Copy constructor
	GaborFilterBank( const GaborFilterBank &other )
	{
		nScales = other.nScales;
		nOrientations = other.nOrientations;
			
		spatialDomain.resize( nScales );
		freqDomain.resize( nScales );
		for( int i=0; i<nScales; ++i )
		{
			spatialDomain[i].resize( nOrientations );
			freqDomain[i].resize( nOrientations );
			for( int j=0; j<nOrientations; ++j )
			{
				spatialDomain[i][j] = other.spatialDomain[i][j].clone();
				freqDomain[i][j] = other.freqDomain[i][j].clone();
			}
		}
	}

	// Assigment operator
	GaborFilterBank &operator=( const GaborFilterBank &other )
	{
		nScales = other.nScales;
		nOrientations = other.nOrientations;
			
		spatialDomain.resize( nScales );
		freqDomain.resize( nScales );
		for( int i=0; i<nScales; ++i )
		{
			spatialDomain[i].resize( nOrientations );
			freqDomain[i].resize( nOrientations );
			for( int j=0; j<nOrientations; ++j )
			{
				spatialDomain[i][j] = other.spatialDomain[i][j].clone();
				freqDomain[i][j] = other.freqDomain[i][j].clone();
			}
		}

		return (*this);
	}
} m_gfb;

/** \brief	Initializes a filter bank of complex Gabor filters. 
 *
 *	\param	fSize			Size of the constructed Gabor filters; the size has to be equal to the size of 
 *							the image that you would like to filter (square image assumed).
 *	\param	nScales			Number of filter scales of the filter bank.
 *	\param	nOrientations	Number of filter orientations of the filter bank.
 *	\param	fMax			Maximal frequency of the filters in the bank (default: 0.25).
 *	\param	ni				X-axis sharpness (default: sqrt(2)).
 *	\param	gamma			Y-axis sharpness (default: sqrt(2)).
 *	\param	separation		Step between two consequtive filter scales (default: sqrt(2)).
 */
void initGaborFilterBank( const int &fSize, const int &nScales, const int &nOrientations, 
	const double &fMax=0.25, const double &ni=sqrt(2.0), const double &gamma=sqrt(2.0), 
	const double &separation=sqrt(2.0) )
{	
	m_gfb.nScales = nScales;
	m_gfb.nOrientations = nOrientations;
	m_gfb.spatialDomain.resize( nScales );
	m_gfb.freqDomain.resize( nScales );

	int sigma_x = fSize; 
	int sigma_y = fSize;

	// Create the filters
	for( int u=0; u<nScales; ++u )
	{
		double fu = fMax / powf( separation, u );
		double alpha = fu / gamma;
		double beta = fu / ni;

		m_gfb.spatialDomain[u].resize( nOrientations );
		m_gfb.freqDomain[u].resize( nOrientations );
		for( int v=0; v<nOrientations; ++v )
		{
			double theta_v = ( (double)v / 8.0 ) * CV_PI;
			
			cv::Mat gaborFilter;
			cv::Mat planes[] = { cv::Mat::zeros( 2*sigma_y, 2*sigma_x, CV_64F ), cv::Mat::zeros( 2*sigma_y, 2*sigma_x, CV_64F ) };
			cv::merge( planes, 2, gaborFilter );						
			for( int y=-sigma_y; y<sigma_y; ++y )
			{
				for( int x=-sigma_x; x<sigma_x; ++x )
				{
					double xc = x*cos(theta_v) + y*sin(theta_v);
					double yc = -x*sin(theta_v) + y*cos(theta_v);

					double magnitude = ( (fu*fu)/(CV_PI*gamma*ni) ) * 
						exp( -(alpha*alpha*xc*xc + beta*beta*yc*yc) );

					double real = magnitude * cos( 2*CV_PI*fu*xc );
					double imaginary = magnitude * sin( 2*CV_PI*fu*xc );

					gaborFilter.at<double>(sigma_y+y,2*(sigma_x+x)) = real;
					gaborFilter.at<double>(sigma_y+y,2*(sigma_x+x)+1) = imaginary;
				}
			}

			m_gfb.spatialDomain[u][v] = gaborFilter.clone();

			// Discrete Fourier transformation
			dft( gaborFilter, gaborFilter );
			m_gfb.freqDomain[u][v] = gaborFilter.clone();
		}
	}
}
 
int main( int argc, char **argv )
{
	int fSize = 128;
	int u = 2;
	int v = 5;
	initGaborFilterBank( fSize, 5, 8 );

	cv::Rect roi( 63, 63, 128, 128 );
	cv::Mat gaborFilter = m_gfb.spatialDomain[u][v]( roi ).clone();

	cv::Mat planes[] = { cv::Mat::zeros( fSize, fSize, CV_64F ), cv::Mat::zeros( fSize, fSize, CV_64F ) };
	cv::split( gaborFilter, planes );

	cv::Mat magI;
	cv::magnitude( planes[0], planes[1], magI );

	cv::normalize( planes[0], planes[0], 0, 1, CV_MINMAX );
	cv::normalize( magI, magI, 0, 1, CV_MINMAX );

#if !defined(ANDROID) && !defined(IOS_BUILD)
	cv::imshow( "Real part of a Gabor filter", planes[0] );
	cv::imshow( "Magnitude of a Gabor filter", magI );

	cv::waitKey( 0 );
#endif
	
	return 0;
}

};
};