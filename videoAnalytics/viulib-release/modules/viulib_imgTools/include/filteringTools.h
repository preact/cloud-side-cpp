/** 
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#ifndef FILTERINGTOOLS_MODULE_H
#define FILTERINGTOOLS_MODULE_H

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#ifdef CV_VERSION_EPOCH
#include "opencv2/legacy/compat.hpp"
#endif
#include "imgTools_exp.h"



namespace viulib
{

namespace imgTools
{
 IMGTOOLS_LIB cv::Mat createGaussianDerivKernel1D(int kernel_size, int deriv, double sigma = 0);
 
 IMGTOOLS_LIB cv::Mat createRotationalGaussianDerivKernel2D(int kernel_radius, int deriv, double sigma = 0);

 IMGTOOLS_LIB cv::Mat createRotationalSchmidKernel2D(int kernel_radius, double tau, double sigma = 0);

 IMGTOOLS_LIB cv::Mat createGaborSinKernel2D(int kernel_radius, double lambda, double sigma = 0);
 IMGTOOLS_LIB cv::Mat createGaborCosKernel2D(int kernel_radius, double lambda, double sigma = 0);

 // add response is only for laplacian-like functionals .. only makes sense for deriv_x == deriv_y and sigma_x == sigma_y ... otherwise the stronger response takes it all
 // max deriv = 4 
 IMGTOOLS_LIB cv::Mat createGaussianDerivKernel2D(int kernel_radius, int deriv_x, int deriv_y, double sigma_x = 0, double sigma_y = 0, bool add_response = false);
 
 IMGTOOLS_LIB cv::Mat createGaussian2D(int kernel_radius, double sigma = 0);
 IMGTOOLS_LIB cv::Mat createLoG2D(int kernel_radius, double sigma = 0);
 IMGTOOLS_LIB cv::Mat createLoLoG2D(int kernel_radius, double sigma = 0);
 
 // differs from the correct Log in ...
 // here:                 = exp( -(x^2 + y^2)/2*sigma^2 ) * (1- 2*( -(x^2 + y^2)/2*sigma^2 ))     
 // correct LoG:          = exp( -(x^2 + y^2)/2*sigma^2 ) * (2- 2*( -(x^2 + y^2)/2*sigma^2 ))
 IMGTOOLS_LIB cv::Mat createSimpleLoG2D(int kernel_radius, double sigma = 0);
 IMGTOOLS_LIB cv::Mat createSimpleLoLoG2D(int kernel_radius, double sigma = 0);

 IMGTOOLS_LIB cv::Mat rotateFilter(const cv::Mat& kernel, double angle, cv::Size cropSize);
}

}

#endif
