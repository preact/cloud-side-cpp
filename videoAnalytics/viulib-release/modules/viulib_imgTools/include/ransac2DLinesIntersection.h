/** 
 * viulib_imgTools (Image Tools) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 *
 * viulib_imgTools is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/),
 * (Spain) all rights reserved.
 * 
 * As viulib_imgTools depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 * 
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_imgTools.
 *
 */

#ifndef RANSAC2DLINESINTERSECTION_H
#define RANSAC2DLINESINTERSECTION_H

#include "../../viulib_mtools/include/mtools.h"

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "stdio.h"
#ifdef CV_VERSION_EPOCH
#include "opencv2/legacy/compat.hpp"
#endif

namespace viulib
{
namespace imgTools
{

/** This is the data structure passed to the RANSAC procedure */
struct ransac_data_struct
{
#ifdef ANDROID
	cv::Mat& L;
#else
	cv::Mat& L;
#endif
	ransac_data_struct( cv::Mat& _l ):L(_l) {}



	//const std::vector<cv::Mat>& lines;
	//ransac_data_struct( const std::vector<cv::Mat>& _lines ): lines(_lines)
	//{
	//}	
};

class RANSAC_2DLineIntersection: public viulib::mtools::RANSAC
{
private:		
	enum DistanceFunction{ CAL_POINT_LINE };	// To be extender to other distance measurements more complex
	DistanceFunction m_distanceFunction;

	cv::Mat m_K;	// calibration matrix

public:	
	RANSAC_2DLineIntersection( const cv::Mat& _K=cv::Mat() ):m_K(_K), m_distanceFunction( CAL_POINT_LINE ) {}
	~RANSAC_2DLineIntersection();

	// Evalaute function (define Consensus Set)
	double evaluateCS(const std::vector<double>& _params, const void* _data, int _numData, std::vector<double>& _fvec, int* _CS_count, int _modeNum);

	// Estimate MSS function
	void estimateMSS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _MSS);	

	// Estimate CS function
	void estimateCS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _ind_CS);

	// Remove data (this is required if RANSAC multimode is called)
	void removeOutliers( void* _data, std::vector<int>& _ind_CS );
};


/** \brief Ransac2DLinesIntersection class
\par Demo Code:
\code
	cv::Point2f intersection;
	cv::Point3f *inliers = new Point3f[lines.size()];
	imgTools::Ransac2DLinesIntersection ransac2DLinesIntersection;
	int num_inliers = ransac2DLinesIntersection.find( &lines[0], lines.size(), inliers, intersection );
	delete inliers;
\endcode
*/
class Ransac2DLinesIntersection
{
public:

	Ransac2DLinesIntersection();

	virtual ~Ransac2DLinesIntersection(void);	

	/** \brief Fit an ellipse to the points using RANSAC algorithm
		\param [in] lines Input 2D lines in homogeneus coordinates ax + by + c =0
		\param [out] Inlier lines. The memory for at least lines.size() linesmust be already reserved
		\param [out] 2D intersection point
		\return the inlier count. -1 if no intersection was found
	*/
	int findIntersection( const std::vector<cv::Point3f> &lines, const std::vector<cv::Point2f> &origin, const std::vector<cv::Point2f> &direction, cv::Point3f *inliers, cv::Point2f &intersection );
};

};
};

#endif // RANSAC2DLINESINTERSECTION_H
