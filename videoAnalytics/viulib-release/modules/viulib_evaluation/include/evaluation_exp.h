

#ifndef EVALUATION_EXP_H_
#define EVALUATION_EXP_H_

// =====================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// =====================================================================================
// When the symbol viulib_XXX_EXPORTS is defined in a project XXX exports functions and 
// classes. In other cases XXX imports them from the DLL.
#ifndef STATIC_BUILD
#ifdef viulib_evaluation_EXPORTS
  #if defined _WIN32 || defined _WIN64
    #define EVALUATION_LIB __declspec( dllexport )
  #else
    #define EVALUATION_LIB
  #endif
#else
  #if defined _WIN32 || defined _WIN64
    #define EVALUATION_LIB __declspec( dllimport )
  #else
    #define EVALUATION_LIB
  #endif
#endif
#else
#define EVALUATION_LIB
#endif


// =====================================================================================
// AUTOMATIC DLL LINKAGE
// =====================================================================================
// If the code is compiled under MS VISUAL STUDIO, and viulib_XXX_EXPORTS is not defined
// (i.e. in a DLL client program) this code will link the appropiate lib file, either in 
// DEBUG or in RELEASE
#if defined( _MSC_VER ) && !defined( viulib_evaluation_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_evaluation_d.lib" )
	#else
		#pragma comment( lib, "viulib_evaluation.lib" )
	#endif
#endif

#endif //EVALUATION_EXP_H_
