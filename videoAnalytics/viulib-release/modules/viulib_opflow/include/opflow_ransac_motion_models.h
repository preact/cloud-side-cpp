/**  
 * viulib_opflow (Optical Flow) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_opflow is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_opflow depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * * lmfit v3.3 (http://joachimwuttke.de/lmfit/index.html)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 * Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
 * Third party copyrights are property of their respective owners. 
 *
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_opflow.
 *
 * License Agreement for lmfit
 * -------------------------------------------------------------------------------------
 * lmfit is released under the LMFIT-BEER-WARE licence:
 * 
 * In writing this software, I borrowed heavily from the public domain,
 * especially from work by Burton S. Garbow, Kenneth E. Hillstrom,
 * Jorge J. More, Steve Moshier, and the authors of lapack. To avoid
 * unneccessary complications, I put my additions and amendments also
 * into the public domain. Please retain this notice. Otherwise feel
 * free to do whatever you want with this stuff. If we meet some day,
 * and you think this work is worth it, you can buy me a beer in return.
 * 
 * - Joachim Wuttke <j.wuttke@fz-juelich.de>
 *
 */

#ifndef OPFLOW_RANSAC_MOTION_MODELS_H
#define OPFLOW_RANSAC_MOTION_MODELS_H

#include "opflow.h"

/** \brief This namespace corresponds to the VIULib library copyrighted by Vicomtech-IK4 
 *	(http://www.vicomtech.es/).
 */
namespace viulib
{
/** \brief This class has tools for the detection, description and matching of keypoints
 */	
namespace opflow
{	

	
	/** This is the data structure passed to the RANSAC procedure */
	struct OPFLOW_LIB ransac_data_struct
	{
		std::vector<cv::KeyPoint>& query_kpts;
		std::vector<cv::KeyPoint>& train_kpts;	

		ransac_data_struct (std::vector<cv::KeyPoint>& _query_kpts, std::vector<cv::KeyPoint>& _train_kpts): 
			query_kpts(_query_kpts), train_kpts(_train_kpts)
		{
		}
	};
	/** \brief This class implements the RANSAC method for estimating 2D motion.
	 * \ingroup viulib_opflow
	 */
	class OPFLOW_LIB RANSAC_2DTMotion : public viulib::mtools::RANSAC
	{
	public:
		~RANSAC_2DTMotion();

		// Evalaute function (define Consensus Set)
		double evaluateCS(const std::vector<double>& _params, const void* _data, int _numData, std::vector<double>& _fvec, int* _CS_count, int _modeNum);

		// Estimate MSS function
		void estimateMSS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _MSS);	

		// Estimate CS function
		void estimateCS(std::vector<double>& _params, const void* _data, int _numData, const std::vector<int>& _ind_CS);

		// Remove data (this is required if RANSAC multimode is called)
		void removeOutliers( void* _data, std::vector<int>& _ind_CS );
	
	};

}
};

#endif // OPFLOW_RANSAC_MOTION_MODELS_H
