/** 
 * viulib_vas (Video Analysis for Surveillance) v13.10
 * 
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * viulib_vas is a module of Viulib (Vision and Image Understanding Library) v13.10
 * Copyright (C) 2011-2013, Vicomtech-IK4 (http://www.vicomtech.es/), 
 * (Spain) all rights reserved.
 * 
 * As viulib_vas depends on other libraries, the user must adhere to and keep in place any 
 * licencing terms of those libraries:
 *
 * * OpenCV v2.4.6 (http://opencv.org/)
 * 
 * License Agreement for OpenCV
 * -------------------------------------------------------------------------------------
 * BSD 2-Clause License (http://opensource.org/licenses/bsd-license.php)
 *
 * The dependence of the "Non-free" module of OpenCV is excluded from viulib_adas.
 *
 */	

#ifndef VAS_EXP_H_
#define VAS_EXP_H_

// =====================================================================================
// MACRO FOR IMPORTING AND EXPORTING FUNCTIONS AND CLASSES FROM DLL
// =====================================================================================
// When the symbol viulib_vas_EXPORTS is defined in a project DTC exports functions and 
// classes. In other cases VAS imports them from the DLL.
#ifndef STATIC_BUILD
#ifdef viulib_vas_EXPORTS
  #if defined _WIN32 || defined _WIN64
    #define VAS_LIB __declspec( dllexport )
  #else
    #define VAS_LIB
  #endif
#else
  #if defined _WIN32 || defined _WIN64
    #define VAS_LIB __declspec( dllimport )
  #else
    #define VAS_LIB
  #endif
#endif
#else
#define VAS_LIB
#endif


// =====================================================================================
// AUTOMATIC DLL LINKAGE
// =====================================================================================
// If the code is compiled under MS VISUAL STUDIO, and viulib_vas_EXPORTS is not defined
// (i.e. in a DLL client program) this code will link the appropiate lib file, either in 
// DEBUG or in RELEASE
#if defined( _MSC_VER ) && !defined( viulib_vas_EXPORTS )
	#ifdef _DEBUG
		#pragma comment( lib, "viulib_vas_d.lib" )
	#else
		#pragma comment( lib, "viulib_vas.lib" )
	#endif
#endif

#endif //VAS_EXP_H_
