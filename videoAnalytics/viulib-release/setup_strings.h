//===================================================================================
//  viulib CMake configuration file
//
//  ** File generated automatically, do not modify **
//
//  Usage from an external project:
//
//    #include "setup_strings.h"
//    std::string dir = viulib::SetupStrings::ExtrasPath;
//    std::string dir = viulib::SetupStrings::viulib_vas_dataPath;
//
// ===================================================================================

#include <string>
namespace viulib
{
  template<typename T>
  struct Strings_
  {
 	static std::string const ExtrasPath;
	static std::string const viulib_vas_dataPath;
  };

  template<typename T> std::string const Strings_<T>::ExtrasPath="/home/mnieto/code/viulib-git/viulib/viulib-extras/";
  template<typename T> std::string const Strings_<T>::viulib_vas_dataPath="/home/mnieto/code/viulib-git/viulib/modules/viulib_vas/data/";

  typedef Strings_<void> SetupStrings;
}


