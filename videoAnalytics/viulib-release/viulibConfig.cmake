# ===================================================================================
#  viulib CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project:
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(viulib REQUIRED ) (after the declaration of PROJECT() )
#	 LINK_DIRECTORIES(${viulib_LIB_DIR})
#    add_subdirectory(${viulib_SRC} "${PROJECT_BINARY_DIR}/lib/")  (at the end of the app's CMakeList file)
#    target_link_libraries ( project_name debug omf_d optimized omf) (after ADD_EXECUTABLE command and in the same file)
#
#    This file will define the following variables:
#      - viulib_LIBS          : The list of libraries to links against.
#      - viulib_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.
#	   - viulib_MODULES       : The path to the directory  of viulib's modules
#      - viulib_DIR           : The path to the build directory
#      - viulib_EXTRAS        : Path to viulib's Extras folder	
#      - viulib_INCLUDE_DIRS  : The path to the selected module's include directories
#
#      - viulib_VERSION       : The  version of this gu build. Example: "1.2.0"
#      - viulib_VERSION_MAJOR : Major version part of gu_VERSION. Example: "1"
#      - viulib_VERSION_MINOR : Minor version part of gu_VERSION. Example: "2"
#      - viulib_VERSION_PATCH : Patch version part of gu_VERSION. Example: "0"
#
# ===================================================================================

get_filename_component(VIULIB_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)
SET( viulib_MODULES     ${VIULIB_CONFIG_PATH}/modules)
SET(viulib_EXTRAS       ${VIULIB_CONFIG_PATH}/extras)
SET(viulib_COMMON       ${VIULIB_CONFIG_PATH}/common)

# ====================================================================
# Select the modules to use
# ====================================================================

OPTION (VIULIB_ADAS FALSE)
OPTION (VIULIB_BGFG FALSE)
OPTION (VIULIB_CALIBRATION FALSE)
OPTION (VIULIB_VAS FALSE)
OPTION (VIULIB_CORE FALSE)
OPTION (VIULIB_DTC FALSE)
OPTION (VIULIB_EVALUATION FALSE)
OPTION (VIULIB_HBP FALSE)
OPTION (VIULIB_IMGTOOLS FALSE)
OPTION (VIULIB_MTOOLS FALSE)
OPTION (VIULIB_OMF FALSE)
OPTION (VIULIB_OPFLOW FALSE)
OPTION (VIULIB_SECURITY FALSE)
OPTION (VIULIB_SYSTEM FALSE)
OPTION (VIULIB_TER FALSE)
OPTION (VIULIB_UTILS FALSE)

OPTION( ENABLE_VIDEOMAN "Enables VideoMan" OFF )
OPTION( ENABLE_OPENNI "Enables OpenNI" OFF )
OPTION( ENABLE_OPENNI2 "Enables OpenNI2" OFF )
OPTION( ENABLE_KINECTSDK "Enables Kinect SDK" OFF )
OPTION( ENABLE_KINECT2SDK "Enables Kinect v2 SDK" OFF )
OPTION( ENABLE_FBXSDK "Enables FBX SDK" OFF )
OPTION( ENABLE_EIGEN "Enables Eigen" OFF )
OPTION( ENABLE_TBB "Enables Threading Building Blocks" OFF )
OPTION( ENABLE_QT5 "Enables Qt5" OFF )
OPTION( ENABLE_QT4 "Enables Qt4" OFF )
OPTION( ENABLE_GLEXT "Enables GLEXT" OFF )
OPTION( ENABLE_NLOPT "Enables NLOPT" OFF )

# ====================================================================
# Dependencies of the libraries
# ====================================================================
IF (VIULIB_HBP)
	SET (VIULIB_CALIBRATION ON CACHE BOOL "" FORCE)
	SET (VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET (VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
	SET (VIULIB_SYSTEM ON CACHE BOOL "" FORCE)
	SET (VIULIB_SECURITY ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_HBP)
IF (VIULIB_OMF)
	SET (VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET (VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
	SET (VIULIB_SYSTEM ON CACHE BOOL "" FORCE)
	SET (VIULIB_SECURITY ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_OMF)
IF (VIULIB_ADAS)
	SET (VIULIB_CALIBRATION ON CACHE BOOL "" FORCE)
	SET (VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET (VIULIB_DTC ON CACHE BOOL "" FORCE)	
	SET (VIULIB_IMGTOOLS ON CACHE BOOL "" FORCE)
	SET (VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
	SET (VIULIB_OPFLOW ON CACHE BOOL "" FORCE)
	SET (VIULIB_SYSTEM ON CACHE BOOL "" FORCE) 	
ENDIF (VIULIB_ADAS)
IF (VIULIB_BGFG)
	SET (VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET (VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
	SET (VIULIB_SYSTEM ON CACHE BOOL "" FORCE)	
ENDIF (VIULIB_BGFG)
IF (VIULIB_CORE)
	SET (VIULIB_SYSTEM ON CACHE BOOL "" FORCE)
	SET (VIULIB_SECURITY ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_CORE)
IF (VIULIB_VAS)
	SET( VIULIB_CORE ON  CACHE BOOL "" FORCE)
	SET( VIULIB_CALIBRATION ON CACHE BOOL "" FORCE)
	SET( VIULIB_DTC ON CACHE BOOL "" FORCE)
	SET( VIULIB_BGFG ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_VAS)
IF (VIULIB_DTC)
	SET (VIULIB_CALIBRATION ON CACHE BOOL "" FORCE)
	SET (VIULIB_OPFLOW ON CACHE BOOL "" FORCE)
	SET (VIULIB_CORE ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_DTC)
IF (VIULIB_OPFLOW)
	SET (VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
ENDIF (VIULIB_OPFLOW)
IF(VIULIB_CALIBRATION)
	SET(VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET(VIULIB_IMGTOOLS ON CACHE BOOL "" FORCE)
	SET(VIULIB_MTOOLS ON CACHE BOOL "" FORCE)	
ENDIF(VIULIB_CALIBRATION)
IF(VIULIB_IMGTOOLS)
	SET(VIULIB_MTOOLS ON CACHE BOOL "" FORCE)
ENDIF(VIULIB_IMGTOOLS)
IF(VIULIB_EVALUATION)
	SET(VIULIB_CORE ON CACHE BOOL "" FORCE)
	SET(VIULIB_SYSTEM ON CACHE BOOL "" FORCE)
ENDIF(VIULIB_EVALUATION)


# Extract the directory where *this* file has been installed (determined at cmake run-time)
#  This variable may or may not be used below, depending on the parsing of VideoManConfig.cmake
#get_filename_component(THIS_viulib_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)

# ======================================================
# Include directories to add to the user project:
# ======================================================

message(STATUS "Include dirs for Viulib modules:")

IF(VIULIB_VAS)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_vas/include)
	message("    " ${viulib_MODULES}/viulib_vas/include)
ENDIF(VIULIB_VAS)
IF(VIULIB_EVALUATION)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_evaluation/include)
	message("    " ${viulib_MODULES}/viulib_evaluation/include)
ENDIF(VIULIB_EVALUATION)
IF(VIULIB_CORE)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_core/include)
	message("    " ${viulib_MODULES}/viulib_core/include)
ENDIF(VIULIB_CORE)
IF(VIULIB_BGFG)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_bgfg/include)
	message("    " ${viulib_MODULES}/viulib_bgfg/include)
ENDIF(VIULIB_BGFG)
IF(VIULIB_DTC)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_dtc/include)
	message("    " ${viulib_MODULES}/viulib_dtc/include)
ENDIF(VIULIB_DTC)
IF(VIULIB_OMF)
	SET(viulib_INCLUDE_DIRS  ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_omf/include)
	message("    " ${viulib_MODULES}/viulib_omf/include)
ENDIF(VIULIB_OMF)
IF(VIULIB_CALIBRATION)
	SET(viulib_INCLUDE_DIRS  ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_calibration/include)
	message("    " ${viulib_MODULES}/viulib_calibration/include)
ENDIF(VIULIB_CALIBRATION)
IF(VIULIB_TER)
	SET(viulib_INCLUDE_DIRS  ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_ter/include)
	message("    " ${viulib_MODULES}/viulib_ter/include)
ENDIF(VIULIB_TER)
IF(VIULIB_HBP)
	SET(viulib_INCLUDE_DIRS  ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_hbp/include)
	message("    " ${viulib_MODULES}/viulib_hbp/include)
ENDIF(VIULIB_HBP)
IF(VIULIB_MTOOLS)
	SET(viulib_INCLUDE_DIRS  ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_mtools/include)
	message("    " ${viulib_MODULES}/viulib_mtools/include)
ENDIF(VIULIB_MTOOLS)
IF(VIULIB_OPFLOW)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_opflow/include)
	message("    " ${viulib_MODULES}/viulib_opflow/include)
ENDIF(VIULIB_OPFLOW)
IF(VIULIB_UTILS)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_utils/include)
	message("    " ${viulib_MODULES}/viulib_utils/include)
ENDIF(VIULIB_UTILS)
IF(VIULIB_IMGTOOLS)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_imgTools/include)
	message("    " ${viulib_MODULES}/viulib_imgTools/include)
ENDIF(VIULIB_IMGTOOLS)
IF(VIULIB_SYSTEM)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_system/include)
	message("    " ${viulib_MODULES}/viulib_system/include)
ENDIF(VIULIB_SYSTEM)
IF(VIULIB_SECURITY)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_security/include)
	message("    " ${viulib_MODULES}/viulib_security/include)
ENDIF(VIULIB_SECURITY)
IF(VIULIB_ADAS)
	SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${viulib_MODULES}/viulib_adas/include)
	message("    " ${viulib_MODULES}/viulib_adas/include)
ENDIF(VIULIB_ADAS)

SET(viulib_INCLUDE_DIRS ${viulib_INCLUDE_DIRS} ${VIULIB_CONFIG_PATH})
INCLUDE_DIRECTORIES(${viulib_INCLUDE_DIRS})
# ======================================================
# Link directories to add to the user project:
# ======================================================

  if(CMAKE_CL_64)
    set(Viulib_ARCH x64)    
  else()
    set(Viulib_ARCH x86)    
  endif()
if(MSVC)
  if(MSVC_VERSION EQUAL 1400)
    set(viulib_RUNTIME vc8)
  elseif(MSVC_VERSION EQUAL 1500)
    set(viulib_RUNTIME vc9)
  elseif(MSVC_VERSION EQUAL 1600)
    set(viulib_RUNTIME vc10)
  elseif(MSVC_VERSION EQUAL 1700)
    set(viulib_RUNTIME vc11)
  endif()
endif()

if (CMAKE_COMPILER_IS_GNUCC)
    execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion
                    OUTPUT_VARIABLE GCC_VERSION)
    string(REGEX MATCHALL "[0-9]+" GCC_VERSION_COMPONENTS ${GCC_VERSION})
    list(GET GCC_VERSION_COMPONENTS 0 GCC_MAJOR)
    list(GET GCC_VERSION_COMPONENTS 1 GCC_MINOR) 

    set(viulib_RUNTIME "gcc${GCC_MAJOR}${GCC_MINOR}")
    #message(viulib_RUNTIME: ${viulib_RUNTIME})

endif()

SET( viulib_BIN      ${VIULIB_CONFIG_PATH}/${Viulib_ARCH}/${viulib_RUNTIME}/bin)
SET( viulib_LIB_DIR      ${VIULIB_CONFIG_PATH}/${Viulib_ARCH}/${viulib_RUNTIME}/lib)

LINK_DIRECTORIES( ${VIULIB_CONFIG_PATH}/${Viulib_ARCH}/${viulib_RUNTIME}/lib )

# ====================================================================
# Link libraries one line for each library
# ====================================================================

IF(VIULIB_VAS)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_vas_d optimized viulib_vas)
ENDIF(VIULIB_VAS)
IF(VIULIB_EVALUATION)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_evaluation_d optimized viulib_evaluation)
ENDIF(VIULIB_EVALUATION)
IF(VIULIB_CORE)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_core_d optimized viulib_core)
ENDIF(VIULIB_CORE)
IF(VIULIB_BGFG)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_bgfg_d optimized viulib_bgfg)
ENDIF(VIULIB_BGFG)
IF(VIULIB_DTC)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_dtc_d optimized viulib_dtc)
ENDIF(VIULIB_DTC)
IF(VIULIB_OMF)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_omf_d optimized viulib_omf)
ENDIF(VIULIB_OMF)
IF(VIULIB_CALIBRATION)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_calibration_d optimized viulib_calibration)
ENDIF(VIULIB_CALIBRATION)
IF(VIULIB_HBP)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_hbp_d optimized viulib_hbp)
ENDIF(VIULIB_HBP)
IF(VIULIB_TER)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_ter_d optimized viulib_ter)
ENDIF(VIULIB_TER)
IF(VIULIB_MTOOLS)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_mtools_d optimized viulib_mtools)
ENDIF(VIULIB_MTOOLS)
IF(VIULIB_OPFLOW)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_opflow_d optimized viulib_opflow)
ENDIF(VIULIB_OPFLOW)
IF(VIULIB_UTILS)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_utils_d optimized viulib_utils)
ENDIF(VIULIB_UTILS)
IF(VIULIB_IMGTOOLS)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_imgTools_d optimized viulib_imgTools)
ENDIF(VIULIB_IMGTOOLS)
IF(VIULIB_SYSTEM)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_system_d optimized viulib_system)
ENDIF(VIULIB_SYSTEM)
IF(VIULIB_SECURITY)
	SET(viulib_LIBS ${viulib_LIBS} debug viulib_security_d optimized viulib_security)
ENDIF(VIULIB_SECURITY)
IF(VIULIB_ADAS)
	SET(viulib_LIBS  ${viulib_LIBS} debug viulib_adas_d optimized viulib_adas)
ENDIF(VIULIB_ADAS)

message(STATUS "Path a Viulib (VIULIB_CONFIG_PATH): " "${VIULIB_CONFIG_PATH}")
message(STATUS "Path a Modulos de Viulib (viulib_MODULES): " "${viulib_MODULES}")
message(STATUS "Path a Extras de Viulib (viulib_EXTRAS): " "${viulib_EXTRAS}")
message(STATUS "Path a las dlls de Viulib (viulib_BIN): " "${viulib_BIN}")
message(STATUS "Path a libs de Viulib (viulib_LIB_DIR): " "${viulib_LIB_DIR}")
message(STATUS "Path a common de Viulib (viulib_COMMON): " "${viulib_COMMON}")
message (STATUS "Selected Libraries (viulib_LIBS): ")
message ("   " "${viulib_LIBS}")

